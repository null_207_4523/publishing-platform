//package com.sxhs.platform;
//
//
//import com.aliyun.openservices.ons.api.Message;
//import com.aliyun.openservices.ons.api.SendResult;
//import com.aliyun.openservices.ons.api.bean.ProducerBean;
//import com.sxhs.platform.system.config.rocketMq.aliyun.MqConfig;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringRunner;
//
//
//
//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = {PlatApplication.class})
//public class PlatTest {
//
//    @Autowired
//    private ProducerBean producerBean;
//
//    @Autowired
//    private MqConfig mqConfig;
//
//    @Test
//    public void testKafkaSendMsg() throws Exception {
//        String msg = "测试rocketMq发送消息";
//        for(int i=0,len = 10;i<len;i++){
//            Message sendMsg = new Message( mqConfig.getTopic(),"edu_type",msg.getBytes());
//            // 默认3秒超时
//            SendResult sendResult = producerBean.send(sendMsg);
//            // 同步发送消息，只要不抛异常就是成功
//            System.out.println(sendResult);
//        }
//
//    }
//}

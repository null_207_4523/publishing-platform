package com.sxhs.platform.publishing.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.sxhs.platform.publishing.dao.PushCategoryConfigDao;
import com.sxhs.platform.publishing.domain.PushCategoryConfigDO;
import com.sxhs.platform.publishing.service.PushCategoryConfigService;



@Service
public class PushCategoryConfigServiceImpl implements PushCategoryConfigService {
	@Autowired
	private PushCategoryConfigDao pushCategoryConfigDao;
	
	@Override
	public PushCategoryConfigDO get(Integer id){
		return pushCategoryConfigDao.get(id);
	}
	
	@Override
	public List<PushCategoryConfigDO> list(Map<String, Object> map){
		return pushCategoryConfigDao.list(map);
	}
	
	@Override
	public int count(Map<String, Object> map){
		return pushCategoryConfigDao.count(map);
	}
	
	@Override
	public int save(PushCategoryConfigDO pushCategoryConfig){
		return pushCategoryConfigDao.save(pushCategoryConfig);
	}
	
	@Override
	public int update(PushCategoryConfigDO pushCategoryConfig){
		return pushCategoryConfigDao.update(pushCategoryConfig);
	}
	
	@Override
	public int remove(Integer id){
		return pushCategoryConfigDao.remove(id);
	}
	
	@Override
	public int batchRemove(Integer[] ids){
		return pushCategoryConfigDao.batchRemove(ids);
	}
	
}

package com.sxhs.platform.publishing.service.impl;

import com.sxhs.platform.publishing.dao.OrgColumnDao;
import com.sxhs.platform.publishing.domain.OrgColumnDO;
import com.sxhs.platform.publishing.service.OrgColumnService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;



@Service
@Transactional
public class OrgColumnServiceImpl implements OrgColumnService {
	@Autowired
	private OrgColumnDao orgColumnDao;
	
	@Override
	public OrgColumnDO get(Integer id){
		return orgColumnDao.get(id);
	}
	
	@Override
	public List<OrgColumnDO> list(Map<String, Object> map){
		return orgColumnDao.list(map);
	}
	
	@Override
	public int count(Map<String, Object> map){
		return orgColumnDao.count(map);
	}
	
	@Override
	public int save(OrgColumnDO orgColumn){
		return orgColumnDao.save(orgColumn);
	}
	
	@Override
	public int update(OrgColumnDO orgColumn){
		return orgColumnDao.update(orgColumn);
	}
	
	@Override
	public int remove(Integer id){
		return orgColumnDao.remove(id);
	}
	
	@Override
	public int batchRemove(Integer[] ids){
		return orgColumnDao.batchRemove(ids);
	}
	
}

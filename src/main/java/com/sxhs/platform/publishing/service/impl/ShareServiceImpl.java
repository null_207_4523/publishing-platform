package com.sxhs.platform.publishing.service.impl;

import com.sxhs.platform.common.config.Constant;
import com.sxhs.platform.common.utils.ApiResult;
import com.sxhs.platform.common.utils.FieldValueUtils;
import com.sxhs.platform.common.utils.VerificateUtil;
import com.sxhs.platform.publishing.dao.ShareDao;
import com.sxhs.platform.publishing.domain.NewsDO;
import com.sxhs.platform.publishing.domain.OrgDailyStatisticsDO;
import com.sxhs.platform.publishing.domain.ShareDO;
import com.sxhs.platform.publishing.service.NewsService;
import com.sxhs.platform.publishing.service.ShareService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;


@Service
@Transactional
public class ShareServiceImpl implements ShareService {
    @Autowired
    private ShareDao shareDao;
    @Autowired
    private NewsService newsService;

    @Override
    public ShareDO get(Integer id) {
        return shareDao.get(id);
    }

    @Override
    public List<ShareDO> list(Map<String, Object> map) {
        return shareDao.list(map);
    }

    @Override
    public int count(Map<String, Object> map) {
        return shareDao.count(map);
    }

    @Override
    public int save(ShareDO share) {
        return shareDao.save(share);
    }

    @Override
    public int update(ShareDO share) {
        return shareDao.update(share);
    }

    @Override
    public int remove(Integer id) {
        return shareDao.remove(id);
    }

    @Override
    public int batchRemove(Integer[] ids) {
        return shareDao.batchRemove(ids);
    }

    @Override
    public OrgDailyStatisticsDO getShareCountByDate(Map<String, Object> params) {
        return shareDao.getShareCountByDate(params);
    }

    @Override
    public ApiResult shareNews(Integer id, Integer userId) {
        // 获取资讯详情
        NewsDO newsDO = newsService.get(id);
        if (Objects.isNull(newsDO) || Objects.equals(newsDO.getHasDelete(), Constant.HAS_DELETE_YES))
            return ApiResult.fail("无此资讯数据，或已被删除");
        Integer hasFans = VerificateUtil.hasFans(userId, newsDO.getOrgId());
        newsDO = (NewsDO) FieldValueUtils.updateCount("shareCount", hasFans, newsDO, Constant.HAS_ADD_YES, 1);
        // 创建分享实体
        ShareDO shareDO = new ShareDO();
        shareDO.setNewsId(id);
        shareDO.setCreatedById(userId);
        shareDO.setCreatedTime(new Date());
        shareDO.setHasDelete(Constant.HAS_DELETE_NO);
        shareDO.setHasFans(hasFans);
        // 分享实体插入数据库
        if (shareDao.save(shareDO) > 0 && newsService.update(newsDO) > 0)
            return ApiResult.success();
        return ApiResult.fail();
    }

}

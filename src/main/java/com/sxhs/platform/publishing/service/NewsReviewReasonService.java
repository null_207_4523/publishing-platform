package com.sxhs.platform.publishing.service;

import com.sxhs.platform.common.utils.R;
import com.sxhs.platform.publishing.domain.NewsDO;
import com.sxhs.platform.publishing.domain.NewsReviewReasonDO;

import java.util.List;
import java.util.Map;

/**
 * 资讯审核原因
 * 
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2021-01-14 09:57:48
 */
public interface NewsReviewReasonService {
	
	NewsReviewReasonDO get(Integer id);
	
	List<NewsReviewReasonDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(NewsReviewReasonDO newsReviewReason);
	
	int update(NewsReviewReasonDO newsReviewReason);
	
	int remove(Integer id);
	
	int batchRemove(Integer[] ids);

    NewsReviewReasonDO getByNewsId(Integer id);

	/**
	 * 用于cms管理员人工审核资讯
	 * @param params
	 * @return
	 */
	R manualReview(Map<String, Object> params);

	/**
	 * 用于机器审核资讯
	 *
	 * @param result
	 * @param newsDo
	 * @return
	 */
	R machineReview(String result, NewsDO newsDo);
}

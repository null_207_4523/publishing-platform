package com.sxhs.platform.publishing.service.impl;

import com.sxhs.platform.common.config.Constant;
import com.sxhs.platform.common.domain.PageDO;
import com.sxhs.platform.common.utils.ApiResult;
import com.sxhs.platform.common.utils.FieldValueUtils;
import com.sxhs.platform.common.utils.VerificateUtil;
import com.sxhs.platform.publishing.dao.CollectDao;
import com.sxhs.platform.publishing.domain.CollectDO;
import com.sxhs.platform.publishing.domain.NewsDO;
import com.sxhs.platform.publishing.domain.OrgDailyStatisticsDO;
import com.sxhs.platform.publishing.service.CollectService;
import com.sxhs.platform.publishing.service.NewsService;
import com.sxhs.platform.publishing.service.OrgSubscriptionService;
import com.sxhs.platform.publishing.vo.NewsBaseVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.*;


@Service
@Transactional
public class CollectServiceImpl implements CollectService {
	@Autowired
	private CollectDao collectDao;
	@Autowired
	private NewsService newsService;
	@Autowired
	private OrgSubscriptionService orgSubscriptionService;

	@Override
	public CollectDO get(Integer id){
		return collectDao.get(id);
	}
	
	@Override
	public List<CollectDO> list(Map<String, Object> map){
		return collectDao.list(map);
	}
	
	@Override
	public int count(Map<String, Object> map){
		return collectDao.count(map);
	}
	
	@Override
	public int save(CollectDO collect){
		return collectDao.save(collect);
	}
	
	@Override
	public int update(CollectDO collect){
		return collectDao.update(collect);
	}
	
	@Override
	public int remove(Integer id){
		return collectDao.remove(id);
	}
	
	@Override
	public int batchRemove(Integer[] ids){
		return collectDao.batchRemove(ids);
	}

	@Override
	public ApiResult collectNews(Integer id, Integer userId) {
		// 获取资讯详情
		NewsDO newsDO = newsService.get(id);
		if (Objects.isNull(newsDO) || Objects.equals(newsDO.getHasDelete(), Constant.HAS_DELETE_YES))
			return ApiResult.fail("无此资讯数据，或已被删除");
		Integer hasFans = VerificateUtil.hasFans(userId, newsDO.getOrgId());
		// 保证每条资讯只能在未收藏状态下被收藏
		Map<String, Object> params = new HashMap<>();
		params.put("newsId", id);
		params.put("createdById", userId);
		params.put("hasDelete", Constant.HAS_DELETE_NO);
		List<CollectDO> collectDOs = collectDao.list(params);
		if (!CollectionUtils.isEmpty(collectDOs)) {
			CollectDO collectDO = collectDOs.get(0);
			newsDO = (NewsDO) FieldValueUtils.updateCount("collectCount", hasFans, newsDO, Constant.HAS_ADD_NO, 1);
			if (collectDao.remove(collectDO.getId()) > 0 && newsService.update(newsDO) > 0)
				return ApiResult.success(Constant.HAS_COLLECT_NO, "取消收藏");
		}
		// 创建收藏实体
		CollectDO collectDO = new CollectDO();
		collectDO.setNewsId(id);
		collectDO.setCreatedById(userId);
		collectDO.setCreatedTime(new Date());
		collectDO.setHasDelete(Constant.HAS_DELETE_NO);
		collectDO.setHasFans(hasFans);
		newsDO = (NewsDO) FieldValueUtils.updateCount("collectCount", hasFans, newsDO, Constant.HAS_ADD_YES, 1);
		// 收藏实体插入数据库
		if (collectDao.save(collectDO) > 0 && newsService.update(newsDO) > 0)
			return ApiResult.success(Constant.HAS_COLLECT_YES, "收藏");
		return ApiResult.fail();
	}

	@Override
	public ApiResult collectList(Integer userId, Integer offset, Integer limit) {
		offset = Objects.isNull(offset) ? 0 : offset;
		limit = Objects.isNull(limit) ? 9 : limit;
		Map<String, Object> params = new HashMap<>();
		params.put("createdUserId", userId);
		params.put("hasDelete", Constant.HAS_DELETE_NO);
		params.put("status", Constant.NEWS_STATUS_APPROVE);
		params.put("offset", offset * limit);
		params.put("limit", limit);
		params.put("sort", "c.created_time");
		params.put("order", Constant.DESC);
		List<NewsBaseVo> list = newsService.collectNewsList(params);
		if(CollectionUtils.isEmpty(list))
			return ApiResult.success(new PageDO<>());
		int total = newsService.collectNewsCount(params);
		PageDO<NewsBaseVo> page = new PageDO<>();
		page.setTotal(total);
		page.setRows(list);
		page.setLimit(offset);
		page.setOffset(limit);
		return ApiResult.success(page);
	}

	@Override
	public OrgDailyStatisticsDO getCollectCountByDate(Map<String, Object> params) {
		return collectDao.getCollectCountByDate(params);
	}

}

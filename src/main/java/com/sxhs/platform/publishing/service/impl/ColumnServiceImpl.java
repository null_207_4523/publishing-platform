package com.sxhs.platform.publishing.service.impl;

import com.sxhs.platform.common.domain.Tree;
import com.sxhs.platform.common.utils.ApiResult;
import com.sxhs.platform.common.utils.BuildTree;
import com.sxhs.platform.publishing.dao.ColumnDao;
import com.sxhs.platform.publishing.domain.ColumnDO;
import com.sxhs.platform.publishing.domain.OrgColumnDO;
import com.sxhs.platform.publishing.service.ColumnService;
import com.sxhs.platform.publishing.service.NewsService;
import com.sxhs.platform.publishing.service.OrgColumnService;
import com.sxhs.platform.publishing.vo.ColumnVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.*;


@Service
@Transactional
public class ColumnServiceImpl implements ColumnService {
    @Autowired
    private ColumnDao columnDao;
    @Autowired
    private OrgColumnService orgColumnService;
    @Autowired
    private NewsService newsService;

    @Override
    public ColumnDO get(Integer id) {
        return columnDao.get(id);
    }

    @Override
    public List<ColumnDO> list(Map<String, Object> map) {
        return columnDao.list(map);
    }

    @Override
    public int count(Map<String, Object> map) {
        return columnDao.count(map);
    }

    @Override
    public ApiResult save(ColumnDO column) {
        if (columnDao.save(column) > 0) {
            // 关联表添加数据
            OrgColumnDO orgColumnDO = new OrgColumnDO();
            orgColumnDO.setColumnId(column.getId());
            orgColumnDO.setColumnOrder(column.getColumnOrder());
            orgColumnDO.setHasShow(column.getHasShow());
            orgColumnDO.setOrgId(column.getColumnOrg());
            if (orgColumnService.save(orgColumnDO) > 0)
                return ApiResult.success();
        }
        return ApiResult.fail();
    }

    @Override
    public ApiResult update(ColumnDO column) {
        ColumnDO dBColumnDO = columnDao.get(column.getId());
        if (columnDao.update(column) > 0) {
            if (Objects.equals(dBColumnDO.getColumnOrder(), column.getColumnOrder()) || Objects.equals(dBColumnDO.getHasShow(), column.getHasShow()))
                return ApiResult.success();
            // 关联表跟新数据'column_order'、'has_show'字段
            Map<String, Object> map = new HashMap<>();
            map.put("orgId", column.getColumnOrg());
            map.put("columnId", column.getId());
            List<OrgColumnDO> list = orgColumnService.list(map);
            if (!CollectionUtils.isEmpty(list)) {
                OrgColumnDO orgColumnDO = list.get(0);
                orgColumnDO.setColumnOrder(column.getColumnOrder());
                orgColumnDO.setHasShow(column.getHasShow());
                if (orgColumnService.update(orgColumnDO) > 0)
                    return ApiResult.success();
            }
        }
        return ApiResult.fail();
    }

    @Override
    public ApiResult remove(Integer id) {
        ColumnDO column = columnDao.get(id);
        if (Objects.nonNull(column)) {
            Map<String, Object> param = new HashMap<>();
            param.put("orgId", column.getColumnOrg());
            param.put("columnId", column.getId());
            // modify 2020/11/26 根据产品要求，栏目下又内容不支持删除
            int count = newsService.count(param);
            if(count > 0)
                return ApiResult.fail("此栏目下(内容列表或回收站)有内容，无法删除");
            List<OrgColumnDO> orgColumnDOList = orgColumnService.list(param);
            if (!CollectionUtils.isEmpty(orgColumnDOList))
                orgColumnService.remove(orgColumnDOList.get(0).getId());
            if(columnDao.remove(id)>0)
                return ApiResult.success();
        }
        return ApiResult.fail();
    }

    @Override
    public int batchRemove(Integer[] ids) {
        return columnDao.batchRemove(ids);
    }

    @Override
    public List<ColumnVo> appOrgColumnlist(Integer orgId) {
        List<ColumnVo> orgColumnlist = columnDao.orgColumnlist(orgId);
        if (CollectionUtils.isEmpty(orgColumnlist)) {
            Map<String, Object> map = new HashMap<>();
            map.put("columnType", 0);
            map.put("hasShow", 0);
            map.put("hasDelete", 0);
            map.put("sort", "column_order");
            map.put("order", "asc");
            return columnDao.sysColumnlist(map);
        }
        return orgColumnlist;
    }

    @Override
    public Tree<ColumnVo> getTree(Integer orgId) {
        List<Tree<ColumnVo>> trees = new ArrayList<Tree<ColumnVo>>();
        List<ColumnVo> columns = columnDao.orgColumnlist(orgId);
        for (ColumnVo column : columns) {
            Tree<ColumnVo> tree = new Tree<ColumnVo>();
            tree.setId(column.getColumnId().toString());
            tree.setParentId("0");
            tree.setText(column.getColumnName());
            Map<String, Object> state = new HashMap<>(16);
            state.put("opened", true);
            tree.setState(state);
            trees.add(tree);
        }
        // 默认顶级菜单为０，根据数据库实际情况调整
        Tree<ColumnVo> t = BuildTree.build(trees);
        return t;
    }

}

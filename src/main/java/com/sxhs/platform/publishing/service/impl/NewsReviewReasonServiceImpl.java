package com.sxhs.platform.publishing.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.sxhs.platform.common.utils.R;
import com.sxhs.platform.common.utils.ShiroUtils;
import com.sxhs.platform.common.utils.StringUtils;
import com.sxhs.platform.publishing.dao.NewsReviewReasonDao;
import com.sxhs.platform.publishing.domain.NewsDO;
import com.sxhs.platform.publishing.domain.NewsReviewReasonDO;
import com.sxhs.platform.publishing.service.NewsReviewReasonService;
import com.sxhs.platform.publishing.service.NewsService;
import com.sxhs.platform.publishing.service.ProducerOrgNewsHandle;
import com.sxhs.platform.system.domain.UserDO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Slf4j
@Service
@Transactional
public class NewsReviewReasonServiceImpl implements NewsReviewReasonService {
    @Autowired
    private NewsReviewReasonDao newsReviewReasonDao;
    @Autowired
    private ProducerOrgNewsHandle producerOrgNewsHandle;
    @Autowired
    private NewsService newsService;

    @Override
    public NewsReviewReasonDO get(Integer id) {
        return newsReviewReasonDao.get(id);
    }

    @Override
    public List<NewsReviewReasonDO> list(Map<String, Object> map) {
        return newsReviewReasonDao.list(map);
    }

    @Override
    public int count(Map<String, Object> map) {
        return newsReviewReasonDao.count(map);
    }

    @Override
    public int save(NewsReviewReasonDO newsReviewReason) {
        return newsReviewReasonDao.save(newsReviewReason);
    }

    @Override
    public int update(NewsReviewReasonDO newsReviewReason) {
        return newsReviewReasonDao.update(newsReviewReason);
    }

    @Override
    public int remove(Integer id) {
        return newsReviewReasonDao.remove(id);
    }

    @Override
    public int batchRemove(Integer[] ids) {
        return newsReviewReasonDao.batchRemove(ids);
    }

    @Override
    public NewsReviewReasonDO getByNewsId(Integer id) {
        return newsReviewReasonDao.getByNewsId(id);
    }

    /**
     * 用于cms管理员人工审核资讯
     *
     * @param params
     * @return
     */
    @Override
    public R manualReview(Map<String, Object> params) {
        UserDO userDO = ShiroUtils.getUser();
        if (Objects.nonNull(params.get("newsId"))) {
            int newsId = Integer.parseInt(params.get("newsId").toString());
            NewsDO news = newsService.get(newsId);
            if (Objects.equals(params.get("status"), "1")) { // 审核同意
                news.setStatus(3);
                news.setPublishTime(new Date());
                if (newsService.update(news) > 0) {
                    producerOrgNewsHandle.orgNewsProducer(news);
                    return R.ok();
                }
            } else if (Objects.equals(params.get("status"), "0")) { // 审核不同意
                if (notApproved(news, NewsReviewReasonDO.Type.MANUALREVIEW.getCode(),userDO.getUserId().intValue(),params.get("reason").toString())){
                    return R.ok();
                }
            }
        }
        return R.error();
    }

    /**
     * 用于机器审核资讯
     *
     * @param result
     * @param newsDo
     * @return
     */
    @Override
    public R machineReview(String result, NewsDO newsDo) {
        // 根据词库接口返回result修改资讯status字段
        if (!StringUtils.equals(result, "block")) {
            log.info("阿里词库机器审核通过资讯 news:{}", JSONObject.toJSONString(newsDo));
            //资讯状态 0：草稿 1：撤回  2：审核中  3：审核通过  4：审核未通过  5.人工审核中
            if (StringUtils.equals(newsDo.getRoleId(), "4")) { // 资讯创建人角色为管理员，不需要人工审核直接发布
                newsDo.setStatus(3);
                newsDo.setPublishTime(new Date());
                // 资讯只会发送一次推送。编辑之后的推送不会再次发送
                if (newsService.update(newsDo) > 0 && Objects.isNull(newsDo.getUpdateTime())) {
                    // 处理机构资讯数据
                    producerOrgNewsHandle.orgNewsProducer(newsDo);
                }
            } else { // 别的角色则需要进入人工审核阶段进行审核
                newsDo.setStatus(5);
                newsService.update(newsDo);
            }
            return R.ok();
        } else {
            log.info("阿里词库机器审核未通过资讯 news:{}", JSONObject.toJSONString(newsDo));
            if (notApproved(newsDo, NewsReviewReasonDO.Type.MACHINEREVIEW.getCode(),Integer.MIN_VALUE,"内容包含敏感词不予通过")){
                return R.ok();
            }
        }
        return R.error();
    }

    /**
     *  保存审核不通过原因，并跟新资讯状态
     * @param newsDo
     * @param type
     * @param createdById 机器审核，创建人id为1；人工审核，创建人id为当前登录人id
     * @param reason
     * @return
     */
    private Boolean notApproved(NewsDO newsDo, Integer type, Integer createdById, String reason) {
        NewsReviewReasonDO newsReviewReasonDO = new NewsReviewReasonDO();
        newsReviewReasonDO.setCreatedById(createdById);
        newsReviewReasonDO.setCreatedTime(new Date());
        newsReviewReasonDO.setNewsId(newsDo.getId());
        newsReviewReasonDO.setReason(reason);
        newsReviewReasonDO.setType(type);
        newsDo.setStatus(4);
        if (newsService.update(newsDo) > 0 && newsReviewReasonDao.save(newsReviewReasonDO) > 0)
            return Boolean.TRUE;
        return Boolean.FALSE;
    }

}

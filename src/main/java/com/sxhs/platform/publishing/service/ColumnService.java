package com.sxhs.platform.publishing.service;

import com.sxhs.platform.common.domain.Tree;
import com.sxhs.platform.common.utils.ApiResult;
import com.sxhs.platform.publishing.domain.ColumnDO;
import com.sxhs.platform.publishing.vo.ColumnVo;

import java.util.List;
import java.util.Map;

/**
 * 栏目表
 * 
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2020-09-21 14:10:43
 */
public interface ColumnService {
	
	ColumnDO get(Integer id);
	
	List<ColumnDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);

	ApiResult save(ColumnDO column);

	ApiResult update(ColumnDO column);

	ApiResult remove(Integer id);
	
	int batchRemove(Integer[] ids);

	List<ColumnVo> appOrgColumnlist(Integer orgId);

    Tree<ColumnVo> getTree(Integer orgId);
}

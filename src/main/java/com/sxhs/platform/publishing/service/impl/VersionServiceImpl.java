package com.sxhs.platform.publishing.service.impl;

import com.sxhs.platform.common.utils.ApiResult;
import com.sxhs.platform.publishing.dao.VersionDao;
import com.sxhs.platform.publishing.domain.VersionDO;
import com.sxhs.platform.publishing.service.VersionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
@Transactional
public class VersionServiceImpl implements VersionService {
    @Autowired
    private VersionDao versionDao;

    @Override
    public VersionDO get(Integer id) {
        return versionDao.get(id);
    }

    @Override
    public List<VersionDO> list(Map<String, Object> map) {
        return versionDao.list(map);
    }

    @Override
    public int count(Map<String, Object> map) {
        return versionDao.count(map);
    }

    @Override
    public int save(VersionDO version) {
        return versionDao.save(version);
    }

    @Override
    public int update(VersionDO version) {
        return versionDao.update(version);
    }

    @Override
    public int remove(Integer id) {
        return versionDao.remove(id);
    }

    @Override
    public int batchRemove(Integer[] ids) {
        return versionDao.batchRemove(ids);
    }

    @Override
    public ApiResult getNewestVersion(String device) {
        Map<String, Object> param = new HashMap<>();
        param.put("dictName", device);
        param.put("dictType", "systemType");
        return  ApiResult.success(versionDao.getNewestVersion(param));
    }

}

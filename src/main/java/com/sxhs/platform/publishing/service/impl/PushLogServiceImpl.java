package com.sxhs.platform.publishing.service.impl;

import com.sxhs.platform.publishing.dao.PushLogDao;
import com.sxhs.platform.publishing.domain.PushLogDO;
import com.sxhs.platform.publishing.service.PushLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service
public class PushLogServiceImpl implements PushLogService {

	@Autowired
	private PushLogDao pushLogDao;


	@Override
	public int save(PushLogDO pushLog){
		return pushLogDao.save(pushLog);
	}


	@Override
	public PushLogDO get(Integer id){
		return pushLogDao.get(id);
	}

	@Override
	public List<PushLogDO> list(Map<String, Object> map){
		return pushLogDao.list(map);
	}

	@Override
	public int count(Map<String, Object> map){
		return pushLogDao.count(map);
	}


	@Override
	public int update(PushLogDO pushLog){
		return pushLogDao.update(pushLog);
	}

	@Override
	public int remove(Integer id){
		return pushLogDao.remove(id);
	}

	@Override
	public int batchRemove(Integer[] ids){
		return pushLogDao.batchRemove(ids);
	}


}

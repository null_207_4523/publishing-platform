package com.sxhs.platform.publishing.service.impl;

import com.sxhs.platform.common.config.Constant;
import com.sxhs.platform.common.domain.PageDO;
import com.sxhs.platform.common.utils.ApiResult;
import com.sxhs.platform.publishing.dao.NewsDao;
import com.sxhs.platform.publishing.domain.NewsDO;
import com.sxhs.platform.publishing.service.NewsService;
import com.sxhs.platform.publishing.vo.NewsBaseVo;
import com.sxhs.platform.publishing.vo.NewsDetailsVo;
import com.sxhs.platform.system.config.rocketMq.aliyun.MqConfig;
import com.sxhs.platform.system.config.rocketMq.aliyun.ProducerUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.*;

@Slf4j
@Service
@Transactional
public class NewsServiceImpl implements NewsService {
    @Autowired
    private NewsDao newsDao;
    @Autowired
    private ProducerUtil producer;
    @Autowired
    private MqConfig config;

    @Override
    public NewsDO get(Integer id) {
        return newsDao.get(id);
    }

    @Override
    public List<NewsDO> list(Map<String, Object> map) {
        return newsDao.list(map);
    }

    @Override
    public int count(Map<String, Object> map) {
        return newsDao.count(map);
    }

    @Override
    public int save(NewsDO news) {
        return newsDao.save(news);
    }

    @Override
    public int update(NewsDO news) {
        return newsDao.update(news);
    }

    @Override
    public int remove(Integer id) {
        return newsDao.remove(id);
    }

    @Override
    public int batchRemove(Integer[] ids) {
        return newsDao.batchRemove(ids);
    }

    @Override
    public ApiResult details(Integer id, Integer userId) {
        Map<String, Object> map = new HashMap<>();
        map.put("id", id);
        map.put("hasDelete", Constant.HAS_DELETE_NO);
        map.put("status", Constant.NEWS_STATUS_APPROVE);
        map.put("createdUserId", userId);
        NewsDetailsVo detailsVo = newsDao.getNewsCollectLikeDetails(map);
        if (Objects.isNull(detailsVo))
            return ApiResult.fail("查无资讯信息，或者资讯已删除");
        // 查询用户是否点赞、收藏这条资讯
        if (!CollectionUtils.isEmpty(detailsVo.getCollectList()))
            detailsVo.setHasCollect(Constant.HAS_COLLECT_YES);
        if (!CollectionUtils.isEmpty(detailsVo.getLikeList()))
            detailsVo.setHasLike(Constant.HAS_LIKE_YES);
        // 添加最多3条同栏目资讯除本条资讯以外
        Map<String, Object> params2 = new HashMap<>();
        params2.put("columnId", detailsVo.getColumnId());
        params2.put("status", Constant.NEWS_STATUS_APPROVE);
        params2.put("hasDelete", Constant.HAS_DELETE_NO);
        params2.put("coverType", 0);
        params2.put("id", id);
        params2.put("limit", 30);
        List<NewsBaseVo> list2 = newsDao.randomNewsByColumn(params2);
        detailsVo.setRecommend(com.sxhs.platform.common.utils.CollectionUtils.random(list2, 3));
        return ApiResult.success(detailsVo);
    }

    @Override
    public NewsDetailsVo getNewsDetails(Map<String, Object> params) {
        return newsDao.getNewsDetails(params);
    }

    @Override
    public List<NewsBaseVo> collectNewsList(Map<String, Object> params) {
        return newsDao.collectNewsList(params);
    }

    @Override
    public int collectNewsCount(Map<String, Object> params) {
        return newsDao.collectNewsCount(params);
    }

    @Override
    public ApiResult listByColumn(Integer userId, Integer columnId, Integer offset, Integer limit) {
        offset = Objects.isNull(offset) ? 0 : offset;
        limit = Objects.isNull(limit) ? 9 : limit;
        //查询列表数据
        Map<String, Object> params = new HashMap<>();
        // columnId=0表示才“推荐”栏目，现在策略是查询排除栏目条件查询全部满足条件资讯
        columnId = Objects.equals(columnId, 0) ? null : columnId;
        params.put("columnId", columnId);
        params.put("status", Constant.NEWS_STATUS_APPROVE);
        params.put("offset", offset * limit);
        params.put("limit", limit);
        params.put("hasDelete", Constant.HAS_DELETE_NO);
        params.put("userId", userId);
        List<NewsBaseVo> newsList = newsDao.listByColumn(params);
        if (CollectionUtils.isEmpty(newsList))
            return ApiResult.success(new PageDO<>());
        int total = newsDao.count(params);
        PageDO<NewsBaseVo> page = new PageDO<>();
        page.setTotal(total);
        page.setRows(newsList);
        page.setLimit(offset);
        page.setOffset(limit);
        return ApiResult.success(page);
    }

    @Override
    public int getNewsCountByDate(Map<String, Object> params) {
        return Objects.isNull(newsDao.getNewsCountByDate(params)) ? 0 : newsDao.getNewsCountByDate(params);
    }

    @Override
    public List<NewsDO> newslist(Map<String, Object> map) {
        return newsDao.newslist(map);
    }

    @Override
    public int countNewslist(Map<String, Object> map) {
        return newsDao.countNewslist(map);
    }

    /**
     * web应用端创建资讯
     *
     * @param news
     * @return
     */
    @Override
    public ApiResult webAppNewsSave(NewsDO news) {
        news.setCreatedTime(new Date());
        news.setType(0);
        news.setHasTop(0);
        // modify by wangzhijun 2021/2/25 cms后台发布默认为0，web应用发布则设置为1
        news.setHasInnerPublish(1);
        news.setHasDelete(Constant.HAS_DELETE_NO);
        news.setFansLikeCount(0);
        news.setLikeCount(0);
        news.setFansCollectCount(0);
        news.setCollectCount(0);
        news.setFansCommentCount(0);
        news.setCommentCount(0);
        news.setFansReadCount(0);
        news.setReadCount(0);
        news.setFansShareCount(0);
        news.setShareCount(0);
        if (newsDao.save(news) > 0) {
//            if (Objects.equals(news.getStatus(), 2)) {
//                contentSafe(news);
//            }
            return ApiResult.success(news);
        }
        return ApiResult.fail();
    }

    /**
     * web应用端编辑资讯
     *
     * @param news
     * @return
     */
    @Override
    public ApiResult webAppNewsUpdate(NewsDO news) {
        news.setUpdateTime(new Date());
        // 未删除的资讯
        if(Objects.equals(news.getHasDelete(),0)){
            if (Objects.equals(news.getStatus(), 1) || Objects.equals(news.getStatus(), 3) || Objects.equals(news.getStatus(), 4)) { // 资讯状态为1：撤回 3：审核通过  4：审核未通过中任意一种的，则不需重新过机审
                news.setStatus(2);
//                contentSafe(news);
            } else if (Objects.equals(news.getStatus(), 2) || Objects.equals(news.getStatus(), 5)){ // 资讯状态为2：机器审核中   5.人工审核中的资讯，产品要求不能进行编辑
                return ApiResult.fail("审核中的文章无法进行编辑");
            }
        }
        if (newsDao.update(news) > 0)
            return ApiResult.success(news);
        return ApiResult.fail();
    }

//    /**
//     * /阿里内容审核流程
//     *
//     * @param news
//     */
//    private void contentSafe(NewsDO news) {
//        try {
//            Map<String, Object> map = new HashMap<>();
//            map.put("id", news.getId());
//            map.put("roldId", news.getRoleId());
//            String jsonString = JSON.toJSONString(map);
//            producer.sendMsg(config.getTopic(), CategoryTagEnum.CONTENTSAFE.getTags(), UUID.randomUUID() + "", jsonString);
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//            log.error("消息发送失败：", e);
//        }
//    }

}

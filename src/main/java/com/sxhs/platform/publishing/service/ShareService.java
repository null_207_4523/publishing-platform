package com.sxhs.platform.publishing.service;

import com.sxhs.platform.common.utils.ApiResult;
import com.sxhs.platform.publishing.domain.OrgDailyStatisticsDO;
import com.sxhs.platform.publishing.domain.ShareDO;

import java.util.List;
import java.util.Map;

/**
 * 资讯分享
 * 
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2020-09-21 14:10:45
 */
public interface ShareService {
	
	ShareDO get(Integer id);
	
	List<ShareDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(ShareDO share);
	
	int update(ShareDO share);
	
	int remove(Integer id);
	
	int batchRemove(Integer[] ids);

	OrgDailyStatisticsDO getShareCountByDate(Map<String, Object> params);

    ApiResult shareNews(Integer id, Integer userId);
}

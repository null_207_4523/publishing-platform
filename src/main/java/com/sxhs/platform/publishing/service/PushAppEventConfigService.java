package com.sxhs.platform.publishing.service;

import com.sxhs.platform.publishing.domain.PushAppEventConfigDO;

import java.util.List;
import java.util.Map;


public interface PushAppEventConfigService {
	
	PushAppEventConfigDO get(Integer id);
	
	List<PushAppEventConfigDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(PushAppEventConfigDO pushAppEventConfig);
	
	int update(PushAppEventConfigDO pushAppEventConfig);
	
	int remove(Integer id);
	
	int batchRemove(Integer[] ids);
}

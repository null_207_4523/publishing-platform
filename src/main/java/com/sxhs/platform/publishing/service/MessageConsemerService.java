package com.sxhs.platform.publishing.service;

import com.sxhs.platform.publishing.message.dto.MessageDTO;

public interface MessageConsemerService {

    /**
     *      保存消息及 友盟推送
     * @param messageBody
     */
    void saveMessageAndUPush(MessageDTO messageBody);
}

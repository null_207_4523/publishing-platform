package com.sxhs.platform.publishing.service.impl;

import com.alibaba.fastjson.JSON;
import com.aliyun.openservices.ons.api.OnExceptionContext;
import com.aliyun.openservices.ons.api.SendCallback;
import com.aliyun.openservices.ons.api.SendResult;
import com.sxhs.platform.publishing.message.dto.BaseMessageDTO;
import com.sxhs.platform.publishing.message.dto.MessageDTO;
import com.sxhs.platform.publishing.message.dto.MessageContentDTO;
import com.sxhs.platform.publishing.message.enums.CategoryTagEnum;
import com.sxhs.platform.publishing.service.MessageSendService;
import com.sxhs.platform.system.config.rocketMq.aliyun.ProducerUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.UnsupportedEncodingException;
import java.util.List;
@Slf4j
@Transactional
@Service
public class MessageSendServiceImpl implements MessageSendService {

    @Autowired
    private ProducerUtil producer;

    @Override
    public MessageDTO getBodyMessage(List<MessageContentDTO> userMessage, BaseMessageDTO baseMessage)
    {
        MessageDTO bodyMsg = new MessageDTO();
        bodyMsg.setMessageContentDTOList(userMessage);
        bodyMsg.setBaseMessageDTO(baseMessage);
        return bodyMsg;
    }

    @Override
    public SendResult sendMessage(String topic, String tags, String key, MessageDTO bodyMsg) {

        CategoryTagEnum category = CategoryTagEnum.getEnumByKey(tags);
        if (null == category) {
            throw new NullPointerException();
        }
        bodyMsg.getBaseMessageDTO().setCategory(category.getTags());
        bodyMsg.getBaseMessageDTO().setCategoryName(category.getName());

        try {
            SendResult sendResult = producer.sendMsg(topic, tags, key, JSON.toJSONString(bodyMsg));
            return sendResult;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public SendResult sendMessageServer(String topic, String tags, String key, MessageDTO bodyMsg) {
        try {
            SendResult sendResult = producer.sendMsg(topic, tags, key, JSON.toJSONString(bodyMsg));
            return sendResult;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

}

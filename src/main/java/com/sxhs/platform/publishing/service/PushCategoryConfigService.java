package com.sxhs.platform.publishing.service;

import com.sxhs.platform.publishing.domain.PushCategoryConfigDO;

import java.util.List;
import java.util.Map;

public interface PushCategoryConfigService {
	
	PushCategoryConfigDO get(Integer id);
	
	List<PushCategoryConfigDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(PushCategoryConfigDO pushCategoryConfig);
	
	int update(PushCategoryConfigDO pushCategoryConfig);
	
	int remove(Integer id);
	
	int batchRemove(Integer[] ids);
}

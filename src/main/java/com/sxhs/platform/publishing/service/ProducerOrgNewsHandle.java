package com.sxhs.platform.publishing.service;

import com.sxhs.platform.common.config.Constant;
import com.sxhs.platform.common.utils.CollectionUtils;
import com.sxhs.platform.publishing.domain.NewsDO;
import com.sxhs.platform.publishing.domain.OrgSubscriptionDO;
import com.sxhs.platform.publishing.message.enums.CategoryTagEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class ProducerOrgNewsHandle {

    private static final Logger logger = LoggerFactory.getLogger(ProducerOrgNewsHandle.class);
    @Autowired
    private UmMessageService umMessageService;
    @Autowired
    private OrgSubscriptionService orgSubscriptionService;

    public void orgNewsProducer(NewsDO news) {
        if (Objects.isNull(news))
            return;
        // 查询该机构是否有人订阅,获取订阅人userId
        Map<String, Object> params = new HashMap<>(16);
        params.put("hasDelete", Constant.HAS_DELETE_NO);
        params.put("orgId", news.getOrgId());
        List<Integer> subscriptionUserIds = orgSubscriptionService.list(params).stream().map(OrgSubscriptionDO::getCreatedUserId).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(subscriptionUserIds))
            return;
        umMessageService.sendNewsMsg(news, subscriptionUserIds, CategoryTagEnum.ATTENTION);
    }

}

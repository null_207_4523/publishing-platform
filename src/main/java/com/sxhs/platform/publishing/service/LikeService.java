package com.sxhs.platform.publishing.service;

import com.sxhs.platform.common.utils.ApiResult;
import com.sxhs.platform.publishing.domain.LikeDO;
import com.sxhs.platform.publishing.domain.OrgDailyStatisticsDO;

import java.util.List;
import java.util.Map;

/**
 * 资讯点赞
 * 
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2020-09-21 14:10:44
 */
public interface LikeService {
	
	LikeDO get(Integer id);
	
	List<LikeDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(LikeDO like);
	
	int update(LikeDO like);
	
	int remove(Integer id);
	
	int batchRemove(Integer[] ids);

	OrgDailyStatisticsDO getlikeCountByDate(Map<String, Object> params);

    ApiResult likeNews(Integer id, Integer userId);
}

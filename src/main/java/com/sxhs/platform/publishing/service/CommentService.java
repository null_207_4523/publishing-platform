package com.sxhs.platform.publishing.service;

import com.sxhs.platform.common.utils.ApiResult;
import com.sxhs.platform.publishing.domain.CommentDO;
import com.sxhs.platform.publishing.domain.OrgDailyStatisticsDO;

import java.util.List;
import java.util.Map;

/**
 * 资讯评论
 * 
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2020-09-21 14:10:43
 */
public interface CommentService {
	
	CommentDO get(Integer id);
	
	List<CommentDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(CommentDO comment);
	
	int update(CommentDO comment);
	
	int remove(Integer id);
	
	int batchRemove(Integer[] ids);

    ApiResult commentNews(Integer id, Integer userId, String userName, String userAvatar, String content);

	ApiResult commentReply(Integer id, Integer commentId, Integer userId, String userName, String userAvatar, String content);

	ApiResult commentDelete(Integer commentId, Integer userId);

	ApiResult pageComment(Integer id, Integer userId, Integer offset, Integer limit);

	ApiResult pageCommentReplys(Integer commentId, Integer userId, Integer offset, Integer limit);

	ApiResult commentDetailsList(Integer id);

	OrgDailyStatisticsDO getCommentCountByDate(Map<String, Object> params);
}

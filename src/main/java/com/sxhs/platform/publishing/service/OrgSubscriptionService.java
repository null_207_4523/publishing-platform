package com.sxhs.platform.publishing.service;

import com.sxhs.platform.common.utils.ApiResult;
import com.sxhs.platform.publishing.domain.OrgSubscriptionDO;

import java.util.List;
import java.util.Map;

/**
 * 资讯机构订阅
 * 
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2020-09-21 14:10:45
 */
public interface OrgSubscriptionService {
	
	OrgSubscriptionDO get(Integer id);
	
	List<OrgSubscriptionDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(OrgSubscriptionDO orgSubscription);
	
	int update(OrgSubscriptionDO orgSubscription);
	
	int remove(Integer id);
	
	int batchRemove(Integer[] ids);

	ApiResult add(Integer userId, Integer orgId);

	ApiResult cancel(Integer userId, Integer orgId);

	int getOrgSubscriptionCountByDate(Map<String, Object> map);
}

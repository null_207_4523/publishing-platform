package com.sxhs.platform.publishing.service;

import com.sxhs.platform.publishing.domain.NewsDO;
import com.sxhs.platform.publishing.message.enums.CategoryTagEnum;

import java.util.List;

public interface UmMessageService {
    Boolean sendNewsMsg(NewsDO news, List<Integer> receiverUserIds, CategoryTagEnum CategoryTag);
}

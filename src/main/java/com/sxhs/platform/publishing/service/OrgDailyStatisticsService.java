package com.sxhs.platform.publishing.service;

import com.sxhs.platform.publishing.domain.OrgDailyStatisticsDO;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * 机构资讯每日统计表
 * 
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2020-09-21 14:10:44
 */
public interface OrgDailyStatisticsService {
	
	OrgDailyStatisticsDO get(Integer id);
	
	List<OrgDailyStatisticsDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(OrgDailyStatisticsDO orgDailyStatistics);
	
	int update(OrgDailyStatisticsDO orgDailyStatistics);
	
	int remove(Integer id);
	
	int batchRemove(Integer[] ids);

	void updateDailyStatistics(Map<String, Object> map) throws ParseException;

	List<OrgDailyStatisticsDO> listByLimit(Map<String, Object> map);

	int countlistByLimit(Map<String, Object> map);

}

package com.sxhs.platform.publishing.service.impl;

import com.sxhs.platform.common.config.Constant;
import com.sxhs.platform.common.utils.ApiResult;
import com.sxhs.platform.common.utils.BeanCopierUtils;
import com.sxhs.platform.common.utils.FieldValueUtils;
import com.sxhs.platform.publishing.dao.CommentLikeDao;
import com.sxhs.platform.publishing.domain.CommentDO;
import com.sxhs.platform.publishing.domain.CommentLikeDO;
import com.sxhs.platform.publishing.service.CommentLikeService;
import com.sxhs.platform.publishing.service.CommentService;
import com.sxhs.platform.publishing.vo.CommentVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.*;


@Service
@Transactional
public class CommentLikeServiceImpl implements CommentLikeService {
    @Autowired
    private CommentLikeDao commentLikeDao;
    @Autowired
    private CommentService commentService;

    @Override
    public CommentLikeDO get(Integer id) {
        return commentLikeDao.get(id);
    }

    @Override
    public List<CommentLikeDO> list(Map<String, Object> map) {
        return commentLikeDao.list(map);
    }

    @Override
    public int count(Map<String, Object> map) {
        return commentLikeDao.count(map);
    }

    @Override
    public int save(CommentLikeDO commentLike) {
        return commentLikeDao.save(commentLike);
    }

    @Override
    public int update(CommentLikeDO commentLike) {
        return commentLikeDao.update(commentLike);
    }

    @Override
    public int remove(Integer id) {
        return commentLikeDao.remove(id);
    }

    @Override
    public int batchRemove(Integer[] ids) {
        return commentLikeDao.batchRemove(ids);
    }

    @Override
    public ApiResult commentLike(Integer commentId, Integer userId, String userName, String userAvatar) {
        Map<String, Object> params = new HashMap<>();
        params.put("id", commentId);
        params.put("hasDelete", Constant.HAS_DELETE_NO);
        params.put("createdById", userId);
        CommentVo commentVo = commentLikeDao.getCommentLikeList(params);
        if (Objects.isNull(commentVo))
            return ApiResult.fail("无此评论数据，或已被删除");
        List<CommentLikeDO> commentLikeDOs = commentVo.getCommentLikeList();
        CommentDO commentDO = new CommentDO();
        BeanCopierUtils.copyProperties(commentVo, commentDO);
        // 保证每条评论只能在未点赞状态下被点赞
        if (!CollectionUtils.isEmpty(commentLikeDOs)) {
            CommentLikeDO commentLikeDO = commentLikeDOs.get(0);
            commentDO = (CommentDO) FieldValueUtils.updateCount("likeCount", Constant.HAS_FANS_NO, commentDO, Constant.HAS_ADD_NO, 1);
            if (commentLikeDao.remove(commentLikeDO.getId()) > 0 && commentService.update(commentDO) > 0)
                return ApiResult.success(Constant.HAS_LIKE_NO, "取消赞");
        }
        // 创建评论点赞实体
        CommentLikeDO commentLikeDO = new CommentLikeDO();
        commentLikeDO.setCommentId(commentId);
        commentLikeDO.setCreatedById(userId);
        commentLikeDO.setCreatedTime(new Date());
        commentLikeDO.setHasDelete(Constant.HAS_DELETE_NO);
        commentLikeDO.setCreatedBy(userName);
        commentLikeDO.setCreatedByAvatar(userAvatar);
        commentDO = (CommentDO) FieldValueUtils.updateCount("likeCount", Constant.HAS_FANS_NO, commentDO, Constant.HAS_ADD_YES, 1);
        // 评论点赞实体插入数据库
        if (commentLikeDao.save(commentLikeDO) > 0 && commentService.update(commentDO) > 0)
            return ApiResult.success(Constant.HAS_LIKE_YES, "点赞");
        return ApiResult.fail();
    }

    @Override
    public ApiResult commentLikerList(Integer commentId) {
        Map<String, Object> params = new HashMap<>();
        params.put("id", commentId);
        params.put("hasDelete", Constant.HAS_DELETE_NO);
        CommentVo commentVo = commentLikeDao.getCommentLikeList(params);
        if (Objects.isNull(commentVo))
            return ApiResult.fail("无此评论数据，或已被删除");
        return ApiResult.success(commentVo.getCommentLikeList());
    }

}

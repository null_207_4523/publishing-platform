package com.sxhs.platform.publishing.service;

import com.sxhs.platform.publishing.domain.PushLogDO;

import java.util.List;
import java.util.Map;

/**
 * 消息推送记录 
 * 
 */
public interface PushLogService {

	int save(PushLogDO pushLog);

	PushLogDO get(Integer id);

	List<PushLogDO> list(Map<String, Object> map);

	int count(Map<String, Object> map);


	int update(PushLogDO pushLog);

	int remove(Integer id);

	int batchRemove(Integer[] ids);

}

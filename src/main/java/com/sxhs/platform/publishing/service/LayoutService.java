package com.sxhs.platform.publishing.service;

import com.sxhs.platform.publishing.domain.LayoutDO;

import java.util.List;
import java.util.Map;

/**
 * 应用布局
 * 
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2020-12-03 16:33:48
 */
public interface LayoutService {

	/** 查询用户自定义布局*/
	LayoutDO selectOneByUser(Integer userId);

	int saveLayout(Integer userId, String layId);

	
	LayoutDO get(Integer id);
	
	List<LayoutDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(LayoutDO layout);
	
	int update(LayoutDO layout);

	int remove(Integer id);

	int batchRemove(Integer[] ids);
}

package com.sxhs.platform.publishing.service.impl;

import com.sxhs.platform.common.config.Constant;
import com.sxhs.platform.common.domain.PageDO;
import com.sxhs.platform.common.utils.ApiResult;
import com.sxhs.platform.common.utils.FieldValueUtils;
import com.sxhs.platform.common.utils.VerificateUtil;
import com.sxhs.platform.publishing.dao.CommentDao;
import com.sxhs.platform.publishing.domain.CommentDO;
import com.sxhs.platform.publishing.domain.NewsDO;
import com.sxhs.platform.publishing.domain.OrgDailyStatisticsDO;
import com.sxhs.platform.publishing.service.CommentService;
import com.sxhs.platform.publishing.service.NewsService;
import com.sxhs.platform.publishing.vo.CommentVo;
import com.sxhs.platform.publishing.vo.NewsDetailsVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.*;


@Service
@Transactional
public class CommentServiceImpl implements CommentService {
    @Autowired
    private CommentDao commentDao;
    @Autowired
    private NewsService newsService;

    @Override
    public CommentDO get(Integer id) {
        return commentDao.get(id);
    }

    @Override
    public List<CommentDO> list(Map<String, Object> map) {
        return commentDao.list(map);
    }

    @Override
    public int count(Map<String, Object> map) {
        return commentDao.count(map);
    }

    @Override
    public int save(CommentDO comment) {
        return commentDao.save(comment);
    }

    @Override
    public int update(CommentDO comment) {
        return commentDao.update(comment);
    }

    @Override
    public int remove(Integer id) {
        return commentDao.remove(id);
    }

    @Override
    public int batchRemove(Integer[] ids) {
        return commentDao.batchRemove(ids);
    }

    @Override
    public ApiResult commentNews(Integer id, Integer userId, String userName, String userAvatar, String content) {
        // 获取资讯详情
        NewsDO newsDO = newsService.get(id);
        if (Objects.isNull(newsDO) || Objects.equals(newsDO.getHasDelete(), Constant.HAS_DELETE_YES))
            return ApiResult.fail("无此资讯数据，或已被删除");
        Integer hasFans = VerificateUtil.hasFans(userId, newsDO.getOrgId());
        // 创建评论实体
        CommentDO commentDO = new CommentDO();
        commentDO.setNewsId(id);
        commentDO.setContent(content);
        commentDO.setLevel(0);
        commentDO.setParentId(0);
        commentDO.setReplies(0);
        commentDO.setCreatedById(userId);
        commentDO.setCreatedBy(userName);
        commentDO.setCreatedByAvatar(userAvatar);
        commentDO.setCreatedTime(new Date());
        commentDO.setHasDelete(Constant.HAS_DELETE_NO);
        commentDO.setHasFans(hasFans);
        commentDO.setLikeCount(0);
        newsDO = (NewsDO) FieldValueUtils.updateCount("commentCount", hasFans, newsDO, Constant.HAS_ADD_YES, 1);
        // 评论实体插入数据库
        if (commentDao.save(commentDO) > 0 && newsService.update(newsDO) > 0)
            return ApiResult.success();
        return ApiResult.fail();
    }

    @Override
    public ApiResult commentReply(Integer id, Integer commentId, Integer userId, String userName, String userAvatar, String content) {
        // 获取资讯详情
        NewsDO newsDO = newsService.get(id);
        if (Objects.isNull(newsDO) || Objects.equals(newsDO.getHasDelete(), Constant.HAS_DELETE_YES))
            return ApiResult.fail("无此资讯数据，或已被删除");
        CommentDO superiorsComment = commentDao.get(commentId);
        if (Objects.isNull(superiorsComment) || Objects.equals(superiorsComment.getHasDelete(), Constant.HAS_DELETE_YES))
            return ApiResult.fail("无此评论数据，或已被删除");
        Integer hasFans = VerificateUtil.hasFans(userId, newsDO.getOrgId());
        // 创建评论实体
        CommentDO commentDO = new CommentDO();
        commentDO.setNewsId(id);
        commentDO.setContent(content);
        commentDO.setLevel(superiorsComment.getLevel() + 1);
        commentDO.setParentId(commentId);
        commentDO.setReplies(0);
        commentDO.setCreatedById(userId);
        commentDO.setCreatedBy(userName);
        commentDO.setCreatedByAvatar(userAvatar);
        commentDO.setCreatedTime(new Date());
        commentDO.setHasDelete(Constant.HAS_DELETE_NO);
        commentDO.setHasFans(hasFans);
        commentDO.setLikeCount(0);
        if (commentDao.save(commentDO) > 0) {
            newsDO = (NewsDO) FieldValueUtils.updateCount("commentCount", hasFans, newsDO, Constant.HAS_ADD_YES, 1);
            superiorsComment = (CommentDO) FieldValueUtils.updateCount("replies", hasFans, superiorsComment, Constant.HAS_ADD_YES, 1);
            String pollingPath = superiorsComment.getPollingPath();
            StringBuffer sb = new StringBuffer();
            if (StringUtils.isNotBlank(pollingPath)) {
                sb.append(pollingPath);
                sb.append(",");
            }
            sb.append(commentDO.getId().toString());
            superiorsComment.setPollingPath(sb.toString());
            // 评论实体插入数据库
            if (commentDao.update(superiorsComment) > 0 && newsService.update(newsDO) > 0)
                return ApiResult.success();
        }
        return ApiResult.fail();
    }

    @Override
    public ApiResult commentDelete(Integer commentId, Integer userId) {
        CommentDO commentDO = commentDao.get(commentId);
        if (Objects.isNull(commentDO) || Objects.equals(commentDO.getHasDelete(), Constant.HAS_DELETE_YES))
            return ApiResult.fail("无此评论数据，或已被删除");
        if (!Objects.equals(userId, commentDO.getCreatedById()))
            return ApiResult.fail("不能刪除他人的评论");
        commentDO.setHasDelete(Constant.HAS_DELETE_YES);
        if (commentDao.update(commentDO) > 0) {
            // 获取资讯详情
            NewsDO newsDO = newsService.get(commentDO.getNewsId());
            if (Objects.isNull(newsDO) || Objects.equals(newsDO.getHasDelete(), Constant.HAS_DELETE_YES))
                return ApiResult.fail("无此资讯数据，或已被删除");
            Integer hasFans = VerificateUtil.hasFans(userId, newsDO.getOrgId());
            // 存在下级评论
            if (Objects.equals(commentDO.getParentId(), 0) || StringUtils.isNotBlank(commentDO.getPollingPath())) {
                // 回复数量加上删除本身一条数据
                Integer variable = commentDO.getReplies() + 1;
                newsDO = (NewsDO) FieldValueUtils.updateCount("commentCount", hasFans, newsDO, Constant.HAS_ADD_NO, variable);
                if (newsService.update(newsDO) > 0)
                    return ApiResult.success();
            }
            // 不存在下级评论
            if (StringUtils.isBlank(commentDO.getPollingPath())) {
                Integer variable = 1;
                CommentDO superiorsComment = commentDao.get(commentDO.getParentId());
                superiorsComment = (CommentDO) FieldValueUtils.updateCount("replies", hasFans, superiorsComment, Constant.HAS_ADD_NO, variable);
                newsDO = (NewsDO) FieldValueUtils.updateCount("commentCount", hasFans, newsDO, Constant.HAS_ADD_NO, variable);
                if (commentDao.update(superiorsComment) > 0 && newsService.update(newsDO) > 0) {
                    return ApiResult.success();
                }
            }
        }
        return ApiResult.fail();
    }

    @Override
    public ApiResult pageComment(Integer id, Integer userId, Integer offset, Integer limit) {
        // 获取资讯详情
        PageDO<CommentVo> page = getCommentData(id, null, userId, offset, limit);
        return ApiResult.success(page);
    }

    @Override
    public ApiResult pageCommentReplys(Integer commentId, Integer userId, Integer offset, Integer limit) {
        // 获取资讯详情
        PageDO<CommentVo> page = getCommentData(null, commentId, userId, offset, limit);
        return ApiResult.success(page);
    }

    @Override
    public ApiResult commentDetailsList(Integer id) {
        Map<String, Object> params = new HashMap<>();
        // 1.查询资讯的全部评论
        params.put("id", id);
        params.put("level", 0);
        params.put("parentId", 0);
        params.put("hasDelete", Constant.HAS_DELETE_NO);
        NewsDetailsVo vo = commentDao.getNewsCommentsList(params);
        if (Objects.isNull(vo))
            return ApiResult.success(new NewsDetailsVo());
        return ApiResult.success(vo.getCommentReplys());
    }

    @Override
    public OrgDailyStatisticsDO getCommentCountByDate(Map<String, Object> params) {
        return commentDao.getCommentCountByDate(params);
    }

    /**
     *  查询资讯评论集合
     * @param id
     * @param commentId
     * @param userId
     * @param offset
     * @param limit
     * @return
     */
    private PageDO<CommentVo> getCommentData(Integer id, Integer commentId, Integer userId, Integer offset, Integer limit) {
        offset = Objects.isNull(offset) ? 0 : offset;
        limit = Objects.isNull(limit) ? 9 : limit;
        // 获取资讯详情
        Map<String, Object> params = new HashMap<>();
        if (Objects.nonNull(id)) {
            params.put("newsId", id);
            params.put("parentId", 0);
        } else if (Objects.nonNull(commentId)) {
            params.put("parentId", commentId);
        }
        params.put("hasDelete", Constant.HAS_DELETE_NO);
        params.put("offset", offset * limit);
        params.put("limit", limit);
        List<CommentVo> list = commentDao.getNewsCommentList(params);
        if (CollectionUtils.isEmpty(list))
            return new PageDO<>();
        // 判断对当前评论是否点赞
        if (Objects.nonNull(userId)) {
            for (CommentVo vo : list) {
                Integer hasLike = vo.getCommentLikeList().stream().filter(item -> item.getCreatedById().equals(userId)).findAny().isPresent() ? Constant.HAS_LIKE_YES : Constant.HAS_LIKE_NO;
                vo.setHasLike(hasLike);
            }
        }
        int total = commentDao.count(params);
        PageDO<CommentVo> page = new PageDO<>();
        page.setTotal(total);
        page.setRows(list);
        page.setLimit(offset);
        page.setOffset(limit);
        return page;
    }




}

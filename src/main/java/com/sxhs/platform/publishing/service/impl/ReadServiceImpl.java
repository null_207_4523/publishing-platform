package com.sxhs.platform.publishing.service.impl;

import com.sxhs.platform.common.config.Constant;
import com.sxhs.platform.common.utils.ApiResult;
import com.sxhs.platform.common.utils.FieldValueUtils;
import com.sxhs.platform.common.utils.VerificateUtil;
import com.sxhs.platform.publishing.dao.ReadDao;
import com.sxhs.platform.publishing.domain.NewsDO;
import com.sxhs.platform.publishing.domain.OrgDailyStatisticsDO;
import com.sxhs.platform.publishing.domain.ReadDO;
import com.sxhs.platform.publishing.service.NewsService;
import com.sxhs.platform.publishing.service.ReadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;


@Service
@Transactional
public class ReadServiceImpl implements ReadService {
    @Autowired
    private ReadDao readDao;
    @Autowired
    private NewsService newsService;

    @Override
    public ReadDO get(Integer id) {
        return readDao.get(id);
    }

    @Override
    public List<ReadDO> list(Map<String, Object> map) {
        return readDao.list(map);
    }

    @Override
    public int count(Map<String, Object> map) {
        return readDao.count(map);
    }

    @Override
    public int save(ReadDO read) {
        return readDao.save(read);
    }

    @Override
    public int update(ReadDO read) {
        return readDao.update(read);
    }

    @Override
    public int remove(Integer id) {
        return readDao.remove(id);
    }

    @Override
    public int batchRemove(Integer[] ids) {
        return readDao.batchRemove(ids);
    }

    @Override
    public OrgDailyStatisticsDO getReadCountByDate(Map<String, Object> params) {
        return readDao.getReadCountByDate(params);
    }

    @Override
    public ApiResult readNews(Integer id, Integer userId) {
        // 获取资讯详情
        NewsDO newsDO = newsService.get(id);
        if (Objects.isNull(newsDO) || Objects.equals(newsDO.getHasDelete(), Constant.HAS_DELETE_YES))
            return ApiResult.fail("无此资讯数据，或已被删除");
        Integer hasFans = VerificateUtil.hasFans(userId, newsDO.getOrgId());
        newsDO = (NewsDO) FieldValueUtils.updateCount("readCount", hasFans, newsDO, Constant.HAS_ADD_YES, 1);
        if(Objects.isNull(userId)){
            if (newsService.update(newsDO) > 0)
                return ApiResult.success();
            return ApiResult.fail();
        }
        // 创建分享实体
        ReadDO readDO = new ReadDO();
        readDO.setNewsId(newsDO.getId());
        readDO.setCreatedById(userId);
        readDO.setCreatedTime(new Date());
        readDO.setHasDelete(Constant.HAS_DELETE_NO);
        readDO.setHasFans(hasFans);
        // 阅读实体插入数据库
        if (readDao.save(readDO) > 0 && newsService.update(newsDO) > 0)
            return ApiResult.success();
        return ApiResult.fail();
    }

}

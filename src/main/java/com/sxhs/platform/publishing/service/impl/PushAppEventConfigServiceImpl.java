package com.sxhs.platform.publishing.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.sxhs.platform.publishing.dao.PushAppEventConfigDao;
import com.sxhs.platform.publishing.domain.PushAppEventConfigDO;
import com.sxhs.platform.publishing.service.PushAppEventConfigService;



@Service
public class PushAppEventConfigServiceImpl implements PushAppEventConfigService {
	@Autowired
	private PushAppEventConfigDao pushAppEventConfigDao;
	
	@Override
	public PushAppEventConfigDO get(Integer id){
		return pushAppEventConfigDao.get(id);
	}
	
	@Override
	public List<PushAppEventConfigDO> list(Map<String, Object> map){
		return pushAppEventConfigDao.list(map);
	}
	
	@Override
	public int count(Map<String, Object> map){
		return pushAppEventConfigDao.count(map);
	}
	
	@Override
	public int save(PushAppEventConfigDO pushAppEventConfig){
		return pushAppEventConfigDao.save(pushAppEventConfig);
	}
	
	@Override
	public int update(PushAppEventConfigDO pushAppEventConfig){
		return pushAppEventConfigDao.update(pushAppEventConfig);
	}
	
	@Override
	public int remove(Integer id){
		return pushAppEventConfigDao.remove(id);
	}
	
	@Override
	public int batchRemove(Integer[] ids){
		return pushAppEventConfigDao.batchRemove(ids);
	}
	
}

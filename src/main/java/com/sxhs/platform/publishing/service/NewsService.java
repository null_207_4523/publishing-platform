package com.sxhs.platform.publishing.service;

import com.sxhs.platform.common.utils.ApiResult;
import com.sxhs.platform.publishing.domain.NewsDO;
import com.sxhs.platform.publishing.vo.NewsBaseVo;
import com.sxhs.platform.publishing.vo.NewsDetailsVo;

import java.util.List;
import java.util.Map;

/**
 * 资讯信息
 * 
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2020-09-21 14:10:44
 */
public interface NewsService {
	
	NewsDO get(Integer id);
	
	List<NewsDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(NewsDO news);
	
	int update(NewsDO news);
	
	int remove(Integer id);
	
	int batchRemove(Integer[] ids);

	ApiResult details(Integer id, Integer userId);

	NewsDetailsVo getNewsDetails(Map<String, Object> params);

	List<NewsBaseVo> collectNewsList(Map<String, Object> params);

	int collectNewsCount(Map<String, Object> params);

	ApiResult listByColumn(Integer userId, Integer columnId, Integer offset, Integer limit);

	int getNewsCountByDate(Map<String, Object> params);

	/**
	 * 用于web应用端获取资讯列表
	 * @param map
	 * @return
	 */
	List<NewsDO> newslist(Map<String, Object> map);

	/**
	 * 用于web应用端获取资讯列表数量
	 * @param map
	 * @return
	 */
	int countNewslist(Map<String, Object> map);

	/**
	 *  web应用端创建资讯
	 * @param news
	 * @return
	 */
	ApiResult webAppNewsSave(NewsDO news);

	/**
	 *  web应用端编辑资讯
	 * @param news
	 * @return
	 */
	ApiResult webAppNewsUpdate(NewsDO news);
}

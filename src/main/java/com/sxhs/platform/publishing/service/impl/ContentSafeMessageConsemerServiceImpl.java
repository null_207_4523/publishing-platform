package com.sxhs.platform.publishing.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.sxhs.platform.publishing.component.ContentSafeUtils;
import com.sxhs.platform.publishing.domain.NewsDO;
import com.sxhs.platform.publishing.service.ContentSafeMessageConsemerService;
import com.sxhs.platform.publishing.service.NewsReviewReasonService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ContentSafeMessageConsemerServiceImpl implements ContentSafeMessageConsemerService {

    @Autowired
    private ContentSafeUtils contentSafeUtils;
    @Autowired
    private NewsReviewReasonService newsReviewReasonService;

    @Override
    public void testText(NewsDO newsDo) {
        log.info("阿里词库机器审核前资讯 news:{}" , JSONObject.toJSONString(newsDo));
        String result = contentSafeUtils.testText(newsDo.getContent());
        log.info("阿里词库机器审核结果----newsId:{},content:{}, result:{}" ,newsDo.getId(),newsDo.getContent(), result);
        newsReviewReasonService.machineReview(result,newsDo);
    }
}

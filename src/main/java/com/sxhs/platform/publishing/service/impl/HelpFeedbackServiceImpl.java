package com.sxhs.platform.publishing.service.impl;

import com.sxhs.platform.publishing.dao.HelpFeedbackDao;
import com.sxhs.platform.publishing.domain.HelpFeedbackDO;
import com.sxhs.platform.publishing.service.HelpFeedbackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;



@Service
public class HelpFeedbackServiceImpl implements HelpFeedbackService {
	@Autowired
	private HelpFeedbackDao helpFeedbackDao;
	
	@Override
	public HelpFeedbackDO get(Integer id){
		return helpFeedbackDao.get(id);
	}
	
	@Override
	public List<HelpFeedbackDO> list(Map<String, Object> map){
		return helpFeedbackDao.list(map);
	}
	
	@Override
	public int count(Map<String, Object> map){
		return helpFeedbackDao.count(map);
	}
	
	@Override
	public int save(HelpFeedbackDO helpFeedback){
		helpFeedback.setCreatTime(new Date());
		helpFeedback.setHasDelete(0);
		return helpFeedbackDao.save(helpFeedback);
	}
	
	@Override
	public int update(HelpFeedbackDO helpFeedback){
		return helpFeedbackDao.update(helpFeedback);
	}
	
	@Override
	public int remove(Integer id){
		return helpFeedbackDao.remove(id);
	}
	
	@Override
	public int batchRemove(Integer[] ids){
		return helpFeedbackDao.batchRemove(ids);
	}
	
}

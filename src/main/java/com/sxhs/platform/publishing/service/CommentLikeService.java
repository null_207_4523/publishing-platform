package com.sxhs.platform.publishing.service;

import com.sxhs.platform.common.utils.ApiResult;
import com.sxhs.platform.publishing.domain.CommentLikeDO;

import java.util.List;
import java.util.Map;

/**
 * 评论点赞 
 * 
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2020-09-29 17:00:26
 */
public interface CommentLikeService {
	
	CommentLikeDO get(Integer id);
	
	List<CommentLikeDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(CommentLikeDO commentLike);
	
	int update(CommentLikeDO commentLike);
	
	int remove(Integer id);
	
	int batchRemove(Integer[] ids);

	ApiResult commentLike(Integer commentId, Integer userId, String userName, String userAvatar);

	ApiResult commentLikerList(Integer commentId);
}

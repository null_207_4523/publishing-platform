package com.sxhs.platform.publishing.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.sxhs.platform.common.utils.CollectionUtils;
import com.sxhs.platform.publishing.domain.NewsDO;
import com.sxhs.platform.publishing.message.dto.BaseMessageDTO;
import com.sxhs.platform.publishing.message.dto.MessageContentDTO;
import com.sxhs.platform.publishing.message.dto.MessageDTO;
import com.sxhs.platform.publishing.message.enums.AppMeesageEnum;
import com.sxhs.platform.publishing.message.enums.CategoryTagEnum;
import com.sxhs.platform.publishing.message.enums.EventEnum;
import com.sxhs.platform.publishing.message.util.Tools;
import com.sxhs.platform.publishing.service.MessageSendService;
import com.sxhs.platform.publishing.service.UmMessageService;
import com.sxhs.platform.system.config.rocketMq.aliyun.MqConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@Slf4j
public class UmMessageServiceImpl implements UmMessageService {

    @Autowired
    private MqConfig mqConfig;
    @Autowired
    private MessageSendService messageSendService;

    @Override
    public Boolean sendNewsMsg(NewsDO news, List<Integer> subscriptionUserIds, CategoryTagEnum categoryTag) {
        List<String> subscriptionUserIdList = Arrays.asList(Tools.listToString(subscriptionUserIds, ','));
        List<List<String>> receiverUserIdsList = new ArrayList<>(16);
        if (subscriptionUserIdList.size() > 500) {
            receiverUserIdsList = CollectionUtils.divide(subscriptionUserIdList, 500);
        } else {
            receiverUserIdsList.add(subscriptionUserIdList);
        }
        String url = "";
        boolean flag = true;
        try {
            List<MessageDTO> list = Lists.newArrayList();
            for (List<String> receiverIds : receiverUserIdsList) {
                // 用户相关信息
                List<MessageContentDTO> messageList = new ArrayList<>();
                MessageContentDTO a = new MessageContentDTO();
                a.setReceiverIds(Tools.listToString(receiverIds,','));
                a.setTitle("您关注的机构【" + news.getOrgName() + "】发布了一条资讯");
                a.setContent(news.getSynopsis());
                a.setUrl(url);
                a.setMsgBody(JSONObject.toJSONString(news));
                messageList.add(a);
                // 应用信息
                BaseMessageDTO base = new BaseMessageDTO();
                base.setAppType(AppMeesageEnum.ZI_XUN.getType());
                base.setAppName(AppMeesageEnum.ZI_XUN.getName());
                base.setCategory(categoryTag.getTags());
                base.setCategoryName(categoryTag.getName());
                base.setEventType(EventEnum.PUBLISH.getType());
                base.setEventName(EventEnum.PUBLISH.getName());
                MessageDTO bodyMessage = messageSendService.getBodyMessage(messageList, base);
                list.add(bodyMessage);
            }
            if (CollectionUtils.isNotEmpty(list)) {
                list.stream().forEach(item -> {
                    messageSendService.sendMessage(mqConfig.getTopic(), categoryTag.getTags(), UUID.randomUUID() + "", item);
                });
            }
        } catch (Exception e) {
            flag = false;
            e.printStackTrace();
        }
        return flag;
    }

}

package com.sxhs.platform.publishing.service;

import com.sxhs.platform.publishing.domain.NewsDO;

public interface ContentSafeMessageConsemerService {

    /**
     *  发送消息进行阿里词库过滤校验
     * @param newsDO
     */
    void testText(NewsDO newsDO);
}

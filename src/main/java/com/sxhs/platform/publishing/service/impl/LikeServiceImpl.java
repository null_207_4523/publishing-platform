package com.sxhs.platform.publishing.service.impl;

import com.sxhs.platform.common.config.Constant;
import com.sxhs.platform.common.utils.ApiResult;
import com.sxhs.platform.common.utils.FieldValueUtils;
import com.sxhs.platform.common.utils.VerificateUtil;
import com.sxhs.platform.publishing.dao.LikeDao;
import com.sxhs.platform.publishing.domain.LikeDO;
import com.sxhs.platform.publishing.domain.NewsDO;
import com.sxhs.platform.publishing.domain.OrgDailyStatisticsDO;
import com.sxhs.platform.publishing.service.LikeService;
import com.sxhs.platform.publishing.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.*;


@Service
@Transactional
public class LikeServiceImpl implements LikeService {
    @Autowired
    private LikeDao likeDao;
    @Autowired
    private NewsService newsService;

    @Override
    public LikeDO get(Integer id) {
        return likeDao.get(id);
    }

    @Override
    public List<LikeDO> list(Map<String, Object> map) {
        return likeDao.list(map);
    }

    @Override
    public int count(Map<String, Object> map) {
        return likeDao.count(map);
    }

    @Override
    public int save(LikeDO like) {
        return likeDao.save(like);
    }

    @Override
    public int update(LikeDO like) {
        return likeDao.update(like);
    }

    @Override
    public int remove(Integer id) {
        return likeDao.remove(id);
    }

    @Override
    public int batchRemove(Integer[] ids) {
        return likeDao.batchRemove(ids);
    }

    @Override
    public OrgDailyStatisticsDO getlikeCountByDate(Map<String, Object> params) {
        return likeDao.getlikeCountByDate(params);
    }

    @Override
    public ApiResult likeNews(Integer id, Integer userId) {
        // 获取资讯详情
        NewsDO newsDO = newsService.get(id);
        if (Objects.isNull(newsDO) || Objects.equals(newsDO.getHasDelete(), Constant.HAS_DELETE_YES))
            return ApiResult.fail("无此资讯数据，或已被删除");
        // 保证每条资讯只能在未点赞状态下被点赞
        Map<String, Object> params = new HashMap<>();
        params.put("newsId", id);
        params.put("createdById", userId);
        params.put("hasDelete", Constant.HAS_DELETE_NO);
        List<LikeDO> likeDOs = likeDao.list(params);
        // 未过对资讯点赞，创建点赞
        Integer hasFans = VerificateUtil.hasFans(userId, newsDO.getOrgId());
        // 有过对资讯点赞，取消点赞
        if (!CollectionUtils.isEmpty(likeDOs)) {
            LikeDO likeDO = likeDOs.get(0);
            newsDO = (NewsDO) FieldValueUtils.updateCount("likeCount", hasFans, newsDO, Constant.HAS_ADD_NO, 1);
            if (likeDao.remove(likeDO.getId()) > 0 && newsService.update(newsDO) > 0)
                return ApiResult.success(Constant.HAS_LIKE_NO, "取消赞");
        }
        // 创建点赞实体
        LikeDO likeDO = new LikeDO();
        likeDO.setNewsId(id);
        likeDO.setCreatedById(userId);
        likeDO.setCreatedTime(new Date());
        likeDO.setHasDelete(Constant.HAS_DELETE_NO);
        likeDO.setHasFans(hasFans);
        newsDO = (NewsDO) FieldValueUtils.updateCount("likeCount", hasFans, newsDO, Constant.HAS_ADD_YES, 1);
        // 点赞实体插入数据库
        if (likeDao.save(likeDO) > 0 && newsService.update(newsDO) > 0)
            return ApiResult.success(Constant.HAS_LIKE_YES, "点赞");
        return ApiResult.fail();
    }


}

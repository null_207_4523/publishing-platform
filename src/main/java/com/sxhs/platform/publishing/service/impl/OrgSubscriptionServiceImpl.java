package com.sxhs.platform.publishing.service.impl;

import com.sxhs.platform.common.config.Constant;
import com.sxhs.platform.common.utils.ApiResult;
import com.sxhs.platform.publishing.dao.OrgSubscriptionDao;
import com.sxhs.platform.publishing.domain.OrgSubscriptionDO;
import com.sxhs.platform.publishing.service.OrgSubscriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.*;


@Service
@Transactional
public class OrgSubscriptionServiceImpl implements OrgSubscriptionService {
	@Autowired
	private OrgSubscriptionDao orgSubscriptionDao;
	
	@Override
	public OrgSubscriptionDO get(Integer id){
		return orgSubscriptionDao.get(id);
	}
	
	@Override
	public List<OrgSubscriptionDO> list(Map<String, Object> map){
		return orgSubscriptionDao.list(map);
	}
	
	@Override
	public int count(Map<String, Object> map){
		return orgSubscriptionDao.count(map);
	}
	
	@Override
	public int save(OrgSubscriptionDO orgSubscription){
		return orgSubscriptionDao.save(orgSubscription);
	}
	
	@Override
	public int update(OrgSubscriptionDO orgSubscription){
		return orgSubscriptionDao.update(orgSubscription);
	}
	
	@Override
	public int remove(Integer id){
		return orgSubscriptionDao.remove(id);
	}
	
	@Override
	public int batchRemove(Integer[] ids){
		return orgSubscriptionDao.batchRemove(ids);
	}

	@Override
	public ApiResult add(Integer userId, Integer orgId) {
		Map<String, Object> map = new HashMap<>();
		map.put("orgId",orgId);
		map.put("createdUserId",userId);
		map.put("hasDelete", Constant.HAS_DELETE_NO);
		List<OrgSubscriptionDO> list = orgSubscriptionDao.list(map);
		if(!CollectionUtils.isEmpty(list))
			return ApiResult.fail("机构不能重复订阅");
		OrgSubscriptionDO orgSubscriptionDO = new OrgSubscriptionDO();
		orgSubscriptionDO.setOrgId(orgId);
		orgSubscriptionDO.setCreatedUserId(userId);
		orgSubscriptionDO.setCreatedTime(new Date());
		orgSubscriptionDO.setHasDelete(Constant.HAS_DELETE_NO);
		if(orgSubscriptionDao.save(orgSubscriptionDO)>0)
			return ApiResult.success();
		return ApiResult.fail();
	}

	@Override
	public ApiResult cancel(Integer userId, Integer orgId) {
		Map<String, Object> map = new HashMap<>();
		map.put("orgId",orgId);
		map.put("createdUserId",userId);
		map.put("hasDelete", Constant.HAS_DELETE_NO);
		List<OrgSubscriptionDO> list = orgSubscriptionDao.list(map);
		if(CollectionUtils.isEmpty(list))
			return ApiResult.fail("未订阅，没法取消");
		OrgSubscriptionDO orgSubscription =list.get(0);
		orgSubscription.setHasDelete(Constant.HAS_DELETE_YES);
		if(orgSubscriptionDao.remove(orgSubscription.getId())>0)
			return ApiResult.success();
		return ApiResult.fail();
	}

	@Override
	public int getOrgSubscriptionCountByDate(Map<String, Object> map) {
		return Objects.isNull(orgSubscriptionDao.getOrgSubscriptionCountByDate(map))?0:orgSubscriptionDao.getOrgSubscriptionCountByDate(map);
	}

}

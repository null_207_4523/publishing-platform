package com.sxhs.platform.publishing.service.impl;

import com.sxhs.platform.publishing.dao.OrgDailyStatisticsDao;
import com.sxhs.platform.publishing.domain.OrgDailyStatisticsDO;
import com.sxhs.platform.publishing.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.Objects;


@Service
@Transactional
public class OrgDailyStatisticsServiceImpl implements OrgDailyStatisticsService {
    @Autowired
    private OrgDailyStatisticsDao orgDailyStatisticsDao;
    @Autowired
    private NewsService newsService;
    @Autowired
    private OrgSubscriptionService orgSubscriptionService;
    @Autowired
    private CollectService collectService;
    @Autowired
    private LikeService likeService;
    @Autowired
    private ReadService readService;
    @Autowired
    private ShareService shareService;
    @Autowired
    private CommentService commentService;

    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public OrgDailyStatisticsDO get(Integer id) {
        return orgDailyStatisticsDao.get(id);
    }

    @Override
    public List<OrgDailyStatisticsDO> list(Map<String, Object> map) {
        return orgDailyStatisticsDao.list(map);
    }

    @Override
    public int count(Map<String, Object> map) {
        return orgDailyStatisticsDao.count(map);
    }

    @Override
    public int save(OrgDailyStatisticsDO orgDailyStatistics) {
        return orgDailyStatisticsDao.save(orgDailyStatistics);
    }

    @Override
    public int update(OrgDailyStatisticsDO orgDailyStatistics) {
        return orgDailyStatisticsDao.update(orgDailyStatistics);
    }

    @Override
    public int remove(Integer id) {
        return orgDailyStatisticsDao.remove(id);
    }

    @Override
    public int batchRemove(Integer[] ids) {
        return orgDailyStatisticsDao.batchRemove(ids);
    }

    @Override
    public void updateDailyStatistics(Map<String, Object> params) throws ParseException {
        // 查询机构日常统计表，判断当前日期统计数据是否存在，存在return
        if (orgDailyStatisticsDao.getCountByDate(params) > 0) {
            return;
        }
        OrgDailyStatisticsDO statisticsDO = new OrgDailyStatisticsDO();
        statisticsDO.setOrgId(Objects.isNull(params.get("orgId"))?0:Integer.valueOf(params.get("orgId").toString()));
        // 订阅信息
        int subscriptionCount = orgSubscriptionService.getOrgSubscriptionCountByDate(params);
        // 机构订阅量信息
        statisticsDO.setSubscriptionCount(subscriptionCount);
        // 资讯发布量
        int newsCount = newsService.getNewsCountByDate(params);
        statisticsDO.setNewsCount(newsCount);
        // 资讯收藏量
        OrgDailyStatisticsDO collectCountVo = collectService.getCollectCountByDate(params);
        statisticsDO.setCollectCount(Objects.nonNull(collectCountVo)?collectCountVo.getCollectCount():0);
        statisticsDO.setFansCollectCount(Objects.nonNull(collectCountVo)?collectCountVo.getFansCollectCount():0);
        // 资讯评论量
        OrgDailyStatisticsDO commentCountVo = commentService.getCommentCountByDate(params);
        statisticsDO.setCommentCount(Objects.nonNull(commentCountVo)?commentCountVo.getCommentCount():0);
        statisticsDO.setFansCommentCount(Objects.nonNull(commentCountVo)?commentCountVo.getFansCommentCount():0);
        // 资讯点赞量
        OrgDailyStatisticsDO likeCountVo = likeService.getlikeCountByDate(params);
        statisticsDO.setLikeCount(Objects.nonNull(likeCountVo)?likeCountVo.getLikeCount():0);
        statisticsDO.setFansLikeCount(Objects.nonNull(likeCountVo)?likeCountVo.getFansLikeCount():0);
        // 资讯阅读量
        OrgDailyStatisticsDO readCountVo = readService.getReadCountByDate(params);
        statisticsDO.setReadCount(Objects.nonNull(readCountVo)?readCountVo.getReadCount():0);
        statisticsDO.setFansReadCount(Objects.nonNull(readCountVo)?readCountVo.getFansReadCount():0);
        // 资讯分享量
        OrgDailyStatisticsDO shareCountVo = shareService.getShareCountByDate(params);
        statisticsDO.setShareCount(Objects.nonNull(shareCountVo)?shareCountVo.getShareCount():0);
        statisticsDO.setFansShareCount(Objects.nonNull(shareCountVo)?shareCountVo.getFansShareCount():0);
        statisticsDO.setCreatedTime(simpleDateFormat.parse(params.get("createdTime").toString()));
        orgDailyStatisticsDao.save(statisticsDO);
    }

    @Override
    public List<OrgDailyStatisticsDO> listByLimit(Map<String, Object> map) {
        return orgDailyStatisticsDao.listByLimit(map);
    }

    @Override
    public int countlistByLimit(Map<String, Object> map) {
        return orgDailyStatisticsDao.countlistByLimit(map);
    }


}

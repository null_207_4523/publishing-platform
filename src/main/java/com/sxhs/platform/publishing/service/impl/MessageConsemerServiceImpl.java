package com.sxhs.platform.publishing.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sxhs.platform.common.config.Constant;
import com.sxhs.platform.common.utils.StringUtils;
import com.sxhs.platform.publishing.dao.PushLogDao;
import com.sxhs.platform.publishing.domain.PushLogDO;
import com.sxhs.platform.publishing.message.dto.BaseMessageDTO;
import com.sxhs.platform.publishing.message.dto.MessageContentDTO;
import com.sxhs.platform.publishing.message.dto.MessageDTO;
import com.sxhs.platform.publishing.message.util.Tools;
import com.sxhs.platform.publishing.service.MessageConsemerService;
import com.sxhs.platform.publishing.uPush.UmPushUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.threads.ThreadPoolExecutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Slf4j
@Service
public class MessageConsemerServiceImpl implements MessageConsemerService {

    @Autowired
    private PushLogDao pushLogDao;

    @Autowired
    private UmPushUtils umPushUtils;

    @Override
    public void saveMessageAndUPush(MessageDTO messageBody) {

        List<MessageContentDTO> contentDTOList = messageBody.getMessageContentDTOList();

        BaseMessageDTO baseMsg = messageBody.getBaseMessageDTO();

        contentDTOList.forEach(contentMsg -> {

            String receiverIds = contentMsg.getReceiverIds();

            List<String> receiverId_list = Arrays.asList(receiverIds.split(","));
            if (receiverId_list == null || receiverId_list.size() < 1) {
                log.error("消息消费未获取到用户组信息！ ");
                return;
            }

            PushLogDO pushRecord = null;
            List<PushLogDO> userMessageList = new ArrayList<>();
            Date currentDate = new Date();
            for (String e : receiverId_list) {
                pushRecord = new PushLogDO();
                pushRecord.setReceiverId(Integer.parseInt(e));
                pushRecord.setTitle(contentMsg.getTitle());
                pushRecord.setContent(contentMsg.getContent());
                pushRecord.setMsgBody(contentMsg.getMsgBody());
                pushRecord.setUrl(contentMsg.getUrl());
                pushRecord.setCategory(baseMsg.getCategory());
                pushRecord.setCategoryName(baseMsg.getCategoryName());
                pushRecord.setEventType(baseMsg.getEventType());
                pushRecord.setEventName(baseMsg.getEventName());
                pushRecord.setAppType(baseMsg.getAppType());
                pushRecord.setAppName(baseMsg.getAppName());
                pushRecord.setBeRead(Constant.BE_READ_NO);
                pushRecord.setBeDelete(Constant.BE_DELETE_NO);
                pushRecord.setReceiverTime(currentDate);
                userMessageList.add(pushRecord);
            }

            // 日志记录
            pushLogDao.insertBatch(userMessageList);
            // 友盟推送
            this.uPushToUser(receiverId_list, contentMsg, baseMsg);
            log.info("消费成功！累计执行条数：{}", receiverId_list.size());

        });
    }

    void uPushToUser(List<String> list, MessageContentDTO messageContentDTO, BaseMessageDTO baseMessageDTO) {

        // 开发者填写自己的alias, 要求不超过500个alias, 多个alias以英文逗号间隔
        int pageSize = 500;
        int recordCount = list.size();
        int totalPage = 0;
        if (recordCount % pageSize > 0) {
            totalPage = recordCount / pageSize + 1;
        } else {
            totalPage = recordCount / pageSize;
        }

        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(5,  totalPage > 5 ? totalPage : 5,
                0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>());
        List<CompletableFuture<Void>> completableFutureList = new ArrayList<>();

        for (int i =1; i <= totalPage; i++) {
            int index = i;
            //runAsync方法不支持返回值
            completableFutureList.add(CompletableFuture.runAsync(() ->{
                List<String> pageUser= list.stream().skip((index - 1) * pageSize).limit(pageSize).collect(Collectors.toList());

                String extraInfo = messageContentDTO.getMsgBody();
                JSONObject extraObject = null;
                if (StringUtils.isNotBlank(extraInfo)) {
                    extraObject = JSONObject.parseObject(extraInfo, JSONObject.class);
                } else {
                    extraObject = new JSONObject();
                }

                extraObject.put("app_url", messageContentDTO.getUrl());
                extraObject.put("app_type", baseMessageDTO.getAppType());
                extraObject.put("event_type", baseMessageDTO.getEventType());
                this.pushMobile(
                        Tools.listToString(pageUser, ','),
                        messageContentDTO.getTitle(),
                        messageContentDTO.getContent(),
                        JSON.toJSONString(extraObject)
                        );
            }, threadPoolExecutor));
        }

    }

    private void pushMobile(String alias, String title, String content,  String extra) {
        // 安卓
        try {
            umPushUtils.sendAndroidCustomizedcast(alias,
                    "您有一条新消息", title, content, extra, "", "");
        } catch (Exception e) {
            log.info("安卓设备友盟消息推送失败：{}", e);
        }
        // IOS
        try {
            umPushUtils.sendIOSCustomizedcast(alias, title, title, content, extra);
        } catch (Exception e) {
            log.info("苹果设备友盟消息推送失败：{}", e);
            e.printStackTrace();
        }
    }


    /**
     *      Demo Test
     */
    public static void main(String[] args) {

        List<String> list = new ArrayList<>();
        for (int s = 1; s < 50001; s++){
            list.add(s + "");
        }

        int pageSize = 500;
        int recordCount = list.size();
        int totalPage = 0;
        if (recordCount % pageSize > 0) {
            totalPage = recordCount / pageSize + 1;
        } else {
            totalPage = recordCount / pageSize;
        }

        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(5,  totalPage > 5 ? totalPage : 5,
                0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>());
        List<CompletableFuture<Void>> completableFutureList = new ArrayList<>();
        for (int i =1; i <= totalPage; i++) {
            int page = i;
            completableFutureList.add(CompletableFuture.runAsync(() ->{
                List<String> pageList= list.stream().skip((page - 1) * pageSize).limit(pageSize).collect(Collectors.toList());
                System.out.println("分页：" + page + "; list:" + pageList.toString());
            }, threadPoolExecutor));
        }
    }
}

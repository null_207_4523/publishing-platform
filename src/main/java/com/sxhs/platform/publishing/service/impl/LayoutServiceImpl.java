package com.sxhs.platform.publishing.service.impl;

import com.google.common.collect.Maps;
import com.sxhs.platform.publishing.dao.LayoutDao;
import com.sxhs.platform.publishing.domain.LayoutDO;
import com.sxhs.platform.publishing.service.LayoutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;



@Service
public class LayoutServiceImpl implements LayoutService {

	@Autowired
	private LayoutDao layoutDao;

	@Override
	public LayoutDO selectOneByUser(Integer userId) {

		return selectLayoutByUser(userId);
	}

	@Override
	public int saveLayout(Integer userId, String layId) {

		LayoutDO layoutDO = selectLayoutByUser(userId);
		// 新增
		if (null == layoutDO) {
			LayoutDO layout = new LayoutDO();
			layout.setUserId(userId);
			layout.setLayId(layId);
			return layoutDao.save(layout);
		}
		// 更新
		layoutDO.setLayId(layId);
		return layoutDao.update(layoutDO);
	}

	private LayoutDO selectLayoutByUser(Integer userId) {
		Map<String, Object> params = Maps.newHashMap();
		params.put("userId", userId);
		List<LayoutDO> list = layoutDao.list(params);
		if (null != list && list.size() > 0) {
			return list.get(0);
		}
		return null;
	}

	@Override
	public LayoutDO get(Integer id){
		return layoutDao.get(id);
	}
	
	@Override
	public List<LayoutDO> list(Map<String, Object> map){
		return layoutDao.list(map);
	}
	
	@Override
	public int count(Map<String, Object> map){
		return layoutDao.count(map);
	}
	
	@Override
	public int save(LayoutDO layout){
		return layoutDao.save(layout);
	}
	
	@Override
	public int update(LayoutDO layout){
		return layoutDao.update(layout);
	}
	
	@Override
	public int remove(Integer id){
		return layoutDao.remove(id);
	}
	
	@Override
	public int batchRemove(Integer[] ids){
		return layoutDao.batchRemove(ids);
	}
	
}

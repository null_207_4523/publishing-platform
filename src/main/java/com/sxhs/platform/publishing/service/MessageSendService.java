package com.sxhs.platform.publishing.service;

import com.aliyun.openservices.ons.api.SendResult;
import com.sxhs.platform.publishing.message.dto.BaseMessageDTO;
import com.sxhs.platform.publishing.message.dto.MessageDTO;
import com.sxhs.platform.publishing.message.dto.MessageContentDTO;

import java.util.List;

public interface MessageSendService {

    /**
     *      生成发送消息内容体

     */
    MessageDTO getBodyMessage(List<MessageContentDTO> contentMessage, BaseMessageDTO baseMessage);


    /**
     *      消息发送 - 同步
     * @param topic
     * @param tags
     * @param key
     * @param bodyMsg
     * @return
     */
    SendResult sendMessage(String topic, String tags, String key, MessageDTO bodyMsg);

    /**
     *      消息发送
     * @param topic
     * @param tags
     * @param key
     * @param bodyMsg
     * @return
     */
    SendResult sendMessageServer(String topic, String tags, String key, MessageDTO bodyMsg);

}

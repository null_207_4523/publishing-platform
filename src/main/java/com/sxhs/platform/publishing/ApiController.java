package com.sxhs.platform.publishing;

import com.sxhs.platform.common.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/test")
public class ApiController {

    @Autowired
    private HttpServletRequest request;

    @PostMapping("/one")
    public R testOne(String oneStr){
        System.out.println(oneStr);
        String client = request.getHeader("client");
        System.out.println(client);
        String token = request.getHeader("token");
        System.out.println(token);
        return R.ok();
    }

}

package com.sxhs.platform.publishing.Interceptor;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @program: wisdomsaas-main
 * @description: 对web请求做一些处理
 * @author: ian
 * @create: 2019-05-27 14:36
 **/
@Component
public class WebInterceptor implements HandlerInterceptor {

    @Value("${editor.domain}")
    private String editor_domain;

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {
        if(modelAndView!=null) {
            String serverName = httpServletRequest.getServerName();
            if(80!=httpServletRequest.getServerPort()&&443!=httpServletRequest.getServerPort()) {
                serverName = serverName+":"+httpServletRequest.getServerPort();
            }

            modelAndView.addObject("serverName",serverName);
            modelAndView.addObject("editor_domain",editor_domain);
        }
    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }
}

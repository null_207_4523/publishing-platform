package com.sxhs.platform.publishing.webapi.column.controller;

import com.sxhs.platform.common.utils.ApiResult;
import com.sxhs.platform.publishing.service.ColumnService;
import com.sxhs.platform.publishing.vo.ColumnVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 栏目表
 * 
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2020-09-21 14:10:43
 */

@RestController
@RequestMapping("/webapi/column")
public class WebApiColumnController {
	@Autowired
	private ColumnService columnService;

	/**
	 * 获取全部栏目数据接口
	 * @return
	 */
	@GetMapping("/list")
	public ApiResult columnList() {
		List<ColumnVo> list = columnService.appOrgColumnlist(0);
		return ApiResult.success(list);
	}
	
}

package com.sxhs.platform.publishing.webapi;

import com.alibaba.fastjson.JSONObject;
import com.sxhs.platform.common.utils.ApiResult;
import com.sxhs.platform.system.config.Pass;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletResponse;
import java.util.concurrent.TimeUnit;

/**
 * @Author LiuLiang
 * @Date 4:45 PM 2020/12/1
 * @Description //单点用户信息获取
 **/
@RestController
@Slf4j
@RequestMapping("/webapi/user")
public class SSOLoginController {

    private String GRANT_TYPE = "authorization_code";

    @Value("${passport.id:}")
    private String CLIENT_ID = "";

    @Value("${passport.secret:}")
    private String CLIENT_SECRET = "";

    @Value("${passport.url:}")
    private String PASSPORT_URL = "";

    private RestTemplate restTemplate = new RestTemplate();


    public static final String TOKEN_KEY = "TOKEN:";

    @Autowired
    RedisTemplate redisTemplate;

    /**
     * @Date 4:46 PM 2020/12/1
     * @Description //单点系统登录接口
     * @Param [code]
     * @return com.sxhs.platform.common.utils.ApiResult
     **/
    
    @Pass
    @GetMapping("/login")
    public ApiResult login(String code,String redirect_uri, HttpServletResponse response){

        ApiResult success = ApiResult.success();
        String OauthUrl = PASSPORT_URL+"/passport/oauth/token";

        try{
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            MultiValueMap postData  = new LinkedMultiValueMap();
            postData.add("grant_type", GRANT_TYPE);
            postData.add("client_id", CLIENT_ID);
            postData.add("client_secret", CLIENT_SECRET);
            postData.add("code", code);
            postData.add("redirect_uri", redirect_uri);
            final HttpEntity< MultiValueMap< String,String>> entity = new HttpEntity< MultiValueMap< String,String>>(postData,headers);
            ResponseEntity<JSONObject> responseEntity = restTemplate.postForEntity(OauthUrl, entity, JSONObject.class);

            if(responseEntity.getStatusCodeValue()== HttpStatus.OK.value()){
                JSONObject body = responseEntity.getBody();
                success.setData(body);
                redisTemplate.opsForValue().set(TOKEN_KEY+body.get("access_token").toString(),true,120, TimeUnit.SECONDS);
                log.info("返回token信息:"+body);
            }else{
                log.error("用户信登录网络异常:{"+responseEntity.getStatusCode().value()+"}"+responseEntity.getStatusCode().getReasonPhrase());
                throw new RuntimeException();
            }
        }catch (Exception e){
            e.printStackTrace();
            log.error("用户信息验证失败！请求地址：code:{},grant_type:{},client_id:{},client_secret:{},url:{}",code,GRANT_TYPE,CLIENT_ID,CLIENT_SECRET,OauthUrl);
            response.setStatus(414);
            return ApiResult.fail("1002",e.getMessage());
        }

        return success;
    }

    @GetMapping("/userDetal")
    public ApiResult userDetal(){
        return ApiResult.success("测试通过");
    }

}

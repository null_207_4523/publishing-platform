package com.sxhs.platform.publishing.webapi.news.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@ApiModel(value = "资讯信息", description = "资讯信息")
public class WebApiNewsVo implements Serializable {

    private static final long serialVersionUID = -5327068068445463034L;
    /**
     * //主键 id
     */
    @ApiModelProperty(value = "主键id")
    private Integer id;
    /**
     * //栏目 id
     */
    @ApiModelProperty(value = "栏目id")
    private String columnId;
    /**
     * //栏目
     */
    @ApiModelProperty(value = "栏目")
    private String columnName;
    /**
     * //标题
     */
    @ApiModelProperty(value = "标题")
    private String title;
    /**
     * //简介
     */
    @ApiModelProperty(value = "简介")
    private String synopsis;
    /**
     * //文章内容
     */
    @ApiModelProperty(value = "文章内容")
    private String content;
    /**
     * //发布机构id
     */
    @ApiModelProperty(value = "发布机构id")
    private Integer orgId;
    /**
     * //发布机构名称
     */
    @ApiModelProperty(value = "发布机构名称")
    private String orgName;
    /**
     * //发布机构logo
     */
    @ApiModelProperty(value = "发布机构logo")
    private String orgLogo;
    /**
     * //类型 0：富文本 1：视频 2：音频
     */
    @ApiModelProperty(value = "类型 0：富文本 1：视频 2：音频")
    private Integer type;
    /**
     * //封面
     */
    @ApiModelProperty(value = "封面")
    private String cover;
    /**
     * //封面类型 0：无封面 1：单图  2：三图
     */
    @ApiModelProperty(value = "封面类型 0：无封面 1：单图  2：三图")
    private Integer coverType;
    /**
     * //创建人
     */
    @ApiModelProperty(value = "创建人")
    private String createdUser;
    /**
     * //创建人id
     */
    @ApiModelProperty(value = "创建人id")
    private String createdUserId;
    /**
     * //创建人头像
     */
    @ApiModelProperty(value = "创建人头像")
    private String createdUserAvatar;
    /**
     * //发布时间
     */
    @ApiModelProperty(value = "发布时间")
    private Date publishTime;
    /**
     * //文章状态 0：草稿 1：撤回  2：审核中  3：审核通过  4：审核未通过  5.人工审核中
     */
    @ApiModelProperty(value = "0：草稿 1：撤回  2：审核中  3：审核通过  4：审核未通过  5.人工审核中")
    private Integer status;
    /**
     * //更新人
     */
    @ApiModelProperty(value = "更新人")
    private String updateUser;
    /**
     * //更新人 id
     */
    @ApiModelProperty(value = "更新人 id")
    private String updateUserId;
    /**
     * //更新时间
     */
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;
    /**
     * //逻辑删除
     */
    @ApiModelProperty(value = "逻辑删除")
    private Integer hasDelete;
    /**
     * //创建人、修改人角色
     */
    @ApiModelProperty(value = "创建人、修改人角色")
    private String roleId;

}

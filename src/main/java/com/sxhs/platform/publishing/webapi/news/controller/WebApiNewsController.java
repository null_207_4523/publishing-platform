package com.sxhs.platform.publishing.webapi.news.controller;

import com.alibaba.fastjson.JSON;
import com.sxhs.platform.common.utils.*;
import com.sxhs.platform.publishing.domain.ColumnDO;
import com.sxhs.platform.publishing.domain.NewsDO;
import com.sxhs.platform.publishing.domain.NewsReviewReasonDO;
import com.sxhs.platform.publishing.message.enums.CategoryTagEnum;
import com.sxhs.platform.publishing.service.ColumnService;
import com.sxhs.platform.publishing.service.NewsReviewReasonService;
import com.sxhs.platform.publishing.service.NewsService;
import com.sxhs.platform.publishing.webapi.news.vo.WebApiNewsVo;
import com.sxhs.platform.system.config.rocketMq.aliyun.MqConfig;
import com.sxhs.platform.system.config.rocketMq.aliyun.ProducerUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.UnsupportedEncodingException;
import java.util.*;

/**
 * 资讯信息
 *
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2020-09-21 14:10:44
 */
@Slf4j
@RestController
@RequestMapping("/webapi/news")
public class WebApiNewsController {
    @Autowired
    private NewsService newsService;
    @Autowired
    private ColumnService columnService;
    @Autowired
    private ProducerUtil producer;
    @Autowired
    private MqConfig config;
    @Autowired
    private NewsReviewReasonService newsReviewReasonService;

    /**
     * 内容管理列表接口
     *
     * @param params
     * @return
     */
    @GetMapping("/list")
    public ApiResult list(@RequestParam Map<String, Object> params) {
        // 前端待审核数据包括两部分（2：审核中  5.人工审核中）
        if (Objects.equals(params.get("status"), "2")) {
            List<Integer> list = new ArrayList<>();
            list.add(2);
            list.add(5);
            params.put("statusList", list);
            params.remove("status");
        }
        //查询列表数据
        Query query = new Query(params);
        List<NewsDO> newsList = newsService.newslist(query);
        int total = newsService.countNewslist(query);
        return ApiResult.success(new PageUtils(newsList, total));
    }

    /**
     * 查看详情接口
     *
     * @param id
     * @return
     */
    @GetMapping("/edit/{id}")
    public ApiResult edit(@PathVariable("id") Integer id) {
        NewsDO news = newsService.get(id);
        if (Objects.nonNull(news) && StringUtils.isNotBlank(news.getColumnId())) {
            ColumnDO columnDO = columnService.get(Integer.valueOf(news.getColumnId()));
            if (Objects.nonNull(columnDO))
                news.setColumnName(columnDO.getColumnName());
        }
        return ApiResult.success(news);
    }

    /**
     * 内容创作接口
     *
     * @param newsVo
     * @return
     */
    @PostMapping("/save")
    public ApiResult save(@RequestBody WebApiNewsVo newsVo) {
        NewsDO news = new NewsDO();
        BeanCopierUtils.copyProperties(newsVo, news);
        if (StringUtils.isBlank(news.getColumnId()) || StringUtils.isBlank(news.getTitle()))
            return ApiResult.fail("标题或栏目不能为空!");
        ApiResult result = newsService.webAppNewsSave(news);
        checkContentSafe(result);
        return result;
    }

    /**
     * 内容编辑交接口
     *
     * @param newsVo
     * @return
     */
    @RequestMapping("/update")
    public ApiResult update(@RequestBody WebApiNewsVo newsVo) {
        NewsDO news = new NewsDO();
        BeanCopierUtils.copyProperties(newsVo, news);
        if (Objects.isNull(news.getId()))
            return ApiResult.fail("主键id不能为空!");
        ApiResult result = newsService.webAppNewsUpdate(news);
        checkContentSafe(result);
        return result;
    }

    /**
     * 内容删除接口
     *
     * @param id
     * @return
     */
    @PostMapping("/remove")
    public ApiResult remove(Integer id) {
        if (newsService.remove(id) > 0) {
            return ApiResult.success();
        }
        return ApiResult.fail();
    }

    /**
     * 获取未通过原因
     *
     * @param id
     * @return
     */
    @GetMapping("/review/reason")
    public ApiResult reviewReason(Integer id) {
        NewsReviewReasonDO reviewReasonDO = newsReviewReasonService.getByNewsId(id);
        if (Objects.nonNull(reviewReasonDO)) {
            return ApiResult.success(reviewReasonDO.getReason());
        }
        return ApiResult.fail();
    }

    /**
     * 判断是否需要发起内容审核
     *
     * @param result
     */
    private void checkContentSafe(ApiResult result) {
        if (Objects.equals(result.getCode(), "200") && Objects.nonNull(result.getData())) {
            NewsDO newsDO = (NewsDO) result.getData();
            if (Objects.equals(newsDO.getStatus(), 2)) {
                contentSafe(newsDO);
            }
        }
    }

    /**
     * /阿里内容审核流程
     *
     * @param news
     */
    private void contentSafe(NewsDO news) {
        try {
            Map<String, Object> map = new HashMap<>();
            map.put("id", news.getId());
            map.put("roldId", news.getRoleId());
            String jsonString = JSON.toJSONString(map);
            producer.sendMsg(config.getTopic(), CategoryTagEnum.CONTENTSAFE.getTags(), UUID.randomUUID() + "", jsonString);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            log.error("消息发送失败：", e);
        }
    }

}

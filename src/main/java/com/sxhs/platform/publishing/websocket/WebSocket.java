package com.sxhs.platform.publishing.websocket;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * @program: wisdomsaas-main
 * @description:
 * @author: ian
 * @create: 2019-05-28 16:47
 **/
@Component
@ServerEndpoint("/api/webSocket/{wsId}")
public class WebSocket {
    private Session session;
    private String wsId;
    private static Logger log = LoggerFactory.getLogger(WebSocket.class);

    private static CopyOnWriteArraySet<WebSocket> webSocketSet = new CopyOnWriteArraySet<>();
    //连接超时
    public static final long MAX_TIME_OUT = 2 * 60 * 1000;

    @OnOpen
    public void onOpen(Session session, @PathParam(value = "wsId") String wsId) {
        session.setMaxIdleTimeout(MAX_TIME_OUT);
        this.session = session;
        this.wsId = wsId;
        webSocketSet.add(this);
        log.info("【websocket消息】有新的连接, 总数:{}", webSocketSet.size());
    }

    @OnClose
    public void onClose() {
        webSocketSet.remove(this);
        log.info("【websocket消息】连接断开, 总数:{}", webSocketSet.size());
    }

    @OnMessage
    public void onMessage(String message) {
        log.info("【websocket消息】收到客户端发来的消息:{}", message);
    }

    public void sendMessage(String message) {
        for (WebSocket webSocket : webSocketSet) {
            log.info("【websocket消息】广播消息, message={}", message);
            try {
                webSocket.session.getBasicRemote().sendText(message);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void sendMessage(String message, String to) {
        for (WebSocket webSocket : webSocketSet) {
            if (webSocket.wsId.equals(to)) {
                log.info("【websocket消息】广播消息, message={}", message);
                try {
                    webSocket.session.getBasicRemote().sendText(message);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    public void receiveMsg(String message) {
        JSONObject jsonObject = JSONObject.parseObject(message);
        String wsId = jsonObject.getString("wsId"); //接受者ID
        sendMessage(message, wsId);
    }

}

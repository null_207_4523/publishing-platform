package com.sxhs.platform.publishing.app;


import com.alibaba.fastjson.JSON;
import com.sxhs.platform.common.utils.ApiResult;
import com.sxhs.platform.common.utils.StringUtils;
import com.sxhs.platform.publishing.domain.LayoutDO;
import com.sxhs.platform.publishing.service.LayoutService;
import com.sxhs.platform.system.config.Pass;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.shiro.crypto.hash.Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/app/layout")
public class AppLayoutController {

    @Autowired
    private LayoutService layoutService;

    @Value("${spring.profiles.active:dev}")
    private String active;

    public static Map<String,Object> app =  null;


    @PostConstruct
    public void init() {
        if(StringUtils.equals("dev",active)){
            app = JSON.parseObject("{\n" +
                    "  9001:{name:\"成绩\",code:\"9001\",url:\"http://ksapp.yceduyun.com/yceduapp\",image:\"https://wisdomsaas.oss-cn-beijing.aliyuncs.com/applogo/jjbg.png\",appType:\"0\"},\n" +
                    "  9002:{name:\"消息\",code:\"9002\",url:\"\",image:\"https://wisdomsaas.oss-cn-beijing.aliyuncs.com/applogo/xx.png\",appType:\"1\"},\n" +
                    "  9003:{name:\"课表\",code:\"9003\",url:\"\",image:\"https://wisdomsaas.oss-cn-beijing.aliyuncs.com/applogo/kb.png\",appType:\"1\"},\n" +
                    "  9004:{name:\"云课堂\",code:\"9004\",url:\"\",image:\"https://wisdomsaas.oss-cn-beijing.aliyuncs.com/applogo/zb.png\",appType:\"1\"},\n" +
                    "  9005:{name:\"生涯规划\",code:\"9005\",url:\"http://sygh-h5.yceduyun.com/\",image:\"https://wisdomsaas.oss-cn-beijing.aliyuncs.com/applogo/sygh.png\",appType:\"0\"},\n" +
                    "  9006:{name:\"食品安全\",code:\"9006\",url:\"http://foodsafe.yceduyun.com/v1/h5/info\",image:\"https://wisdomsaas.oss-cn-beijing.aliyuncs.com/applogo/spaq.png\",appType:\"0\"},\n" +
                    "  9007:{name:\"心理测评\",code:\"9007\",url:\"http://devcloud.jsyixin.com/?r=third/huanshuo/Callback/Teacher&path=teachertest\",image:\"https://wisdomsaas.oss-cn-beijing.aliyuncs.com/applogo/xlcp.png\",appType:\"0\"},\n" +
                    "  9008:{name:\"测评\",code:\"9008\",url:\"http://ks.yceduyun.com/zy/sso/authenticate/hs/h5\",image:\"https://wisdomsaas.oss-cn-beijing.aliyuncs.com/applogo/pc.png\",appType:\"0\"},\n" +
                    "  9009:{name:\"资产管理\",code:\"9009\",url:\"http://eam.hseduyun.net/m/index.html#/\",image:\"https://wisdomsaas.oss-cn-beijing.aliyuncs.com/applogo/zcgl.png\",appType:\"0\"},\n" +
                    "  9010:{name:\"数据报表\",code:\"9010\",url:\"http://report.hseduyun.net/statiscal/H5/page/application/application-data.html\",image:\"https://wisdomsaas.oss-cn-beijing.aliyuncs.com/applogo/sjbb.png\",appType:\"0\"},\n" +
                    "  9011:{name:\"授课\",code:\"9011\",url:\"xueletsc://cloudTeach/teachCourse&main/indexPage?tab=teach\",image:\"https://wisdomsaas.oss-cn-beijing.aliyuncs.com/applogo/xly-bsk.png\",appType:\"2\"},\n" +
                    "  9012:{name:\"作业\",code:\"9012\",url:\"xueletsc://cloudTeach/homework&main/indexPage?tab=homework\",image:\"https://wisdomsaas.oss-cn-beijing.aliyuncs.com/applogo/xly-zy.png\",appType:\"2\"},\n" +
                    "  9013:{name:\"OA审批\",code:\"9013\",url:\"http://mobile-gateway.hseduyun.net/bpm-front/new_main.html\",image:\"https://wisdomsaas.oss-cn-beijing.aliyuncs.com/applogo/xtbg-oa.png\",appType:\"0\"},\n" +
                    "  9014:{name:\"平板管控\",code:\"9013\",url:\"http://h5gg.hseduyun.net\",image:\"https://wisdomsaas.oss-cn-beijing.aliyuncs.com/applogo/xtbg-oa.png\",appType:\"0\"}\n" +
                    "}");
        }else {
            app = JSON.parseObject("{\n" +
                    "  9001:{name:\"成绩\",code:\"9001\",url:\"http://ksapp.yceduyun.com/yceduapp\",image:\"https://wisdomsaas.oss-cn-beijing.aliyuncs.com/applogo/jjbg.png\",appType:\"0\"},\n" +
                    "  9002:{name:\"消息\",code:\"9002\",url:\"\",image:\"https://wisdomsaas.oss-cn-beijing.aliyuncs.com/applogo/xx.png\",appType:\"1\"},\n" +
                    "  9003:{name:\"课表\",code:\"9003\",url:\"\",image:\"https://wisdomsaas.oss-cn-beijing.aliyuncs.com/applogo/kb.png\",appType:\"1\"},\n" +
                    "  9004:{name:\"云课堂\",code:\"9004\",url:\"\",image:\"https://wisdomsaas.oss-cn-beijing.aliyuncs.com/applogo/zb.png\",appType:\"1\"},\n" +
                    "  9005:{name:\"生涯规划\",code:\"9005\",url:\"http://sygh-h5.yceduyun.com/\",image:\"https://wisdomsaas.oss-cn-beijing.aliyuncs.com/applogo/sygh.png\",appType:\"0\"},\n" +
                    "  9006:{name:\"食品安全\",code:\"9006\",url:\"http://foodsafe.yceduyun.com/v1/h5/info\",image:\"https://wisdomsaas.oss-cn-beijing.aliyuncs.com/applogo/spaq.png\",appType:\"0\"},\n" +
                    "  9007:{name:\"心理测评\",code:\"9007\",url:\"http://devcloud.jsyixin.com/?r=third/huanshuo/Callback/Teacher&path=teachertest\",image:\"https://wisdomsaas.oss-cn-beijing.aliyuncs.com/applogo/xlcp.png\",appType:\"0\"},\n" +
                    "  9008:{name:\"测评\",code:\"9008\",url:\"http://ks.yceduyun.com/zy/sso/authenticate/hs/h5\",image:\"https://wisdomsaas.oss-cn-beijing.aliyuncs.com/applogo/pc.png\",appType:\"0\"},\n" +
                    "  9009:{name:\"资产管理\",code:\"9009\",url:\"http://eam.yceduyun.com/m/index.html#/\",image:\"https://wisdomsaas.oss-cn-beijing.aliyuncs.com/applogo/zcgl.png\",appType:\"0\"},\n" +
                    "  9010:{name:\"数据报表\",code:\"9010\",url:\"https://tj.yceduyun.com/statiscal/H5/page/application/application-data.html\",image:\"https://wisdomsaas.oss-cn-beijing.aliyuncs.com/applogo/sjbb.png\",appType:\"0\"},\n" +
                    "  9011:{name:\"备授课\",code:\"9011\",url:\"xueletsc://cloudTeach/teachCourse&main/indexPage?tab=teach\",image:\"https://wisdomsaas.oss-cn-beijing.aliyuncs.com/applogo/xly-bsk.png\",appType:\"2\"},\n" +
                    "  9012:{name:\"作业\",code:\"9012\",url:\"xueletsc://cloudTeach/homework&main/indexPage?tab=homework\",image:\"https://wisdomsaas.oss-cn-beijing.aliyuncs.com/applogo/xly-zy.png\",appType:\"2\"},\n" +
                    "  9013:{name:\"OA审批\",code:\"9013\",url:\"http://mobile-gateway.yceduyun.com/bpm-front/new_main.html\",image:\"https://wisdomsaas.oss-cn-beijing.aliyuncs.com/applogo/xtbg-oa.png\",appType:\"0\"}\n" +
                    "}");
        }

    }

    @Pass
    @GetMapping("/list")
    public ApiResult getLayout(Integer userId, Integer type) {

        ArrayList<Object> layout = new ArrayList<>();
        if (type == 0) {
            String layoutStr = "9011,9012,9004,9002";
            if (userId != null) {
                LayoutDO userLayout = layoutService.selectOneByUser(userId);
                if (null != userLayout && StringUtils.isNotBlank(userLayout.getLayId())) {
                    layoutStr = userLayout.getLayId();
                }
            }
            String[] layList = layoutStr.split(",");
            for (String key : layList) {
                layout.add(app.get(key));
            }

        } else {
            for (String key : app.keySet()) {
                layout.add(app.get(key));
            }
        }
        return ApiResult.success(layout);
    }

    @Pass
    @GetMapping("/edit")
    public ApiResult updateLayout(Integer userId, String layoutId){
        int i = layoutService.saveLayout(userId, layoutId);
        if (i == 0) {
            return ApiResult.fail("操作失败");
        }
        return ApiResult.success();
    }


}

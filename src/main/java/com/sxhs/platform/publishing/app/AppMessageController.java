package com.sxhs.platform.publishing.app;

import com.alibaba.fastjson.JSON;
import com.sxhs.platform.common.utils.ApiResult;
import com.sxhs.platform.publishing.message.dto.MessageDTO;
import com.sxhs.platform.publishing.message.enums.CategoryTagEnum;
import com.sxhs.platform.publishing.service.MessageSendService;
import com.sxhs.platform.system.config.rocketMq.aliyun.MqConfig;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping("/api/app/messages")
@Api(value = "消息发送接口", description = "消息发送")
@Slf4j
public class AppMessageController {

    @Autowired
    private MessageSendService messageSendService;

    @Autowired
    private MqConfig mqConfig;

    @PostMapping("/push")
    @ApiOperation("增加发送消息")
    public ApiResult pushMessages(@Valid @RequestBody MessageDTO messageBody){
        messageBody.getBaseMessageDTO().setCategory(CategoryTagEnum.PLATFORM.getTags());
        messageBody.getBaseMessageDTO().setCategoryName(CategoryTagEnum.PLATFORM.getName());
        log.warn("platform push message:{}",JSON.toJSONString(messageBody));
        messageSendService.sendMessage(mqConfig.getTopic(), CategoryTagEnum.PLATFORM.getTags(), UUID.randomUUID() + "", messageBody);
        return ApiResult.success();
    }


}

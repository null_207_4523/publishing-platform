package com.sxhs.platform.publishing.app;

import com.sxhs.platform.common.utils.ApiResult;
import com.sxhs.platform.publishing.service.VersionService;
import com.sxhs.platform.system.config.Pass;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 移动端版本调用controller
 */
@Controller
@RequestMapping("/api/app/version")
@Api(value = "移动端版本控制controller", description = "移动端版本控制接口")
public class AppVersionController {

    @Autowired
    private VersionService versionService;

    /**
     * 根据设备类型获取最新的app版本信息接口
     *
     * @param device
     * @return
     */
    @Pass
    @GetMapping("newest")
    @ResponseBody
    @ApiOperation("根据设备类型获取最新的app版本信息接口")
    public ApiResult collectNews(
            @ApiParam(name = "device", value = "设备：android、ios", required = true) @RequestParam("device") String device) {
        return versionService.getNewestVersion(device);
    }

}

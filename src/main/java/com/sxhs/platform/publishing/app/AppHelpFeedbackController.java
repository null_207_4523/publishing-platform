package com.sxhs.platform.publishing.app;


import com.alibaba.fastjson.JSON;
import com.sxhs.platform.common.domain.DictDO;
import com.sxhs.platform.common.service.DictService;
import com.sxhs.platform.publishing.domain.HelpFeedbackDO;
import com.sxhs.platform.publishing.service.HelpFeedbackService;
import com.sxhs.platform.system.config.Pass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Controller
@RequestMapping("/api/helpFeedback")
public class AppHelpFeedbackController {

    @Autowired
    private HelpFeedbackService helpFeedbackService;

    @Autowired
    private DictService dictService;

    @Pass
    @GetMapping("")
    public String allHelp(Model model) {
        List<DictDO> dictDOS = dictService.listByType("fklx");
        model.addAttribute("fklx",dictDOS);
        System.out.println(JSON.toJSON(model).toString());
        return "publishing/helpContent/index";
    }

    @Pass
    @GetMapping("/hot")
    public String hotHelp(Model model) {
        Map<String,Object> params = new HashMap<String,Object>();
        params.put("hasDelete",0);
        params.put("hasHot",1);
        params.put("helpStatus",1);
        List<HelpFeedbackDO> list = helpFeedbackService.list(params);
        model.addAttribute("list",list);
        System.out.println(JSON.toJSON(model).toString());
        return "publishing/helpContent/hot";
    }

    @Pass
    @GetMapping("/list")
    public String list(Integer helpType, Model model) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("hasDelete", 0);
        params.put("helpType", helpType);
        params.put("helpStatus", 1);
        List<DictDO> dictDOS = dictService.listByType("fklx");
        for(DictDO dictDO:dictDOS){
            if(dictDO.getValue().equals(helpType+"")){
                model.addAttribute("tital",dictDO.getName());
            }
        }
        List<HelpFeedbackDO> list = helpFeedbackService.list(params);
        model.addAttribute("list", list);
        System.out.println(JSON.toJSON(model).toString());
        return "publishing/helpContent/list";
    }

    @Pass
    @GetMapping("/detail")
    public String detal(Integer id, Model model) {
        HelpFeedbackDO helpFeedbackDO = helpFeedbackService.get(id);
        model.addAttribute("help",helpFeedbackDO);
        System.out.println(JSON.toJSON(model).toString());
        return "publishing/helpContent/detail";
    }

}

package com.sxhs.platform.publishing.app;


import com.sxhs.platform.common.config.Constant;
import com.sxhs.platform.common.utils.ApiResult;
import com.sxhs.platform.publishing.domain.PushLogDO;
import com.sxhs.platform.publishing.service.PushLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/api/pushLog")
@Api(value = "消息推送记录", description = "消息推送记录接口")
public class AppPushLogController {

    @Autowired
    private PushLogService pushLogService;

    @GetMapping("")
    @ApiOperation("当前用户某归属类目下消息推送记录")
    public ApiResult<List<PushLogDO>> getPushLog(@ApiParam(name = "category", value = "归属类目 (点赞、评论、关注)") String category, @ApiParam(name = "userId", value = "用户id")  Integer userId) {
        Map<String,Object> params = new HashMap<String,Object>();
        params.put("category",category);
        params.put("receiverId",userId);
        params.put("beRead", Constant.BE_READ_NO);
        params.put("beDelete",Constant.BE_DELETE_NO);
        params.put("sort","receiver_time");
        params.put("order",Constant.DESC);
        List<PushLogDO> list = pushLogService.list(params);
        return ApiResult.success(list);
    }

    @PutMapping("toRead")
    @ApiOperation("消息已读")
    public ApiResult toRead(@ApiParam(name = "id", value = "消息id") Integer id){
        PushLogDO pushLogDO = new PushLogDO();
        pushLogDO.setId(id);
        pushLogDO.setBeRead(Constant.BE_READ_YES);
        pushLogService.update(pushLogDO);
        return ApiResult.success();
    }



}

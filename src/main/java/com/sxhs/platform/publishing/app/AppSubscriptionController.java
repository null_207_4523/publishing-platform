package com.sxhs.platform.publishing.app;

import com.sxhs.platform.common.utils.ApiResult;
import com.sxhs.platform.publishing.service.OrgSubscriptionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 移动端订阅调用controller
 */
@Controller
@RequestMapping("/api/app/subscription")
@Api(value="机构订阅controller",description="机构订阅接口")
public class AppSubscriptionController {
    @Autowired
    private OrgSubscriptionService orgSubscriptionService;

    /**
     * 添加订阅
     */
    @ResponseBody
    @PostMapping("/add")
    @ApiOperation("添加订阅")
    public ApiResult save(@ApiParam(name="orgId",value="机构id",required=true)@RequestParam("orgId") Integer orgId,
                          @ApiParam(name="userId",value="用户id",required=true) @RequestParam("userId") Integer userId){
        return orgSubscriptionService.add(userId,orgId);
    }

    /**
     * 取消订阅
     */
    @ResponseBody
    @PostMapping("/cancel")
    @ApiOperation("取消订阅")
    public ApiResult cancel(@ApiParam(name="orgId",value="机构id",required=true) @RequestParam("orgId") Integer orgId,
                    @ApiParam(name="userId",value="用户id",required=true) @RequestParam("userId") Integer userId){
        return orgSubscriptionService.cancel(userId,orgId);
    }
}

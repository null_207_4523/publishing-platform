package com.sxhs.platform.publishing.app;

import com.sxhs.platform.common.utils.ApiResult;
import com.sxhs.platform.publishing.service.LikeService;
import com.sxhs.platform.publishing.service.NewsService;
import com.sxhs.platform.publishing.service.ReadService;
import com.sxhs.platform.publishing.service.ShareService;
import com.sxhs.platform.system.config.Pass;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * 移动端资讯调用controller
 */
@Controller
@RequestMapping("/api/app/news")
@Api(value = "资讯操作controller", description = "资讯操作接口")
public class AppNewsController {
    @Autowired
    private NewsService newsService;
    @Autowired
    private ReadService readService;
    @Autowired
    private ShareService shareService;
    @Autowired
    private LikeService likeService;

    /**
     * 栏目资讯列表
     *
     * @param columnId
     * @return
     */
    @Pass
    @GetMapping("/listByColumn")
    @ResponseBody
    @ApiOperation("根据栏目id获取资讯列表")
    public ApiResult listByColumn(
            @ApiParam(name = "userId", value = "用户id") @RequestParam(name = "userId", required = false) Integer userId,
            @ApiParam(name = "columnId", value = "栏目id", required = true) @RequestParam("columnId") Integer columnId,
            @ApiParam(name = "offset", value = "第几页从0开始", required = true) @RequestParam(name = "offset", required = false) Integer offset,
            @ApiParam(name = "limit", value = "每页条数", required = true) @RequestParam(name = "limit", required = false) Integer limit) {
        return newsService.listByColumn(userId, columnId, offset, limit);
    }

    /**
     * 资讯详情接口
     *
     * @param id
     * @return
     */
    @Pass
    @GetMapping("/details")
    @ResponseBody
    @ApiOperation("资讯详情接口")
    public ApiResult details(
            @ApiParam(name = "id", value = "资讯id", required = true) @RequestParam("id") Integer id,
            @ApiParam(name = "userId", value = "用户id") @RequestParam(name = "userId", required = false) Integer userId) {
        //查询列表数据
        return newsService.details(id, userId);
    }

    /**
     * 资讯点赞接口
     *
     * @param id     资讯id
     * @param userId 用户id
     * @return
     */
    @PostMapping("/like")
    @ResponseBody
    @ApiOperation("资讯点赞、取消赞接口")
    public ApiResult likeNews(
            @ApiParam(name = "id", value = "资讯id", required = true) @RequestParam("id") Integer id,
            @ApiParam(name = "userId", value = "用户id", required = true) @RequestParam("userId") Integer userId) {
        return likeService.likeNews(id, userId);
    }

    /**
     * 资讯分享接口
     *
     * @param id     资讯id
     * @param userId 用户id
     * @return
     */
    @PostMapping("/share")
    @ResponseBody
    @ApiOperation("资讯分享接口")
    public ApiResult shareNews(
            @ApiParam(name = "id", value = "资讯id", required = true) @RequestParam("id") Integer id,
            @ApiParam(name = "userId", value = "用户id", required = true) @RequestParam("userId") Integer userId) {
        return shareService.shareNews(id, userId);
    }

    /**
     * 资讯阅读接口
     *
     * @param id     资讯id
     * @param userId 用户id
     * @return
     */
    @Pass
    @PostMapping("/read")
    @ResponseBody
    @ApiOperation("资讯阅读接口")
    public ApiResult readNews(
            @ApiParam(name = "id", value = "资讯id", required = true) @RequestParam("id") Integer id,
            @ApiParam(name = "userId", value = "用户id", required = false) @RequestParam(name = "userId", required = false) Integer userId) {
        return readService.readNews(id, userId);
    }

}

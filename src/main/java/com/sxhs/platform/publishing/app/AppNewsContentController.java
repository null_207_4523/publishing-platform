package com.sxhs.platform.publishing.app;


import com.sxhs.platform.common.config.Constant;
import com.sxhs.platform.common.utils.StringUtils;
import com.sxhs.platform.publishing.domain.NewsDO;
import com.sxhs.platform.publishing.service.NewsService;
import com.sxhs.platform.publishing.service.OrgSubscriptionService;
import com.sxhs.platform.publishing.vo.NewsDetailsVo;
import com.sxhs.platform.system.config.Pass;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Controller
@RequestMapping("/api/news")
@Api(value = "资讯页面跳转controller", description = "资讯页面跳转接口")
public class AppNewsContentController {

    @Autowired
    private NewsService newsService;

    @Autowired
    private OrgSubscriptionService orgSubscriptionService;

    @Pass
    @GetMapping("newsContent")
    @ApiOperation("资讯内容页面")
    public String newsContent(Integer id,Integer userId,Model model) {
        NewsDO newsDO = newsService.get(id);
        // modify by wangzhijun 2021/1/21 根据1.4产品需求，用户创建人头像不为空，将页面orgLogo页面赋值为用户头像
        if(StringUtils.isNotEmpty(newsDO.getCreatedUserAvatar())){
            newsDO.setOrgLogo(newsDO.getCreatedUserAvatar());
        }
        model.addAttribute("news", newsDO);
        if(!ObjectUtils.isEmpty(userId)){
            Map<String, Object> params = new HashMap<>();
            params.put("orgId",newsDO.getOrgId());
            params.put("createdUserId",userId);
            params.put("hasDelete",Constant.HAS_DELETE_NO);
            int count = orgSubscriptionService.count(params);
            model.addAttribute("subscription",count);
        }
        model.addAttribute("orgId",newsDO.getOrgId());
        model.addAttribute("userId",userId);
        return "publishing/newsContent/newsContent";
    }

    @Pass
    @GetMapping("newsContentShare")
    @ApiOperation("资讯内容页面分享")
    public String newsContentShare(Integer id, Model model) {
        Map<String, Object> params = new HashMap<>();
        params.put("id",id);
        params.put("hasDelete", Constant.HAS_DELETE_NO);
        params.put("level", 0);
        params.put("parentId", 0);
        params.put("status",Constant.NEWS_STATUS_APPROVE);
        params.put("offset", 0);
        params.put("limit", 5);
        NewsDetailsVo detailsVo = newsService.getNewsDetails(params);
        // modify by wangzhijun 2021/1/21 根据1.4产品需求，用户创建人头像不为空，将页面orgLogo页面赋值为用户头像
        if(StringUtils.isNotEmpty(detailsVo.getCreatedUserAvatar())){
            detailsVo.setOrgLogo(detailsVo.getCreatedUserAvatar());
        }
        model.addAttribute("news", detailsVo);
        if(Objects.isNull(detailsVo))
            model.addAttribute("commentList", Collections.EMPTY_LIST);
        model.addAttribute("commentList", detailsVo.getCommentReplys());
        return "publishing/newsContent/newsContentShare";
    }

    @Pass
    @GetMapping("infoDetal")
    public String infoDetal(Integer id,Integer userId,Model model) {
        return "publishing/newsContent/infoDetal";
    }
}

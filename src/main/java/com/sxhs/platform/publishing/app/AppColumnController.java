package com.sxhs.platform.publishing.app;

import com.sxhs.platform.common.utils.ApiResult;
import com.sxhs.platform.publishing.service.ColumnService;
import com.sxhs.platform.publishing.vo.ColumnVo;
import com.sxhs.platform.system.config.Pass;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 移动端栏目调用controller
 */
@Controller
@RequestMapping("/api/app/column")
@Api(value = "栏目controller", description = "栏目操作接口")
public class AppColumnController {
    @Autowired
    private ColumnService columnService;

    @Pass
    @GetMapping("/list")
    @ResponseBody
    @ApiOperation("机构栏目列表")
    public ApiResult orgColumnlist(@ApiParam(name = "orgId", value = "机构id", required = true) @RequestParam("orgId") Integer orgId) {
        //查询列表数据
        List<ColumnVo> columnList = columnService.appOrgColumnlist(orgId);
        columnList.add(0, new ColumnVo().builder().id(0).columnId(0).columnName("推荐").orgId(0).build());
        return ApiResult.success(columnList);
    }

}

package com.sxhs.platform.publishing.app;

import com.sxhs.platform.common.utils.ApiResult;
import com.sxhs.platform.publishing.service.CommentLikeService;
import com.sxhs.platform.publishing.service.CommentService;
import com.sxhs.platform.system.config.Pass;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * 移动端资讯调用controller
 */
@Controller
@RequestMapping("/api/app/news/comment")
@Api(value = "资讯评论操作controller", description = "资讯评论操作接口")
public class AppCommentController {

    @Autowired
    private CommentService commentService;
    @Autowired
    private CommentLikeService commentLikeService;

    /**
     * 资讯一级评论接口
     *
     * @param id
     * @param userId
     * @param userName
     * @param userAvatar
     * @param content
     * @return
     */
    @PostMapping
    @ResponseBody
    @ApiOperation("资讯一级评论接口")
    public ApiResult commentNews(
            @ApiParam(name = "id", value = "资讯id", required = true) @RequestParam("id") Integer id,
            @ApiParam(name = "userId", value = "用户id", required = true) @RequestParam("userId") Integer userId,
            @ApiParam(name = "userName", value = "用户名") @RequestParam(name = "userName", required = false) String userName,
            @ApiParam(name = "userAvatar", value = "用户头像") @RequestParam(name = "userAvatar", required = false) String userAvatar,
            @ApiParam(name = "content", value = "评论内容", required = true) @RequestParam("content") String content) {
        return commentService.commentNews(id, userId, userName, userAvatar, content);
    }

    /**
     * 资讯评论回复接口
     *
     * @param id
     * @param commentId
     * @param userId
     * @param userName
     * @param userAvatar
     * @param content
     * @return
     */
    @PostMapping("/reply")
    @ResponseBody
    @ApiOperation("资讯评论回复接口")
    public ApiResult commentReply(
            @ApiParam(name = "id", value = "资讯id", required = true) @RequestParam("id") Integer id,
            @ApiParam(name = "commentId", value = "上级评论id", required = true) @RequestParam("commentId") Integer commentId,
            @ApiParam(name = "userId", value = "用户id", required = true) @RequestParam("userId") Integer userId,
            @ApiParam(name = "userName", value = "用户名") @RequestParam(name = "userName", required = false) String userName,
            @ApiParam(name = "userAvatar", value = "用户头像") @RequestParam(name = "userAvatar", required = false) String userAvatar,
            @ApiParam(name = "content", value = "评论内容", required = true) @RequestParam("content") String content) {
        return commentService.commentReply(id, commentId, userId, userName, userAvatar, content);
    }

    /**
     * 资讯评论刪除
     *
     * @param commentId
     * @param userId
     * @return
     */
    @PostMapping("/delect")
    @ResponseBody
    @ApiOperation("资讯评论刪除")
    public ApiResult commentDelete(
            @ApiParam(name = "commentId", value = "上级评论id", required = true) @RequestParam("commentId") Integer commentId,
            @ApiParam(name = "userId", value = "用户id", required = true) @RequestParam("userId") Integer userId) {
        return commentService.commentDelete(commentId, userId);
    }

    /**
     * 资讯评论点赞
     *
     * @param commentId
     * @param userId
     * @return
     */
    @PostMapping("/like")
    @ResponseBody
    @ApiOperation("资讯评论点赞、取消赞接口")
    public ApiResult commentLike(
            @ApiParam(name = "commentId", value = "评论id", required = true) @RequestParam("commentId") Integer commentId,
            @ApiParam(name = "userId", value = "用户id", required = true) @RequestParam("userId") Integer userId,
            @ApiParam(name = "userName", value = "用户名") @RequestParam(name = "userName", required = false) String userName,
            @ApiParam(name = "userAvatar", value = "用户头像") @RequestParam(name = "userAvatar", required = false) String userAvatar) {
        return commentLikeService.commentLike(commentId, userId, userName, userAvatar);
    }

    /**
     * @param commentId
     * @return
     */
    @Pass
    @GetMapping("/liker/list")
    @ResponseBody
    @ApiOperation("资讯评论点赞人列表")
    public ApiResult commentLikerList(
            @ApiParam(name = "commentId", value = "评论id", required = true) @RequestParam("commentId") Integer commentId) {
        return commentLikeService.commentLikerList(commentId);
    }

    /**
     * 资讯全部评论分页列表
     *
     * @param userId
     * @param id
     * @param offset
     * @param limit
     * @return
     */
    @Pass
    @GetMapping("/page")
    @ResponseBody
    @ApiOperation("资讯全部评论分页列表")
    public ApiResult pageComment(
            @ApiParam(name = "userId", value = "用户id") @RequestParam(name = "userId", required = false) Integer userId,
            @ApiParam(name = "id", value = "资讯id", required = true) @RequestParam("id") Integer id,
            @ApiParam(name = "offset", value = "第几页从0开始") @RequestParam(name = "offset", required = false) Integer offset,
            @ApiParam(name = "limit", value = "每页条数") @RequestParam(name = "limit", required = false) Integer limit) {
        return commentService.pageComment(id, userId, offset, limit);
    }

    /**
     * 资讯二级评论分页列表
     *
     * @param userId
     * @param commentId
     * @param offset
     * @param limit
     * @return
     */
    @Pass
    @GetMapping("/replys/page")
    @ResponseBody
    @ApiOperation("资讯二级评论分页列表")
    public ApiResult pageCommentReplys(
            @ApiParam(name = "userId", value = "用户id", required = true) @RequestParam(name = "userId", required = false) Integer userId,
            @ApiParam(name = "commentId", value = "上级评论id,查看单条", required = true) @RequestParam("commentId") Integer commentId,
            @ApiParam(name = "offset", value = "第几页从0开始") @RequestParam(name = "offset", required = false) Integer offset,
            @ApiParam(name = "limit", value = "每页条数") @RequestParam(name = "limit", required = false) Integer limit) {
        return commentService.pageCommentReplys(commentId, userId, offset, limit);
    }

}

package com.sxhs.platform.publishing.app;

import com.sxhs.platform.common.utils.ApiResult;
import com.sxhs.platform.publishing.service.CollectService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * 移动端资讯收藏调用controller
 */
@Controller
@RequestMapping("/api/app/news/collect")
@Api(value = "资讯收藏操作controller", description = "资讯收藏操作接口")
public class AppCollectController {

    @Autowired
    private CollectService collectService;

    /**
     * 资讯收藏接口
     *
     * @param id     资讯id
     * @param userId 用户id
     * @return
     */
    @PostMapping
    @ResponseBody
    @ApiOperation("资讯收藏、取消收藏接口")
    public ApiResult collectNews(
            @ApiParam(name = "id", value = "资讯id", required = true) @RequestParam("id") Integer id,
            @ApiParam(name = "userId", value = "用户id", required = true) @RequestParam("userId") Integer userId) {
        return collectService.collectNews(id, userId);
    }

    /**
     * @param userId
     * @return
     */
    @GetMapping("/list")
    @ResponseBody
    @ApiOperation("我的收藏列表")
    public ApiResult collectList(
            @ApiParam(name = "userId", value = "用户id", required = true) @RequestParam("userId") Integer userId,
            @ApiParam(name = "offset", value = "第几页从0开始") @RequestParam(name = "offset", required = false) Integer offset,
            @ApiParam(name = "limit", value = "每页条数") @RequestParam(name = "limit", required = false) Integer limit) {
        return collectService.collectList(userId, offset, limit);
    }

}

package com.sxhs.platform.publishing.schedule;

import com.sxhs.platform.common.config.Constant;
import com.sxhs.platform.common.utils.CollectionUtils;
import com.sxhs.platform.common.utils.DateUtils;
import com.sxhs.platform.publishing.domain.NewsDO;
import com.sxhs.platform.publishing.service.NewsService;
import com.sxhs.platform.publishing.service.OrgDailyStatisticsService;
import com.sxhs.platform.publishing.service.ProducerOrgNewsHandle;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
@Slf4j
public class ScheduleTask {

    @Autowired
    private OrgDailyStatisticsService orgDailyStatisticsService;
    @Autowired
    private NewsService newsService;
    @Autowired
    private ProducerOrgNewsHandle producerOrgNewsHandle;

    @Value("${review.time}")
    private Integer review_time;


    @XxlJob("dailyStatistics")
    public ReturnT<String> dailyStatistics(String param) {
        String createdTime = DateUtils.format(new Date());
        Map<String, Object> map = new HashMap<>();
        map.put("createdTime", createdTime);
        map.put("status", 3);
        map.put("hasDelete", Constant.HAS_DELETE_NO);
        try {
            orgDailyStatisticsService.updateDailyStatistics(map);
        } catch (Exception e) {
            log.error("统计定时任务错误消息：{}", e.getMessage(), e);
            return ReturnT.FAIL;
        }
        return ReturnT.SUCCESS;
    }

    /**
     * 资讯发布通过后一段时间（24h、48h），人工未审核数据处理为发布状态
     *
     * @param
     * @return
     */
    @XxlJob("updateNewsStatus")
    public ReturnT<String> updateNewsStatus(String param) {
        log.info("统计定时任务updateNewsStatus开始执行");
        Date now = new Date();
        // 获取全部status=5且hasDelete=0的全部待人工审核的资讯信息
        Map<String, Object> map = new HashMap<>();
        map.put("status", 5);
        map.put("hasDelete", Constant.HAS_DELETE_NO);
        try {
            List<NewsDO> list = newsService.list(map);
            if (CollectionUtils.isNotEmpty(list)) {
                for (NewsDO news : list) {
                    Date newsCreatedTime = news.getCreatedTime();
                    if (now.getTime() - newsCreatedTime.getTime() < review_time) {// 机审通过小于24小时的，不处理
                        continue;
                    }
                    // 1.曾经发布成功的资讯，编辑后机审再次通过24小时后;2.首次发布的资讯,机审通过48小时后，人工未审核的数据直接发布成功
                    if ((Objects.nonNull(news.getPublishTime()) && now.getTime() - newsCreatedTime.getTime() >= review_time) || now.getTime() - newsCreatedTime.getTime() > 2 * review_time) {
                        news.setStatus(3);
                        news.setPublishTime(now);
                        if (newsService.update(news) > 0 && Objects.isNull(news.getUpdateTime())) {
                            // 处理机构资讯数据
                            producerOrgNewsHandle.orgNewsProducer(news);
                        }
                    }
                }
            }
        } catch (Exception e) {
            log.error("统计定时任务错误消息：{}", e.getMessage(), e);
            return ReturnT.FAIL;
        }
        return ReturnT.SUCCESS;
    }

}

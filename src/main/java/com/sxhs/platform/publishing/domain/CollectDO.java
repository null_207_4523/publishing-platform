package com.sxhs.platform.publishing.domain;

import java.io.Serializable;
import java.util.Date;



/**
 * 资讯收藏
 * 
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2020-09-21 14:10:43
 */
public class CollectDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
    /**
     * //主键 id
     */
	private Integer id;
    /**
     * //资讯 id
     */
	private Integer newsId;
    /**
     * //收藏人 id
     */
	private Integer createdById;
    /**
     * //收藏时间
     */
	private Date createdTime;
    /**
     * //是否粉丝
     */
	private Integer hasFans;
    /**
     * //逻辑删除
     */
	private Integer hasDelete;

	/**
	 * 设置：主键 id
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：主键 id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：资讯 id
	 */
	public void setNewsId(Integer newsId) {
		this.newsId = newsId;
	}
	/**
	 * 获取：资讯 id
	 */
	public Integer getNewsId() {
		return newsId;
	}
	/**
	 * 设置：收藏人 id
	 */
	public void setCreatedById(Integer createdById) {
		this.createdById = createdById;
	}
	/**
	 * 获取：收藏人 id
	 */
	public Integer getCreatedById() {
		return createdById;
	}
	/**
	 * 设置：收藏时间
	 */
	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}
	/**
	 * 获取：收藏时间
	 */
	public Date getCreatedTime() {
		return createdTime;
	}
	/**
	 * 设置：是否粉丝
	 */
	public void setHasFans(Integer hasFans) {
		this.hasFans = hasFans;
	}
	/**
	 * 获取：是否粉丝
	 */
	public Integer getHasFans() {
		return hasFans;
	}
	/**
	 * 设置：逻辑删除
	 */
	public void setHasDelete(Integer hasDelete) {
		this.hasDelete = hasDelete;
	}
	/**
	 * 获取：逻辑删除
	 */
	public Integer getHasDelete() {
		return hasDelete;
	}
}

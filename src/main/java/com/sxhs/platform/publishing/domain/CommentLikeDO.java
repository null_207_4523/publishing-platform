package com.sxhs.platform.publishing.domain;

import java.io.Serializable;
import java.util.Date;


/**
 * 评论点赞
 *
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2020-09-29 17:00:26
 */
public class CommentLikeDO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * //主键 id
     */
    private Integer id;
    /**
     * //评论id
     */
    private Integer commentId;
    /**
     * //点赞人 id
     */
    private Integer createdById;
    /**
     * //点赞时间
     */
    private Date createdTime;
    /**
     * //逻辑删除
     */
    private Integer hasDelete;
    /**
     * //点赞人
     */
    private String createdBy;
    /**
     * //点赞人头像
     */
    private String createdByAvatar;


    /**
     * 设置：主键 id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取：主键 id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置：评论id
     */
    public void setCommentId(Integer commentId) {
        this.commentId = commentId;
    }

    /**
     * 获取：评论id
     */
    public Integer getCommentId() {
        return commentId;
    }

    /**
     * 设置：点赞人 id
     */
    public void setCreatedById(Integer createdById) {
        this.createdById = createdById;
    }

    /**
     * 获取：点赞人 id
     */
    public Integer getCreatedById() {
        return createdById;
    }

    /**
     * 设置：点赞时间
     */
    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    /**
     * 获取：点赞时间
     */
    public Date getCreatedTime() {
        return createdTime;
    }

    /**
     * 设置：逻辑删除
     */
    public void setHasDelete(Integer hasDelete) {
        this.hasDelete = hasDelete;
    }

    /**
     * 获取：逻辑删除
     */
    public Integer getHasDelete() {
        return hasDelete;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedByAvatar() {
        return createdByAvatar;
    }

    public void setCreatedByAvatar(String createdByAvatar) {
        this.createdByAvatar = createdByAvatar;
    }
}

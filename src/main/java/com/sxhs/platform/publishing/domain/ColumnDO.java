package com.sxhs.platform.publishing.domain;

import java.io.Serializable;
import java.util.Date;



/**
 * 栏目表
 * 
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2020-09-21 14:10:43
 */
public class ColumnDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
    /**
     * //主键 id
     */
	private Integer id;
    /**
     * //栏目名称
     */
	private String columnName;
    /**
     * //栏目类型 0:公共栏目1:机构栏目
     */
	private Integer columnType;
    /**
     * //栏目排序
     */
	private Integer columnOrder;
    /**
     * //栏目机构 0:公共栏目机构
     */
	private Integer columnOrg;
    /**
     * //是否显示
     */
	private Integer hasShow;
    /**
     * //应用角色
     */
	private String applicationRoles;
    /**
     * //创建人
     */
	private String createdUser;
    /**
     * //创建人id
     */
	private String createdUserId;
    /**
     * //创建时间
     */
	private Date createdTime;
    /**
     * //更新人
     */
	private String updateUser;
    /**
     * //更新人id
     */
	private String updateUserId;
    /**
     * //更新时间
     */
	private Date updateTime;
    /**
     * //逻辑删除
     */
	private Integer hasDelete;

	/**
	 * 设置：主键 id
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：主键 id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：栏目名称
	 */
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	/**
	 * 获取：栏目名称
	 */
	public String getColumnName() {
		return columnName;
	}
	/**
	 * 设置：栏目类型 0:公共栏目1:机构栏目
	 */
	public void setColumnType(Integer columnType) {
		this.columnType = columnType;
	}
	/**
	 * 获取：栏目类型 0:公共栏目1:机构栏目
	 */
	public Integer getColumnType() {
		return columnType;
	}
	/**
	 * 设置：栏目排序
	 */
	public void setColumnOrder(Integer columnOrder) {
		this.columnOrder = columnOrder;
	}
	/**
	 * 获取：栏目排序
	 */
	public Integer getColumnOrder() {
		return columnOrder;
	}
	/**
	 * 设置：栏目机构 0:公共栏目机构
	 */
	public void setColumnOrg(Integer columnOrg) {
		this.columnOrg = columnOrg;
	}
	/**
	 * 获取：栏目机构 0:公共栏目机构
	 */
	public Integer getColumnOrg() {
		return columnOrg;
	}
	/**
	 * 设置：是否显示
	 */
	public void setHasShow(Integer hasShow) {
		this.hasShow = hasShow;
	}
	/**
	 * 获取：是否显示
	 */
	public Integer getHasShow() {
		return hasShow;
	}
	/**
	 * 设置：应用角色
	 */
	public void setApplicationRoles(String applicationRoles) {
		this.applicationRoles = applicationRoles;
	}
	/**
	 * 获取：应用角色
	 */
	public String getApplicationRoles() {
		return applicationRoles;
	}
	/**
	 * 设置：创建人
	 */
	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}
	/**
	 * 获取：创建人
	 */
	public String getCreatedUser() {
		return createdUser;
	}
	/**
	 * 设置：创建人id
	 */
	public void setCreatedUserId(String createdUserId) {
		this.createdUserId = createdUserId;
	}
	/**
	 * 获取：创建人id
	 */
	public String getCreatedUserId() {
		return createdUserId;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreatedTime() {
		return createdTime;
	}
	/**
	 * 设置：更新人
	 */
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	/**
	 * 获取：更新人
	 */
	public String getUpdateUser() {
		return updateUser;
	}
	/**
	 * 设置：更新人id
	 */
	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}
	/**
	 * 获取：更新人id
	 */
	public String getUpdateUserId() {
		return updateUserId;
	}
	/**
	 * 设置：更新时间
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	/**
	 * 获取：更新时间
	 */
	public Date getUpdateTime() {
		return updateTime;
	}
	/**
	 * 设置：逻辑删除
	 */
	public void setHasDelete(Integer hasDelete) {
		this.hasDelete = hasDelete;
	}
	/**
	 * 获取：逻辑删除
	 */
	public Integer getHasDelete() {
		return hasDelete;
	}
}

package com.sxhs.platform.publishing.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sxhs.platform.common.config.Constant;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * 资讯评论
 *
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2020-09-21 14:10:43
 */
@Data
public class CommentDO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * //主键 id
     */
    private Integer id;
    /**
     * //资讯 id
     */
    private Integer newsId;
    /**
     * //评论信息
     */
    private String content;
    /**
     * //是否粉丝
     */
    private Integer hasFans;
    /**
     * //评论父id
     */
    private Integer parentId;
    /**
     * //评论轮询路径
     */
    private String pollingPath;
    /**
     * //评论等级
     */
    private Integer level;
    /**
     * //回复数量
     */
    private Integer replies;
    /**
     * //评论人
     */
    private String createdBy;
    /**
     * //评论人id
     */
    private Integer createdById;
    /**
     * //评论人头像
     */
    private String createdByAvatar;
    /**
     * //创建时间
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdTime;
    /**
     * //逻辑删除
     */
    private Integer hasDelete;
    /**
     * //评论点赞数量
     */
    private Integer likeCount;
    /**
     * //是否点赞
     */
    private Integer hasLike = Constant.HAS_LIKE_NO;
    /**
     * //回复
     */
    private transient List<CommentDO> commentReplys = new ArrayList<>();

}

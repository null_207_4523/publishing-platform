package com.sxhs.platform.publishing.domain;

import java.io.Serializable;

public class OrgColumnDO implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * //主键
     */
    private Integer id;
    /**
     * //机构id
     */
    private Integer orgId;
    /**
     * //栏目id
     */
    private Integer columnId;
    /**
     * //栏目排序
     */
    private Integer columnOrder;
    /**
     * //是否显示
     */
    private Integer hasShow;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOrgId() {
        return orgId;
    }

    public void setOrgId(Integer orgId) {
        this.orgId = orgId;
    }

    public Integer getColumnId() {
        return columnId;
    }

    public void setColumnId(Integer columnId) {
        this.columnId = columnId;
    }

    public Integer getColumnOrder() {
        return columnOrder;
    }

    public void setColumnOrder(Integer columnOrder) {
        this.columnOrder = columnOrder;
    }

    public Integer getHasShow() {
        return hasShow;
    }

    public void setHasShow(Integer hasShow) {
        this.hasShow = hasShow;
    }
}

package com.sxhs.platform.publishing.domain;

import java.io.Serializable;
import java.util.Date;



/**
 * 
 * 
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2020-10-30 16:48:35
 */
public class PushAppEventConfigDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
    /**
     * //
     */
	private Integer id;
    /**
     * //所属类目
     */
	private Integer categoryId;
    /**
     * //应用名称
     */
	private String appName;
    /**
     * //应用编码  （对应消息中心 应用code）
     */
	private String appType;
    /**
     * //事件名称
     */
	private String eventName;
    /**
     * //事件编码  （对应消息中心 流程code）
     */
	private String eventType;
    /**
     * //上线状态   0. 下线  1. 上线
     */
	private Integer onlineStutus;

	/**
	 * 设置：
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：所属类目
	 */
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
	/**
	 * 获取：所属类目
	 */
	public Integer getCategoryId() {
		return categoryId;
	}
	/**
	 * 设置：应用名称
	 */
	public void setAppName(String appName) {
		this.appName = appName;
	}
	/**
	 * 获取：应用名称
	 */
	public String getAppName() {
		return appName;
	}
	/**
	 * 设置：应用编码  （对应消息中心 应用code）
	 */
	public void setAppType(String appType) {
		this.appType = appType;
	}
	/**
	 * 获取：应用编码  （对应消息中心 应用code）
	 */
	public String getAppType() {
		return appType;
	}
	/**
	 * 设置：事件名称
	 */
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	/**
	 * 获取：事件名称
	 */
	public String getEventName() {
		return eventName;
	}
	/**
	 * 设置：事件编码  （对应消息中心 流程code）
	 */
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	/**
	 * 获取：事件编码  （对应消息中心 流程code）
	 */
	public String getEventType() {
		return eventType;
	}
	/**
	 * 设置：上线状态   0. 下线  1. 上线
	 */
	public void setOnlineStutus(Integer onlineStutus) {
		this.onlineStutus = onlineStutus;
	}
	/**
	 * 获取：上线状态   0. 下线  1. 上线
	 */
	public Integer getOnlineStutus() {
		return onlineStutus;
	}
}

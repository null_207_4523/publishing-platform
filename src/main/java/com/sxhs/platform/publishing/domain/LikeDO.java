package com.sxhs.platform.publishing.domain;

import java.io.Serializable;
import java.util.Date;



/**
 * 资讯点赞
 * 
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2020-09-21 14:10:44
 */
public class LikeDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
    /**
     * //主键
     */
	private Integer id;
    /**
     * //资讯 id
     */
	private Integer newsId;
    /**
     * //点赞人 id
     */
	private Integer createdById;
    /**
     * //点赞时间
     */
	private Date createdTime;
    /**
     * //是否粉丝
     */
	private Integer hasFans;
    /**
     * //逻辑删除
     */
	private Integer hasDelete;

	/**
	 * 设置：主键
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：主键
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：资讯 id
	 */
	public void setNewsId(Integer newsId) {
		this.newsId = newsId;
	}
	/**
	 * 获取：资讯 id
	 */
	public Integer getNewsId() {
		return newsId;
	}
	/**
	 * 设置：点赞人 id
	 */
	public void setCreatedById(Integer createdById) {
		this.createdById = createdById;
	}
	/**
	 * 获取：点赞人 id
	 */
	public Integer getCreatedById() {
		return createdById;
	}
	/**
	 * 设置：点赞时间
	 */
	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}
	/**
	 * 获取：点赞时间
	 */
	public Date getCreatedTime() {
		return createdTime;
	}
	/**
	 * 设置：是否粉丝
	 */
	public void setHasFans(Integer hasFans) {
		this.hasFans = hasFans;
	}
	/**
	 * 获取：是否粉丝
	 */
	public Integer getHasFans() {
		return hasFans;
	}
	/**
	 * 设置：逻辑删除
	 */
	public void setHasDelete(Integer hasDelete) {
		this.hasDelete = hasDelete;
	}
	/**
	 * 获取：逻辑删除
	 */
	public Integer getHasDelete() {
		return hasDelete;
	}
}

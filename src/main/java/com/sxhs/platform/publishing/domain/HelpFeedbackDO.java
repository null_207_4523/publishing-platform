package com.sxhs.platform.publishing.domain;

import java.io.Serializable;
import java.util.Date;



/**
 * 帮助反馈
 * 
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2020-10-17 10:34:40
 */
public class HelpFeedbackDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
    /**
     * //主键 id
     */
	private Integer id;
    /**
     * //类型
     */
	private Integer helpType;
    /**
     * //标题
     */
	private String tital;
    /**
     * //内容
     */
	private String contents;
    /**
     * //热点
     */
	private Integer hasHot;
    /**
     * //创建时间
     */
	private Date creatTime;
    /**
     * //状态
     */
	private Integer helpStatus;
    /**
     * //逻辑删除
     */
	private Integer hasDelete;

	/**
	 * 设置：主键 id
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：主键 id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：类型
	 */
	public void setHelpType(Integer helpType) {
		this.helpType = helpType;
	}
	/**
	 * 获取：类型
	 */
	public Integer getHelpType() {
		return helpType;
	}
	/**
	 * 设置：标题
	 */
	public void setTital(String tital) {
		this.tital = tital;
	}
	/**
	 * 获取：标题
	 */
	public String getTital() {
		return tital;
	}
	/**
	 * 设置：内容
	 */
	public void setContents(String contents) {
		this.contents = contents;
	}
	/**
	 * 获取：内容
	 */
	public String getContents() {
		return contents;
	}
	/**
	 * 设置：热点
	 */
	public void setHasHot(Integer hasHot) {
		this.hasHot = hasHot;
	}
	/**
	 * 获取：热点
	 */
	public Integer getHasHot() {
		return hasHot;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreatTime(Date creatTime) {
		this.creatTime = creatTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreatTime() {
		return creatTime;
	}
	/**
	 * 设置：状态
	 */
	public void setHelpStatus(Integer helpStatus) {
		this.helpStatus = helpStatus;
	}
	/**
	 * 获取：状态
	 */
	public Integer getHelpStatus() {
		return helpStatus;
	}
	/**
	 * 设置：逻辑删除
	 */
	public void setHasDelete(Integer hasDelete) {
		this.hasDelete = hasDelete;
	}
	/**
	 * 获取：逻辑删除
	 */
	public Integer getHasDelete() {
		return hasDelete;
	}
}

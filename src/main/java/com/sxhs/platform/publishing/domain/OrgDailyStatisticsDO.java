package com.sxhs.platform.publishing.domain;

import java.io.Serializable;
import java.util.Date;


/**
 * 机构资讯每日统计表
 *
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2020-09-21 14:10:44
 */
public class OrgDailyStatisticsDO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * //主键 id
     */
    private Integer id;
    /**
     * //机构 id
     */
    private Integer orgId;
    /**
     * //订阅量
     */
    private Integer subscriptionCount = 0;
    /**
     * //资讯发布量
     */
    private Integer newsCount = 0;
    /**
     * //评论量
     */
    private Integer commentCount = 0;
    /**
     * //粉丝评论量
     */
    private Integer fansCommentCount = 0;
    /**
     * //点赞量
     */
    private Integer likeCount = 0;
    /**
     * //粉丝点赞量
     */
    private Integer fansLikeCount = 0;
    /**
     * //阅读量
     */
    private Integer readCount = 0;
    /**
     * //粉丝阅读量
     */
    private Integer fansReadCount = 0;
    /**
     * //收藏量
     */
    private Integer collectCount = 0;
    /**
     * //粉丝收藏量
     */
    private Integer fansCollectCount = 0;
    /**
     * //分享量
     */
    private Integer shareCount = 0;
    /**
     * //粉丝分享量
     */
    private Integer fansShareCount = 0;
    /**
     * //逻辑删除
     */
    private Integer hasDelete = 0;
    /**
     * //创建时间
     */
    private Date createdTime;

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    /**
     * 设置：主键 id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取：主键 id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置：机构 id
     */
    public void setOrgId(Integer orgId) {
        this.orgId = orgId;
    }

    /**
     * 获取：机构 id
     */
    public Integer getOrgId() {
        return orgId;
    }

    /**
     * 设置：订阅量
     */
    public void setSubscriptionCount(Integer subscriptionCount) {
        this.subscriptionCount = subscriptionCount;
    }

    /**
     * 获取：订阅量
     */
    public Integer getSubscriptionCount() {
        return subscriptionCount;
    }

    /**
     * 设置：资讯发布量
     */
    public void setNewsCount(Integer newsCount) {
        this.newsCount = newsCount;
    }

    /**
     * 获取：资讯发布量
     */
    public Integer getNewsCount() {
        return newsCount;
    }

    /**
     * 设置：评论量
     */
    public void setCommentCount(Integer commentCount) {
        this.commentCount = commentCount;
    }

    /**
     * 获取：评论量
     */
    public Integer getCommentCount() {
        return commentCount;
    }

    /**
     * 设置：粉丝评论量
     */
    public void setFansCommentCount(Integer fansCommentCount) {
        this.fansCommentCount = fansCommentCount;
    }

    /**
     * 获取：粉丝评论量
     */
    public Integer getFansCommentCount() {
        return fansCommentCount;
    }

    /**
     * 设置：点赞量
     */
    public void setLikeCount(Integer likeCount) {
        this.likeCount = likeCount;
    }

    /**
     * 获取：点赞量
     */
    public Integer getLikeCount() {
        return likeCount;
    }

    /**
     * 设置：粉丝点赞量
     */
    public void setFansLikeCount(Integer fansLikeCount) {
        this.fansLikeCount = fansLikeCount;
    }

    /**
     * 获取：粉丝点赞量
     */
    public Integer getFansLikeCount() {
        return fansLikeCount;
    }

    /**
     * 设置：阅读量
     */
    public void setReadCount(Integer readCount) {
        this.readCount = readCount;
    }

    /**
     * 获取：阅读量
     */
    public Integer getReadCount() {
        return readCount;
    }

    /**
     * 设置：粉丝阅读量
     */
    public void setFansReadCount(Integer fansReadCount) {
        this.fansReadCount = fansReadCount;
    }

    /**
     * 获取：粉丝阅读量
     */
    public Integer getFansReadCount() {
        return fansReadCount;
    }

    /**
     * 设置：收藏量
     */
    public void setCollectCount(Integer collectCount) {
        this.collectCount = collectCount;
    }

    /**
     * 获取：收藏量
     */
    public Integer getCollectCount() {
        return collectCount;
    }

    /**
     * 设置：粉丝收藏量
     */
    public void setFansCollectCount(Integer fansCollectCount) {
        this.fansCollectCount = fansCollectCount;
    }

    /**
     * 获取：粉丝收藏量
     */
    public Integer getFansCollectCount() {
        return fansCollectCount;
    }

    /**
     * 设置：分享量
     */
    public void setShareCount(Integer shareCount) {
        this.shareCount = shareCount;
    }

    /**
     * 获取：分享量
     */
    public Integer getShareCount() {
        return shareCount;
    }

    /**
     * 设置：粉丝分享量
     */
    public void setFansShareCount(Integer fansShareCount) {
        this.fansShareCount = fansShareCount;
    }

    /**
     * 获取：粉丝分享量
     */
    public Integer getFansShareCount() {
        return fansShareCount;
    }

    /**
     * 设置：逻辑删除
     */
    public void setHasDelete(Integer hasDelete) {
        this.hasDelete = hasDelete;
    }

    /**
     * 获取：逻辑删除
     */
    public Integer getHasDelete() {
        return hasDelete;
    }
}

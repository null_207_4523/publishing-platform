package com.sxhs.platform.publishing.domain;

import java.io.Serializable;
import java.util.Date;



/**
 * 资讯审核原因
 * 
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2021-01-14 09:57:48
 */
public class NewsReviewReasonDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static enum Type {
		MACHINEREVIEW(0, "机器审核"), MANUALREVIEW(1, "人工审核");

		int code;
		String name;

		Type(int code, String name) {
			this.code = code;
			this.name = name;
		}

		public int getCode() {
			return code;
		}

		public void setCode(int code) {
			this.code = code;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
	}


    /**
     * //主键 id
     */
	private Integer id;
    /**
     * //资讯 id
     */
	private Integer newsId;
    /**
     * //创建人id
     */
	private Integer createdById;
    /**
     * //创建时间
     */
	private Date createdTime;
    /**
     * //不通过原因
     */
	private String reason;
	/**
	 * //不通过类型 0:机器审核；1:人工审核
	 */
	private Integer type;

	/**
	 * 设置：主键 id
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：主键 id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：资讯 id
	 */
	public void setNewsId(Integer newsId) {
		this.newsId = newsId;
	}
	/**
	 * 获取：资讯 id
	 */
	public Integer getNewsId() {
		return newsId;
	}
	/**
	 * 设置：创建人id
	 */
	public void setCreatedById(Integer createdById) {
		this.createdById = createdById;
	}
	/**
	 * 获取：创建人id
	 */
	public Integer getCreatedById() {
		return createdById;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreatedTime() {
		return createdTime;
	}
	/**
	 * 设置：不通过原因
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}
	/**
	 * 获取：不通过原因
	 */
	public String getReason() {
		return reason;
	}
	/**
	 * 设置：审核不通过类型
	 */
	public Integer getType() {
		return type;
	}
	/**
	 * 获取：审核不通过类型
	 */
	public void setType(Integer type) {
		this.type = type;
	}
}

package com.sxhs.platform.publishing.domain;

import java.io.Serializable;
import java.util.Date;



/**
 * 
 * 
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2020-10-30 16:48:19
 */
public class PushCategoryConfigDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
    /**
     * //
     */
	private Integer id;
    /**
     * //tag 分离
     */
	private String categoryTag;
    /**
     * //名称
     */
	private String categoryName;
    /**
     * //是否启用 0.禁用 1.启用
     */
	private Integer openStatus;

	/**
	 * 设置：
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：tag 分离
	 */
	public void setCategoryTag(String categoryTag) {
		this.categoryTag = categoryTag;
	}
	/**
	 * 获取：tag 分离
	 */
	public String getCategoryTag() {
		return categoryTag;
	}
	/**
	 * 设置：名称
	 */
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	/**
	 * 获取：名称
	 */
	public String getCategoryName() {
		return categoryName;
	}
	/**
	 * 设置：是否启用 0.禁用 1.启用
	 */
	public void setOpenStatus(Integer openStatus) {
		this.openStatus = openStatus;
	}
	/**
	 * 获取：是否启用 0.禁用 1.启用
	 */
	public Integer getOpenStatus() {
		return openStatus;
	}
}

package com.sxhs.platform.publishing.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;


/**
 * 资讯信息
 *
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2020-09-21 14:10:44
 */
@ApiModel(value = "栏目对象", description = "栏目对象")
public class NewsDO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * //主键 id
     */
    @ApiModelProperty(value = "主键 id")
    private Integer id;
    /**
     * //栏目 id
     */
    @ApiModelProperty(value = "栏目 id")
    private String columnId;
    /**
     * //标题
     */
    @ApiModelProperty(value = "标题")
    private String title;
    /**
     * //类型 0：富文本 1：视频 2：音频
     */
    @ApiModelProperty(value = "类型 0：富文本 1：视频 2：音频")
    private Integer type;
    /**
     * //是否置顶
     */
    @ApiModelProperty(value = "是否置顶")
    private Integer hasTop;
    /**
     * //是否内部发布
     */
    @ApiModelProperty(value = "是否内部发布")
    private Integer hasInnerPublish;
    /**
     * //文章内容
     */
    @ApiModelProperty(value = "文章内容")
    private String content;
    /**
     * //视频地址
     */
    @ApiModelProperty(value = "视频地址")
    private String vedioUrl;
    /**
     * //封面
     */
    @ApiModelProperty(value = "封面")
    private String cover;
    /**
     * //封面类型 0：无封面 1：单图  2：三图
     */
    @ApiModelProperty(value = "封面类型 0：无封面 1：单图  2：三图")
    private Integer coverType;
    /**
     * //简介
     */
    @ApiModelProperty(value = "简介")
    private String synopsis;
    /**
     * //文章状态 0：草稿 1：撤回  2：机器审核中  3：审核通过  4：审核未通过  5.人工审核中
     */
    @ApiModelProperty(value = "文章状态 0：草稿 1：撤回  2：机器审核中  3：审核通过  4：审核未通过  5.人工审核中")
    private Integer status;
    /**
     * //发布机构 id
     */
    @ApiModelProperty(value = "发布机构 id")
    private Integer orgId;
    /**
     * //发布机构名称
     */
    @ApiModelProperty(value = "发布机构名称")
    private String orgName;
    /**
     * //发布机构头像
     */
    @ApiModelProperty(value = "发布机构头像")
    private String orgLogo;
    /**
     * //发布时间
     */
    @ApiModelProperty(value = "发布时间")
    private Date publishTime;
    /**
     * //评论量
     */
    @ApiModelProperty(value = "评论量")
    private Integer commentCount;
    /**
     * //粉丝评论量
     */
    @ApiModelProperty(value = "粉丝评论量")
    private Integer fansCommentCount;
    /**
     * //阅读量
     */
    @ApiModelProperty(value = "阅读量")
    private Integer readCount;
    /**
     * //粉丝阅读量
     */
    @ApiModelProperty(value = "粉丝阅读量")
    private Integer fansReadCount;
    /**
     * //点赞量
     */
    @ApiModelProperty(value = "点赞量")
    private Integer likeCount;
    /**
     * //粉丝点赞量
     */
    @ApiModelProperty(value = "粉丝点赞量")
    private Integer fansLikeCount;
    /**
     * //收藏量
     */
    @ApiModelProperty(value = "收藏量")
    private Integer collectCount;
    /**
     * //粉丝收藏量
     */
    @ApiModelProperty(value = "粉丝收藏量")
    private Integer fansCollectCount;
    /**
     * //分享量
     */
    @ApiModelProperty(value = "分享量")
    private Integer shareCount;
    /**
     * //粉丝分享量
     */
    @ApiModelProperty(value = "粉丝分享量")
    private Integer fansShareCount;
    /**
     * //创建人
     */
    @ApiModelProperty(value = "创建人")
    private String createdUser;
    /**
     * //创建人 id
     */
    @ApiModelProperty(value = "创建人 id")
    private String createdUserId;
    /**
     * //创建人头像
     */
    @ApiModelProperty(value = "创建人头像")
    private String createdUserAvatar;
    /**
     * //创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdTime;
    /**
     * //更新人
     */
    @ApiModelProperty(value = "更新人")
    private String updateUser;
    /**
     * //更新人 id
     */
    @ApiModelProperty(value = "更新人 id")
    private String updateUserId;
    /**
     * //更新时间
     */
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;
    /**
     * //逻辑删除
     */
    @ApiModelProperty(value = "逻辑删除")
    private Integer hasDelete;
    /**
     * //栏目名称
     */
    private transient String columnName;

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }
    /**
     * //创建人、修改人角色
     */
    private transient String roleId;

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    /**
     * 设置：主键 id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取：主键 id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置：栏目 id
     */
    public void setColumnId(String columnId) {
        this.columnId = columnId;
    }

    /**
     * 获取：栏目 id
     */
    public String getColumnId() {
        return columnId;
    }

    /**
     * 设置：标题
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 获取：标题
     */
    public String getTitle() {
        return title;
    }

    /**
     * 设置：类型 0：富文本 1：视频 2：音频
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 获取：类型 0：富文本 1：视频 2：音频
     */
    public Integer getType() {
        return type;
    }

    /**
     * 设置：是否置顶
     */
    public void setHasTop(Integer hasTop) {
        this.hasTop = hasTop;
    }

    /**
     * 获取：是否置顶
     */
    public Integer getHasTop() {
        return hasTop;
    }

    /**
     * 设置：是否内部发布
     */
    public void setHasInnerPublish(Integer hasInnerPublish) {
        this.hasInnerPublish = hasInnerPublish;
    }

    /**
     * 获取：是否内部发布
     */
    public Integer getHasInnerPublish() {
        return hasInnerPublish;
    }

    /**
     * 设置：文章内容
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * 获取：文章内容
     */
    public String getContent() {
        return content;
    }

    /**
     * 设置：视频地址
     */
    public void setVedioUrl(String vedioUrl) {
        this.vedioUrl = vedioUrl;
    }

    /**
     * 获取：视频地址
     */
    public String getVedioUrl() {
        return vedioUrl;
    }

    /**
     * 设置：封面
     */
    public void setCover(String cover) {
        this.cover = cover;
    }

    /**
     * 获取：封面
     */
    public String getCover() {
        return cover;
    }

    /**
     * 设置：封面类型 0：无封面 1：单图  2：三图
     */
    public void setCoverType(Integer coverType) {
        this.coverType = coverType;
    }

    /**
     * 获取：封面类型 0：无封面 1：单图  2：三图
     */
    public Integer getCoverType() {
        return coverType;
    }

    /**
     * 设置：简介
     */
    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    /**
     * 获取：简介
     */
    public String getSynopsis() {
        return synopsis;
    }

    /**
     * 设置：文章状态 0：草稿 1：保存  2：审核中  3：审核通过  4：审核未通过
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 获取：文章状态 0：草稿 1：保存  2：审核中  3：审核通过  4：审核未通过
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 设置：发布机构 id
     */
    public void setOrgId(Integer orgId) {
        this.orgId = orgId;
    }

    /**
     * 获取：发布机构 id
     */
    public Integer getOrgId() {
        return orgId;
    }

    /**
     * 设置：发布机构名称
     */
    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    /**
     * 获取：发布机构名称
     */
    public String getOrgName() {
        return orgName;
    }

    /**
     * 设置：发布机构头像
     */
    public void setOrgLogo(String orgLogo) {
        this.orgLogo = orgLogo;
    }

    /**
     * 获取：发布机构头像
     */
    public String getOrgLogo() {
        return orgLogo;
    }

    /**
     * 设置：发布时间
     */
    public void setPublishTime(Date publishTime) {
        this.publishTime = publishTime;
    }

    /**
     * 获取：发布时间
     */
    public Date getPublishTime() {
        return publishTime;
    }

    /**
     * 设置：评论量
     */
    public void setCommentCount(Integer commentCount) {
        this.commentCount = commentCount;
    }

    /**
     * 获取：评论量
     */
    public Integer getCommentCount() {
        return commentCount;
    }

    /**
     * 设置：粉丝评论量
     */
    public void setFansCommentCount(Integer fansCommentCount) {
        this.fansCommentCount = fansCommentCount;
    }

    /**
     * 获取：粉丝评论量
     */
    public Integer getFansCommentCount() {
        return fansCommentCount;
    }

    /**
     * 设置：阅读量
     */
    public void setReadCount(Integer readCount) {
        this.readCount = readCount;
    }

    /**
     * 获取：阅读量
     */
    public Integer getReadCount() {
        return readCount;
    }

    /**
     * 设置：粉丝阅读量
     */
    public void setFansReadCount(Integer fansReadCount) {
        this.fansReadCount = fansReadCount;
    }

    /**
     * 获取：粉丝阅读量
     */
    public Integer getFansReadCount() {
        return fansReadCount;
    }

    /**
     * 设置：点赞量
     */
    public void setLikeCount(Integer likeCount) {
        this.likeCount = likeCount;
    }

    /**
     * 获取：点赞量
     */
    public Integer getLikeCount() {
        return likeCount;
    }

    /**
     * 设置：粉丝点赞量
     */
    public void setFansLikeCount(Integer fansLikeCount) {
        this.fansLikeCount = fansLikeCount;
    }

    /**
     * 获取：粉丝点赞量
     */
    public Integer getFansLikeCount() {
        return fansLikeCount;
    }

    /**
     * 设置：收藏量
     */
    public void setCollectCount(Integer collectCount) {
        this.collectCount = collectCount;
    }

    /**
     * 获取：收藏量
     */
    public Integer getCollectCount() {
        return collectCount;
    }

    /**
     * 设置：粉丝收藏量
     */
    public void setFansCollectCount(Integer fansCollectCount) {
        this.fansCollectCount = fansCollectCount;
    }

    /**
     * 获取：粉丝收藏量
     */
    public Integer getFansCollectCount() {
        return fansCollectCount;
    }

    /**
     * 设置：分享量
     */
    public void setShareCount(Integer shareCount) {
        this.shareCount = shareCount;
    }

    /**
     * 获取：分享量
     */
    public Integer getShareCount() {
        return shareCount;
    }

    /**
     * 设置：粉丝分享量
     */
    public void setFansShareCount(Integer fansShareCount) {
        this.fansShareCount = fansShareCount;
    }

    /**
     * 获取：粉丝分享量
     */
    public Integer getFansShareCount() {
        return fansShareCount;
    }

    /**
     * 设置：创建人
     */
    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser;
    }

    /**
     * 获取：创建人
     */
    public String getCreatedUser() {
        return createdUser;
    }

    /**
     * 设置：创建人 id
     */
    public void setCreatedUserId(String createdUserId) {
        this.createdUserId = createdUserId;
    }

    /**
     * 获取：创建人 id
     */
    public String getCreatedUserId() {
        return createdUserId;
    }

    /**
     * 设置：创建人头像
     */
    public void setCreatedUserAvatar(String createdUserAvatar) {
        this.createdUserAvatar = createdUserAvatar;
    }

    /**
     * 获取：创建人头像
     */
    public String getCreatedUserAvatar() {
        return createdUserAvatar;
    }

    /**
     * 设置：创建时间
     */
    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    /**
     * 获取：创建时间
     */
    public Date getCreatedTime() {
        return createdTime;
    }

    /**
     * 设置：更新人
     */
    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    /**
     * 获取：更新人
     */
    public String getUpdateUser() {
        return updateUser;
    }

    /**
     * 设置：更新人 id
     */
    public void setUpdateUserId(String updateUserId) {
        this.updateUserId = updateUserId;
    }

    /**
     * 获取：更新人 id
     */
    public String getUpdateUserId() {
        return updateUserId;
    }

    /**
     * 设置：更新时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * 获取：更新时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置：逻辑删除
     */
    public void setHasDelete(Integer hasDelete) {
        this.hasDelete = hasDelete;
    }

    /**
     * 获取：逻辑删除
     */
    public Integer getHasDelete() {
        return hasDelete;
    }
}

package com.sxhs.platform.publishing.domain;

import java.io.Serializable;



/**
 * 应用布局
 * 
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2020-12-03 16:33:48
 */
public class LayoutDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
    /**
     * //
     */
	private Integer id;
    /**
     * //用户id
     */
	private Integer userId;
    /**
     * //首页应用id
     */
	private String layId;

	/**
	 * 设置：
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：用户id
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	/**
	 * 获取：用户id
	 */
	public Integer getUserId() {
		return userId;
	}
	/**
	 * 设置：首页应用id
	 */
	public void setLayId(String layId) {
		this.layId = layId;
	}
	/**
	 * 获取：首页应用id
	 */
	public String getLayId() {
		return layId;
	}
}

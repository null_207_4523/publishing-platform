package com.sxhs.platform.publishing.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;



/**
 * 消息推送记录 
 * 
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2020-10-21 09:52:56
 */
@ApiModel(value = "消息推送记录", description = "消息推送记录")
public class PushLogDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
    /**
     * //主键 id
     */
	@ApiModelProperty(value = "主键 id")
	private Integer id;
    /**
     * //消息标题
     */
	@ApiModelProperty(value = "消息标题")
	private String title;
    /**
     * //消息内容
     */
	@ApiModelProperty(value = "消息内容")
	private String content;
    /**
     * //推送信息内容(额外参数，用于业务跳转)
     */
	@ApiModelProperty(value = "推送信息内容(额外参数，用于业务跳转)")
	private String msgBody;
    /**
     * //消息接收人id
     */
	@ApiModelProperty(value = "消息接收人id")
	private Integer receiverId;
    /**
     * //推送时间
     */
	@ApiModelProperty(value = "推送时间")
	private Date receiverTime;
    /**
     * //归属类目 (点赞 TAG-PRAISE、评论 TAG-COMMENT、关注 TAG-ATTENTION)
     */
	@ApiModelProperty(value = "归属类目")
	private String category;
    /**
     * //归属类目名称
     */
	@ApiModelProperty(value = "归属类目名称")
	private String categoryName;
    /**
     * //业务事件标识
     */
	@ApiModelProperty(value = "业务事件标识")
	private String eventType;
    /**
     * //业务事件名称
     */
	@ApiModelProperty(value = "业务事件名称")
	private String eventName;
    /**
     * //应用
     */
	@ApiModelProperty(value = "应用")
	private Integer appType;
    /**
     * //应用名称
     */
	@ApiModelProperty(value = "应用名称")
	private String appName;
    /**
     * //状态
     */
	@ApiModelProperty(value = "状态")
	private Integer receiverStatus;
    /**
     * //已读状态
     */
	@ApiModelProperty(value = "已读状态")
	private Integer beRead;
    /**
     * //是否删除
     */
	@ApiModelProperty(value = "是否删除")
	private Integer beDelete;

	/**
	 *  跳转url
	 */
	@ApiModelProperty(value = "跳转url")
	private String url;

	/**
	 * 设置：主键 id
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：主键 id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：消息标题
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * 获取：消息标题
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * 设置：消息内容
	 */
	public void setContent(String content) {
		this.content = content;
	}
	/**
	 * 获取：消息内容
	 */
	public String getContent() {
		return content;
	}
	/**
	 * 设置：推送信息内容(额外参数，用于业务跳转)
	 */
	public void setMsgBody(String msgBody) {
		this.msgBody = msgBody;
	}
	/**
	 * 获取：推送信息内容(额外参数，用于业务跳转)
	 */
	public String getMsgBody() {
		return msgBody;
	}
	/**
	 * 设置：消息接收人id
	 */
	public void setReceiverId(Integer receiverId) {
		this.receiverId = receiverId;
	}
	/**
	 * 获取：消息接收人id
	 */
	public Integer getReceiverId() {
		return receiverId;
	}
	/**
	 * 设置：推送时间
	 */
	public void setReceiverTime(Date receiverTime) {
		this.receiverTime = receiverTime;
	}
	/**
	 * 获取：推送时间
	 */
	public Date getReceiverTime() {
		return receiverTime;
	}
	/**
	 * 设置：归属类目 (点赞、评论..)
	 */
	public void setCategory(String category) {
		this.category = category;
	}
	/**
	 * 获取：归属类目 (点赞、评论..)
	 */
	public String getCategory() {
		return category;
	}
	/**
	 * 设置：归属类目名称
	 */
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	/**
	 * 获取：归属类目名称
	 */
	public String getCategoryName() {
		return categoryName;
	}
	/**
	 * 设置：业务事件标识
	 */
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	/**
	 * 获取：业务事件标识
	 */
	public String getEventType() {
		return eventType;
	}
	/**
	 * 设置：业务事件名称
	 */
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	/**
	 * 获取：业务事件名称
	 */
	public String getEventName() {
		return eventName;
	}
	/**
	 * 设置：应用
	 */
	public void setAppType(Integer appType) {
		this.appType = appType;
	}
	/**
	 * 获取：应用
	 */
	public Integer getAppType() {
		return appType;
	}
	/**
	 * 设置：应用名称
	 */
	public void setAppName(String appName) {
		this.appName = appName;
	}
	/**
	 * 获取：应用名称
	 */
	public String getAppName() {
		return appName;
	}
	/**
	 * 设置：状态
	 */
	public void setReceiverStatus(Integer receiverStatus) {
		this.receiverStatus = receiverStatus;
	}
	/**
	 * 获取：状态
	 */
	public Integer getReceiverStatus() {
		return receiverStatus;
	}
	/**
	 * 设置：已读状态
	 */
	public void setBeRead(Integer beRead) {
		this.beRead = beRead;
	}
	/**
	 * 获取：已读状态
	 */
	public Integer getBeRead() {
		return beRead;
	}
	/**
	 * 设置：是否删除
	 */
	public void setBeDelete(Integer beDelete) {
		this.beDelete = beDelete;
	}
	/**
	 * 获取：是否删除
	 */
	public Integer getBeDelete() {
		return beDelete;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}

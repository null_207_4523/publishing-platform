package com.sxhs.platform.publishing.domain;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;


/**
 * 
 * 
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2020-11-09 11:39:55
 */
@Data
public class VersionDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
    /**
     * //主键 id
     */
	private Integer id;
    /**
     * //版本号
     */
	private String code;
	/**
	 * //版本名称
	 */
	private String name;
    /**
     * //版本信息
     */
	private String message;
    /**
     * //版本创建时间
     */
	private Date createTime;
    /**
     * //版本更新时间
     */
	private Date updateTime;
    /**
     * //是否强更 0 强制更新  1 不强制
     */
	private Integer forceUpdate;
    /**
     * //是否删除 0 正常  1 删除
     */
	private Integer hasDelete;
    /**
     * //启用状态 0启用，1禁用
     */
	private Integer hasDisabled;
    /**
     * //系统类型 0. Android;;1 Ios;2.cms
     */
	private Integer systemType;
	/**
	 * //文件大小
	 */
	private String size;
	/**
	 * //下载地址
	 */
	private String url;

}

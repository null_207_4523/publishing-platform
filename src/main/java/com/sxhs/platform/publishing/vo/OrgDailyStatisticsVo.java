package com.sxhs.platform.publishing.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class OrgDailyStatisticsVo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * //机构 id
     */
    private Integer orgId;
    /**
     * //订阅量
     */
    private Integer subscriptionCount;
    /**
     * //资讯发布量
     */
    private Integer newsCount;
    /**
     * //评论量
     */
    private Integer commentCount;
    /**
     * //粉丝评论量
     */
    private Integer fansCommentCount;
    /**
     * //点赞量
     */
    private Integer likeCount;
    /**
     * //粉丝点赞量
     */
    private Integer fansLikeCount;
    /**
     * //阅读量
     */
    private Integer readCount;
    /**
     * //粉丝阅读量
     */
    private Integer fansReadCount;
    /**
     * //收藏量
     */
    private Integer collectCount;
    /**
     * //粉丝收藏量
     */
    private Integer fansCollectCount;
    /**
     * //分享量
     */
    private Integer shareCount;
    /**
     * //粉丝分享量
     */
    private Integer fansShareCount;
    /**
     * //逻辑删除
     */
    private Integer hasDelete = 0;
    /**
     * //创建时间
     */
    private Date createdTime;
}

package com.sxhs.platform.publishing.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sxhs.platform.common.config.Constant;
import com.sxhs.platform.publishing.domain.CommentLikeDO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@ApiModel(value = "评论信息", description = "评论信息")
public class CommentVo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * //主键 id
     */
    @ApiModelProperty(value = "主键id")
    private Integer id;
    /**
     * //资讯 id
     */
    @ApiModelProperty(value = "资讯 id")
    private Integer newsId;
    /**
     * //评论信息
     */
    @ApiModelProperty(value = "评论信息")
    private String content;
    /**
     * //是否粉丝
     */
    @ApiModelProperty(value = "是否粉丝")
    private Integer hasFans;
    /**
     * //回复数量
     */
    @ApiModelProperty(value = "回复数量")
    private Integer replies;
    /**
     * //评论人
     */
    @ApiModelProperty(value = "评论人")
    private String createdBy;
    /**
     * //评论人id
     */
    @ApiModelProperty(value = "评论人id")
    private Integer createdById;
    /**
     * //评论人头像
     */
    @ApiModelProperty(value = "评论人头像")
    private String createdByAvatar;
    /**
     * //创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdTime;
    /**
     * //评论点赞数量
     */
    @ApiModelProperty(value = "评论点赞数量")
    private Integer likeCount = 0;
    /**
     * //是否点赞
     */
    @ApiModelProperty(value = "是否点赞")
    private Integer hasLike = Constant.HAS_LIKE_NO;
    /**
     * //下级评论集合
     */
    @ApiModelProperty(value = "下级评论集合")
    private List<CommentVo> commentReplys = new ArrayList<>();
    /**
     * //点赞人集合
     */
    @ApiModelProperty(value = "点赞人集合")
    private List<CommentLikeDO> commentLikeList = new ArrayList<>();
}

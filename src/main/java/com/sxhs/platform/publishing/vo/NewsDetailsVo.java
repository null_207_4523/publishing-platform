package com.sxhs.platform.publishing.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.sxhs.platform.publishing.domain.CollectDO;
import com.sxhs.platform.publishing.domain.CommentDO;
import com.sxhs.platform.publishing.domain.LikeDO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@ApiModel(value = "资讯详细信息", description = "资讯详细信息")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class NewsDetailsVo extends NewsBaseVo {

    private static final long serialVersionUID = 1L;

    /**
     * //文章内容
     */
    @ApiModelProperty(value = "文章内容")
    private String content;

    /**
     * //资讯一级评论列表
     */
    @ApiModelProperty(value = "资讯一级评论列表")
    private List<CommentDO> commentReplys = new ArrayList<>();

    /**
     * //资讯收藏列表
     */
    @JsonIgnore
    @ApiModelProperty(value = "资讯收藏列表")
    private List<CollectDO> collectList= new ArrayList<>();

    /**
     * //资讯点赞列表
     */
    @JsonIgnore
    @ApiModelProperty(value = "资讯点赞列表")
    private List<LikeDO> likeList = new ArrayList<>();
}

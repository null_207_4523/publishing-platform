package com.sxhs.platform.publishing.vo;

import com.sxhs.platform.common.config.Constant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Data
@ApiModel(value = "资讯基础信息", description = "资讯基础信息")
public class NewsBaseVo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * //主键 id
     */
    @ApiModelProperty(value = "主键id")
    private Integer id;
    /**
     * //栏目 id
     */
    @ApiModelProperty(value = "栏目id")
    private String columnId;
    /**
     * //标题
     */
    @ApiModelProperty(value = "标题")
    private String title;
    /**
     * //类型 0：富文本 1：视频 2：音频
     */
    @ApiModelProperty(value = "类型 0：富文本 1：视频 2：音频")
    private Integer type;
    /**
     * //是否置顶
     */
    @ApiModelProperty(value = "是否置顶")
    private Integer hasTop;
    /**
     * //封面
     */
    @ApiModelProperty(value = "封面")
    private String cover;
    /**
     * //封面类型 0：无封面 1：单图  2：三图
     */
    @ApiModelProperty(value = "封面类型 0：无封面 1：单图  2：三图")
    private Integer coverType;
    /**
     * //简介
     */
    @ApiModelProperty(value = "简介")
    private String synopsis;
    /**
     * //发布机构头像
     */
    @ApiModelProperty(value = "发布机构头像")
    private String orgLogo;
    /**
     * //发布机构名称
     */
    @ApiModelProperty(value = "发布机构名称")
    private String orgName;
    /**
     * //创建人
     */
    @ApiModelProperty(value = "创建人")
    private String createdUser;
    /**
     * //创建人头像
     */
    @ApiModelProperty(value = "创建人头像")
    private String createdUserAvatar;
    /**
     * //发布时间
     */
    @ApiModelProperty(value = "发布时间")
    private Date publishTime;
    /**
     * //文章状态 0：草稿 1：保存  2：审核中  3：审核通过  4：审核未通过
     */
    @ApiModelProperty(value = "文章状态 0：草稿 1：保存  2：审核中  3：审核通过  4：审核未通过")
    private Integer status;
    /**
     * //评论量
     */
    @ApiModelProperty(value = "评论量")
    private Integer commentCount;
    /**
     * //粉丝评论量
     */
    @ApiModelProperty(value = "粉丝评论量")
    private Integer fansCommentCount;
    /**
     * //阅读量
     */
    @ApiModelProperty(value = "阅读量")
    private Integer readCount;
    /**
     * //粉丝阅读量
     */
    @ApiModelProperty(value = "粉丝阅读量")
    private Integer fansReadCount;
    /**
     * //点赞量
     */
    @ApiModelProperty(value = "点赞量")
    private Integer likeCount;
    /**
     * //粉丝点赞量
     */
    @ApiModelProperty(value = "粉丝点赞量")
    private Integer fansLikeCount;
    /**
     * //收藏量
     */
    @ApiModelProperty(value = "收藏量")
    private Integer collectCount;
    /**
     * //粉丝收藏量
     */
    @ApiModelProperty(value = "粉丝收藏量")
    private Integer fansCollectCount;
    /**
     * //分享量
     */
    @ApiModelProperty(value = "分享量")
    private Integer shareCount;
    /**
     * //粉丝分享量
     */
    @ApiModelProperty(value = "粉丝分享量")
    private Integer fansShareCount;

    /**
     * //是否点赞
     */
    @ApiModelProperty(value = "是否点赞")
    private Integer hasLike = Constant.HAS_LIKE_NO;

    /**
     * //是否点赞
     */
    @ApiModelProperty(value = "是否收藏")
    private Integer hasCollect = Constant.HAS_COLLECT_NO;
    /**
     * //是否点赞
     */
    @ApiModelProperty(value = "是否内部发布")
    private Integer hasInnerPublish;

    /**
     * //推荐资讯
     */
    @ApiModelProperty(value = "推荐资讯")
    private List<NewsBaseVo> recommend = Collections.EMPTY_LIST;

}

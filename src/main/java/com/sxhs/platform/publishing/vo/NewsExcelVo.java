package com.sxhs.platform.publishing.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * 用于咨询列表导出
 */
@Data
public class NewsExcelVo {

    @ExcelProperty(value = "序号", index = 0)
    private Integer id;

    @ExcelProperty(value = "标题", index = 1)
    private String title;

    @ExcelProperty(value = "发布机构", index = 2)
    private String orgName;

    @ExcelProperty(value = "类型", index = 3)
    private String type;

    @ExcelProperty(value = "发布时间", index = 4)
    private String publishTime;

    @ExcelProperty(value = "状态", index = 5)
    private String status;

    @ExcelProperty(value = "是否置顶", index = 6)
    private String hasTop;

}

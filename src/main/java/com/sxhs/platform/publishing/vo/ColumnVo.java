package com.sxhs.platform.publishing.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(value="栏目对象",description="栏目对象")
public class ColumnVo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    private Integer id;

    @ApiModelProperty(value = "栏目 id")
    private Integer columnId;

    @ApiModelProperty(value = "栏目名称")
    private String columnName;

    @ApiModelProperty(value = "机构id")
    private Integer orgId;

}

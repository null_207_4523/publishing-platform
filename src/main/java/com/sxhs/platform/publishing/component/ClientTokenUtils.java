package com.sxhs.platform.publishing.component;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sxhs.platform.system.config.RedisEnum;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.TimeUnit;

@Component
@Slf4j
public class ClientTokenUtils {

    private String GRANT_TYPE = "authorization_code";

    @Value("${passport.id:}")
    private String CLIENT_ID = "";

    @Value("${passport.secret:}")
    private String CLIENT_SECRET = "";

    @Value("${passport.url:}")
    private String PASSPORT_URL = "";

    private RestTemplate restTemplate = new RestTemplate();

    private String token = "access_token";

    @Autowired
    RedisTemplate redisTemplate;

    public String getToken() {
        String  access_token = "";
        if(redisTemplate.hasKey(RedisEnum.CLIENTTOKEN.getPath("0"))){
            access_token = (String) redisTemplate.opsForValue().get(RedisEnum.CLIENTTOKEN.getPath("0"));
        }else{
            try{
                String ChackTokenUrl = PASSPORT_URL+"/passport/oauth/token";
                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
                MultiValueMap postData  = new LinkedMultiValueMap();

                postData.add("scope", "read");
                postData.add("grant_type", "client_credentials");
                postData.add("client_id", CLIENT_ID);
                postData.add("client_secret", CLIENT_SECRET);
                final HttpEntity< MultiValueMap< String,String>> entity = new HttpEntity< MultiValueMap< String,String>>(postData,headers);
                ResponseEntity<String> responseEntity = restTemplate.postForEntity(ChackTokenUrl, entity, String.class);

                JSONObject json = null;
                if(responseEntity.getStatusCodeValue()== HttpStatus.OK.value()){
                    json = JSON.parseObject(responseEntity.getBody());
                }else{
                    log.error("客户端登录网络异常:{"+responseEntity.getStatusCode().value()+"}"+responseEntity.getStatusCode().getReasonPhrase());
                    throw new RuntimeException();
                }
                log.info("返回验证信息:"+json.toJSONString());
                if(StringUtils.isBlank(json.getString(token))){
                    throw new RuntimeException();
                }
                access_token = json.getString(token);
                redisTemplate.opsForValue().set(RedisEnum.CLIENTTOKEN.getPath("0"),access_token,120, TimeUnit.MINUTES);
            }catch (Exception e){
                e.printStackTrace();
                log.error("客户端登录失败！");
            }
        }
        return access_token;
    }


}

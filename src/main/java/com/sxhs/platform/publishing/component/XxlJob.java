package com.sxhs.platform.publishing.component;

import com.sxhs.platform.common.utils.DateUtils;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.log.XxlJobLogger;
import groovy.util.logging.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.concurrent.TimeUnit;

@Component
@Slf4j
public class XxlJob {

    /**
     * 1、简单任务示例（Bean模式）
     */
    @com.xxl.job.core.handler.annotation.XxlJob("sampleJobHandler")
    public ReturnT<String> demoJobHandler(String param) throws Exception {
        System.out.println(DateUtils.format(new Date()));
        XxlJobLogger.log("XXL-JOB, Hello World.");

        for (int i = 0; i < 5; i++) {
            XxlJobLogger.log("beat at:" + i);
            TimeUnit.SECONDS.sleep(2);
        }
        return ReturnT.SUCCESS;
    }

}

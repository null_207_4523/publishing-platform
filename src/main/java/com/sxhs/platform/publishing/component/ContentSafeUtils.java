package com.sxhs.platform.publishing.component;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.aliyun.openservices.shade.org.apache.commons.lang3.StringEscapeUtils;
import com.sxhs.platform.common.config.Constant;
import com.sxhs.platform.system.config.RedisEnum;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.ResourceHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.support.AllEncompassingFormHttpMessageConverter;
import org.springframework.http.converter.xml.SourceHttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
@Slf4j
public class ContentSafeUtils {

    @Autowired
    private ClientTokenUtils clientTokenUtils;

    @Value("${gwss.url:}")
    private String GWSS_URL;

    private RestTemplate restTemplate = new RestTemplate();

    private String StatusCode = "0";

    private String ScanCode = "200";

    @PostConstruct
    public void setRestTemplate(){
        List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
        messageConverters.add(new ByteArrayHttpMessageConverter());
        /** 解决中文乱码的converter */
        StringHttpMessageConverter stringHttpMessageConverter = new StringHttpMessageConverter(Charset.forName
                ("UTF-8"));
        messageConverters.add(stringHttpMessageConverter);
        messageConverters.add(new ResourceHttpMessageConverter());
        messageConverters.add(new SourceHttpMessageConverter());
        messageConverters.add(new AllEncompassingFormHttpMessageConverter());
        restTemplate.setMessageConverters(messageConverters);
    }

    public String testText(String content){
        String token = clientTokenUtils.getToken();

        String ChackTokenUrl = GWSS_URL+"/apis/appcenter/v1.0/contentSafe?access_token="+token;
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        MultiValueMap<String, Object> postData  = new LinkedMultiValueMap();

        //language=JSON
        String postEntity = "{\n" +
                "  \"typeId\": 1,\n" +
                "  \"textScanParamVo\": {\n" +
                "    \"bizType\": \"hskj\",\n" +
                "    \"textScanEntityList\": [{\n" +
                "      \"content\": \""+ StringEscapeUtils.escapeHtml4(getText(content))+"\"\n" +
                "    }]\n" +
                "  }\n" +
                "}";
        log.info("postEntity:{}",postEntity);
        final HttpEntity< String> entity = new HttpEntity< String>(postEntity,headers);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(ChackTokenUrl, entity, String.class);

        JSONObject json = null;
        if(responseEntity.getStatusCodeValue()== HttpStatus.OK.value()){
            log.info(responseEntity.getBody());
            json = JSON.parseObject(responseEntity.getBody());
            if(StatusCode.equals(json.get("code").toString())){
                if(ScanCode.equals(json.getJSONObject("data").getJSONObject("textScanReturn").getString("code"))){
                    for(Object ScanData:json.getJSONObject("data").getJSONObject("textScanReturn").getJSONArray("data")){
                        if(ScanCode.equals(((JSONObject)ScanData).getString("code"))){
                            JSONObject result = (JSONObject) ((JSONObject)ScanData).getJSONArray("results").get(0);
                            return result.getString("suggestion");
                        }
                    }
                }
            }
        }

        return "block";
    }

    public static String getText(String richText) {
        String regx = "(<.+?>)|(</.+?>)";
        Matcher matcher = Pattern.compile(regx).matcher(richText);
        while (matcher.find()) {
            // 替换图片
            richText = matcher.replaceAll("").replace(" ", "");
        }
        return richText;
    }

}

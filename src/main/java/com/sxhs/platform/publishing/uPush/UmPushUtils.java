package com.sxhs.platform.publishing.uPush;


import com.sxhs.platform.common.utils.StringUtils;
import com.sxhs.platform.publishing.uPush.android.*;
import com.sxhs.platform.publishing.uPush.ios.*;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class UmPushUtils {

    @Value("${Upush.androidAppkey}")
    private String androidAppkey;

    @Value("${Upush.androidAppMasterSecret}")
    private String androidAppMasterSecret;

    @Value("${Upush.iosAppkey}")
    private String iosAppkey;

    @Value("${Upush.iosAppMasterSecret}")
    private String iosAppMasterSecret;

    @Value("${Upush.productionMode}")
    private String productionMode;

    @Value("${Upush.aliasType}")
    private String aliasType;


    @Autowired
    private PushClient pushClient;


    /**
     * @Date 5:29 PM 2020/10/17
     * @Description 广播(broadcast)：向安装该App的所有设备发送消息
     * @Param [ticker, title, text, Extra, channelActivity, channelProperties]
     * @return void
     **/
    
    public void sendAndroidBroadcast(String ticker, String title, String text, Map<String,String> Extra,String channelActivity,String channelProperties) throws Exception {
        AndroidBroadcast broadcast = new AndroidBroadcast(androidAppkey, androidAppMasterSecret);
        broadcast.setTicker(ticker);
        broadcast.setTitle(title);
        broadcast.setText(text);
        broadcast.goAppAfterOpen();
        broadcast.setDisplayType(AndroidNotification.DisplayType.NOTIFICATION);
        // TODO Set 'production_mode' to 'false' if it's a test device.c
        // For how to register a test device, please see the developer doc.
        broadcast.setProductionMode();
        // Set customized fields
        for(String key:Extra.keySet()){
            broadcast.setExtraField(key, Extra.get(key));
        }

        //厂商通道相关参数
        if(StringUtils.isNotBlank(channelActivity)){
            broadcast.setChannelActivity(channelActivity);
        }
        if(StringUtils.isNotBlank(channelProperties)){
            broadcast.setChannelProperties(channelProperties);
        }
        pushClient.send(broadcast);
    }

    /**
     * @Date 5:33 PM 2020/10/17
     * @Description 单播(unicast)：向指定的设备发送消息。
     * @Param [deviceToken, ticker, title, text, Extra, channelActivity, channelProperties]
     * @return void
     **/
    
    public void sendAndroidUnicast(String deviceToken,String ticker, String title, String text, Map<String,String> Extra,String channelActivity,String channelProperties) throws Exception {
        AndroidUnicast unicast = new AndroidUnicast(androidAppkey, androidAppMasterSecret);
        // TODO Set your device token
        unicast.setDeviceToken(deviceToken);
        unicast.setTicker(ticker);
        unicast.setTitle(title);
        unicast.setText(text);
        unicast.goAppAfterOpen();
        unicast.setDisplayType(AndroidNotification.DisplayType.NOTIFICATION);
        // TODO Set 'production_mode' to 'false' if it's a test device.
        // For how to register a test device, please see the developer doc.
        unicast.setProductionMode();
        // Set customized fields
        for(String key:Extra.keySet()){
            unicast.setExtraField(key, Extra.get(key));
        }

        //厂商通道相关参数
        if(StringUtils.isNotBlank(channelActivity)){
            unicast.setChannelActivity(channelActivity);
        }
        if(StringUtils.isNotBlank(channelProperties)){
            unicast.setChannelProperties(channelProperties);
        }
        pushClient.send(unicast);
    }
    /**
     * @Date 5:34 PM 2020/10/17
     * @Description 组播(groupcast):：向满足特定条件的设备集合发送消息，例如: “特定版本”、”特定地域”等。
     * @Param []
     * @return void
     *  Construct the filter condition:
     *  "where":
     *	{
     *		"and":
     *		[
     *			{"tag":"test"},
     *			{"tag":"Test"}
     *		]
     *	}
     **/

    public void sendAndroidGroupcast(List<String> tags,String ticker, String title, String text,String channelActivity,String channelProperties) throws Exception {
        AndroidGroupcast groupcast = new AndroidGroupcast(androidAppkey, androidAppMasterSecret);
        JSONObject filterJson = new JSONObject();
        JSONObject whereJson = new JSONObject();
        JSONArray tagArray = new JSONArray();
        JSONObject tagJson = new JSONObject();
        for(String tag:tags){
            tagJson.put("tag", tag);
        }
        tagArray.put(tagJson);
        whereJson.put("and", tagArray);
        filterJson.put("where", whereJson);
        // Set filter condition into rootJson
        groupcast.setFilter(filterJson);
        groupcast.setTicker(ticker);
        groupcast.setTitle(title);
        groupcast.setText(text);
        groupcast.goAppAfterOpen();
        groupcast.setDisplayType(AndroidNotification.DisplayType.NOTIFICATION);
        // TODO Set 'production_mode' to 'false' if it's a test device.
        // For how to register a test device, please see the developer doc.
        groupcast.setProductionMode();
        //厂商通道相关参数
        if(StringUtils.isNotBlank(channelActivity)){
            groupcast.setChannelActivity(channelActivity);
        }
        if(StringUtils.isNotBlank(channelProperties)){
            groupcast.setChannelProperties(channelProperties);
        }
        pushClient.send(groupcast);
    }


    /**
     * @Date 10:35 AM 2020/10/19
     * @Description 自定义播(customizedcast)：开发者通过自有的alias进行推送，可以针对单个或者一批alias进行推送，也可以将alias存放到文件进行发送。
     * @Param []
     * @return void
     **/
    
    public void sendAndroidCustomizedcast(String alias,String ticker, String title, String text, String custom, String channelActivity,String channelProperties) throws Exception {
        AndroidCustomizedcast customizedcast = new AndroidCustomizedcast(androidAppkey, androidAppMasterSecret);
        // TODO Set your alias here, and use comma to split them if there are multiple alias.
        // And if you have many alias, you can also upload a file containing these alias, then
        // use file_id to send customized notification.
        customizedcast.setAlias(alias, aliasType);
        customizedcast.setTicker(ticker);
        customizedcast.setTitle(title);
        customizedcast.setText(text);
        customizedcast.goAppAfterOpen();
        customizedcast.setDisplayType(AndroidNotification.DisplayType.NOTIFICATION);
        // TODO Set 'production_mode' to 'false' if it's a test device.
        // For how to register a test device, please see the developer doc.
        if (isProductionMode()) {
            customizedcast.setProductionMode();
        } else {
            customizedcast.setTestMode();
        }
        //厂商通道相关参数
        if(StringUtils.isNotBlank(channelActivity)){
            customizedcast.setChannelActivity(channelActivity);
        }
        if(StringUtils.isNotBlank(channelProperties)){
            customizedcast.setChannelProperties(channelProperties);
        }
        if(StringUtils.isNotBlank(custom)){
            customizedcast.setCustomField(custom);
        }
        pushClient.send(customizedcast);
    }

    /**
     * @Date 5:29 PM 2020/10/17
     * @Description 广播(broadcast)：向安装该App的所有设备发送消息
     * @Param [ticker, title, text, Extra, channelActivity, channelProperties]
     * @return void
     **/


    public void sendIOSBroadcast(String title ,String subtitle , String body) throws Exception {
        IOSBroadcast broadcast = new IOSBroadcast(iosAppkey, iosAppMasterSecret);
        //alert值设置为字符串
        //broadcast.setAlert("IOS 广播测试");
        //alert的值设置为字典
        broadcast.setAlert(title , subtitle , body);
        broadcast.setBadge( 0);
        // TODO set 'production_mode' to 'true' if your app is under production mode
        broadcast.setProductionMode();
        pushClient.send(broadcast);
    }

    /**
     * @Date 10:57 AM 2020/10/19
     * @Description 单播(unicast)：向指定的设备发送消息。
     * @Param [deviceToken, title, subtitle, body]
     * @return void
     **/
    
    public void sendIOSUnicast(String deviceToken,String title ,String subtitle , String body) throws Exception {
        IOSUnicast unicast = new IOSUnicast(iosAppkey, iosAppMasterSecret);
        // TODO Set your device token
        unicast.setDeviceToken(deviceToken);
        //alert值设置为字符串
        //unicast.setAlert("IOS 单播测试");
        //alert的值设置为字典
        unicast.setAlert(title,subtitle,body);
        unicast.setBadge( 0);
        // TODO set 'production_mode' to 'true' if your app is under production mode
        unicast.setTestMode();
        pushClient.send(unicast);
    }

    /**
     * @Date 5:34 PM 2020/10/17
     * @Description 组播(groupcast):：向满足特定条件的设备集合发送消息，例如: “特定版本”、”特定地域”等。
     * @Param []
     * @return void
     *  Construct the filter condition:
     *  "where":
     *	{
     *		"and":
     *		[
     *			{"tag":"test"},
     *			{"tag":"Test"}
     *		]
     *	}
     **/
    public void sendIOSGroupcast(List<String> tags,String title ,String subtitle , String body) throws Exception {
        IOSGroupcast groupcast = new IOSGroupcast(iosAppkey, iosAppMasterSecret);
        JSONObject filterJson = new JSONObject();
        JSONObject whereJson = new JSONObject();
        JSONArray tagArray = new JSONArray();
        JSONObject tagJson = new JSONObject();
        for(String tag:tags){
            tagJson.put("tag", tag);
        }
        tagArray.put(tagJson);
        whereJson.put("and", tagArray);
        filterJson.put("where", whereJson);
        System.out.println(filterJson.toString());

        // Set filter condition into rootJson
        groupcast.setFilter(filterJson);
        groupcast.setAlert(title ,subtitle,body);
        groupcast.setBadge( 0);
        // TODO set 'production_mode' to 'true' if your app is under production mode
        groupcast.setTestMode();
        pushClient.send(groupcast);
    }

    /**
     * @Date 10:35 AM 2020/10/19
     * @Description 自定义播(customizedcast)：开发者通过自有的alias进行推送，可以针对单个或者一批alias进行推送，也可以将alias存放到文件进行发送。
     * @Param []
     * @return void
     **/
    public void sendIOSCustomizedcast(String alias,String title ,String subtitle , String body, String custom) throws Exception {
        IOSCustomizedcast customizedcast = new IOSCustomizedcast(iosAppkey, iosAppMasterSecret);
        // And if you have many alias, you can also upload a file containing these alias, then
        // use file_id to send customized notification.
        customizedcast.setAlias(alias, aliasType);
        //alert的值设置为字典
        customizedcast.setAlert(title , subtitle , body);
        customizedcast.setBadge( 0);
        // TODO set 'production_mode' to 'true' if your app is under production mode
        if (isProductionMode()) {
            customizedcast.setProductionMode();
        } else {
            customizedcast.setTestMode();
        }

        if(StringUtils.isNotBlank(custom)){
            customizedcast.setCustomizedField("message", custom);
        }
        pushClient.send(customizedcast);
    }

    public boolean isProductionMode() {
        return Boolean.parseBoolean(productionMode);
    }

}

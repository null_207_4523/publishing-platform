package com.sxhs.platform.publishing.message.api.requestDTO;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@ToString
@NoArgsConstructor
public class RequestBaseMessageDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /** 归属类目  */
    private String category;

    /** 归属类目名称 */
    private String categoryName;

    /** 业务事件标识 */
    @NotBlank(message = "业务事件标识不能为空")
    private String eventType;

    /** 业务事件名称 */
    @NotBlank(message = "业务事件名称不能为空")
    private String eventName;

    /** 应用  */
    @NotNull(message = "应用标识不能为空")
    private Integer appType;

    /** 应用名称 */
    @NotBlank(message = "应用名称不能为空")
    private String appName;

    @NotBlank(message = "租户信息不能为空")
    private String tenantId;

}

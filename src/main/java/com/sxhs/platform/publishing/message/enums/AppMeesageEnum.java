package com.sxhs.platform.publishing.message.enums;

/**
 *      消息接收 应用
 */
public enum AppMeesageEnum {

    ZI_XUN(1, "咨询");

    private int type;

    private String name;

    AppMeesageEnum(int type, String name) {
        this.type = type;
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static AppMeesageEnum getEnumByKey(int type) {
        for (AppMeesageEnum temp : AppMeesageEnum.values()) {
            if (temp.getType() == type) {
                return temp;
            }
        }
        return null;
    }
}

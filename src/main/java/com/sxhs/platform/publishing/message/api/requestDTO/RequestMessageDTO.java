package com.sxhs.platform.publishing.message.api.requestDTO;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

@Data
@ToString
@NoArgsConstructor
public class RequestMessageDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "消息体属性不能为空")
    @Size(min = 1, message = "消息体至少要有一个对象")
    private List<@Valid RequestMessageContentDTO> messageContentDTOList;

    @NotNull(message = "缺少应用信息不能为空")
    private @Valid RequestBaseMessageDTO baseMessageDTO;

}

package com.sxhs.platform.publishing.message.enums;

/**
 *      消息类目标签
 */
public enum CategoryTagEnum {

    PRAISE("TAG-PRAISE", "点赞"),

    COMMENT("TAG-COMMENT", "评论"),

    ATTENTION("TAG-ATTENTION", "关注"),

    PLATFORM("TAG-PLATFORM", "平台消息"),

    CONTENTSAFE("TAG-CONTENTSAFE", "资讯内容审核");

    private String tags;

    private String name;

    CategoryTagEnum(String tags, String name) {
        this.tags = tags;
        this.name = name;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static CategoryTagEnum getEnumByKey(String tags) {
        for (CategoryTagEnum temp : CategoryTagEnum.values()) {
            if (temp.getTags().equals(tags)) {
                return temp;
            }
        }
        return null;
    }
}

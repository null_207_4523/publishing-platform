package com.sxhs.platform.publishing.message.api;

import com.google.common.collect.Maps;
import com.sxhs.platform.common.utils.ApiResult;
import com.sxhs.platform.common.utils.BDException;
import com.sxhs.platform.publishing.domain.PushAppEventConfigDO;
import com.sxhs.platform.publishing.domain.PushCategoryConfigDO;
import com.sxhs.platform.publishing.message.api.requestDTO.RequestMessageDTO;
import com.sxhs.platform.publishing.message.dto.MessageDTO;
import com.sxhs.platform.publishing.message.enums.CategoryTagEnum;
import com.sxhs.platform.publishing.service.MessageSendService;
import com.sxhs.platform.publishing.service.PushAppEventConfigService;
import com.sxhs.platform.publishing.service.PushCategoryConfigService;
import com.sxhs.platform.system.config.Pass;
import com.sxhs.platform.system.config.rocketMq.aliyun.MqConfig;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.UUID;
@Slf4j
@Api(value = "APP消息", description = "app消息接口")
@RestController
@RequestMapping("/app/api/message")
public class AppMessageApi {

    @Autowired
    private PushAppEventConfigService pushAppEventConfigService;

    @Autowired
    private PushCategoryConfigService pushCategoryConfigService;

    @Autowired
    private MessageSendService messageSendService;

    @Autowired
    private ModelMapper modelMaper;

    @Autowired
    private MqConfig mqConfig;

    @Pass
    @ApiOperation("APP 消息接收")
    @RequestMapping(value = "/received",
            method = RequestMethod.POST,
                consumes="application/json",
                produces="application/json"
    )
    public ApiResult receiveAndSaveAppMessage(
            @RequestBody  @Valid RequestMessageDTO message,
            BindingResult bindingResult
    ){

        if (bindingResult.hasErrors()) {
            return ApiResult.fail(bindingResult.getAllErrors().get(0).getDefaultMessage());
        }

        try {
            this.vaideConfig(message);

            MessageDTO dto = modelMaper.map(message, MessageDTO.class);
            // 所属Tag
            String tags = message.getBaseMessageDTO().getCategory();
            String keys = "KEY_" + UUID.randomUUID();
            messageSendService.sendMessageServer(mqConfig.getTopic(), tags, keys, dto);
            return ApiResult.successMsg("接收消息成功！");
        } catch (BDException bd) {
            return ApiResult.fail(bd.getMsg());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ApiResult.fail("消息服务调用异常");

    }


    public void vaideConfig(RequestMessageDTO message) {

        Map<String,Object> params = Maps.newHashMap();
        params.put("appType", message.getBaseMessageDTO().getAppType());
        params.put("eventType", message.getBaseMessageDTO().getEventType());
        List<PushAppEventConfigDO> eventList = pushAppEventConfigService.list(params);
        PushCategoryConfigDO category = null;
        if (null == eventList || eventList.size() == 0) {
//            throw new BDException(this.getMessageInfo(message,"不支持移动端消息推送"));
            // 不存在则放入平台类目
            category = new PushCategoryConfigDO();
            category.setCategoryTag(CategoryTagEnum.PLATFORM.getTags());
            category.setCategoryName(CategoryTagEnum.PLATFORM.getName());
        } else {
            PushAppEventConfigDO eventConfig = eventList.get(0);
            if (eventConfig.getOnlineStutus() != 1) {
                throw new BDException(this.getMessageInfo(message,"已关闭消息接收"));
            }
            category = pushCategoryConfigService.get(eventConfig.getCategoryId());
            if (category == null || category.getOpenStatus() != 1) {
                throw new BDException(this.getMessageInfo(message,"内部异常，请联系管理员。 消息所属类目未配置或已禁用"));
            }
        }
        message.getBaseMessageDTO().setCategory(category.getCategoryTag());
        message.getBaseMessageDTO().setCategoryName(category.getCategoryName());
        log.info(this.getMessageInfo(message, "消息接收处理..."));
    }

    public String getMessageInfo(RequestMessageDTO message, String appendMessage) {
        StringBuilder sb = new StringBuilder();
        sb.append("应用：【").
                    append( message.getBaseMessageDTO().getAppName())
                .append("】;")
                    .append("业务：【")
                        .append(message.getBaseMessageDTO().getEventName())
                    .append("】;")
                        .append("--")
                            .append(appendMessage);
        return sb.toString();
    }
}

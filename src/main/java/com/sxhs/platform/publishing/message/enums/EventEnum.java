package com.sxhs.platform.publishing.message.enums;

public enum EventEnum {



    PUBLISH("PUBLISH", "发布");

    private String type;

    private String name;


    EventEnum(String type, String name) {
        this.type = type;
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static EventEnum getEnumByKey(String event) {
        for (EventEnum temp : EventEnum.values()) {
            if (temp.getType().equals(event)) {
                return temp;
            }
        }
        return null;
    }
}

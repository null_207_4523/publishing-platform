package com.sxhs.platform.publishing.message.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Data
@ToString
@NoArgsConstructor
@ApiModel(description = "消息实体")
public class MessageDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "messageContentDTOList不能为空")
    @ApiModelProperty(value = "消息发送实体")
    private List<@Valid MessageContentDTO> messageContentDTOList;
    @NotNull(message = "baseMessageDTO不能为空")
    @ApiModelProperty(value = "消息应用实体")
    private @Valid BaseMessageDTO baseMessageDTO;
}

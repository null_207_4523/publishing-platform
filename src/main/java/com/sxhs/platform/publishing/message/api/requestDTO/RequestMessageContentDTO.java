package com.sxhs.platform.publishing.message.api.requestDTO;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
@ToString
@NoArgsConstructor
public class RequestMessageContentDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /** 消息接收用户ID，多个使用","逗号分隔 */
    @NotBlank(message = "请选择科目")
    private String receiverIds;

    /** 消息标题 */
    @NotBlank(message = "请选择科目")
    private String title;

    /** 消息内容  */
    @NotBlank(message = "请选择科目")
    private String content;

    /** 推送信息内容(额外参数，用于业务跳转) */
    private String msgBody;

    /** h5 跳转地址*/
    private String url;
}

package com.sxhs.platform.publishing.message.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@Data
@ToString
@NoArgsConstructor
@ApiModel(description = "MessageContentDTO 消息信息实体")
public class MessageContentDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /** 消息接收用户ID，多个使用","逗号分隔 */
    @ApiModelProperty(value = "消息接收用户ID，多个使用,逗号分隔 不能超过500")
    @NotEmpty(message = "消息接收用户ID不能为空")
    private String receiverIds;

    /** 消息标题 */
    @ApiModelProperty(value = "消息标题")
    @NotEmpty(message = "消息标题不能为空")
    private String title;

    /** 消息内容  */
    @NotEmpty(message = "消息内容不能为空")
    @ApiModelProperty(value = "消息内容")
    private String content;

    /** 推送信息内容(额外参数，用于业务跳转) */
    @ApiModelProperty(value = "消息扩展信息 json字符串")
    private String msgBody;

    @ApiModelProperty(value = "H5跳转地址")
    private String url;
}

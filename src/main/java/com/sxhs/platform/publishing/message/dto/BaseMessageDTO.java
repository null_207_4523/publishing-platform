package com.sxhs.platform.publishing.message.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
@Data
@ToString
@NoArgsConstructor
@ApiModel(description = "消息应用实体")
public class BaseMessageDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /** 归属类目  */
    @ApiModelProperty(value = "归属类目")
    private String category;

    /** 归属类目名称 */
    @ApiModelProperty(value = "归属类目名称")
    private String categoryName;

    /** 业务事件标识 */
    @ApiModelProperty(value = "业务事件标识")
    @NotEmpty(message = "业务事件标识不能为空")
    private String eventType;

    /** 业务事件名称 */
    @ApiModelProperty(value = "业务事件名称")
    @NotEmpty(message = "业务事件名称不能为空")
    private String eventName;

    /** 应用  */
    @ApiModelProperty(value = "应用id")
    @NotNull(message = "应用id不能为空")
    private Integer appType;

    /** 应用名称 */
    @ApiModelProperty(value = "应用名称")
    @NotEmpty(message = "应用名称不能为空")
    private String appName;

}

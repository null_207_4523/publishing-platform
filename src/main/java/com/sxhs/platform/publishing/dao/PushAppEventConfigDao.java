package com.sxhs.platform.publishing.dao;

import com.sxhs.platform.publishing.domain.PushAppEventConfigDO;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2020-10-30 16:48:35
 */
@Mapper
public interface PushAppEventConfigDao {

	PushAppEventConfigDO get(Integer id);
	
	List<PushAppEventConfigDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(PushAppEventConfigDO pushAppEventConfig);
	
	int update(PushAppEventConfigDO pushAppEventConfig);
	
	int remove(Integer id);
	
	int batchRemove(Integer[] ids);
}

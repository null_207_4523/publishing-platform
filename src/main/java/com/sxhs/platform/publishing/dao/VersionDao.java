package com.sxhs.platform.publishing.dao;


import com.sxhs.platform.publishing.domain.VersionDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2020-11-09 11:39:55
 */
@Mapper
public interface VersionDao {

	VersionDO get(Integer id);
	
	List<VersionDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(VersionDO version);
	
	int update(VersionDO version);
	
	int remove(Integer id);
	
	int batchRemove(Integer[] ids);

	VersionDO getNewestVersion(Map<String, Object> param);
}

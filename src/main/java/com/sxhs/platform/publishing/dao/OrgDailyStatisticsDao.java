package com.sxhs.platform.publishing.dao;

import com.sxhs.platform.publishing.domain.OrgDailyStatisticsDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 机构资讯每日统计表
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2020-09-21 14:10:44
 */
@Mapper
public interface OrgDailyStatisticsDao {

	OrgDailyStatisticsDO get(Integer id);
	
	List<OrgDailyStatisticsDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(OrgDailyStatisticsDO orgDailyStatistics);
	
	int update(OrgDailyStatisticsDO orgDailyStatistics);
	
	int remove(Integer id);
	
	int batchRemove(Integer[] ids);

	int getCountByDate(Map<String, Object> map);

	List<OrgDailyStatisticsDO> listByLimit(Map<String, Object> map);

	int countlistByLimit(Map<String, Object> map);

}

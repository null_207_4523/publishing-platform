package com.sxhs.platform.publishing.dao;

import com.sxhs.platform.publishing.domain.PushCategoryConfigDO;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2020-10-30 16:48:19
 */
@Mapper
public interface PushCategoryConfigDao {

	PushCategoryConfigDO get(Integer id);
	
	List<PushCategoryConfigDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(PushCategoryConfigDO pushCategoryConfig);
	
	int update(PushCategoryConfigDO pushCategoryConfig);
	
	int remove(Integer id);
	
	int batchRemove(Integer[] ids);
}

package com.sxhs.platform.publishing.dao;

import com.sxhs.platform.publishing.domain.CommentLikeDO;
import com.sxhs.platform.publishing.vo.CommentVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 评论点赞 
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2020-09-29 17:00:26
 */
@Mapper
public interface CommentLikeDao {

	CommentLikeDO get(Integer id);
	
	List<CommentLikeDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(CommentLikeDO commentLike);
	
	int update(CommentLikeDO commentLike);
	
	int remove(Integer id);
	
	int batchRemove(Integer[] ids);

    CommentVo getCommentLikeList(Map<String, Object> params);
}

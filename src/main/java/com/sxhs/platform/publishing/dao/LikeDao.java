package com.sxhs.platform.publishing.dao;

import com.sxhs.platform.publishing.domain.LikeDO;
import com.sxhs.platform.publishing.domain.OrgDailyStatisticsDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 资讯点赞
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2020-09-21 14:10:44
 */
@Mapper
public interface LikeDao {

	LikeDO get(Integer id);
	
	List<LikeDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(LikeDO like);
	
	int update(LikeDO like);
	
	int remove(Integer id);
	
	int batchRemove(Integer[] ids);

	OrgDailyStatisticsDO getlikeCountByDate(Map<String, Object> params);
}

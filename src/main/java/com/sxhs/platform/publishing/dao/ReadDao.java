package com.sxhs.platform.publishing.dao;

import com.sxhs.platform.publishing.domain.OrgDailyStatisticsDO;
import com.sxhs.platform.publishing.domain.ReadDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 资讯阅读
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2020-09-21 14:10:45
 */
@Mapper
public interface ReadDao {

	ReadDO get(Integer id);
	
	List<ReadDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(ReadDO read);
	
	int update(ReadDO read);
	
	int remove(Integer id);
	
	int batchRemove(Integer[] ids);

	OrgDailyStatisticsDO getReadCountByDate(Map<String, Object> params);
}

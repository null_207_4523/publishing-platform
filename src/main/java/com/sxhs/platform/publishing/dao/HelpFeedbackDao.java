package com.sxhs.platform.publishing.dao;

import com.sxhs.platform.publishing.domain.HelpFeedbackDO;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

/**
 * 帮助反馈
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2020-10-17 10:34:40
 */
@Mapper
public interface HelpFeedbackDao {

	HelpFeedbackDO get(Integer id);
	
	List<HelpFeedbackDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(HelpFeedbackDO helpFeedback);
	
	int update(HelpFeedbackDO helpFeedback);
	
	int remove(Integer id);
	
	int batchRemove(Integer[] ids);
}

package com.sxhs.platform.publishing.dao;

import com.sxhs.platform.publishing.domain.LayoutDO;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

/**
 * 应用布局
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2020-12-03 16:33:48
 */
@Mapper
public interface LayoutDao {

	LayoutDO get(Integer id);
	
	List<LayoutDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(LayoutDO layout);
	
	int update(LayoutDO layout);
	
	int remove(Integer id);
	
	int batchRemove(Integer[] ids);
}

package com.sxhs.platform.publishing.dao;

import com.sxhs.platform.publishing.domain.OrgSubscriptionDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 资讯机构订阅
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2020-09-21 14:10:45
 */
@Mapper
public interface OrgSubscriptionDao {

	OrgSubscriptionDO get(Integer id);
	
	List<OrgSubscriptionDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(OrgSubscriptionDO orgSubscription);
	
	int update(OrgSubscriptionDO orgSubscription);
	
	int remove(Integer id);
	
	int batchRemove(Integer[] ids);

    Integer getOrgSubscriptionCountByDate(Map<String, Object> map);
}

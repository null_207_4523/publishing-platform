package com.sxhs.platform.publishing.dao;

import com.sxhs.platform.publishing.domain.NewsDO;
import com.sxhs.platform.publishing.vo.NewsBaseVo;
import com.sxhs.platform.publishing.vo.NewsDetailsVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 资讯信息
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2020-09-21 14:10:44
 */
@Mapper
public interface NewsDao {

	NewsDO get(Integer id);
	
	List<NewsDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(NewsDO news);
	
	int update(NewsDO news);
	
	int remove(Integer id);
	
	int batchRemove(Integer[] ids);

	List<NewsBaseVo> listByColumn(Map<String, Object> map);

	int updateCount(Map<String, Object> map);

    List<NewsBaseVo> randomNewsByColumn(Map<String, Object> map);

	List<NewsBaseVo> collectNewsList(Map<String, Object> params);

	int collectNewsCount(Map<String, Object> params);

    NewsDetailsVo getNewsDetails(Map<String, Object> params);

	NewsDetailsVo getNewsCollectLikeDetails(Map<String, Object> params);

	Integer getNewsCountByDate(Map<String, Object> params);

    List<NewsDO> newslist(Map<String, Object> map);

	int countNewslist(Map<String, Object> map);
}

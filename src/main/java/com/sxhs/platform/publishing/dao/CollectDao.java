package com.sxhs.platform.publishing.dao;

import com.sxhs.platform.publishing.domain.CollectDO;
import com.sxhs.platform.publishing.domain.OrgDailyStatisticsDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 资讯收藏
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2020-09-21 14:10:43
 */
@Mapper
public interface CollectDao {

	CollectDO get(Integer id);
	
	List<CollectDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(CollectDO collect);
	
	int update(CollectDO collect);
	
	int remove(Integer id);
	
	int batchRemove(Integer[] ids);

	OrgDailyStatisticsDO getCollectCountByDate(Map<String, Object> params);
}

package com.sxhs.platform.publishing.dao;

import com.sxhs.platform.publishing.domain.CommentDO;
import com.sxhs.platform.publishing.domain.OrgDailyStatisticsDO;
import com.sxhs.platform.publishing.vo.CommentVo;
import com.sxhs.platform.publishing.vo.NewsDetailsVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 资讯评论
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2020-09-21 14:10:43
 */
@Mapper
public interface CommentDao {

	CommentDO get(Integer id);
	
	List<CommentDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(CommentDO comment);
	
	int update(CommentDO comment);
	
	int remove(Integer id);
	
	int batchRemove(Integer[] ids);

	List<CommentDO> commentReplysList(Map<String, Object> map);

	int countCommentReplys(Map<String, Object> params);

	/**
	 * 资讯的全部评论集合
	 * @param params
	 * @return
	 */
	NewsDetailsVo getNewsCommentsList(Map<String, Object> params);

	/**
	 *  资讯的评论集合
	 * @param params
	 * @return
	 */
	List<CommentVo> getNewsCommentList(Map<String, Object> params);

	OrgDailyStatisticsDO getCommentCountByDate(Map<String, Object> params);
}

package com.sxhs.platform.publishing.dao;

import com.sxhs.platform.publishing.domain.ColumnDO;
import com.sxhs.platform.publishing.vo.ColumnVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 栏目表
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2020-09-21 14:10:43
 */
@Mapper
public interface ColumnDao {

	ColumnDO get(Integer id);
	
	List<ColumnDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(ColumnDO column);
	
	int update(ColumnDO column);
	
	int remove(Integer id);
	
	int batchRemove(Integer[] ids);

	List<ColumnVo> orgColumnlist(Integer orgId);

	List<ColumnVo> sysColumnlist(Map<String, Object> map);
}

package com.sxhs.platform.publishing.dao;

import com.sxhs.platform.publishing.domain.NewsReviewReasonDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 资讯审核原因
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2021-01-14 09:57:48
 */
@Mapper
public interface NewsReviewReasonDao {

	NewsReviewReasonDO get(Integer id);
	
	List<NewsReviewReasonDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(NewsReviewReasonDO newsReviewReason);
	
	int update(NewsReviewReasonDO newsReviewReason);
	
	int remove(Integer id);
	
	int batchRemove(Integer[] ids);

	/**
	 *  根据newsId获取最新的一条为通过原因数据
	 * @param newsId
	 * @return
	 */
	NewsReviewReasonDO getByNewsId(Integer newsId);
}

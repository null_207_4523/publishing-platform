package com.sxhs.platform.publishing.dao;

import com.sxhs.platform.publishing.domain.OrgColumnDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 机构栏目 
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2020-10-14 09:24:58
 */
@Mapper
public interface OrgColumnDao {

	OrgColumnDO get(Integer id);
	
	List<OrgColumnDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(OrgColumnDO orgColumn);
	
	int update(OrgColumnDO orgColumn);
	
	int remove(Integer id);
	
	int batchRemove(Integer[] ids);
}

package com.sxhs.platform.publishing.dao;

import com.sxhs.platform.publishing.domain.PushLogDO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;


@Mapper
public interface PushLogDao {


	PushLogDO get(Integer id);

	List<PushLogDO> list(Map<String,Object> map);

	int count(Map<String,Object> map);

	int save(PushLogDO pushLog);

	int update(PushLogDO pushLog);

	int remove(Integer id);

	int batchRemove(Integer[] ids);

	int insertBatch(List<PushLogDO> list);

}

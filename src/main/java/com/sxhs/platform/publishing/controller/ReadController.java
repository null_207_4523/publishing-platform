package com.sxhs.platform.publishing.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sxhs.platform.publishing.domain.ReadDO;
import com.sxhs.platform.publishing.service.ReadService;
import com.sxhs.platform.common.utils.PageUtils;
import com.sxhs.platform.common.utils.Query;
import com.sxhs.platform.common.utils.R;

/**
 * 资讯阅读
 * 
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2020-09-21 14:10:45
 */
 
@Controller
@RequestMapping("/publishing/read")
public class ReadController {
	@Autowired
	private ReadService readService;
	
	@GetMapping()
	@RequiresPermissions("publishing:read:read")
	String Read(){
	    return "publishing/read/read";
	}
	
	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("publishing:read:read")
	public PageUtils list(@RequestParam Map<String, Object> params){
		//查询列表数据
        Query query = new Query(params);
		List<ReadDO> readList = readService.list(query);
		int total = readService.count(query);
		PageUtils pageUtils = new PageUtils(readList, total);
		return pageUtils;
	}
	
	@GetMapping("/add")
	@RequiresPermissions("publishing:read:add")
	String add(){
	    return "publishing/read/add";
	}

	@GetMapping("/edit/{id}")
	@RequiresPermissions("publishing:read:edit")
	String edit(@PathVariable("id") Integer id,Model model){
		ReadDO read = readService.get(id);
		model.addAttribute("read", read);
	    return "publishing/read/edit";
	}
	
	/**
	 * 保存
	 */
	@ResponseBody
	@PostMapping("/save")
	@RequiresPermissions("publishing:read:add")
	public R save( ReadDO read){
		if(readService.save(read)>0){
			return R.ok();
		}
		return R.error();
	}
	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping("/update")
	@RequiresPermissions("publishing:read:edit")
	public R update( ReadDO read){
		readService.update(read);
		return R.ok();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/remove")
	@ResponseBody
	@RequiresPermissions("publishing:read:remove")
	public R remove( Integer id){
		if(readService.remove(id)>0){
		return R.ok();
		}
		return R.error();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/batchRemove")
	@ResponseBody
	@RequiresPermissions("publishing:read:batchRemove")
	public R remove(@RequestParam("ids[]") Integer[] ids){
		readService.batchRemove(ids);
		return R.ok();
	}
	
}

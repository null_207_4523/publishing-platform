package com.sxhs.platform.publishing.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.sxhs.platform.publishing.message.dto.BaseMessageDTO;
import com.sxhs.platform.publishing.message.dto.MessageDTO;
import com.sxhs.platform.publishing.message.dto.MessageContentDTO;
import com.sxhs.platform.publishing.message.enums.CategoryTagEnum;
import com.sxhs.platform.publishing.service.MessageSendService;
import com.sxhs.platform.publishing.uPush.UmPushUtils;
import com.sxhs.platform.system.config.rocketMq.aliyun.MqConfig;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/demo/v1")
public class DemoMqController {

    @Autowired
    MqConfig mqConfig;

    @Autowired
    MessageSendService messageSendService;

    @RequestMapping("/test1")
    public String test(int type) throws Exception {

//        JSONObject extra = new JSONObject();
//        extra.put("xxx", "xxxx");
//        List<MessageDTO> list = Lists.newArrayList();
//
//        // 用户相关信息
//        List<MessageContentDTO>  messageList = new ArrayList<>();
//        MessageContentDTO a = new MessageContentDTO();
//        a.setReceiverIds("1,2,3");
//        a.setTitle("某某文章点赞");
//        a.setContent("文章内容");
//        a.setMsgBody(JSON.toJSONString(extra));
//        a.setUrl("https://www.baidu.com");
//        messageList.add(a);
//
//        // 应用信息
//        BaseMessageDTO base = new BaseMessageDTO();
//        base.setAppType(1);
//        base.setAppName("资讯服务");
//        base.setEventType("xxx");
//        base.setEventName("热门活动点赞");
//        MessageDTO bodyMessage = messageSendService.getBodyMessage(messageList, base);
//        // 类目属性不需要填充， 发送消息会根据topic 填充
//
//        // 发送消息
//        String tag = CategoryTagEnum.COMMENT.getTags();
//        if (type == 1) {
//            tag = CategoryTagEnum.PRAISE.getTags();
//        }
//        messageSendService.sendMessage(mqConfig.getTopic(), tag, UUID.randomUUID() + "", bodyMessage);

        return "success";
    }
}

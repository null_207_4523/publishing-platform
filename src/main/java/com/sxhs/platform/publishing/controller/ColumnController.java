package com.sxhs.platform.publishing.controller;

import com.sxhs.platform.common.domain.Tree;
import com.sxhs.platform.common.utils.*;
import com.sxhs.platform.publishing.domain.ColumnDO;
import com.sxhs.platform.publishing.service.ColumnService;
import com.sxhs.platform.publishing.service.NewsService;
import com.sxhs.platform.publishing.vo.ColumnVo;
import com.sxhs.platform.system.domain.UserDO;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 栏目表
 * 
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2020-09-21 14:10:43
 */
 
@Controller
@RequestMapping("/publishing/column")
public class ColumnController {
	@Autowired
	private ColumnService columnService;
	private String prefix = "publishing/column";
	@Autowired
	private NewsService newsService;
	
	@GetMapping()
	@RequiresPermissions("publishing:column:column")
	String Column(){
	    return "publishing/column/column";
	}
	
	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("publishing:column:column")
	public PageUtils list(@RequestParam Map<String, Object> params){
		//查询列表数据
        Query query = new Query(params);
		List<ColumnDO> columnList = columnService.list(query);
		int total = columnService.count(query);
		PageUtils pageUtils = new PageUtils(columnList, total);
		return pageUtils;
	}
	
	@GetMapping("/add")
	@RequiresPermissions("publishing:column:add")
	String add(){
	    return "publishing/column/add";
	}

	@GetMapping("/edit/{id}")
	@RequiresPermissions("publishing:column:edit")
	String edit(@PathVariable("id") Integer id,Model model){
		ColumnDO column = columnService.get(id);
		model.addAttribute("column", column);
	    return "publishing/column/edit";
	}
	
	/**
	 * 保存
	 */
	@ResponseBody
	@PostMapping("/save")
	@RequiresPermissions("publishing:column:add")
	public ApiResult save(ColumnDO column){
		UserDO userDO = ShiroUtils.getUser();
		column.setCreatedUser(userDO.getUsername());
		column.setCreatedUserId(userDO.getUserId().toString());
		column.setCreatedTime(new Date());
		return columnService.save(column);
	}
	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping("/update")
	@RequiresPermissions("publishing:column:edit")
	public ApiResult update( ColumnDO column){
		UserDO userDO = ShiroUtils.getUser();
		column.setUpdateTime(new Date());
		column.setUpdateUser(userDO.getUsername());
		column.setUpdateUserId(userDO.getUserId().toString());
		return columnService.update(column);
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/remove")
	@ResponseBody
	@RequiresPermissions("publishing:column:remove")
	public ApiResult remove( Integer id){
		return columnService.remove(id);
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/batchRemove")
	@ResponseBody
	@RequiresPermissions("publishing:column:batchRemove")
	public R remove(@RequestParam("ids[]") Integer[] ids){
		columnService.batchRemove(ids);
		return R.ok();
	}

	@GetMapping("/tree")
	@ResponseBody
	public Tree<ColumnVo> tree(@RequestParam("orgId") Integer orgId) {
		//查询列表数据
		Tree<ColumnVo> tree = new Tree<ColumnVo>();
		tree = columnService.getTree(orgId);
		return tree;
	}

	/**
	 *
	 * @return
	 */
	@GetMapping("/treeView")
	String treeView() {
		return  prefix + "/columnTree";
	}
	
}

package com.sxhs.platform.publishing.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sxhs.platform.publishing.domain.PushLogDO;
import com.sxhs.platform.publishing.service.PushLogService;
import com.sxhs.platform.common.utils.PageUtils;
import com.sxhs.platform.common.utils.Query;
import com.sxhs.platform.common.utils.R;

/**
 * 消息推送记录 
 * 
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2020-10-25 18:40:23
 */
 
@Controller
@RequestMapping("/publishing/pushLog")
public class PushLogController {
	@Autowired
	private PushLogService pushLogService;
	
	@GetMapping()
	@RequiresPermissions("publishing:pushLog:pushLog")
	String PushLog(){
	    return "publishing/pushLog/pushLog";
	}
	
	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("publishing:pushLog:pushLog")
	public PageUtils list(@RequestParam Map<String, Object> params){
		//查询列表数据
        Query query = new Query(params);
		List<PushLogDO> pushLogList = pushLogService.list(query);
		int total = pushLogService.count(query);
		PageUtils pageUtils = new PageUtils(pushLogList, total);
		return pageUtils;
	}
	
	@GetMapping("/add")
	@RequiresPermissions("publishing:pushLog:add")
	String add(){
	    return "publishing/pushLog/add";
	}

	@GetMapping("/edit/{id}")
	@RequiresPermissions("publishing:pushLog:edit")
	String edit(@PathVariable("id") Integer id,Model model){
		PushLogDO pushLog = pushLogService.get(id);
		model.addAttribute("pushLog", pushLog);
	    return "publishing/pushLog/edit";
	}
	
	/**
	 * 保存
	 */
	@ResponseBody
	@PostMapping("/save")
	@RequiresPermissions("publishing:pushLog:add")
	public R save( PushLogDO pushLog){
		if(pushLogService.save(pushLog)>0){
			return R.ok();
		}
		return R.error();
	}
	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping("/update")
	@RequiresPermissions("publishing:pushLog:edit")
	public R update( PushLogDO pushLog){
		pushLogService.update(pushLog);
		return R.ok();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/remove")
	@ResponseBody
	@RequiresPermissions("publishing:pushLog:remove")
	public R remove( Integer id){
		if(pushLogService.remove(id)>0){
		return R.ok();
		}
		return R.error();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/batchRemove")
	@ResponseBody
	@RequiresPermissions("publishing:pushLog:batchRemove")
	public R remove(@RequestParam("ids[]") Integer[] ids){
		pushLogService.batchRemove(ids);
		return R.ok();
	}
	
}

package com.sxhs.platform.publishing.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sxhs.platform.publishing.domain.ShareDO;
import com.sxhs.platform.publishing.service.ShareService;
import com.sxhs.platform.common.utils.PageUtils;
import com.sxhs.platform.common.utils.Query;
import com.sxhs.platform.common.utils.R;

/**
 * 资讯分享
 * 
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2020-09-21 14:10:45
 */
 
@Controller
@RequestMapping("/publishing/share")
public class ShareController {
	@Autowired
	private ShareService shareService;
	
	@GetMapping()
	@RequiresPermissions("publishing:share:share")
	String Share(){
	    return "publishing/share/share";
	}
	
	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("publishing:share:share")
	public PageUtils list(@RequestParam Map<String, Object> params){
		//查询列表数据
        Query query = new Query(params);
		List<ShareDO> shareList = shareService.list(query);
		int total = shareService.count(query);
		PageUtils pageUtils = new PageUtils(shareList, total);
		return pageUtils;
	}
	
	@GetMapping("/add")
	@RequiresPermissions("publishing:share:add")
	String add(){
	    return "publishing/share/add";
	}

	@GetMapping("/edit/{id}")
	@RequiresPermissions("publishing:share:edit")
	String edit(@PathVariable("id") Integer id,Model model){
		ShareDO share = shareService.get(id);
		model.addAttribute("share", share);
	    return "publishing/share/edit";
	}
	
	/**
	 * 保存
	 */
	@ResponseBody
	@PostMapping("/save")
	@RequiresPermissions("publishing:share:add")
	public R save( ShareDO share){
		if(shareService.save(share)>0){
			return R.ok();
		}
		return R.error();
	}
	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping("/update")
	@RequiresPermissions("publishing:share:edit")
	public R update( ShareDO share){
		shareService.update(share);
		return R.ok();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/remove")
	@ResponseBody
	@RequiresPermissions("publishing:share:remove")
	public R remove( Integer id){
		if(shareService.remove(id)>0){
		return R.ok();
		}
		return R.error();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/batchRemove")
	@ResponseBody
	@RequiresPermissions("publishing:share:batchRemove")
	public R remove(@RequestParam("ids[]") Integer[] ids){
		shareService.batchRemove(ids);
		return R.ok();
	}
	
}

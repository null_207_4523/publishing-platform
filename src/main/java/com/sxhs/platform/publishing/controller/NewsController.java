package com.sxhs.platform.publishing.controller;

import com.alibaba.excel.EasyExcel;
import com.alibaba.fastjson.JSON;
import com.sxhs.platform.common.config.Constant;
import com.sxhs.platform.common.utils.*;
import com.sxhs.platform.publishing.domain.ColumnDO;
import com.sxhs.platform.publishing.domain.NewsDO;
import com.sxhs.platform.publishing.domain.NewsReviewReasonDO;
import com.sxhs.platform.publishing.service.ColumnService;
import com.sxhs.platform.publishing.service.NewsReviewReasonService;
import com.sxhs.platform.publishing.service.NewsService;
import com.sxhs.platform.publishing.service.ProducerOrgNewsHandle;
import com.sxhs.platform.publishing.vo.NewsExcelVo;
import com.sxhs.platform.system.domain.UserDO;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.util.*;

/**
 * 资讯信息
 *
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2020-09-21 14:10:44
 */

@Controller
@RequestMapping("/publishing/news")
public class NewsController {
    @Autowired
    private NewsService newsService;
    @Autowired
    private ColumnService columnService;
    @Autowired
    private ProducerOrgNewsHandle producerOrgNewsHandle;
    @Autowired
    private NewsReviewReasonService newsReviewReasonService;
    @Autowired
    protected HttpServletRequest request;

    /**
     *  资讯审核管理界面
     * @param model
     * @return
     */
    @GetMapping()
    @RequiresPermissions("publishing:news:news")
    String NewsReview(Model model) {
        Map<String,Object> params = new HashMap<>();
        params.put("hasShow",Constant.HAS_SHOW_YES);
        params.put("hasDelete",Constant.HAS_DELETE_NO);
        List<ColumnDO> columnDOList = columnService.list(params);
        model.addAttribute("columnList", JSON.toJSONString(columnDOList));
        model.addAttribute("columns", columnDOList);
        return "publishing/news/news";
    }

    /**
     *  资讯内容管理界面
     * @param model
     * @return
     */
    @GetMapping("/newsContent")
    @RequiresPermissions("publishing:news:newsContent")
    String NewsContent(Model model) {
        Map<String,Object> params = new HashMap<>();
        params.put("hasShow",Constant.HAS_SHOW_YES);
        params.put("hasDelete",Constant.HAS_DELETE_NO);
        List<ColumnDO> columnDOList = columnService.list(params);
        model.addAttribute("columnList", JSON.toJSONString(columnDOList));
        model.addAttribute("columns", columnDOList);
        return "publishing/news/newsContent";
    }

    @ResponseBody
    @GetMapping("/list")
    @RequiresPermissions("publishing:news:news")
    public PageUtils list(@RequestParam Map<String, Object> params) {
        if(Objects.nonNull(params.get("permission")) && !Objects.equals(params.get("permission"),"1")){
            UserDO userDO = ShiroUtils.getUser();
            params.put("createdUserId",userDO.getUserId().toString());
        }
        //查询列表数据
        Query query = new Query(params);
        List<NewsDO> newsList = newsService.list(query);
        int total = newsService.count(query);
        PageUtils pageUtils = new PageUtils(newsList, total);
        return pageUtils;
    }

    @GetMapping("/add")
    @RequiresPermissions("publishing:news:add")
    String add(Model model) {
        model.addAttribute("wsId", UUID.randomUUID());
        getWsPro(model);
        return "publishing/news/add";
    }

    @GetMapping("/edit/{id}")
    @RequiresPermissions("publishing:news:edit")
    String edit(@PathVariable("id") Integer id, Model model) {
        NewsDO news = newsService.get(id);
        if (Objects.nonNull(news) && StringUtils.isNotBlank(news.getColumnId())) {
            ColumnDO columnDO = columnService.get(Integer.valueOf(news.getColumnId()));
            if(Objects.nonNull(columnDO))
                news.setColumnName(columnDO.getColumnName());
        }
        model.addAttribute("news", news);
        model.addAttribute("wsId", UUID.randomUUID());
        getWsPro(model);
        return "publishing/news/edit";
    }

    /**
     * 保存
     */
    @ResponseBody
    @PostMapping("/save")
    @RequiresPermissions("publishing:news:add")
    public R save(NewsDO news) {
        if (StringUtils.isBlank(news.getColumnId()) || StringUtils.isBlank(news.getTitle()))
            return R.error(1, "标题或栏目不能为空!");
        UserDO userDO = ShiroUtils.getUser();
        news.setCreatedUser(userDO.getName());
        news.setCreatedUserId(userDO.getUserId().toString());
        news.setCreatedTime(new Date());
        news.setType(0);
        news.setHasTop(0);
        news.setHasInnerPublish(0);
        news.setHasDelete(Constant.HAS_DELETE_NO);
        news.setFansLikeCount(0);
        news.setLikeCount(0);
        news.setFansCollectCount(0);
        news.setCollectCount(0);
        news.setFansCommentCount(0);
        news.setCommentCount(0);
        news.setFansReadCount(0);
        news.setReadCount(0);
        news.setFansShareCount(0);
        news.setShareCount(0);
        if (Objects.equals(news.getStatus(), 3))
            news.setPublishTime(news.getCreatedTime());
        if (newsService.save(news) > 0) {
            // 处理机构资讯数据
            producerOrgNewsHandle.orgNewsProducer(news);
            return R.ok();
        }
        return R.error();
    }

    /**
     * 修改
     */
    @ResponseBody
    @RequestMapping("/update")
    @RequiresPermissions("publishing:news:edit")
    public R update(NewsDO news) {
        UserDO userDO = ShiroUtils.getUser();
        news.setUpdateUser(userDO.getName());
        news.setUpdateUserId(userDO.getUserId().toString());
        news.setUpdateTime(new Date());
        if (Objects.equals(news.getStatus(), 3))
            news.setPublishTime(new Date());
        if (newsService.update(news) > 0)
            return R.ok();
        return R.error();
    }

    /**
     * 删除
     */
    @PostMapping("/remove")
    @ResponseBody
    @RequiresPermissions("publishing:news:remove")
    public R remove(Integer id) {
        if (newsService.remove(id) > 0) {
            return R.ok();
        }
        return R.error();
    }

    /**
     * 删除
     */
    @PostMapping("/batchRemove")
    @ResponseBody
    @RequiresPermissions("publishing:news:batchRemove")
    public R remove(@RequestParam("ids[]") Integer[] ids) {
        newsService.batchRemove(ids);
        return R.ok();
    }

    /**
     * 数据导出
     *
     * @return
     */
    @PostMapping("/export")
    public void exporter(HttpServletResponse response) {
        //查询列表数据
        Map<String, Object> params = new HashMap<>(16);
        params.put("status", Constant.NEWS_STATUS_APPROVE);
        params.put("hasDelete", Constant.HAS_DELETE_NO);
        List<NewsDO> newsList = newsService.list(params);
        List<NewsExcelVo> list = new ArrayList<>(newsList.size());
        for (NewsDO newsDO : newsList) {
            String publishTime = DateUtils.format(newsDO.getPublishTime());
            String type = "音频";
            switch (newsDO.getType()) {
                //0：富文本 1：视频 2：音频
                case 0:
                    type = "富文本";
                    break;
                case 1:
                    type = "视频";
                    break;
                default:
                    break;
            }
            String status = "审核未通过";
            switch (newsDO.getStatus()) {
                //0：草稿 1：保存  2：审核中  3：审核通过  4：审核未通过
                case 0:
                    status = "草稿";
                    break;
                case 1:
                    status = "保存";
                    break;
                case 2:
                    status = "审核中";
                    break;
                case 3:
                    status = "审核通过";
                    break;
                default:
                    break;
            }
            String hasTop = "置顶";
            switch (newsDO.getHasTop()) {
                case 0:
                    hasTop = "未置顶";
                    break;
                default:
                    break;
            }
            NewsExcelVo newsExcelVo = new NewsExcelVo();
            BeanCopierUtils.copyProperties(newsDO, newsExcelVo);
            newsExcelVo.setHasTop(hasTop);
            newsExcelVo.setStatus(status);
            newsExcelVo.setType(type);
            newsExcelVo.setPublishTime(publishTime);
            list.add(newsExcelVo);
        }
        try {
            response.setContentType("application/vnd.ms-excel");
            response.setCharacterEncoding("utf-8");
            // 这里URLEncoder.encode可以防止中文乱码 当然和easyexcel没有关系
            String fileName = URLEncoder.encode("内容列表", "UTF-8");
            response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".xlsx");
            EasyExcel.write(response.getOutputStream(), NewsExcelVo.class).sheet("内容列表").doWrite(list);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     *  资讯封面编辑
     * @return
     */
    @GetMapping("/cover")
    @RequiresPermissions("publishing:news:cover")
    String cover(@RequestParam(name = "num") String num, Model model) {
        model.addAttribute("num",num);
        return "publishing/news/cover";
    }

    /**
     *  资讯预览页面
     * @return
     */
    @GetMapping("/preview")
    @RequiresPermissions("publishing:news:preview")
    String preview() {
        return "publishing/news/preview";
    }

    /**
     *  资讯审核页面
     * @return
     */
    @GetMapping("/review/{id}")
    String review(@PathVariable("id") Integer id,Model model) {
        NewsDO newsDO = newsService.get(id);
        model.addAttribute("news", newsDO);
        return "publishing/news/review";
    }

    /**
     *  查看审核不通过原因查看界面
     * @return
     */
    @GetMapping("/review/reason/{id}")
    String reviewReason(@PathVariable("id") Integer id, Model model) {
        NewsReviewReasonDO reasonDO = newsReviewReasonService.getByNewsId(id);
        model.addAttribute("newsReviewReason", reasonDO);
        return "publishing/newsReviewReason/edit";
    }

    /**
     * 资讯审核接口
     * @param params
     * @return
     */
    @PostMapping("/review")
    @ResponseBody
    public R review(@RequestParam Map<String, Object> params) {
        return newsReviewReasonService.manualReview(params);
    }

    /**
     * 获取头信息referer，判断WebSocket协议
     * @param model
     */
    private void getWsPro(Model model) {
        String url = request.getHeader("referer");
        if(StringUtils.isBlank(url)) {
            url = request.getScheme();
        }
        if(StringUtils.isNotBlank(url) && url.startsWith("https")) {
            model.addAttribute("scheme","https");
            model.addAttribute("wsPro","wss");
        } else if(StringUtils.isNotBlank(url) && url.startsWith("http")) {
            model.addAttribute("scheme","http");
            model.addAttribute("wsPro","ws");
        }
    }

}

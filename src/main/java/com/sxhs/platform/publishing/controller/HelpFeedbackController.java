package com.sxhs.platform.publishing.controller;

import java.util.List;
import java.util.Map;

import com.sxhs.platform.common.service.DictService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sxhs.platform.publishing.domain.HelpFeedbackDO;
import com.sxhs.platform.publishing.service.HelpFeedbackService;
import com.sxhs.platform.common.utils.PageUtils;
import com.sxhs.platform.common.utils.Query;
import com.sxhs.platform.common.utils.R;

/**
 * 帮助反馈
 * 
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2020-10-17 10:34:40
 */
 
@Controller
@RequestMapping("/publishing/helpFeedback")
public class HelpFeedbackController {
	@Autowired
	private HelpFeedbackService helpFeedbackService;

	@Autowired
	private DictService dictService;
	
	@GetMapping()
	@RequiresPermissions("publishing:helpFeedback:helpFeedback")
	String HelpFeedback(){
	    return "publishing/helpFeedback/helpFeedback";
	}
	
	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("publishing:helpFeedback:helpFeedback")
	public PageUtils list(@RequestParam Map<String, Object> params){
		//查询列表数据
        Query query = new Query(params);
		List<HelpFeedbackDO> helpFeedbackList = helpFeedbackService.list(query);
		int total = helpFeedbackService.count(query);
		PageUtils pageUtils = new PageUtils(helpFeedbackList, total);
		return pageUtils;
	}
	
	@GetMapping("/add")
	@RequiresPermissions("publishing:helpFeedback:add")
	String add(Model model){
		model.addAttribute("fklxDic",dictService.listByType("fklx"));
		return "publishing/helpFeedback/add";
	}

	@GetMapping("/edit/{id}")
	@RequiresPermissions("publishing:helpFeedback:edit")
	String edit(@PathVariable("id") Integer id,Model model){
		model.addAttribute("fklxDic",dictService.listByType("fklx"));
		HelpFeedbackDO helpFeedback = helpFeedbackService.get(id);
		model.addAttribute("helpFeedback", helpFeedback);
	    return "publishing/helpFeedback/edit";
	}
	
	/**
	 * 保存
	 */
	@ResponseBody
	@PostMapping("/save")
	@RequiresPermissions("publishing:helpFeedback:add")
	public R save( HelpFeedbackDO helpFeedback){
		if(helpFeedbackService.save(helpFeedback)>0){
			return R.ok();
		}
		return R.error();
	}
	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping("/update")
	@RequiresPermissions("publishing:helpFeedback:edit")
	public R update( HelpFeedbackDO helpFeedback){
		helpFeedbackService.update(helpFeedback);
		return R.ok();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/remove")
	@ResponseBody
	@RequiresPermissions("publishing:helpFeedback:remove")
	public R remove( Integer id){
		if(helpFeedbackService.remove(id)>0){
		return R.ok();
		}
		return R.error();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/batchRemove")
	@ResponseBody
	@RequiresPermissions("publishing:helpFeedback:batchRemove")
	public R remove(@RequestParam("ids[]") Integer[] ids){
		helpFeedbackService.batchRemove(ids);
		return R.ok();
	}
	
}

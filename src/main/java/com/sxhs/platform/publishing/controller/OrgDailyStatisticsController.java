package com.sxhs.platform.publishing.controller;

import com.sxhs.platform.common.config.Constant;
import com.sxhs.platform.common.utils.PageUtils;
import com.sxhs.platform.common.utils.Query;
import com.sxhs.platform.common.utils.R;
import com.sxhs.platform.publishing.domain.OrgDailyStatisticsDO;
import com.sxhs.platform.publishing.service.OrgDailyStatisticsService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 机构资讯每日统计表
 *
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2020-09-21 14:10:44
 */

@Controller
@RequestMapping("/publishing/orgDailyStatistics")
public class OrgDailyStatisticsController {
	@Autowired
	private OrgDailyStatisticsService orgDailyStatisticsService;

	@GetMapping()
	@RequiresPermissions("publishing:orgDailyStatistics:orgDailyStatistics")
	String OrgDailyStatistics(Model model){
		// 查询最近两天统计数据，对阅读量、评论量、点赞量比较
		Map<String,Object> pararm = new HashMap<>();
		pararm.put("hasDelete", Constant.HAS_DELETE_NO);
		pararm.put("offset", 0);
		pararm.put("limit", 2);
		List<OrgDailyStatisticsDO> list = orgDailyStatisticsService.list(pararm);
		int yesterdayLikeCount = 0;
		int yesterdayCommentCount = 0;
		int yesterdayReadCount = 0;
		int likeCount = 0;
		int commentCount = 0;
		int readCount = 0;
		if(!CollectionUtils.isEmpty(list)){
			// 有两天以上的统计数据
			OrgDailyStatisticsDO yesterday = list.get(0); // 昨天统计数据
			yesterdayLikeCount = yesterday.getLikeCount();
			yesterdayCommentCount = yesterday.getCommentCount();
			yesterdayReadCount = yesterday.getReadCount();
			if(list.size() == 2){
				OrgDailyStatisticsDO beforeYesterday = list.get(1); // 前天统计数据
				likeCount = yesterdayLikeCount-beforeYesterday.getLikeCount();
				commentCount = yesterdayCommentCount-beforeYesterday.getCommentCount();
				readCount = yesterdayReadCount-beforeYesterday.getReadCount();
			} else {
				likeCount = yesterdayLikeCount;
				commentCount = yesterdayCommentCount;
				readCount = yesterdayReadCount;
			}
		}
		model.addAttribute("likeCount", likeCount);
		model.addAttribute("commentCount", commentCount);
		model.addAttribute("readCount", readCount);
		model.addAttribute("yesterdayLikeCount", yesterdayLikeCount);
		model.addAttribute("yesterdayCommentCount", yesterdayCommentCount);
		model.addAttribute("yesterdayReadCount", yesterdayReadCount);
		return "publishing/orgDailyStatistics/orgDailyStatistics";
	}

	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("publishing:orgDailyStatistics:orgDailyStatistics")
	public PageUtils list(@RequestParam Map<String, Object> params){
		//查询列表数据
        Query query = new Query(params);
		List<OrgDailyStatisticsDO> orgDailyStatisticsList = orgDailyStatisticsService.listByLimit(query);
		int total = orgDailyStatisticsService.countlistByLimit(query);
		PageUtils pageUtils = new PageUtils(orgDailyStatisticsList, total);
		return pageUtils;
	}

	@GetMapping("/add")
	@RequiresPermissions("publishing:orgDailyStatistics:add")
	String add(){
	    return "publishing/orgDailyStatistics/add";
	}

	@GetMapping("/edit/{id}")
	@RequiresPermissions("publishing:orgDailyStatistics:edit")
	String edit(@PathVariable("id") Integer id,Model model){
		OrgDailyStatisticsDO orgDailyStatistics = orgDailyStatisticsService.get(id);
		model.addAttribute("orgDailyStatistics", orgDailyStatistics);
	    return "publishing/orgDailyStatistics/edit";
	}

	/**
	 * 保存
	 */
	@ResponseBody
	@PostMapping("/save")
	@RequiresPermissions("publishing:orgDailyStatistics:add")
	public R save( OrgDailyStatisticsDO orgDailyStatistics){
		if(orgDailyStatisticsService.save(orgDailyStatistics)>0){
			return R.ok();
		}
		return R.error();
	}
	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping("/update")
	@RequiresPermissions("publishing:orgDailyStatistics:edit")
	public R update( OrgDailyStatisticsDO orgDailyStatistics){
		orgDailyStatisticsService.update(orgDailyStatistics);
		return R.ok();
	}

	/**
	 * 删除
	 */
	@PostMapping( "/remove")
	@ResponseBody
	@RequiresPermissions("publishing:orgDailyStatistics:remove")
	public R remove( Integer id){
		if(orgDailyStatisticsService.remove(id)>0){
		return R.ok();
		}
		return R.error();
	}

	/**
	 * 删除
	 */
	@PostMapping( "/batchRemove")
	@ResponseBody
	@RequiresPermissions("publishing:orgDailyStatistics:batchRemove")
	public R remove(@RequestParam("ids[]") Integer[] ids){
		orgDailyStatisticsService.batchRemove(ids);
		return R.ok();
	}

}

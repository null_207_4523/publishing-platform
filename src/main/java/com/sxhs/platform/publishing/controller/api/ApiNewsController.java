package com.sxhs.platform.publishing.controller.api;

import com.alibaba.fastjson.JSONObject;
import com.aliyun.openservices.shade.org.apache.commons.lang3.StringEscapeUtils;
import com.sxhs.platform.common.utils.ApiResult;
import com.sxhs.platform.common.utils.R;
import com.sxhs.platform.common.utils.RedisUtil;
import com.sxhs.platform.publishing.domain.NewsDO;
import com.sxhs.platform.publishing.service.NewsService;
import com.sxhs.platform.system.config.Pass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Objects;
import java.util.UUID;

/**
 * 资讯信息
 *
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2020-09-21 14:10:44
 */
@Controller
@RequestMapping(path = {"/api/publishing/news","/webapi/publishing/news"})
@CrossOrigin
public class ApiNewsController {

    @Autowired
    private NewsService newsService;
    @Autowired
    private StringRedisTemplate redisTemplate;
    @Autowired
    private RedisUtil redisUtil;

    @PostMapping("/transContent")
    @ResponseBody
    @Pass
    public R transContent(String content, String wsId) {
        JSONObject data = new JSONObject();
        data.put("content",content);
        data.put("wsId",wsId);
        try {
            redisTemplate.convertAndSend("websocket135", data.toJSONString());
            return  R.ok();
        } catch (Exception e) {
            e.printStackTrace();
            return R.error();
        }
    }

    @PostMapping("/getContent")
    @ResponseBody
    @Pass
    public ApiResult getContent(Integer id) {
        NewsDO newsDO = newsService.get(id);
        return ApiResult.success(newsDO);
    }

    @PostMapping("/getTempContent")
    @ResponseBody
    @Pass
    public ApiResult getTempContent(String id) {
        Object object = redisUtil.get(id);
        String content = "";
        if (Objects.nonNull(object))
            content = StringEscapeUtils.unescapeJava(object.toString());
        return ApiResult.success(content);
    }

    @PostMapping("/saveTempContent")
    @ResponseBody
    @Pass
    public ApiResult saveTempContent(String content) {
        String uuid = UUID.randomUUID().toString();
        redisUtil.set(uuid,content,60);
        return ApiResult.success(uuid);
    }

}

package com.sxhs.platform.publishing.controller;

import com.sxhs.platform.common.utils.PageUtils;
import com.sxhs.platform.common.utils.Query;
import com.sxhs.platform.common.utils.R;
import com.sxhs.platform.publishing.domain.CollectDO;
import com.sxhs.platform.publishing.service.CollectService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 资讯收藏
 * 
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2020-09-21 14:10:43
 */
 
@Controller
@RequestMapping("/publishing/collect")
public class CollectController {
	@Autowired
	private CollectService collectService;
	
	@GetMapping()
	@RequiresPermissions("publishing:collect:collect")
	String Collect(){
	    return "publishing/collect/collect";
	}
	
	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("publishing:collect:collect")
	public PageUtils list(@RequestParam Map<String, Object> params){
		//查询列表数据
        Query query = new Query(params);
		List<CollectDO> collectList = collectService.list(query);
		int total = collectService.count(query);
		PageUtils pageUtils = new PageUtils(collectList, total);
		return pageUtils;
	}
	
	@GetMapping("/add")
	@RequiresPermissions("publishing:collect:add")
	String add(){
	    return "publishing/collect/add";
	}

	@GetMapping("/edit/{id}")
	@RequiresPermissions("publishing:collect:edit")
	String edit(@PathVariable("id") Integer id,Model model){
		CollectDO collect = collectService.get(id);
		model.addAttribute("collect", collect);
	    return "publishing/collect/edit";
	}
	
	/**
	 * 保存
	 */
	@ResponseBody
	@PostMapping("/save")
	@RequiresPermissions("publishing:collect:add")
	public R save( CollectDO collect){
		if(collectService.save(collect)>0){
			return R.ok();
		}
		return R.error();
	}
	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping("/update")
	@RequiresPermissions("publishing:collect:edit")
	public R update( CollectDO collect){
		collectService.update(collect);
		return R.ok();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/remove")
	@ResponseBody
	@RequiresPermissions("publishing:collect:remove")
	public R remove( Integer id){
		if(collectService.remove(id)>0){
		return R.ok();
		}
		return R.error();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/batchRemove")
	@ResponseBody
	@RequiresPermissions("publishing:collect:batchRemove")
	public R remove(@RequestParam("ids[]") Integer[] ids){
		collectService.batchRemove(ids);
		return R.ok();
	}
	
}

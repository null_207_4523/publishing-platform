package com.sxhs.platform.publishing.controller;

import com.sxhs.platform.common.config.Constant;
import com.sxhs.platform.common.service.DictService;
import com.sxhs.platform.common.utils.PageUtils;
import com.sxhs.platform.common.utils.Query;
import com.sxhs.platform.common.utils.R;
import com.sxhs.platform.publishing.domain.VersionDO;
import com.sxhs.platform.publishing.service.VersionService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * 
 * 
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2020-11-09 11:39:55
 */
 
@Controller
@RequestMapping("/publishing/version")
public class VersionController {
	@Autowired
	private VersionService versionService;
	@Autowired
	private DictService dictService;
	
	@GetMapping()
	@RequiresPermissions("publishing:version:version")
	String Version(){
	    return "publishing/version/version";
	}
	
	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("publishing:version:version")
	public PageUtils list(@RequestParam Map<String, Object> params){
		//查询列表数据
        Query query = new Query(params);
		List<VersionDO> versionList = versionService.list(query);
		int total = versionService.count(query);
		PageUtils pageUtils = new PageUtils(versionList, total);
		return pageUtils;
	}
	
	@GetMapping("/add")
	@RequiresPermissions("publishing:version:add")
	String add(Model model){
		model.addAttribute("systemType",dictService.listByType("systemType"));
	    return "publishing/version/add";
	}

	@GetMapping("/edit/{id}")
	@RequiresPermissions("publishing:version:edit")
	String edit(@PathVariable("id") Integer id,Model model){
		VersionDO version = versionService.get(id);
		model.addAttribute("version", version);
		model.addAttribute("systemType",dictService.listByType("systemType"));
	    return "publishing/version/edit";
	}
	
	/**
	 * 保存
	 */
	@ResponseBody
	@PostMapping("/save")
	@RequiresPermissions("publishing:version:add")
	public R save( VersionDO version){
		version.setCreateTime(new Date());
		String url = dictService.listByType("appDownloadUrl").stream().filter(d-> Objects.equals(d.getValue(),version.getSystemType().toString())).findAny().get().getDescription();
		version.setUpdateTime(version.getCreateTime());
		version.setHasDelete(Constant.HAS_DELETE_NO);
		version.setHasDisabled(0);
		version.setUrl(url);
		if(versionService.save(version)>0){
			return R.ok();
		}
		return R.error();
	}
	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping("/update")
	@RequiresPermissions("publishing:version:edit")
	public R update( VersionDO version){
		version.setUpdateTime(new Date());
		versionService.update(version);
		return R.ok();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/remove")
	@ResponseBody
	@RequiresPermissions("publishing:version:remove")
	public R remove( Integer id){
		if(versionService.remove(id)>0){
		return R.ok();
		}
		return R.error();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/batchRemove")
	@ResponseBody
	@RequiresPermissions("publishing:version:batchRemove")
	public R remove(@RequestParam("ids[]") Integer[] ids){
		versionService.batchRemove(ids);
		return R.ok();
	}
	
}

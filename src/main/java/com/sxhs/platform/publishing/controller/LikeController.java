package com.sxhs.platform.publishing.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sxhs.platform.publishing.domain.LikeDO;
import com.sxhs.platform.publishing.service.LikeService;
import com.sxhs.platform.common.utils.PageUtils;
import com.sxhs.platform.common.utils.Query;
import com.sxhs.platform.common.utils.R;

/**
 * 资讯点赞
 * 
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2020-09-21 14:10:44
 */
 
@Controller
@RequestMapping("/publishing/like")
public class LikeController {
	@Autowired
	private LikeService likeService;
	
	@GetMapping()
	@RequiresPermissions("publishing:like:like")
	String Like(){
	    return "publishing/like/like";
	}
	
	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("publishing:like:like")
	public PageUtils list(@RequestParam Map<String, Object> params){
		//查询列表数据
        Query query = new Query(params);
		List<LikeDO> likeList = likeService.list(query);
		int total = likeService.count(query);
		PageUtils pageUtils = new PageUtils(likeList, total);
		return pageUtils;
	}
	
	@GetMapping("/add")
	@RequiresPermissions("publishing:like:add")
	String add(){
	    return "publishing/like/add";
	}

	@GetMapping("/edit/{id}")
	@RequiresPermissions("publishing:like:edit")
	String edit(@PathVariable("id") Integer id,Model model){
		LikeDO like = likeService.get(id);
		model.addAttribute("like", like);
	    return "publishing/like/edit";
	}
	
	/**
	 * 保存
	 */
	@ResponseBody
	@PostMapping("/save")
	@RequiresPermissions("publishing:like:add")
	public R save( LikeDO like){
		if(likeService.save(like)>0){
			return R.ok();
		}
		return R.error();
	}
	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping("/update")
	@RequiresPermissions("publishing:like:edit")
	public R update( LikeDO like){
		likeService.update(like);
		return R.ok();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/remove")
	@ResponseBody
	@RequiresPermissions("publishing:like:remove")
	public R remove( Integer id){
		if(likeService.remove(id)>0){
		return R.ok();
		}
		return R.error();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/batchRemove")
	@ResponseBody
	@RequiresPermissions("publishing:like:batchRemove")
	public R remove(@RequestParam("ids[]") Integer[] ids){
		likeService.batchRemove(ids);
		return R.ok();
	}
	
}

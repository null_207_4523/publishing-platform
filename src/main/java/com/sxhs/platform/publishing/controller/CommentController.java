package com.sxhs.platform.publishing.controller;

import com.sxhs.platform.common.utils.ApiResult;
import com.sxhs.platform.common.utils.PageUtils;
import com.sxhs.platform.common.utils.Query;
import com.sxhs.platform.common.utils.R;
import com.sxhs.platform.publishing.domain.CommentDO;
import com.sxhs.platform.publishing.service.CommentService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 资讯评论
 *
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2020-09-21 14:10:43
 */

@Controller
@RequestMapping("/publishing/comment")
public class CommentController {
    @Autowired
    private CommentService commentService;

    @GetMapping()
    @RequiresPermissions("publishing:comment:comment")
    String Comment() {
        return "publishing/comment/comment";
    }

    @ResponseBody
    @GetMapping("/list")
    @RequiresPermissions("publishing:comment:comment")
    public PageUtils list(@RequestParam Map<String, Object> params) {
        //查询列表数据
        Query query = new Query(params);
        List<CommentDO> commentList = commentService.list(query);
        int total = commentService.count(query);
        PageUtils pageUtils = new PageUtils(commentList, total);
        return pageUtils;
    }

    @GetMapping("/add")
    @RequiresPermissions("publishing:comment:add")
    String add() {
        return "publishing/comment/add";
    }

    @GetMapping("/edit/{id}")
    @RequiresPermissions("publishing:comment:edit")
    String edit(@PathVariable("id") Integer id, Model model) {
        ApiResult commentResult = commentService.commentDetailsList(id);
        model.addAttribute("commentList", commentResult.getData());
        return "publishing/comment/edit";
    }

    /**
     * 保存
     */
    @ResponseBody
    @PostMapping("/save")
    @RequiresPermissions("publishing:comment:add")
    public R save(CommentDO comment) {
        if (commentService.save(comment) > 0) {
            return R.ok();
        }
        return R.error();
    }

    /**
     * 修改
     */
    @ResponseBody
    @RequestMapping("/update")
    @RequiresPermissions("publishing:comment:edit")
    public R update(CommentDO comment) {
        commentService.update(comment);
        return R.ok();
    }

    /**
     * 删除
     */
    @PostMapping("/remove")
    @ResponseBody
    @RequiresPermissions("publishing:comment:remove")
    public R remove(Integer id) {
        if (commentService.remove(id) > 0) {
            return R.ok();
        }
        return R.error();
    }

    /**
     * 删除
     */
    @PostMapping("/batchRemove")
    @ResponseBody
    @RequiresPermissions("publishing:comment:batchRemove")
    public R remove(@RequestParam("ids[]") Integer[] ids) {
        commentService.batchRemove(ids);
        return R.ok();
    }

}

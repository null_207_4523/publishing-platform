package com.sxhs.platform.publishing.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sxhs.platform.publishing.domain.OrgSubscriptionDO;
import com.sxhs.platform.publishing.service.OrgSubscriptionService;
import com.sxhs.platform.common.utils.PageUtils;
import com.sxhs.platform.common.utils.Query;
import com.sxhs.platform.common.utils.R;

/**
 * 资讯机构订阅
 * 
 * @author liuliang
 * @email 284759838@qq.com
 * @date 2020-09-21 14:10:45
 */
 
@Controller
@RequestMapping("/publishing/orgSubscription")
public class OrgSubscriptionController {
	@Autowired
	private OrgSubscriptionService orgSubscriptionService;
	
	@GetMapping()
	@RequiresPermissions("publishing:orgSubscription:orgSubscription")
	String OrgSubscription(){
	    return "publishing/orgSubscription/orgSubscription";
	}
	
	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("publishing:orgSubscription:orgSubscription")
	public PageUtils list(@RequestParam Map<String, Object> params){
		//查询列表数据
        Query query = new Query(params);
		List<OrgSubscriptionDO> orgSubscriptionList = orgSubscriptionService.list(query);
		int total = orgSubscriptionService.count(query);
		PageUtils pageUtils = new PageUtils(orgSubscriptionList, total);
		return pageUtils;
	}
	
	@GetMapping("/add")
	@RequiresPermissions("publishing:orgSubscription:add")
	String add(){
	    return "publishing/orgSubscription/add";
	}

	@GetMapping("/edit/{id}")
	@RequiresPermissions("publishing:orgSubscription:edit")
	String edit(@PathVariable("id") Integer id,Model model){
		OrgSubscriptionDO orgSubscription = orgSubscriptionService.get(id);
		model.addAttribute("orgSubscription", orgSubscription);
	    return "publishing/orgSubscription/edit";
	}
	
	/**
	 * 保存
	 */
	@ResponseBody
	@PostMapping("/save")
	@RequiresPermissions("publishing:orgSubscription:add")
	public R save( OrgSubscriptionDO orgSubscription){
		if(orgSubscriptionService.save(orgSubscription)>0){
			return R.ok();
		}
		return R.error();
	}
	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping("/update")
	@RequiresPermissions("publishing:orgSubscription:edit")
	public R update( OrgSubscriptionDO orgSubscription){
		orgSubscriptionService.update(orgSubscription);
		return R.ok();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/remove")
	@ResponseBody
	@RequiresPermissions("publishing:orgSubscription:remove")
	public R remove( Integer id){
		if(orgSubscriptionService.remove(id)>0){
		return R.ok();
		}
		return R.error();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/batchRemove")
	@ResponseBody
	@RequiresPermissions("publishing:orgSubscription:batchRemove")
	public R remove(@RequestParam("ids[]") Integer[] ids){
		orgSubscriptionService.batchRemove(ids);
		return R.ok();
	}
	
}

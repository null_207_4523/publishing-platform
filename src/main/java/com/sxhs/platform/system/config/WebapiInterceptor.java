package com.sxhs.platform.system.config;

import com.alibaba.fastjson.JSONObject;
import com.sxhs.platform.common.utils.ApiResult;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;


/**
 * @Author LiuLiang
 * @Date 4:47 PM 2020/12/1
 * @Description //web单点登录验证拦截器
 **/
@Slf4j
public class WebapiInterceptor implements HandlerInterceptor {

    public static final String TOKEN_KEY = "TOKEN:";

    @Autowired
    RedisTemplate redisTemplate;

    private RestTemplate restTemplate = new RestTemplate();

    @Value("${passport.url:}")
    private String PASSPORT_URL;

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object object) throws Exception {
        String access_token = httpServletRequest.getHeader("access_token");
        // 如果不是映射到方法直接通过
        if(!(object instanceof HandlerMethod)){
            return true;
        }
        HandlerMethod handlerMethod=(HandlerMethod)object;
        Method method=handlerMethod.getMethod();
        //检查是否有passtoken注释，有则跳过认证
        if (method.isAnnotationPresent(Pass.class)) {
            Pass pass = method.getAnnotation(Pass.class);
            if (pass.required()) {
                return true;
            }
        }
        try {
            // 执行认证
            if(StringUtils.isBlank(access_token)){
                throw new RuntimeException("请传递用户access_token！");
            }
            if(!checkToken(access_token)){
                throw new RuntimeException("access_token验证失败请重新登录");
            }

        }catch (RuntimeException e){
            httpServletResponse.setCharacterEncoding("UTF-8");
            httpServletResponse.setStatus(414);
            httpServletResponse.getWriter().write(JSONObject.toJSONString(ApiResult.fail("1002",e.getMessage())));
            return false;
        }

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest,
                           HttpServletResponse httpServletResponse,
                           Object o, ModelAndView modelAndView) throws Exception {

    }
    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest,
                                HttpServletResponse httpServletResponse,
                                Object o, Exception e) throws Exception {
    }

    private boolean checkToken(String token) {
        if(StringUtils.isBlank(token)) {
            return false;
        }

        if(redisTemplate.hasKey(TOKEN_KEY+token)){
            boolean chack_status = (boolean)redisTemplate.opsForValue().get(TOKEN_KEY+token);
            return chack_status;
        }

        try{
            String ChackTokenUrl = PASSPORT_URL+"/passport/oauth/check_token";
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            MultiValueMap postData  = new LinkedMultiValueMap();
            postData.add("token", token);
            final HttpEntity< MultiValueMap< String,String>> entity = new HttpEntity< MultiValueMap< String,String>>(postData,headers);
            ResponseEntity<JSONObject> responseEntity = restTemplate.postForEntity(ChackTokenUrl, entity, JSONObject.class);

            JSONObject json = new JSONObject();
            if(responseEntity.getStatusCodeValue()== HttpStatus.OK.value()){
                json = responseEntity.getBody();
            }else{
                log.error("用户信息验证网络异常:{"+responseEntity.getStatusCode().value()+"}"+responseEntity.getStatusCode().getReasonPhrase());
                throw new RuntimeException();
            }
            log.info("返回验证信息:"+json.toJSONString());
            String USER_NAME_KEY = "user_name";
            if(StringUtils.isBlank(json.getString(USER_NAME_KEY))){
                throw new RuntimeException();
            }
        }catch (Exception e){
            e.printStackTrace();
            redisTemplate.opsForValue().set(TOKEN_KEY+token,false,120, TimeUnit.SECONDS);
            log.error("+++++++++{}:用户信息验证失败！++++++++++++",token);
            return false;
        }

        redisTemplate.opsForValue().set(TOKEN_KEY+token,true,120,TimeUnit.SECONDS);

        return true;
    }
}

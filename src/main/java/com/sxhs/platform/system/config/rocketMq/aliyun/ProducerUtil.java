package com.sxhs.platform.system.config.rocketMq.aliyun;

import com.aliyun.openservices.ons.api.Message;
import com.aliyun.openservices.ons.api.SendCallback;
import com.aliyun.openservices.ons.api.SendResult;
import com.aliyun.openservices.ons.api.bean.ProducerBean;
import com.aliyun.openservices.ons.api.exception.ONSClientException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *      生产者工具类
 */
@Slf4j
@Component
public class ProducerUtil {


    @Autowired
    private MqConfig config;

    @Autowired
    private ProducerBean producer;


    /**
     *      同步发送消息
     *
     * @param topic     主题
     * @param tags    标签，可用于消息小分类标注
     * @param keys    消息key值，建议设置全局唯一，可不传，不影响消息投递
     * @param messageBody   消息body内容
     * @return
     */
    public SendResult sendMsg(String topic, String tags, String keys, String messageBody) throws UnsupportedEncodingException {
        Message msg = new Message(topic, tags, keys, messageBody.getBytes("UTF-8"));
        return this.send(msg, Boolean.FALSE);
    }


    /**
     *          同步发送定时/延时消息
     *
     * @param topic     主题
     * @param tags    标签，可用于消息小分类标注，对消息进行再归类
     * @param keys    消息key值，建议设置全局唯一，可不传，不影响消息投递
     * @param messageBody   消息body内容
     * @param delayTime 服务端发送消息时间，立即发送输入0或比更早的时间   单位毫秒（ms），在指定延迟时间（当前时间之后）进行投递，例如消息在 3 秒后投递。
     * @return
     */
    public SendResult sendTimeMsg(String topic, String tags, String keys, String messageBody, long delayTime) throws UnsupportedEncodingException {
        Message msg = new Message(topic, tags, keys, messageBody.getBytes("UTF-8"));
        msg.setStartDeliverTime(delayTime);
        return this.send(msg, Boolean.FALSE);
    }

    /**
     *          发送单向消息
     * @param topic     主题
     * @param tags    标签，可用于消息小分类标注
     * @param keys    消息key值，建议设置全局唯一，可不传，不影响消息投递
     * @param messageBody   消息body内容
     * @throws UnsupportedEncodingException
     */
    public void sendOneWayMsg(String topic, String tags ,String keys, String messageBody) throws UnsupportedEncodingException {
        Message msg = new Message(topic, tags, keys, messageBody.getBytes("UTF-8"));
        this.send(msg, Boolean.TRUE);
    }

    /**
     *  普通消息发送发放
     * @param msg 消息
     * @param isOneWay 是否单向发送
     */
    private SendResult send(Message msg, Boolean isOneWay) {
        try {
            if(isOneWay) {
                //由于在 oneway 方式发送消息时没有请求应答处理，一旦出现消息发送失败，则会因为没有重试而导致数据丢失。
                //若数据不可丢，建议选用同步或异步发送方式。
                producer.sendOneway(msg);
                success(msg, "单向消息MsgId不返回");
                return null;
            }else {
                //可靠同步发送
                SendResult sendResult = producer.send(msg);
                //获取发送结果，不抛异常即发送成功
                if (sendResult != null) {
                    success(msg, sendResult.getMessageId());
                    return sendResult;
                }else {
                    error(msg,null);
                    return null;
                }
            }
        } catch (Exception e) {
            error(msg, e);
        }
        return null;
    }

    //对于使用异步接口，可设置单独的回调处理线程池，拥有更灵活的配置和监控能力。
    //根据项目需要，服务器配置合理设置线程数，线程太多有OOM 风险，
    private ExecutorService threads = Executors.newFixedThreadPool(3);
    //仅建议执行轻量级的Callback任务，避免阻塞公共线程池 引起其它链路超时。


    /**
     *          异步发送普通消息
     * @param topic
     * @param tags
     * @param keys
     * @param messageBody
     * @param sendCallback
     * @throws UnsupportedEncodingException
     */
    public void sendAsyncMsg(String topic, String tags, String keys, String messageBody, SendCallback sendCallback) throws UnsupportedEncodingException {

        producer.setCallbackExecutor(threads);

        Message msg = new Message(topic, tags, keys, messageBody.getBytes("UTF-8"));
        try {
            producer.sendAsync(msg, sendCallback);
        } catch (ONSClientException e) {
            error(msg, e);
        }
    }


    private void success(Message msg, String messageId) {
        log.info("发送MQ消息成功 -- Topic:{} ,msgId:{} , Key:{}, tag:{}, body:{}"
                , msg.getTopic(), messageId, msg.getKey(), msg.getTag(), new String(msg.getBody()));
    }

    private void error(Message msg, Exception e) {
        log.error("发送MQ消息失败-- Topic:{}, Key:{}, tag:{}, body:{}"
                , msg.getTopic(), msg.getKey(), msg.getTag(), new String(msg.getBody()));
        log.error("errorMsg --- {}", e.getMessage());
    }


}

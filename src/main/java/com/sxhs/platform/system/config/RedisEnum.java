package com.sxhs.platform.system.config;

/**
 * @Author LiuLiang
 * @Date 4:51 PM 2020/9/18
 * @Description //redis枚举类
 **/
public enum RedisEnum {
    USER("BOOTDO:USER:"),
    APP("APP:USER:"),
    CLIENTTOKEN("CLIENT:TOKEN:"),
    DIC("BOOTDO:DIC:");

    private String path;

    private RedisEnum(String path){
        this.path = path;
    }

    public String getPath(String id) {
        return path+id;
    }

    public void setPath(String path) {
        this.path = path;
    }
}

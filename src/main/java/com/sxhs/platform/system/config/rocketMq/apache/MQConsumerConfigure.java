//package com.sxhs.platform.system.config.rocketMq.apache;
//
//
//import com.aliyun.openservices.shade.com.alibaba.rocketmq.client.consumer.DefaultMQPushConsumer;
//import com.aliyun.openservices.shade.com.alibaba.rocketmq.client.exception.MQClientException;
//import com.aliyun.openservices.shade.com.alibaba.rocketmq.common.consumer.ConsumeFromWhere;
//import com.aliyun.openservices.shade.com.alibaba.rocketmq.common.protocol.heartbeat.MessageModel;
//import lombok.Getter;
//import lombok.Setter;
//import lombok.ToString;
//
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//
//@Getter
//@Setter
//@ToString
//@Configuration
//@ConfigurationProperties(prefix = "rocketmq.consumer")
//public class MQConsumerConfigure {
//    public static final Logger LOGGER = LoggerFactory.getLogger(MQConsumerConfigure.class);
//
//    private String groupName;
//    private String namesrvAddr;
//    private String topic;
//    // 消费者线程数据量
//    private Integer consumeThreadMin;
//    private Integer consumeThreadMax;
//    private Integer consumeMessageBatchMaxSize;
//
//    @Autowired
//    private MQConsumeMsgListenerProcessor consumeMsgListenerProcessor;
//    /**
//     * mq 消费者配置
//     * @return
//     * @throws MQClientException
//     */
//    @Bean
//    @ConditionalOnProperty(prefix = "rocketmq.consumer", value = "isOn", havingValue = "true")
//    public DefaultMQPushConsumer defaultConsumer() throws MQClientException {
//        try {
//            LOGGER.info("defaultConsumer 正在创建---------------------------------------");
//            DefaultMQPushConsumer consumer = new DefaultMQPushConsumer(groupName);
//            // 设置该消费者订阅的主题和tag，如果订阅该主题下的所有tag，则使用*,
//            consumer.subscribe(topic, "*");
//            consumer.setNamesrvAddr(namesrvAddr);
//            consumer.setConsumeThreadMin(consumeThreadMin);
//            consumer.setConsumeThreadMax(consumeThreadMax);
//            consumer.setConsumeMessageBatchMaxSize(consumeMessageBatchMaxSize);
//            /**
//             * 设置consumer第一次启动是从队列头部开始还是队列尾部开始
//             * 如果不是第一次启动，那么按照上次消费的位置继续消费
//             */
//            consumer.setConsumeFromWhere(ConsumeFromWhere.CONSUME_FROM_LAST_OFFSET);
//            /**
//             * 设置消费模型，集群还是广播，默认为集群
//             */
//            consumer.setMessageModel(MessageModel.CLUSTERING);
//            // 设置监听
//            consumer.registerMessageListener(consumeMsgListenerProcessor);
//            consumer.start();
//            LOGGER.info("consumer 创建成功 groupName={}, topics={}, namesrvAddr={}",groupName,topic,namesrvAddr);
//
//            return consumer;
//        } catch (MQClientException e) {
//            LOGGER.error("consumer 创建失败!");
//        }
//        return null;
//    }
//
//}

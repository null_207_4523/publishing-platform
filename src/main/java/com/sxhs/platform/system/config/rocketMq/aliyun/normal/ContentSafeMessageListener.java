package com.sxhs.platform.system.config.rocketMq.aliyun.normal;

import com.alibaba.fastjson.JSONObject;
import com.aliyun.openservices.ons.api.Action;
import com.aliyun.openservices.ons.api.ConsumeContext;
import com.aliyun.openservices.ons.api.Message;
import com.aliyun.openservices.ons.api.MessageListener;
import com.aliyun.openservices.shade.com.alibaba.rocketmq.remoting.common.RemotingHelper;
import com.sxhs.platform.publishing.domain.NewsDO;
import com.sxhs.platform.publishing.service.ContentSafeMessageConsemerService;
import com.sxhs.platform.publishing.service.NewsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Objects;

@Component
@Slf4j
public class ContentSafeMessageListener implements MessageListener {

    @Autowired
    private ContentSafeMessageConsemerService contentSafeMessageConsemerService;
    @Autowired
    private NewsService newsService;

    @Override
    public Action consume(Message message, ConsumeContext context) {
        log.info("消费执行----topic:{}, Tag:{}", message.getTopic(), message.getTag());
        try {
            String msgBody = new String(message.getBody(), RemotingHelper.DEFAULT_CHARSET);
            log.info("消息数据----msgBody:{}", msgBody);
            Map<String, Object> mapType = JSONObject.parseObject(msgBody, Map.class);
            Integer newsId = Integer.valueOf(mapType.get("id").toString());
            String roldId = mapType.get("roldId").toString();
            log.info("消息数据----mapType:{}, newsId:{}, roldId:{}", mapType, newsId, roldId);
            NewsDO newsDO = newsService.get(newsId);
            if (Objects.nonNull(newsDO)) {
                newsDO.setRoleId(roldId);
                contentSafeMessageConsemerService.testText(newsDO);
            }
            return Action.CommitMessage;
        } catch (Exception e) {
            //消费失败
            e.printStackTrace();
            log.error("消息消费失败：", e);
            return Action.ReconsumeLater;
        }
    }
}

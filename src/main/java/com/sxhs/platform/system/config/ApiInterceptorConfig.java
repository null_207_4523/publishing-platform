package com.sxhs.platform.system.config;


import com.sxhs.platform.publishing.Interceptor.WebInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;

@Configuration
public class ApiInterceptorConfig implements WebMvcConfigurer {
    @Resource
    private WebInterceptor webInterceptor;
    /**
     * webInterceptor不需要拦截的请求
     * 比如swagger的请求、比如一些静态资源的访问、比如错误统一处理的页面
     */
    private static final String[] EXCLUDE_SESSION_PATH= {};
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(webInterceptor).excludePathPatterns(EXCLUDE_SESSION_PATH);
        registry.addInterceptor(authenticationInterceptor())
                // 拦截所有请求，通过判断是否有 @LoginRequired 注解 决定是否需要登录
                .addPathPatterns("/api/**");
        registry.addInterceptor(webapiInterceptor()).addPathPatterns("/webapi/**");
    }
    @Bean
    public AuthenticationInterceptor authenticationInterceptor() {
        return new AuthenticationInterceptor();
    }

    @Bean
    public WebapiInterceptor webapiInterceptor(){
        return new WebapiInterceptor();
    }

}

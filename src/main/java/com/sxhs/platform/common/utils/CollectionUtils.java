package com.sxhs.platform.common.utils;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class CollectionUtils extends org.apache.commons.collections.CollectionUtils {

    public static <T> List<List<T>> divide(List<T> origin, int size) {
        if (org.apache.commons.collections.CollectionUtils.isEmpty(origin)) {
            return Collections.emptyList();
        }

        int block = (origin.size() + size - 1) / size;
        return IntStream.range(0, block).
                boxed().map(i -> {
            int start = i * size;
            int end = Math.min(start + size, origin.size());
            return origin.subList(start, end);
        }).collect(Collectors.toList());
    }

    /**
     * 随机从列表中返回
     *
     * @param t     目标
     * @param count 随机个数
     * @param <T>   list
     * @return 随机选择的list
     */
    public static <T> List<T> random(List<T> t, int count) {
        //null
        if (t == null) {
            throw new NullPointerException("t 不能为空");
        }
        //小于0
        if (count <= 0) {
            throw new IllegalArgumentException("count 必须大于0");
        }
        //空
        if (t.isEmpty()) {
            return t;
        }
        //除重
        Set<T> notRepeat = new HashSet(t);
        //随机数大于了目标，直接返回
        if (notRepeat.size() <= count) {
            return new ArrayList<>(notRepeat);
        }
        //筛选
        Set<T> resultSet = new HashSet<>();
        while (resultSet.size() < count) {
            resultSet.add(t.get(new Random().nextInt(t.size() - 1)));
        }
        return new ArrayList<>(resultSet);
    }
}

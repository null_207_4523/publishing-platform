
package com.sxhs.platform.common.utils;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class SpringbeanUtil {
	
	/** applicationContext */
	private static ApplicationContext applicationContext;

	/**
	 * 不可实例化
	 */
	private SpringbeanUtil() {
	}

	public void destroy() throws Exception {
		applicationContext = null;
	}

	public static void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		if(SpringbeanUtil.applicationContext == null){
			SpringbeanUtil.applicationContext  = applicationContext;
		}
	}
	
	public static ApplicationContext getApplicationContext() {
		return applicationContext;
	}
	

	public static Object getBean(String name) {
		return applicationContext.getBean(name);
	}


	public static <T> T getBean(String name, Class<T> type) {
		return applicationContext.getBean(name, type);
	}
	public static <T> T getBean( Class<T> type) {
        return applicationContext.getBean(type);
    }
}

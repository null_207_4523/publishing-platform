package com.sxhs.platform.common.utils;

import com.sxhs.platform.common.config.Constant;
import com.sxhs.platform.publishing.domain.OrgSubscriptionDO;
import com.sxhs.platform.publishing.service.OrgSubscriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class VerificateUtil {

    @Autowired
    private OrgSubscriptionService orgSubscriptionService;

    private static VerificateUtil verificateUtil;

    @PostConstruct
    public void init() {
        verificateUtil = this;
        verificateUtil.orgSubscriptionService = this.orgSubscriptionService;
    }

    /**
     * 根据资讯orgId，校验用户是否为机构粉丝
     *
     * @param userId app登录用户id
     * @param orgId  资讯orgId
     * @return 0：不是粉丝，1：粉丝
     */
    public static Integer hasFans(Integer userId, Integer orgId) {
        Integer hasFans = Constant.HAS_FANS_NO;
        if (Objects.isNull(userId) || Objects.isNull(orgId))
            return hasFans;
        // 判断用户是否为粉丝(用户订阅机构是否跟资讯发布机构id相同)
        Map<String, Object> params = new HashMap<>();
        params.put("createdUserId", userId);
        params.put("hasDelete", Constant.HAS_DELETE_NO);
        //查询列表数据
        List<OrgSubscriptionDO> list = verificateUtil.orgSubscriptionService.list(params);
        List<Integer> orgIds = list.stream().map(OrgSubscriptionDO::getOrgId).collect(Collectors.toList());
        // 粉丝关注
        if (!org.springframework.util.CollectionUtils.isEmpty(orgIds) && CollectionUtils.contains(orgIds.iterator(), orgId))
            hasFans = Constant.HAS_FANS_YES;
        return hasFans;
    }

}

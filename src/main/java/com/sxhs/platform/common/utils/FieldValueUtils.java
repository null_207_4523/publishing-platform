package com.sxhs.platform.common.utils;

import com.sxhs.platform.common.config.Constant;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class FieldValueUtils {

    /**
     * 根据属性名获取属性值
     *
     * @param fieldName
     * @param object
     * @return
     */
    public static Integer getFieldValueByFieldName(String fieldName, Object object) {
        try {
            String firstLetter = fieldName.substring(0, 1).toUpperCase();
            String getter = "get" + firstLetter + fieldName.substring(1);
            Method method = object.getClass().getMethod(getter, new Class[]{});
            Integer value = (Integer) method.invoke(object, new Object[]{});
            return value;
        } catch (Exception e) {
            return 0;
        }
    }

    /**
     * 根据属性，拿到set方法，并把值set到对象中
     *
     * @param obj       对象
     * @param filedName 属性名称
     * @param value
     */
    public static void setValue(Object obj, String filedName, Object value) {
        Class<?> clazz = obj.getClass();
        String methodName = "set" + filedName.substring(0, 1).toUpperCase() + filedName.substring(1);
        Class<?> typeClass = null;
        try {
            typeClass = clazz.getDeclaredField(filedName).getType();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        try {
            Method method = clazz.getDeclaredMethod(methodName, new Class[]{typeClass});
            method.invoke(obj, new Object[]{getClassTypeValue(typeClass, value)});
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static Object updateCount(String field, Integer hasFans, Object o, Integer type, Integer variable) {
        List<String> fields = new ArrayList<>();
        fields.add(field);
        // 根据是否为粉丝更新资讯实体对应的字段数据,不是粉丝只更新总量字段，粉丝还要更新对应粉丝数量字段
        if (Objects.equals(Constant.HAS_FANS_YES, hasFans)) {
            String fieldName = toUpperCase4Index(field.substring(0, 1)) + field.substring(1);
            field = "fans" + fieldName;
            fields.add(field);
        }
        for (String filedName : fields) {
            Integer value = FieldValueUtils.getFieldValueByFieldName(filedName, o);
            if (Objects.isNull(value))
                value = 0;
            if (Objects.isNull(variable))
                variable = 1;
            if (Objects.equals(type, Constant.HAS_ADD_YES)) {
                FieldValueUtils.setValue(o, filedName, value + variable);
            } else if (Objects.equals(type, Constant.HAS_ADD_NO)) {
                if (value >= variable) {
                    FieldValueUtils.setValue(o, filedName, value - variable);
                } else {
                    FieldValueUtils.setValue(o, filedName, value);
                }
            }
        }
        return o;
    }

    /**
     * 首字母转换为大写
     *
     * @param string
     * @return
     */
    private static String toUpperCase4Index(String string) {
        char[] methodName = string.toCharArray();
        if (97 <= methodName[0] && methodName[0] <= 122) {
            methodName[0] ^= 32;
        }
        return String.valueOf(methodName);
    }

    /**
     * 通过class类型获取获取对应类型的值
     *
     * @param typeClass class类型
     * @param value     值
     * @return Object
     */
    private static Object getClassTypeValue(Class<?> typeClass, Object value) {
        if (typeClass == int.class || value instanceof Integer) {
            if (null == value) {
                return 0;
            }
            return value;
        } else if (typeClass == short.class) {
            if (null == value) {
                return 0;
            }
            return value;
        } else if (typeClass == byte.class) {
            if (null == value) {
                return 0;
            }
            return value;
        } else if (typeClass == double.class) {
            if (null == value) {
                return 0;
            }
            return value;
        } else if (typeClass == long.class) {
            if (null == value) {
                return 0;
            }
            return value;
        } else if (typeClass == String.class) {
            if (null == value) {
                return "";
            }
            return value;
        } else if (typeClass == boolean.class) {
            if (null == value) {
                return true;
            }
            return value;
        } else if (typeClass == BigDecimal.class) {
            if (null == value) {
                return new BigDecimal(0);
            }
            return new BigDecimal(value + "");
        } else {
            return typeClass.cast(value);
        }
    }

}

package com.sxhs.platform.common.utils;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(description = "返回相应数据")
public class ApiResult<T> implements Serializable {

    private static final long serialVersionUID = 2739276981783310859L;
    @ApiModelProperty(value = "编码")
    private String code;
    @ApiModelProperty(value = "信息")
    private String msg;
    @ApiModelProperty(value = "数据")
    private T data;

    public ApiResult() {
        this.code = "500";
        this.msg = "";
    }

    public ApiResult(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public ApiResult(String code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }


    //  默认方法

    public static <T> ApiResult success(){
        return successMsg("请求成功");
    }

    public static <T> ApiResult successMsg(String msg){
        ApiResult<T> apiBean = new ApiResult<>("200", msg, null);
        return apiBean;
    }

    public static <T> ApiResult success(T data, String msg){
        ApiResult<T> apiBean = new ApiResult<>("200", msg, data);
        return apiBean;
    }

    public static <T> ApiResult success(T data){
        ApiResult<T> apiBean = new ApiResult<>("200", "", data);
        return apiBean;
    }

    public static <T> ApiResult fail(){
        return fail("请求失败");
    }

    public static <T> ApiResult fail(String msg){
        ApiResult<T> apiBean = new ApiResult<>("500", msg);
        return apiBean;
    }

    public static ApiResult<?> fail(String code, String msg){
        ApiResult<?> apiBean = new ApiResult<Object>(code, msg);
        return apiBean;
    }






}

package com.sxhs.platform.common.utils;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Supplier;

/**
 * @Author LiuLiang
 * @Date 3:10 PM 2019/7/16
 * @Description //多线程归集数据
 * @Param
 * @return
 **/
public class DataService {

    public void test_po(){
        List<Integer> list = new ArrayList<>();
        List<CompletableFuture<Integer>> futureList = new ArrayList<>();
        for(int i=0; i<10; i++) {
            Integer index = i;
            futureList.add(CompletableFuture.supplyAsync(new Supplier<Integer>(){
                @Override
                public Integer get() {
                    if(index==1){
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    System.out.println("执行任务："+index);
                    return index;
                }
            }));
        }
        Long getResultStart = System.currentTimeMillis();
        System.out.println("结果归集开始时间="+ LocalDate.now());
        for(CompletableFuture<Integer> future : futureList) {
            Integer i = null;
            try {
                i = future.get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            System.out.println("任务i="+i+"获取完成!"+LocalDate.now());
            list.add(i);
        }
        System.out.println("list = " + Arrays.toString(list.toArray()));
        Long start = System.currentTimeMillis();
        System.out.println("总耗时="+(System.currentTimeMillis()-start)+",取结果归集耗时="+(System.currentTimeMillis()-getResultStart));

    }

    public static void main(String[] args) {
        DataService dataService = new DataService();
        dataService.test_po();
    }


}

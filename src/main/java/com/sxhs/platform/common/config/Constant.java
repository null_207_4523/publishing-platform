package com.sxhs.platform.common.config;

public class Constant {
    //演示系统账户
    public static String DEMO_ACCOUNT = "test";
    //自动去除表前缀
    public static String AUTO_REOMVE_PRE = "true";
    //停止计划任务
    public static String STATUS_RUNNING_STOP = "stop";
    //开启计划任务
    public static String STATUS_RUNNING_START = "start";
    //通知公告阅读状态-未读
    public static String OA_NOTIFY_READ_NO = "0";
    //通知公告阅读状态-已读
    public static int OA_NOTIFY_READ_YES = 1;
    //部门根节点id
    public static Long DEPT_ROOT_ID = 0L;
    //缓存方式
    public static String CACHE_TYPE_REDIS ="redis";

    public static String LOG_ERROR = "error";

    // 是否删除-未删除
    public static int HAS_DELETE_NO = 0;
    // 是否删除-已删除
    public static int HAS_DELETE_YES = 1;

    // 是否显示-显示
    public static int HAS_SHOW_YES = 0;
    // 是否显示-隐藏
    public static int HAS_SHOW_NO = 1;

    // 是否粉丝-不是
    public static int HAS_FANS_NO = 0;
    // 是否粉丝-是
    public static int HAS_FANS_YES = 1;

    // 是否点赞-不是
    public static int HAS_LIKE_NO = 0;
    // 是否点赞-是
    public static int HAS_LIKE_YES = 1;

    // 是否收藏-不是
    public static int HAS_COLLECT_NO = 0;
    // 是否收藏-是
    public static int HAS_COLLECT_YES = 1;

    // 数量增加-不是
    public static Integer HAS_ADD_NO = 0;
    // 数量增加-是
    public static Integer HAS_ADD_YES = 1;

    // 文章状态-草稿
    public static Integer NEWS_STATUS_DRAFT = 0;
    // 文章状态-保存
    public static Integer NEWS_STATUS_STORE = 1;
    // 文章状态-审核中
    public static Integer NEWS_STATUS_IN_REVIEW = 2;
    // 文章状态-审核通过
    public static Integer NEWS_STATUS_APPROVE = 3;
    // 文章状态-审核未通过
    public static Integer NEWS_STATUS_NO_APPROVE = 4;

    // 已读状态-未读
    public static int BE_READ_NO = 0;
    // 已读状态-已读
    public static int BE_READ_YES = 1;

    // 是否删除-不是
    public static final int BE_DELETE_NO = 0;
    // 是否删除-是
    public static final int BE_DELETE_YES = 1;


    // 是否删除-不是
    public static final String ASC = "ASC";
    // 是否删除-是
    public static final String DESC = "DESC";

}

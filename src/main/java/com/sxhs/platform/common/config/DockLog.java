package com.sxhs.platform.common.config;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class DockLog {

    private static final Logger logger = LoggerFactory.getLogger(DockLog.class);

    public void log(String message){
        logger.info(message);
    }

    public void error(String message){
        logger.error(message);
    }

}

package com.sxhs.platform.common.controller;

import com.sxhs.platform.system.domain.UserToken;
import org.springframework.stereotype.Controller;
import com.sxhs.platform.common.utils.ShiroUtils;
import com.sxhs.platform.system.domain.UserDO;

@Controller
public class BaseController {
	public UserDO getUser() {
		return ShiroUtils.getUser();
	}

	public Long getUserId() {
		return getUser().getUserId();
	}

	public String getUsername() {
		return getUser().getUsername();
	}
}
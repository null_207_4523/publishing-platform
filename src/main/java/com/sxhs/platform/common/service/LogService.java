package com.sxhs.platform.common.service;

import org.springframework.stereotype.Service;

import com.sxhs.platform.common.domain.LogDO;
import com.sxhs.platform.common.domain.PageDO;
import com.sxhs.platform.common.utils.Query;

@Service
public interface LogService {
	void save(LogDO logDO);
	PageDO<LogDO> queryList(Query query);
	int remove(Long id);
	int batchRemove(Long[] ids);
}

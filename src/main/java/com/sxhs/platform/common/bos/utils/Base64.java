package com.sxhs.platform.common.bos.utils;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;

/**
 * Base64处理类类。
 */
public class Base64 {
	
	public static String getStrByBase64(byte[] b)
	  {
	    BASE64Encoder encoder = new BASE64Encoder();
	    String str = encoder.encode(b);
	    return str;
	  }

	  public static byte[] getByteByString(String source)
	  {
	    BASE64Decoder decoder = new BASE64Decoder();
	    byte[] buf = (byte[])null;
	    try {
	    	buf = decoder.decodeBuffer(source);
	    } catch (IOException e) {
	      e.printStackTrace();
	    }
	    return buf;
	  }

	  public static String compressByGZIP(String str)
	    throws IOException
	  {
	    ByteArrayOutputStream out = new ByteArrayOutputStream();
	    DeflaterOutputStream gout = new DeflaterOutputStream(out);
	    gout.write(str.getBytes());
	    gout.finish();
	    gout.close();
	    return getStrByBase64(out.toByteArray());
	  }

	  public static String decompressByGZIP(String str)
	    throws IOException
	  {
	    byte[] buf = getByteByString(str);

	    ByteArrayOutputStream out = new ByteArrayOutputStream();
	    ByteArrayInputStream in = new ByteArrayInputStream(buf);
	    InflaterInputStream gin = new InflaterInputStream(in);
	    int i = 1024;
	    byte[] buffer = new byte[i];
	    while ((i = gin.read(buffer)) > 0) {
	      out.write(buffer, 0, i);
	    }
	    gin.close();
	    out.close();

	    return new String(out.toByteArray(), "UTF-8");
	  }
	  
	  /**
		 * 将字符串转化为base64编码
		 * @param s
		 * @return
		 * @throws UnsupportedEncodingException 
		 */
		public static String getBase64(String s) throws UnsupportedEncodingException {
			byte[] bytes= org.apache.commons.codec.binary.Base64.encodeBase64(s.getBytes("utf-8"));
			return new String(bytes, "utf-8");
			
		}

		/**
		 * 将 BASE64 编码的字符串 s 进行解码
		 * @param s
		 * @return
		 * @throws UnsupportedEncodingException 
		 */
		public static String getFromBase64(String s) throws UnsupportedEncodingException {
			byte[] bytes=s.getBytes("GBK");
			byte[] convertBytes=org.apache.commons.codec.binary.Base64.decodeBase64(bytes);
			return new String(convertBytes, "GBK");
		}
}

package com.sxhs.platform.common.bos.common;


import com.aliyun.oss.ClientConfiguration;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.common.comm.Protocol;
import com.aliyun.oss.model.*;
import com.sxhs.platform.common.bos.file.FilePropertiesUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import javax.swing.filechooser.FileSystemView;
import java.io.*;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @class:AliyunOSSClientUtil
 * @descript:java使用阿里云OSS存储对象上传图片
 * @date:2018年6月22日 下午5:58:08
 * @author qing
 */
public class OSSClientUtil {
    public final static String SAVEPATH = "/tmp/";
    //log日志
    private static Logger logger = LoggerFactory.getLogger(OSSClientUtil.class);
    //阿里云API的内或外网域名
    private static String ENDPOINT;
    //阿里云API的密钥Access Key ID
    private static String ACCESS_KEY_ID;
    //阿里云API的密钥Access Key Secret
    private static String ACCESS_KEY_SECRET;
    /*//阿里云API的bucket名称
    private static String BACKET_NAME;*/
    //阿里云API的文件夹名称
    private static String FOLDER = "";
    //阿里云API的文件夹路径
    private static String FOLDERGROUP;
    //阿里云API的文件夹子路径
    private static String PATH;

    private static final String COMPRESS = "compress";

    //初始化属性
    static{
        ENDPOINT = FilePropertiesUtil.getEndpoint();
        ACCESS_KEY_ID = FilePropertiesUtil.getAccessKeyId();
        ACCESS_KEY_SECRET = FilePropertiesUtil.getAccessKeySecret();
        //BACKET_NAME = FilePropertiesUtil.getBucket();
        PATH = FilePropertiesUtil.getImgFilePath();
    }

    /**
     * 获取阿里云OSS客户端对象
     * 并设置跨域资源共享规则
     * 以下代码用于设置指定存储空间的跨域资源共享规则，已存在的规则将被覆盖
     * @return ossClient
     */
    private static OSSClient getOSSClient(){
        ClientConfiguration config = new ClientConfiguration();
        config.setProtocol(Protocol.HTTPS);
        // 创建OSSClient实例。
        return new OSSClient(ENDPOINT,ACCESS_KEY_ID, ACCESS_KEY_SECRET,config);
    }

    /**
     * 上传到阿里云并设置为公共读权限
     * @param inputStream
     * @param fileName
     * @return
     */
    public static String upload(InputStream inputStream, String fileName,String backetName){
        OSSClient client = getOSSClient();
        //上传图片
        client.putObject(backetName, PATH + fileName, inputStream);
        //修改文件ACL权限为公共读
        //client.setObjectAcl(backetName, PATH + fileName, CannedAccessControlList.PublicRead);
        client.shutdown();
        return "https://" + backetName + "." + ENDPOINT + "/" + PATH + fileName;
    }
    /**
     * 上传文件 MultipartFile
     **/
    public static Map<String, Object> upload(MultipartFile file, String fileName, String backetName) throws IOException{
        OSSClient ossClient=OSSClientUtil.getOSSClient();
        return send(ossClient,file,backetName,FOLDERGROUP,fileName);
    }

    /**
     * 上传文件 File
     **/
    public static Map<String, Object> upload(File file, String fileName,String backetName) throws IOException{
        OSSClient ossClient=OSSClientUtil.getOSSClient();
        return send(ossClient,file, backetName,FOLDERGROUP,fileName);
    }

    /**
     * 删除文件
     * @param  key 文件url
     **/
    public static void delete(String key,String backetName) throws IOException{
        OSSClient ossClient=OSSClientUtil.getOSSClient();
        deleteFile(ossClient,backetName,FOLDER,key);
    }

    public static String getUrl(String backetName,String key){
        OSSClient ossClient=OSSClientUtil.getOSSClient();
        //获取CORS规则列表。
//        ArrayList<SetBucketCORSRequest.CORSRule> corsRules =  (ArrayList<SetBucketCORSRequest.CORSRule>) ossClient.getBucketCORSRules(BACKET_NAME);
        return getImg(ossClient,backetName,FOLDER,key);

    }

    /**
     * 获取原图文件连接
     * @param  key 压缩图url
     **/
    public static String getRawUrl(String key,String backetName) throws IOException{
        OSSClient ossClient=OSSClientUtil.getOSSClient();
        //获取CORS规则列表。
//        ArrayList<SetBucketCORSRequest.CORSRule> corsRules =  (ArrayList<SetBucketCORSRequest.CORSRule>) ossClient.getBucketCORSRules(BACKET_NAME);
        key = key.substring(COMPRESS.length());
       return getImg(ossClient,backetName,FOLDER,key);

    }


    /**
     * 上传图片至OSS  如果同名文件会覆盖服务器上的
     * @param ossClient  oss连接
     * @param file 上传文件（文件全路径如：D:\\image\\cake.jpg）
     * @param bucketName  存储空间
     * @param folder 模拟文件夹名 如"qj_nanjing/"
     * @return String 返回的唯一MD5数字签名
     * */
    public static  Map<String, Object> send(OSSClient ossClient, MultipartFile file, String bucketName, String folder, String fileName) {
        InputStream instream = null;
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            //以输入流的形式上传文件
            instream = file.getInputStream();
            //文件名
//            String fileName = file.getOriginalFilename().replaceAll("\\s*", "");// 去除名称空格

            //文件大小
            Long fileSize = file.getSize();
            //创建上传Object的Metadata
            ObjectMetadata metadata = new ObjectMetadata();
            //上传的文件的长度
            uploadToOss(ossClient, bucketName, folder, fileName, instream, map, fileSize, metadata);
            //解析结果
        }
        catch (IOException e) {
            e.printStackTrace();
            logger.error("上传阿里云OSS服务器异常." + e.getMessage(), e);
        } finally {
            try {
                if (instream != null) {
                    instream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return map;
    }



    /**
     * 上传图片至OSS  如果同名文件会覆盖服务器上的
     * @param ossClient  oss连接
     * @param file 上传文件（文件全路径如：D:\\image\\cake.jpg）
     * @param bucketName  存储空间
     * @param folder 模拟文件夹名 如"qj_nanjing/"
     * @return String 返回的唯一MD5数字签名
     * */
    public static  Map<String, Object>  send(OSSClient ossClient, File file, String bucketName, String folder, String fileName) {
        InputStream instream = null;
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            //以输入流的形式上传文件
          instream = new FileInputStream(file);
            //文件名
//            String fileName = file.getName();
            //文件大小
            Long fileSize = file.length();
            //创建上传Object的Metadata
            ObjectMetadata metadata = new ObjectMetadata();
            uploadToOss(ossClient, bucketName, folder, fileName, instream, map, fileSize, metadata);
            //解析结果
        }
        catch (Exception e) {
            e.printStackTrace();
            logger.error("上传阿里云OSS服务器异常." + e.getMessage(), e);
        } finally {
            try {
                if (instream != null) {
                    instream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return map;
    }

    private static void uploadToOss(OSSClient ossClient, String bucketName, String folder, String fileName, InputStream instream, Map<String, Object> map, Long fileSize, ObjectMetadata metadata) throws IOException {
        //上传的文件的长度
        metadata.setContentLength(instream.available());
        //指定该Object被下载时的网页的缓存行为
        metadata.setCacheControl("no-cache");
        //指定该Object下设置Header
        metadata.setHeader("Pragma", "no-cache");
        //指定该Object被下载时的内容编码格式
        metadata.setContentEncoding("utf-8");
        //文件的MIME，定义文件的类型及网页编码，决定浏览器将以什么形式、什么编码读取文件。如果用户没有指定则根据Key或文件名的扩展名生成，
        //如果没有扩展名则填默认值application/octet-stream
        metadata.setContentType(getContentType(fileName));
        //指定该Object被下载时的名称（指示MINME用户代理如何显示附加的文件，打开或下载，及文件名称）

        //处理图片名称
//            fileName = getFileName(fileName);
        metadata.setContentDisposition("filename/filesize=" + fileName + "/" + fileSize + "Byte.");
        //上传文件   (上传文件流的形式)
        PutObjectResult putResult = ossClient.putObject(bucketName, folder + fileName, instream, metadata);

        // 销毁、关闭client
        ossClient.shutdown();
        Date expiration = new Date(new Date().getTime() + 300L * 1000);
        String url = ossClient.generatePresignedUrl(bucketName, folder +fileName, expiration).toString();
//        System.out.println(url);
        map.put("url",PATH+fileName);
//            map.put("fileName",fileName);
        map.put("ETag",putResult.getETag());
        map.put("downlad",url);
    }

    /**
     * 获取缩略图名称
     */
    public static String getCompressFileName(String fileName) {
        StringBuffer stringBuffer = new StringBuffer();
        String substring = fileName.substring(0,fileName.length()-fileName.substring(fileName.lastIndexOf(".")).length()); //图片后缀
        String lastname = fileName.substring(fileName.lastIndexOf("."));//图片后缀
        stringBuffer.append(substring).append(COMPRESS).append(lastname);
        return stringBuffer.toString();
    }

    /**
     * 获得图片路径
     * @param key
     * @return
     */
    private static String getImg(OSSClient ossClient , String bucketName, String folder, String key) {
        // 设置URL过期时间为10年  3600l* 1000*24*365*10
        Date expiration = new Date(System.currentTimeMillis() + 300L * 1000);

        // 生成URL
        String url = ossClient.generatePresignedUrl(bucketName, folder + key, expiration).toString();
        url = url.substring(0,url.indexOf("?"));
        if (url != null) {
            return url.toString();
        }
        return null;
    }

    /**
     * 根据key删除OSS服务器上的文件
     * @param ossClient  oss连接
     * @param bucketName  存储空间
     * @param folder  模拟文件夹名 如"qj_nanjing/"
     * @param key 文件名 如："cake.jpg"
     */
    public static void deleteFile(OSSClient ossClient, String bucketName, String folder, String key){
        ossClient.deleteObject(bucketName, folder + key);
        logger.info("删除" + bucketName + "下的文件" + folder + key + "成功");
    }

    /**
     * 删除图片 警告：在没有调用其他方法的情况下，请调用closeClient方法
     *
     * @param key URL全路径
     */
    public void deleteImg(OSSClient ossClient, String bucketName, String folder, String key) {
        if (key == null || "".equals(key)) {
            return;
        }
        String[] paths = key.split("[.]");
        /**
         * 文件夹是否存在
         */
        if (!ossClient.doesObjectExist(bucketName, folder)) {
            ossClient.putObject(bucketName, folder, new ByteArrayInputStream(new byte[0]));
        }
        String[] name = paths[paths.length - 2].split("[/]");
        /**
         * 对象是否存在
         */
        if (ossClient.doesObjectExist(bucketName,
                        folder + name[name.length - 1] + "." + paths[paths.length - 1])) {
            /**
             * 删除存在对象
             */
            ossClient.deleteObject(bucketName, folder + name[name.length - 1] + "." + paths[paths.length - 1]);
        }
        ossClient.shutdown();
        logger.info("删除" + bucketName + "下的文件" + folder + key + "成功");
    }

    /**
     * 通过文件名判断并获取OSS服务文件上传时文件的contentType
     * @param fileName 文件名
     * @return 文件的contentType
     */
    private static  String getContentType(String fileName){
        //文件的后缀名
        String fileExtension = fileName.substring(fileName.lastIndexOf("."));
        if(".bmp".equalsIgnoreCase(fileExtension)) {
            return "image/bmp";
        }
        if(".gif".equalsIgnoreCase(fileExtension)) {
            return "image/gif";
        }
        if(".jpeg".equalsIgnoreCase(fileExtension) || ".jpg".equalsIgnoreCase(fileExtension)  || ".png".equalsIgnoreCase(fileExtension) ) {
            return "image/jpeg";
        }
        if(".html".equalsIgnoreCase(fileExtension)) {
            return "text/html";
        }
        if(".txt".equalsIgnoreCase(fileExtension)) {
            return "text/plain";
        }
        if(".vsd".equalsIgnoreCase(fileExtension)) {
            return "application/vnd.visio";
        }
        if(".ppt".equalsIgnoreCase(fileExtension) || "pptx".equalsIgnoreCase(fileExtension)) {
            return "application/vnd.ms-powerpoint";
        }
        if(".doc".equalsIgnoreCase(fileExtension) || "docx".equalsIgnoreCase(fileExtension)) {
            return "application/msword";
        }
        if(".xml".equalsIgnoreCase(fileExtension)) {
            return "text/xml";
        }
        if(".pdf".equalsIgnoreCase(fileExtension)) {
            return "application/pdf";
        }
        //默认返回类型
        return "image/jpeg";
    }


    /**
     * 创建存储空间
     * @param ossClient      OSS连接
     * @param bucketName 存储空间
     * @return
     */
    public  static String createBucketName(OSSClient ossClient, String bucketName){
        //存储空间
        final String bucketNames=bucketName;
        if(!ossClient.doesBucketExist(bucketName)){
            //创建存储空间
            Bucket bucket=ossClient.createBucket(bucketName);
            logger.info("创建存储空间成功");
            return bucket.getName();
        }
        return bucketNames;
    }

    /**
     * 删除存储空间buckName
     * @param ossClient  oss对象
     * @param bucketName  存储空间
     */
    public static  void deleteBucket(OSSClient ossClient, String bucketName){
        ossClient.deleteBucket(bucketName);
        logger.info("删除" + bucketName + "Bucket成功");
    }

    /**
     * 创建模拟文件夹
     * @param ossClient oss连接
     * @param bucketName 存储空间
     * @param folder   模拟文件夹名如"qj_nanjing/"
     * @return  文件夹名
     */
    public  static String createFolder(OSSClient ossClient, String bucketName, String folder){
        //文件夹名
        final String keySuffixWithSlash =folder;
        //判断文件夹是否存在，不存在则创建
        if(!ossClient.doesObjectExist(bucketName, keySuffixWithSlash)){
            //创建文件夹
            ossClient.putObject(bucketName, keySuffixWithSlash, new ByteArrayInputStream(new byte[0]));
            logger.info("创建文件夹成功");
            //得到文件夹名
            OSSObject object = ossClient.getObject(bucketName, keySuffixWithSlash);
            String fileDir=object.getKey();
            return fileDir;
        }
        return keySuffixWithSlash;
    }


    /*//设置跨域规则
    private void setGuiZe(){
        OSSClient ossClient = new OSSClient(ENDPOINT,ACCESS_KEY_ID, ACCESS_KEY_SECRET);

        SetBucketCORSRequest request = new SetBucketCORSRequest(BACKET_NAME);
        //CORS规则的容器，每个Bucket最多允许10条规则。
        ArrayList<SetBucketCORSRequest.CORSRule> putCorsRules = new ArrayList<SetBucketCORSRequest.CORSRule>();
        SetBucketCORSRequest.CORSRule corRule = new SetBucketCORSRequest.CORSRule();
        ArrayList<String> allowedOrigin = new ArrayList<String>();
        //指定允许跨域请求的来源。
//        allowedOrigin.add( "http://192.168.1.33:8033");
        allowedOrigin.add( "*");
        ArrayList<String> allowedMethod = new ArrayList<String>();
        //指定允许的跨域请求方法(GET/PUT/DELETE/POST/HEAD)。
        allowedMethod.add("GET");
        ArrayList<String> allowedHeader = new ArrayList<String>();
        //是否允许预取指令（OPTIONS）中Access-Control-Request-Headers头中指定的Header。
        allowedHeader.add("*");
//        ArrayList<String> exposedHeader = new ArrayList<String>();
//        指定允许用户从应用程序中访问的响应头。
//        exposedHeader.add("x-oss-test1");
        //最多支持一个*通配符
        corRule.setAllowedMethods(allowedMethod);
        corRule.setAllowedOrigins(allowedOrigin);
        corRule.setAllowedHeaders(allowedHeader);
//        corRule.setExposeHeaders(exposedHeader);
        //指定浏览器对特定资源的预取（OPTIONS）请求返回结果的缓存时间，单位为秒。
        corRule.setMaxAgeSeconds(10);
        //最多允许10条规则。
        putCorsRules.add(corRule);
        request.setCorsRules(putCorsRules);
        ossClient.setBucketCORS(request);
        ossClient.shutdown();
    }*/

    //下载到指定文件
    public static  String downladFile(String key,String BACKET_NAME) throws IOException {
        OSSClient ossClient=OSSClientUtil.getOSSClient();
        // 下载OSS文件到本地文件。如果指定的本地文件存在会覆盖，不存在则新建。
        File com = FileSystemView.getFileSystemView().getHomeDirectory();//读取桌面路径
        String substring = key.substring(key.lastIndexOf("/")+1);
//        String compressFileName = getCompressFileName(substring);
//        String downladPath = com  + "\\" +compressFileName;
        String downladPath = com  + "\\" +substring;
        ossClient.getObject(new GetObjectRequest(BACKET_NAME, FOLDER + key), new File(downladPath));
        // 关闭OSSClient。
        ossClient.shutdown();
        return downladPath;
    }

    private static void judeDirExists(File file) throws IOException {
        if (file.exists()) {
            if (file.isDirectory()) {
                String canonicalPath = file.getCanonicalPath();
                System.out.println("dir exists");
            } else {
                System.out.println("the same name file exists, can not create dir");
            }
        } else {
            System.out.println("dir not exists, create it ...");
            file.mkdir();
        }
    }

    public static OSSObject getObject(String key, String bucketName){
        OSSClient ossClient=OSSClientUtil.getOSSClient();
        return ossClient.getObject(bucketName,key);
    }

    public static String putObject(String buckname,String objectKey, InputStream inputStream){
        OSSClient ossClient=OSSClientUtil.getOSSClient();
        ObjectMetadata meta = new ObjectMetadata();
        PutObjectResult result = ossClient.putObject(buckname,objectKey,inputStream);
        // 打印ETag
        System.out.println(result.getETag());
        return result.getETag();
    }

    public static String putObjectWithSuffix(String buckname,String objectKey, InputStream inputStream){
        OSSClient ossClient=OSSClientUtil.getOSSClient();
        ObjectMetadata metadata = new ObjectMetadata();
        //文件的MIME，定义文件的类型及网页编码，决定浏览器将以什么形式、什么编码读取文件。如果用户没有指定则根据Key或文件名的扩展名生成，
        //如果没有扩展名则填默认值application/octet-stream
        metadata.setContentType(getContentType(objectKey));

        PutObjectResult result = ossClient.putObject(buckname,objectKey,inputStream,metadata);

        // 销毁、关闭client
        ossClient.shutdown();
        // 打印ETag
        System.out.println(result.getETag());
        return result.getETag();
    }

    public static String PutObjectByWord(String buckname,String objectKey, File file){
        OSSClient ossClient=OSSClientUtil.getOSSClient();
        ObjectMetadata meta = new ObjectMetadata();
        // 设置自定义元数据name的值为my-data
        meta.addUserMetadata("filename",file.getName());
        // 以文件形式上传Object
        PutObjectResult result = ossClient.putObject(buckname,objectKey,file,meta);
        // 打印ETag
        System.out.println(result.getETag());
        return objectKey;
    }


}
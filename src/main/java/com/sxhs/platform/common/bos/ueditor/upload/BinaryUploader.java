package com.sxhs.platform.common.bos.ueditor.upload;




import com.sxhs.platform.common.bos.common.BaseUtil;
import com.sxhs.platform.common.bos.common.UploadOSSUtil;
import com.sxhs.platform.common.bos.ueditor.PathFormat;
import com.sxhs.platform.common.bos.ueditor.define.AppInfo;
import com.sxhs.platform.common.bos.ueditor.define.BaseState;
import com.sxhs.platform.common.bos.ueditor.define.FileType;
import com.sxhs.platform.common.bos.ueditor.define.State;
import com.sxhs.platform.common.utils.DateUtils;
import org.apache.tomcat.util.http.fileupload.FileItemIterator;
import org.apache.tomcat.util.http.fileupload.FileItemStream;
import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class BinaryUploader {
    public static final State save(HttpServletRequest request, Map<String, Object> conf, int flag) {
        FileItemStream fileStream = null;
        boolean isAjaxUpload = request.getHeader("X_Requested_With") != null;

        if (!ServletFileUpload.isMultipartContent(request)) {
            return new BaseState(false, AppInfo.NOT_MULTIPART_CONTENT);
        }

        ServletFileUpload upload = new ServletFileUpload(new DiskFileItemFactory());

        if (isAjaxUpload) {
            upload.setHeaderEncoding("UTF-8");
        }

        try {
            FileItemIterator iterator = upload.getItemIterator(request);

            while (iterator.hasNext()) {
                fileStream = iterator.next();

                if (!fileStream.isFormField())
                    break;
                fileStream = null;
            }

            if (fileStream == null) {
                return new BaseState(false, AppInfo.NOTFOUND_UPLOAD_DATA);
            }

            String savePath = (String) conf.get("savePath");
            String originFileName = fileStream.getName();
            String suffix = FileType.getSuffixByFilename(originFileName);

            originFileName = originFileName.substring(0, originFileName.length() - suffix.length());
            savePath = savePath + suffix;

            long maxSize = ((Long) conf.get("maxSize")).longValue();

            if (!validType(suffix, (String[]) conf.get("allowFiles"))) {
                return new BaseState(false, AppInfo.NOT_ALLOW_FILE_TYPE);
            }

            savePath = PathFormat.parse(savePath, originFileName);

            String physicalPath = (String) conf.get("rootPath") + savePath;

            InputStream is = fileStream.openStream();
            BaseUtil baseUtil = new BaseUtil();
            String fileName = new StringBuffer()
                    .append(baseUtil.FILE_PATH)
                    .append(DateUtils.getMonth(new Date()))
                    .append("/")
                    .append(new Date().getTime())
                    .append(fileStream.getName().substring(fileStream.getName().indexOf("."))).toString();
            State storageState = null;
            String url ;
            try {
//                new UploadOSSUtil();
                if (flag==1) {
                    //视频
                    url = UploadOSSUtil.uploadVideo(is, fileName,baseUtil.getBUCKETNAME_IMG());
                }else{
                     url = UploadOSSUtil.uploadImgAliyun(is, fileName,baseUtil.getBUCKETNAME_IMG());
                    
                }
                storageState = StorageManager.saveFileByInputStream(is, physicalPath, maxSize);
                storageState.putInfo("state", "SUCCESS");
                storageState.putInfo("url", url);
                storageState.putInfo("title", fileName);
                storageState.putInfo("original", fileName);
            } catch (Exception e) {
                System.out.println(e.getMessage());
                storageState.putInfo("state", "文件上传失败!");
                storageState.putInfo("url", "");
                storageState.putInfo("title", "");
                storageState.putInfo("original", "");
                // System.out.println("文件 "+fileName+" 上传失败!");
            }

            // ********************结束**********************

            is.close();
            /*
             * if (storageState.isSuccess()) { storageState.putInfo("url",
             * PathFormat.format(savePath)); storageState.putInfo("type",
             * suffix); storageState.putInfo("original", originFileName +
             * suffix); }
             */
            // System.out.println("storageState="+storageState);
            return storageState;
        } catch (FileUploadException e) {
            return new BaseState(false, AppInfo.PARSE_REQUEST_ERROR);
        } catch (IOException e) {
        }
        return new BaseState(false, AppInfo.IO_ERROR);
    }

    private static boolean validType(String type, String[] allowTypes) {
        /* 103 */ List list = Arrays.asList(allowTypes);

        /* 105 */ return list.contains(type);
    }
}

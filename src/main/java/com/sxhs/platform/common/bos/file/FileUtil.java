package com.sxhs.platform.common.bos.file;


import com.sxhs.platform.common.bos.utils.Base64;
import com.sxhs.platform.common.bos.utils.CloseableUtil;
import com.sxhs.platform.common.bos.utils.OutputStreamUtil;
import com.sxhs.platform.common.bos.utils.StringPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ObjectUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Random;


/**
 * File - 工具类
 */
public class FileUtil {
	private static final Logger LOG = LoggerFactory.getLogger(FileUtil.class);

	public static String readFile(String filePath) {
		try {
			File file = new File(filePath);
			if (file.isFile() && file.exists()) { // 判断文件是否存在
				return readFile(file);
			} else {
				throw new FileException("找不到指定的文件" + filePath);
			}
		} catch (Exception e) {
			throw new FileException("读取文件内容出错:" + e.getMessage(), e);
		}
	}

	private static String readFile(File file) throws FileNotFoundException,
			IOException {
		return readInputStream(new FileInputStream(file));
	}

	public static byte[] readByte(String filePath) {
		try {
			return readByte(new FileInputStream(new File(filePath)));
		} catch (IOException e) {
			throw new FileException(e.getMessage(), e);
		}
	}

	public static byte[] readByte(InputStream is) {
		try {
			byte[] r = new byte[is.available()];
			while (is.read(r) != -1)
				;
			return r;
		} catch (IOException e) {
			throw new FileException(e.getMessage(), e);
		} finally {
			CloseableUtil.close(is);
		}
	}

	public static String readInputStream(InputStream inputStream)
			throws IOException {
		StringBuilder html = new StringBuilder();
		InputStreamReader read = null;
		try {
			read = new InputStreamReader(inputStream, StringPool.UTF_8);// 考虑到编码格式
			BufferedReader bufferedReader = new BufferedReader(read);
			String lineTxt = null;
			while ((lineTxt = bufferedReader.readLine()) != null) {
				html.append(lineTxt);
			}
		} finally {
			CloseableUtil.close(read);
		}
		return html.toString();
	}

	/**
	 * 把数据写至文件中
	 * 
	 * @param filePath
	 * @param name
	 * @param fileContent
	 */
	public static void writeFile(String filePath, String name,
			String fileContent) {
		FileOutputStream fos = null;
		FileChannel fcout = null;
		try {
			File file = new File(filePath);
			file.setWritable(true);
			if (!file.exists()) {
				file.mkdirs();
			}

			fos = new FileOutputStream(filePath + name, true);
			fcout = fos.getChannel();

			ByteBuffer buf = ByteBuffer.wrap(fileContent
					.getBytes(StringPool.UTF_8));
			buf.put(fileContent.getBytes(StringPool.UTF_8));
			buf.flip();

			fcout.write(buf);
		} catch (IOException e) {
			throw new FileException("写文件内容操作出错:" + e.getMessage(), e);
		} finally {
			CloseableUtil.close(fcout, fos);
		}
	}

	/**
	 * 把数据写至文件中
	 * 
	 * @param filePathAndName
	 * @param fileContent
	 */
	public static void writeFile(String filePathAndName, String fileContent) {
		OutputStreamWriter write = null;
		FileOutputStream fos = null;
		try {
			File f = new File(filePathAndName);
			if (!f.exists()) {
				f.createNewFile();
			}
			fos = new FileOutputStream(fileContent);
			write = new OutputStreamWriter(fos, StringPool.UTF_8);
			BufferedWriter writer = new BufferedWriter(write);
			writer.write(fileContent);
		} catch (Exception e) {
			throw new FileException("写文件内容操作出错:" + e.getMessage(), e);
		} finally {
			CloseableUtil.close(write, fos);
		}
	}

	/**
	 * 删除文件
	 * 
	 * @param filePath
	 */
	public static void deletePath(String filePath) {
		if (ObjectUtils.isEmpty(filePath)) {
			return;
		}

		File file = new File(filePath);
		if (!file.exists()) {
			return;
		}
		file.delete();
	}

	/**
	 * 删除目录（文件夹）以及目录下的文件
	 * 
	 * @param sPath
	 *            被删除目录的文件路径
	 * @return 目录删除成功返回true，否则返回false
	 */
	public static void deleteDirectory(String sPath) {
		
		if (!sPath.endsWith(File.separator)) {// 如果sPath不以文件分隔符结尾，自动添加文件分隔符
			sPath = sPath + File.separator;
		}
		
		File dirFile = new File(sPath);
		if (!dirFile.exists() || !dirFile.isDirectory()) {// 如果dir对应的文件不存在，或者不是一个目录，则退出
			return;
		}
		
		for (File file : dirFile.listFiles()) {// 删除文件夹下的所有文件(包括子目录)
			if (file.isFile()) {// 删除子文件
				file.delete();
			}else {// 删除子目录
				deleteDirectory(file.getAbsolutePath());
			}
		}
		dirFile.delete();
	}

	/**
	 * 删除文件
	 * 
	 * @param sPath
	 * @return
	 */
	public static boolean deleteFile(String sPath) {
		File file = new File(sPath);
		if (file.exists()) {
			return file.delete();
		}
		return false;
	}

	/**
	 * 下载文件。
	 * 
	 * @param response
	 * @param fullPath
	 *            文件的全路径
	 * @param fileName
	 *            文件名称。
	 * @throws IOException
	 */
	public static void downLoadFile(HttpServletRequest request,
                                    HttpServletResponse response, String fullPath, String fileName)
			throws IOException {
		File file = new File(fullPath);
		if (file.exists()) {
			downLoadFile(request, response, new FileInputStream(fullPath),
					fileName);
		} else {
			OutputStreamUtil.out(response, "文件不存在!");
		}
	}

	public static void downLoadFile(HttpServletRequest request,
                                    HttpServletResponse response, InputStream in, String fileName)
			throws IOException {

		OutputStream outp = response.getOutputStream();

		response.setContentType("APPLICATION/OCTET-STREAM");
		String filedisplay = fileName;
		String agent = (String) request.getHeader("USER-AGENT");
		// firefox，谷歌 Trident是标识是ie浏览器 特别处理ie11 的问题
		if (agent != null && agent.indexOf("MSIE") == -1
				&& agent.indexOf("Trident") == -1) {
			String enableFileName = "=?UTF-8?B?"
					+ (new String(Base64.getBase64(filedisplay))) + "?=";
			response.setHeader("Content-Disposition", "attachment; filename="
					+ enableFileName);
		} else {
			// ie
			filedisplay = URLEncoder.encode(filedisplay, "utf-8");
			response.addHeader("Content-Disposition", "attachment;filename="
					+ filedisplay);
		}
		try {
			outp = response.getOutputStream();
			byte[] b = new byte[1024];
			int i = 0;
			while ((i = in.read(b)) > 0) {
				outp.write(b, 0, i);
			}
			outp.flush();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		} finally {
			CloseableUtil.close(in, outp);
			response.flushBuffer();
		}
	}

	/**
	 * 复制
	 * 
	 * @param oldPath
	 * @param newPath
	 */
	public static void copy(String oldPath, String newPath) {
		File oldfile = new File(oldPath);
		if (!oldfile.exists()) {
			return;
		}

		InputStream inStream = null;
		FileOutputStream fs = null;
		try {
			int byteread = 0;
			inStream = new FileInputStream(oldPath);
			fs = new FileOutputStream(newPath);
			byte[] buffer = new byte[4096];
			while ((byteread = inStream.read(buffer)) != -1) {
				fs.write(buffer, 0, byteread);
			}
		} catch (Exception e) {
			throw new FileException(e.getMessage(), e);
		} finally {
			CloseableUtil.close(inStream, fs);
		}
	}

	/**
	 * 复制
	 * 
	 * @param oldfile
	 * @param newPath
	 */
	public static void copy(File oldfile, String newPath) {
		try {
			if (oldfile.exists()) {
				copy(new FileInputStream(oldfile.getPath()), newPath);
			}
		} catch (Exception e) {
			throw new FileException(e.getMessage(), e);
		}
	}

	/**
	 * 复制
	 * 
	 * @param inStream
	 * @param newPath
	 */
	public static void copy(InputStream inStream, String newPath) {
		int byteread = 0;
		FileOutputStream fs = null;
		try {
			fs = new FileOutputStream(newPath);
			byte[] buffer = new byte[4096];
			while ((byteread = inStream.read(buffer)) != -1) {
				fs.write(buffer, 0, byteread);
			}
		} catch (Exception e) {
			throw new FileException(e.getMessage(), e);
		} finally {
			CloseableUtil.close(fs, inStream);
		}
	}

	/**
	 * 获取文件名称
	 * 
	 * @param filePath
	 * @return
	 */
	public static String getFileName(String filePath) {

		int lastSeparatorIndexOf = filePath.lastIndexOf("/") + 1;

		String lastFileName = filePath.substring(lastSeparatorIndexOf < 0 ? 0
				: lastSeparatorIndexOf);

		int lastDotIndexOf = lastFileName.lastIndexOf(StringPool.DOT);

		return lastFileName.substring(0, lastDotIndexOf);
	}

	/**
	 * 是否存在
	 * 
	 * @param filePath
	 * @return
	 */
	public static boolean exists(String filePath) {
		return new File(filePath).exists();
	}

	/**
	 * 创建文件
	 * 
	 * @param filePath
	 * @throws IOException
	 */
	public static void createNewFile(String filePath) throws IOException {
		File file = new File(filePath);

		if (file.exists()) {
			return;
		}

		if (!file.getParentFile().exists()) {
			file.getParentFile().mkdirs();
		}

		file.createNewFile();
	}

	/**
	 * 检查文件夹,不存在的话 创建该文件夹
	 * 
	 * @param filePath
	 */
	public static void mkdirs(String filePath) {
		File file = new File(filePath);
		if (!file.exists()) {
			file.mkdirs();
		}
	}
	
	/**
	 * 移动文件或文件夹到另一个目录
	 * @param from
	 * @param to
	 * @throws Exception
	 */
	public static void renameTo(String from, String to) {// 移动指定文件夹内的全部文件
		
		File dir = new File(from);
		
		if(!dir.exists()){
			return;
		}
		
		File moveDir = new File(to);// 创建目标目录
		if (!moveDir.exists()) {// 判断目标目录是否存在
			moveDir.mkdirs();// 不存在则创建
		}
		
		File[] files = null;
		if(dir.isDirectory()){
			files = dir.listFiles();// 将文件或文件夹放入文件集
		}else{
			files = new File[]{dir};
		}
		
		if (files == null){
			return;
		}

		for (File file :files) {// 遍历文件集
			if (file.isDirectory()) {// 如果是文件夹或目录,则递归调用fileMove方法，直到获得目录下的文件
				renameTo(file.getPath(), to + File.separator + file.getName());// 递归移动文件
				file.delete();// 删除文件所在原目录
			}
			File moveFile = new File(moveDir.getPath() + File.separator + file.getName());
			if (moveFile.exists()) {// 目标文件夹下存在的话，删除
				moveFile.delete();
			}
			file.renameTo(moveFile);// 移动文件
		}
	}
	/**
	 * Description: 判断OSS服务文件上传时文件的contentType
	 *
	 * @param filenameExtension
	 *            文件后缀
	 * @return String
	 */
	public static String getcontentType(String filenameExtension) {
		if (filenameExtension.equalsIgnoreCase("bmp")) {
			return "image/bmp";
		}
		if (filenameExtension.equalsIgnoreCase("gif")) {
			return "image/gif";
		}
		if (filenameExtension.equalsIgnoreCase("jpeg") || filenameExtension.equalsIgnoreCase("jpg")
				|| filenameExtension.equalsIgnoreCase("png")) {
			return "image/jpeg";
		}
		if (filenameExtension.equalsIgnoreCase("html")) {
			return "text/html";
		}
		if (filenameExtension.equalsIgnoreCase("txt")) {
			return "text/plain";
		}
		if (filenameExtension.equalsIgnoreCase("vsd")) {
			return "application/vnd.visio";
		}
		if (filenameExtension.equalsIgnoreCase("pptx") || filenameExtension.equalsIgnoreCase("ppt")) {
			return "application/vnd.ms-powerpoint";
		}
		if (filenameExtension.equalsIgnoreCase("docx") || filenameExtension.equalsIgnoreCase("doc")) {
			return "application/msword";
		}
		if (filenameExtension.equalsIgnoreCase("xml")) {
			return "text/xml";
		}
		return "image/jpeg";
	}
	public static String getRandomName(String originalFilename){
		String substring = originalFilename.substring(originalFilename.lastIndexOf(".")).toLowerCase();
		Random random = new Random();
		String name = random.nextInt(10000) + System.currentTimeMillis() + substring;
		return name;
	}
	/**
	 * MultipartFile 转 File
	 * @param mf  MultipartFile
	 */
	public static File multiFileToFile(MultipartFile mf) {
		File file = null;
		try {
			if(mf.equals("")||mf.getSize()<=0){
				mf = null;
			}else {
				InputStream ins = mf.getInputStream();
				file = new File(mf.getOriginalFilename());
				OutputStream os = new FileOutputStream(file);
				int bytesRead = 0;
				byte[] buffer = new byte[8192];
				while ((bytesRead = ins.read(buffer, 0, 8192)) != -1) {
					os.write(buffer, 0, bytesRead);
				}
				os.close();
				ins.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return  file;
	}
}

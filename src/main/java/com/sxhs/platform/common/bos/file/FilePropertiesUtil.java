package com.sxhs.platform.common.bos.file;


import com.sxhs.platform.common.bos.common.BaseUtil;

import java.lang.reflect.Field;

public class FilePropertiesUtil {

    private static final String HTTP = "http://";

    private static BaseUtil baseUtil = new BaseUtil();



    private FilePropertiesUtil() {
    }

    public static String getHttpEndpoint() {
        return HTTP + getEndpoint();
    }

    public static String getEndpoint() {
        return get("endpoint");
    }

    public static String getAccessKeyId() {
        return get("ACCESS_KEY_ID");
    }

    public static String getAccessKeySecret() {
        return get("SECRET_ACCESS_KEY");
    }

    public static String getSecurityToken() {
        return get("ENDPOINT");
    }

    public static String getRoleArnWrite() {
        return get("roleArnWrite");
    }

    public static String getRoleArnRead() {
        return get("roleArnRead");
    }

    public static String getRoleSessionName() {
        return get("roleSessionName");
    }

    public static String getBucket() {
        return get("BUCKETNAME");
    }

    public static String getBucketTest() {
        return get("bucketTest");
    }

    public static String getImgFilePath() {
        return get("imgFilePath");
    }

    public static String getRegion() {
        return get("region");
    }

    public static String getStsApiVersion() {
        return get("stsApiVersion");
    }

    public static String getDownLoadFileName() {
        return get("downLoadFileName");
    }

    public static String getFileCatalogue() {
        String osName = System.getProperty("os.name");
        if (osName.toLowerCase().startsWith("win")) {
            return get("fileWindowsPath");
        }
        return get("fileLinuxPath");
    }

    private static String get(String key) {
        try {
            Field field = baseUtil.getClass().getField(key.toUpperCase());
            //设置对象的访问权限，保证对private的属性的访问
            return  (String)field.get(baseUtil);
        } catch (Exception e) {
            e.printStackTrace();
           throw new RuntimeException("Unknown Internal Error");
        }
    }
}

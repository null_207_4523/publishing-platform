package com.sxhs.platform.common.bos.common;

import com.aliyun.oss.OSSClient;
import com.sxhs.platform.common.bos.file.FilePropertiesUtil;
import com.sxhs.platform.common.bos.file.FileUtil;
import com.sxhs.platform.common.bos.file.ImageCompressUtil;
import org.springframework.web.multipart.MultipartFile;

import javax.swing.filechooser.FileSystemView;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Date;

public class UploadOSSUtil {
    public UploadOSSUtil() {
    }

    public static String uploadImgAliyun(InputStream inputStream, String fileName,String bucketName) throws FileNotFoundException {
        // 实例化client
        String accesskeyId = FilePropertiesUtil.getAccessKeyId();
        String accessKeySecret = FilePropertiesUtil.getAccessKeySecret();
        String endpoint = FilePropertiesUtil.getEndpoint();
        String filePath = FilePropertiesUtil.getImgFilePath();
        OSSClient client = new OSSClient(endpoint, accesskeyId, accessKeySecret);
        // 上传
        client.putObject(bucketName, filePath + fileName, inputStream);
        //client.setObjectAcl(bucketName, filePath + fileName, CannedAccessControlList.PublicRead);
        client.shutdown();
        // 设置URL过期时间为10年  3600l* 1000*24*365*10
        Date expiration = new Date(System.currentTimeMillis() + 300L * 1000);
        String url = client.generatePresignedUrl(bucketName, filePath + fileName, expiration).toString();
        url = url.substring(0,url.indexOf("?"));
        return url;
    }

    // 压缩后上传
    public static String uploadImgAliyun2(InputStream inputStream, String fileName,String backetName) throws Exception {
        // 压缩
        File com = FileSystemView.getFileSystemView().getHomeDirectory();
        String localBath = com.toString() + "\\" + fileName;
        ImageCompressUtil.equalProportionCompress(inputStream, localBath, 800d, 1d);

        // 上传压缩后的图片到阿里云
        return OSSClientUtil.upload(new FileInputStream(new File(localBath)), fileName,backetName);
    }

    // 上传视频
    public static String uploadVideo(InputStream inputStream, String fileName,String backetName) {
        String vodId = VodUtil.uploadStream(fileName, fileName, inputStream);
        String url = "";
        if (vodId != null) {// 上传成功！
            while (true) {
                // 这里循环去拿视频的链接，由于转码需要时间，所以每隔五秒等待再重新拿。
                url = VodUtil.returnUrl(vodId);
                System.out.print("-----------" + url);//
                if (url != null && !url.equals("")) {
                    break;
                }
                try {
                    Thread.sleep(5000);
                } catch (Exception e) {

                }
            }
        }
        return url;
    }

    public static String upload(MultipartFile file, String backetName) throws Exception {
        // 拿到图片后压缩并保存到本地
        File com = FileSystemView.getFileSystemView().getHomeDirectory();
        String fileName = FileUtil.getRandomName(file.getOriginalFilename());
        String localBath = com.toString() + "\\" + fileName;
        ImageCompressUtil.equalProportionCompress(FileUtil.multiFileToFile(file), localBath, 800d, 1d);
        File f = new File(localBath);
        // 上传压缩后的图片到阿里云
        return OSSClientUtil.upload(new FileInputStream(f), fileName,backetName);
    }
}
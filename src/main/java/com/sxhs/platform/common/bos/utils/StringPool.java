package com.sxhs.platform.common.bos.utils;

public interface StringPool {
	
	String HTML = "html";
	
	String DOT = ".";
	
	String PDF = "pdf";
	
	String UTF_8 = "UTF-8";
	
	String DOT_HTML = DOT + HTML;
	
	String DOT_PDF = DOT + PDF;
	
	String ML = "_ML";
	
	String FM = "_FM";
	
    String UNDERLINE = "_";
    
    String VERTICAL_BAR = "|";
    
    String ZIP = "zip";
    
    String GBK = "GBK";
    
    String DOT_ZIP = DOT + ZIP;
    
    String COMMA = ",";
    
    String IDS = "ids";
    
    String STATUS = "status";
    
    String BR = "<br/>";
    
    String BODY_START = "<body>";
    
    String BODY_END = "</body>";
    
    String ASTERISK = "*";
    
    Integer MAX_BATCH_NUMBER = 1000;
}

package com.sxhs.platform.common.bos.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;

/**
 * 工具类 - OutputStreamUtil
 */
public class OutputStreamUtil {
	
	private static final Logger LOG = LoggerFactory.getLogger(OutputStreamUtil.class);
	
	private OutputStreamUtil(){
	}
	
	public static void out(HttpServletResponse response, String str){
		OutputStream outp = null;
		try {
			response.setCharacterEncoding(StringPool.UTF_8);
			response.setContentType("text/html;charset="  + StringPool.UTF_8);
			outp = response.getOutputStream();
			outp.write(str.getBytes(StringPool.UTF_8));
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}finally{
			CloseableUtil.close(outp);
		}
	}
}

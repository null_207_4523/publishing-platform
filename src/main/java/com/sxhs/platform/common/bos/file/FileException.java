package com.sxhs.platform.common.bos.file;

/**
 * Created by lenovo on 2018/7/25.
 */
public class FileException extends RuntimeException {

    private static final long serialVersionUID = -2315843801764714493L;

    public FileException(String string) {
        super(string);
    }

    public FileException(String string, Throwable e) {
        super(string, e);
    }

    public FileException() {
        super();
    }
    
}
package com.sxhs.platform.common.bos.common;


import com.aliyun.vod.upload.impl.UploadVideoImpl;
import com.aliyun.vod.upload.req.UploadStreamRequest;
import com.aliyun.vod.upload.resp.UploadStreamResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.vod.model.v20170321.*;
import org.apache.commons.lang3.StringUtils;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @Author:
 * @Date:2018/8/10
 * @Description:
 */
public class VodUtil {

    private static String accessKeyId = BaseUtil.VOD_ACCESS_KEY_ID;
    private static String accessKeySecret = BaseUtil.VOD_SECRET_ACCESS_KEY;

    /**
     * 流式上传接口
     *
     * @param title
     * @param fileName
     * @param inputStream
     */
    public static String uploadStream(String title, String fileName, InputStream inputStream) {

        UploadStreamRequest request = new UploadStreamRequest(accessKeyId, accessKeySecret, title, fileName, inputStream);
         //是否使用默认水印(可选)，指定模板组ID时，根据模板组配置确定是否使用默认水印
        request.setShowWaterMark(true);

         /* 设置上传完成后的回调URL(可选)，建议通过点播控制台配置消息监听事件，参见文档
         * https://help.aliyun.com/document_detail/57029.html
         */
        // request.setCallback("http://callback.sample.com");
         //视频分类ID(可选)
       // request.setCateId(1L);
         //视频标签,多个用逗号分隔(可选)
        //request.setTags("标签1,标签2");
        // 视频描述(可选)
        request.setDescription(title);
         //封面图片(可选)
        //request.setCoverURL("http://cover.sample.com/sample.jpg");
         //模板组ID(可选)
        // request.setTemplateGroupId("");
         //存储区域(可选)
        // request.setStorageLocation("");
        UploadVideoImpl uploader = new UploadVideoImpl();
        UploadStreamResponse response = uploader.uploadStream(request);
        // System.out.print("RequestId=" + response.getRequestId() + "\n");
        // //请求视频点播服务的请求ID
        if (response.isSuccess()) {
            System.out.print("VideoId=" + response.getVideoId() + "\n");
            return response.getVideoId();
        } else {
            return null;
        }
    }

    public static String returnUrl(String videoId) {
        DefaultAcsClient client = initVodClient(accessKeyId, accessKeySecret);
        GetPlayInfoResponse response = new GetPlayInfoResponse();
        String url = "";
        try {
            response = getPlayInfo(client, videoId);
            List<GetPlayInfoResponse.PlayInfo> playInfoList = response.getPlayInfoList();
            // 播放地址
            for (GetPlayInfoResponse.PlayInfo playInfo : playInfoList) {
                if (playInfo.getFormat().equals("mp4")) {
                    System.out.print("PlayInfo.PlayURL = " + playInfo.getPlayURL() + "\n");
                    url = playInfo.getPlayURL();
                }
            }

        } catch (Exception e) {
            System.out.print("ErrorMessage = " + e.getLocalizedMessage());
        }
        return url;
    }

    public static DefaultAcsClient initVodClient(String accessKeyId, String accessKeySecret) {
        // 点播服务所在的Region，国内请填cn-shanghai，不要填写别的区域
        String regionId = "cn-beijing";
        DefaultProfile profile = DefaultProfile.getProfile(regionId, accessKeyId, accessKeySecret);
        DefaultAcsClient client = new DefaultAcsClient(profile);
        return client;
    }

    /* 获取播放地址函数 */
    public static GetPlayInfoResponse getPlayInfo(DefaultAcsClient client, String videoId) throws Exception {
        GetPlayInfoRequest request = new GetPlayInfoRequest();
        request.setVideoId(videoId);
        return client.getAcsResponse(request);
    }

    public static Map<String,Object> getMediaResource( String mediaId) {
        DefaultAcsClient client = initVodClient(accessKeyId, accessKeySecret);
        GetPlayInfoResponse response = new GetPlayInfoResponse();
        String url = "";
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            response = getPlayInfo(client, mediaId);
            List<GetPlayInfoResponse.PlayInfo> playInfoList = response.getPlayInfoList();
            if (StringUtils.equals(StringUtils.lowerCase(response.getVideoBase().getStatus()), "normal")) {
                // 播放地址
                for (GetPlayInfoResponse.PlayInfo playInfo : playInfoList) {
                    if (playInfo.getFormat().equals("mp4")) {
                        if (StringUtils.equals(StringUtils.lowerCase(playInfo.getStatus()), "normal")) {
                            map.put("status", StringUtils.lowerCase(response.getVideoBase().getStatus()));
                            map.put("size", playInfo.getSize());
                            return map;
                        }
                    }
                }
            }
            map.put("status", "RUNNING");
        } catch (Exception e) {
            map.put("status", "RUNNING");
            System.out.print("ErrorMessage = " + e.getLocalizedMessage());
        }
        return map;
    }

    public static Map<String,Object> generateMediaDeliveryInfo(String mediaId) {
        DefaultAcsClient client = initVodClient(accessKeyId, accessKeySecret);
        GetPlayInfoResponse response = new GetPlayInfoResponse();
        Map<String,Object> data=new HashMap<String,Object>();
        try {
            response = getPlayInfo(client, mediaId);
            data.put("mediaSourceUrl",returnUrl(mediaId));
            data.put("mediaCoverPage",response.getVideoBase().getCoverURL());
            data.put("uuid", UUID.randomUUID());
        }catch (Exception e) {
            System.out.print("ErrorMessage = " + e.getLocalizedMessage());
        }
        return data;
    }

    /**
     * 获取视频上传地址和凭证
     */
    public static HashMap<String, Object> createUploadVideo(Map<String, Object> param) {
        DefaultAcsClient client = initVodClient(accessKeyId, accessKeySecret);

        CreateUploadVideoRequest request = new CreateUploadVideoRequest();
        request.setTitle(param.get("title").toString());
        request.setFileName(param.get("fileName").toString());
        //JSONObject userData = new JSONObject();
        //JSONObject messageCallback = new JSONObject();
        //messageCallback.put("CallbackURL", "http://xxxxx");
        //messageCallback.put("CallbackType", "http");
        //userData.put("MessageCallback", messageCallback.toJSONString());
        //JSONObject extend = new JSONObject();
        //extend.put("MyId", "user-defined-id");
        //userData.put("Extend", extend.toJSONString());
        //request.setUserData(userData.toJSONString());
        HashMap<String, Object> map = new HashMap<>();
        try {
            CreateUploadVideoResponse response = client.getAcsResponse(request);
            map.put("VideoId", response.getVideoId());
            map.put("UploadAddress", response.getUploadAddress());
            map.put("UploadAuth", response.getUploadAuth());
            map.put("RequestId", response.getRequestId());
        } catch (ClientException e) {
            e.printStackTrace();
            map.put("ErrorMessage", e.getLocalizedMessage());
        }

        return map;
    }

    /**
     * 刷新视频上传凭证
     */
    public static Map<String, Object> refreshUploadVideo(Map<String, Object> param) {
        DefaultAcsClient client = initVodClient(accessKeyId, accessKeySecret);
        RefreshUploadVideoRequest request = new RefreshUploadVideoRequest();
        request.setVideoId(param.get("videoId").toString());

        Map<String, Object> map = new HashMap<>();
        try {
            RefreshUploadVideoResponse response = client.getAcsResponse(request);
            map.put("VideoId", response.getVideoId());
            map.put("UploadAddress", response.getUploadAddress());
            map.put("UploadAuth", response.getUploadAuth());
        } catch (ClientException e) {
            e.printStackTrace();
            map.put("ErrorMessage", e.getLocalizedMessage());
        }
        return map;
    }

    /*获取播放凭证函数*/
    public static Map<String, Object> getVideoPlayAuth(Map<String, Object> param) {
        DefaultAcsClient client = initVodClient(accessKeyId, accessKeySecret);
        GetVideoPlayAuthRequest request = new GetVideoPlayAuthRequest();
        request.setVideoId(param.get("videoId").toString());
        request.setAuthInfoTimeout(Long.valueOf(param.get("authInfoTimeout").toString()));
        Map<String, Object> map = new HashMap<>();
        try {
            GetVideoPlayAuthResponse response = client.getAcsResponse(request);
            map.put("PlayAuth", response.getPlayAuth());
            map.put("RequestId", response.getRequestId());
            map.put("VideoId", response.getVideoMeta().getVideoId());
            map.put("Title", response.getVideoMeta().getTitle());
            map.put("CoverUrl", response.getVideoMeta().getCoverURL());
        } catch (ClientException e) {
            e.printStackTrace();
            map.put("ErrorMessage", e.getLocalizedMessage());
        }
        return map;
    }

    /**
     * 获取视频状态
     * @param videoId
     * @param format
     * @return
     */
    public static String getVideoPlayStatus( String videoId, String format){
        DefaultAcsClient client = initVodClient(accessKeyId, accessKeySecret);
        GetVideoInfoRequest request = new GetVideoInfoRequest();
        request.setVideoId(videoId);
        try {
            GetVideoInfoResponse response = client.getAcsResponse(request);
            if (!response.getVideo().getStatus().equalsIgnoreCase("normal")){
                return response.getVideo().getStatus();
            }else{
                GetPlayInfoResponse playInfoResponse = getPlayInfo(client,videoId);
                for (GetPlayInfoResponse.PlayInfo playInfo : playInfoResponse.getPlayInfoList()) {
                    if (StringUtils.equals(StringUtils.lowerCase(playInfo.getFormat()), format)) {
                        return playInfo.getStatus();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("视频状态非法，请重新上传");
        }
        return null;
    }

    public static void delete(String mediaId){
        UploadVideoImpl uploader = new UploadVideoImpl();
        uploader.removeFile(mediaId);
    }
}

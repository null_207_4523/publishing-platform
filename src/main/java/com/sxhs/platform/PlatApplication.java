package com.sxhs.platform;

import com.aliyun.openservices.shade.com.google.common.base.Charsets;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.util.List;

@EnableAutoConfiguration(exclude = {
        org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration.class
})
@EnableTransactionManagement
@ServletComponentScan
@MapperScan("com.sxhs.platform.*.dao")
@SpringBootApplication
@EnableCaching
@EnableScheduling
public class PlatApplication {
    public static void main(String[] args) {
        SpringApplication.run(PlatApplication.class, args);
        System.out.println("                _               _                 _         _             _                    _ \n" +
                "               (_)             | |               | |       | |           | |                  | |\n" +
                " ___ _ __  _ __ _ _ __   __ _  | |__   ___   ___ | |_   ___| |_ __ _ _ __| |_    ___ _ __   __| |\n" +
                "/ __| '_ \\| '__| | '_ \\ / _` | | '_ \\ / _ \\ / _ \\| __| / __| __/ _` | '__| __|  / _ \\ '_ \\ / _` |\n" +
                "\\__ \\ |_) | |  | | | | | (_| | | |_) | (_) | (_) | |_  \\__ \\ || (_| | |  | |_  |  __/ | | | (_| |\n" +
                "|___/ .__/|_|  |_|_| |_|\\__, | |_.__/ \\___/ \\___/ \\__| |___/\\__\\__,_|_|   \\__|  \\___|_| |_|\\__,_|\n" +
                "    | |                  __/ |                                                                   \n" +
                "    |_|                 |___/   ");
    }

    @Bean
    public ModelMapper modelMapper() {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setFullTypeMatchingRequired(true);
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        return modelMapper;
    }

}
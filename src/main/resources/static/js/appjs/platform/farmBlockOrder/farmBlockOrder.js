
var prefix = "/platform/farmBlockOrder"
$(function() {
	orderDic = getDicVal("DDLX");
	orderStatus = ['待处理','已处理']
    loadShed();
    loadBlock();
	load();
});

function load() {
	$('#exampleTable')
			.bootstrapTable(
					{
						method : 'get', // 服务器数据的请求方式 get or post
						url : prefix + "/orderList", // 服务器数据的加载地址
					//	showRefresh : true,
					//	showToggle : true,
					//	showColumns : true,
						iconSize : 'outline',
						toolbar : '#exampleToolbar',
						striped : true, // 设置为true会有隔行变色效果
						dataType : "json", // 服务器返回的数据类型
						pagination : true, // 设置为true会在底部显示分页条
						// queryParamsType : "limit",
						// //设置为limit则会发送符合RESTFull格式的参数
						singleSelect : false, // 设置为true将禁止多选
						// contentType : "application/x-www-form-urlencoded",
						// //发送到服务器的数据编码类型
						pageSize : 10, // 如果设置了分页，每页数据条数
						pageNumber : 1, // 如果设置了分布，首页页码
						//search : true, // 是否显示搜索框
						showColumns : false, // 是否显示内容下拉框（选择显示的列）
						sidePagination : "server", // 设置在哪里进行分页，可选值为"client" 或者 "server"
						queryParams : function(params) {
							return {
								//说明：传入后台的参数包括offset开始索引，limit步长，sort排序列，order：desc或者,以及所有列的键值对
								limit: params.limit,
								offset:params.offset,
					           // name:$('#searchName').val(),
					           	fiid:$('#fiid').val()
							};
						},
						// //请求服务器数据时，你可以通过重写参数的方式添加一些额外的参数，例如 toolbar 中的参数 如果
						// queryParamsType = 'limit' ,返回参数必须包含
						// limit, offset, search, sort, order 否则, 需要包含:
						// pageSize, pageNumber, searchText, sortName,
						// sortOrder.
						// 返回false将会终止请求
						columns : [
								{
									checkbox : true
								},
																{
									field : 'name',
									title : '大棚'
								},
																{
									field : 'blockName',
									title : '地块'
								},
																{
									field : 'orderType', 
									title : '订单类型' ,
									formatter : function(value, row, index){
										return orderDic[value];
									}
								},
																{
									field : 'orderTime', 
									title : '订单时间' 
								},
																{
									field : 'remarks', 
									title : '订单备注信息' 
								},
																{
									field : 'orderUser',
									title : '订单发布人'
								},
								{
									field : 'orderStatus',
									title : '订单状态',
                                    formatter : function(value, row, index){
                                        return orderStatus[value];
                                    }
								},
																{
									title : '操作',
									field : 'id',
									align : 'center',
									formatter : function(value, row, index) {
										var e = '<a class="btn btn-primary btn-sm '+s_edit_h+'" href="#" mce_href="#" title="编辑" onclick="edit(\''
												+ row.fboid
												+ '\')"><i class="fa fa-edit"></i></a> ';
										var d = '<a class="btn btn-warning btn-sm '+s_remove_h+'" href="#" title="删除"  mce_href="#" onclick="remove(\''
												+ row.fboid
												+ '\')"><i class="fa fa-remove"></i></a> ';
										var f = '<a class="btn btn-success btn-sm" href="#" title="备用"  mce_href="#" onclick="resetPwd(\''
												+ row.fboid
												+ '\')"><i class="fa fa-key"></i></a> ';
										var g = '<a class="btn btn-success btn-sm '+s_farmBlockOrderLog_h+'" href="#" title="日志"  mce_href="#" onclick="farmBlockOrderLog(\''
												+ row.fboid
												+ '\')">日志</i></a> ';
                                        var h = '<a class="btn btn-success btn-sm " href="#" title="处理"  mce_href="#" onclick="farmBlockOrderOperate(\''
                                            + row.fboid
                                            + '\')">处理</i></a> ';
                                        if(row.orderStatus==1){
                                            h = '<a class="btn btn-warning btn-sm " href="#" title="处理"  mce_href="#">已处理</i></a> ';
                                        }
										return e + d + h;
									}
								} ]
					});
}
function reLoad() {
	$('#exampleTable').bootstrapTable('refresh');
}

function loadShed(){
    var html = "";
    $.ajax({
        url : '/platform/farmShed/shedList',
        success : function(result) {
        	var data = result.farmShedList;
            //加载数据
            for (var i = 0; i < data.length; i++) {
                html += '<option value="' + data[i].fsid + '">' + data[i].name + '</option>'
            }
            $("#fsid").append(html);
            $("#fsid").chosen({
                maxHeight : 200
            });
            //点击事件
            $('#fsid').on('change', function(e, params) {
                var opt = {
                    query : {
                        fsid : params.selected
                    }
                }
                loadBlock(params.selected)
                $('#exampleTable').bootstrapTable('refresh', opt);
            });
        }
    });
}

function loadBlock(fsid){
    var html = "<option value=''>---地块---</option>";
    $.ajax({
        url : '/platform/farmBlock/blockList?fsid='+fsid,
        success : function(result) {
            var data = result.farmBlockList;
            //加载数据
            for (var i = 0; i < data.length; i++) {
                html += '<option value="' + data[i].fbid + '">' + data[i].name + '</option>'
            }
            $("#fbid").html(html);
            $("#fbid").chosen({
                maxHeight : 200
            });
            $("#fbid").trigger('chosen:updated');
            //点击事件
            $('#fbid').on('change', function(e, params) {
                var opt = {
                    query : {
                    	fsid : fsid,
                        fbid : params.selected
                    }
                }
                $('#exampleTable').bootstrapTable('refresh', opt);
            });
        }
    });
}

function add() {
	layer.open({
		type : 2,
		title : '增加',
		maxmin : true,
		shadeClose : false, // 点击遮罩关闭层
		area : [ '800px', '520px' ],
		content : prefix + '/add' // iframe的url
	});
}
function edit(id) {
	layer.open({
		type : 2,
		title : '编辑',
		maxmin : true,
		shadeClose : false, // 点击遮罩关闭层
		area : [ '800px', '520px' ],
		content : prefix + '/edit/' + id // iframe的url
	});
}
function remove(id) {
	layer.confirm('确定要删除选中的记录？', {
		btn : [ '确定', '取消' ]
	}, function() {
		$.ajax({
			url : prefix+"/remove",
			type : "post",
			data : {
				'fboid' : id
			},
			success : function(r) {
				if (r.code==0) {
					layer.msg(r.msg);
					reLoad();
				}else{
					layer.msg(r.msg);
				}
			}
		});
	})
}

function resetPwd(id) {
}
function farmBlockOrderLog(id) {
    var page = layer.open({
        type : 2,
        title : '日志',
        maxmin : true,
        shadeClose : false, // 点击遮罩关闭层
        area : [ '800px', '520px' ],
        content : '/platform/farmBlockOrderLog?fboid=' + id // iframe的url
    });
    layer.full(page);
}

function farmBlockOrderOperate(id) {
    $.ajax({
        url : '/platform/farmBlockOrder/changeStatus?fboid='+id,
        success : function(result) {
            layer.msg("处理完成！");
            reLoad();
        }
    });

}

function batchRemove() {
	var rows = $('#exampleTable').bootstrapTable('getSelections'); // 返回所有选择的行，当没有选择的记录时，返回一个空数组
	if (rows.length == 0) {
		layer.msg("请选择要删除的数据");
		return;
	}
	layer.confirm("确认要删除选中的'" + rows.length + "'条数据吗?", {
		btn : [ '确定', '取消' ]
	// 按钮
	}, function() {
		var ids = new Array();
		// 遍历所有选择的行数据，取每条数据对应的ID
		$.each(rows, function(i, row) {
			ids[i] = row['fboid'];
		});
		$.ajax({
			type : 'POST',
			data : {
				"ids" : ids
			},
			url : prefix + '/batchRemove',
			success : function(r) {
				if (r.code == 0) {
					layer.msg(r.msg);
					reLoad();
				} else {
					layer.msg(r.msg);
				}
			}
		});
	}, function() {

	});
}
$().ready(function() {
    laydate.render({
        elem: '#birth' //指定元素
        ,trigger: 'click'
    });

    jQuery.validator.addMethod("chackMobile",function(value,element){
        var chack_ok = false;
        if(value == $("#mobile_").val()){
            return true;
        }
        $.ajax({
            cache : true,
            type : "GET",
            url : "/platform/userPlatform/chackMobile?value="+value,
            async : false,
            success : function(data) {
                if(data==0){
                    chack_ok = true;
                }

            }
        });
        return chack_ok;
    },"手机号重复");

	validateRule();
});

$.validator.setDefaults({
	submitHandler : function() {
		update();
	}
});
function update() {
	$.ajax({
		cache : true,
		type : "POST",
		url : "/platform/userPlatform/update",
		data : $('#signupForm').serialize(),// 你的formid
		async : false,
		error : function(request) {
			parent.layer.alert("Connection error");
		},
		success : function(data) {
			if (data.code == 0) {
				parent.layer.msg("操作成功");
				parent.reLoad();
				var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
				parent.layer.close(index);

			} else {
				parent.layer.alert(data.msg)
			}

		}
	});

}
function validateRule() {
	var icon = "<i class='fa fa-times-circle'></i> ";
	$("#signupForm").validate({
        rules : {
            name : {
                required : true
            },
            mobile : {
                required : true,
                isPhone : true,
                chackMobile : true
            },
            birth : {
                required : true
            }
        },
        messages : {
            name : {
                required : icon + "请输入姓名"
            },
            mobile : {
                required : icon + "请输入手机号",
                isPhone : icon + "请输入正确的手机号",
                chackMobile : icon + "手机号重复"
            },
            birth : {
                required : icon + "请输入生日"
            }
        }
	})
}
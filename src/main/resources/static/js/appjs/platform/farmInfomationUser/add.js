$().ready(function() {
    bsSuggestLoad();
});
function bsSuggestLoad() {
    $("#mobile").bsSuggest({
        emptyTip: '未检索到匹配的数据',
        allowNoKeyword: false,   //是否允许无关键字时请求数据。为 false 则无输入时不执行过滤请求
        multiWord: true,         //以分隔符号分割的多关键字支持
        separator: ",",          //多关键字支持时的分隔符，默认为空格
        getDataMethod: "url",    //获取数据的方式，总是从 URL 获取
        url: '/platform/userPlatform/searchUser?roleType=1&mobile=', //优先从url ajax 请求 json 帮助数据，注意最后一个参数为关键字请求参数
        effectiveFields: ['name','mobile'],
        keyField: 'mobile',
        showHeader: false,
        fnProcessData: function (json) {   // url 获取数据时，对数据的处理，作为 fnGetData 的回调函数
            var data = {value: []};
            if (!json.code === 0) {
                return false;
            }
            for (var user of json.userList) {
                data.value.push({
                    name : user.name,
                    mobile: user.mobile,
                    userId: user.userId
                });
            }
            data.defaults = '';
            //字符串转化为 js 对象
            return data;
        }
    }).on('onSetSelectValue', function (e, keyword, data) {
        $("#userid").val(data.userId);
    });
}


function save() {
    if(!$("#userid").val()){
        layer.msg("请选择用户!");
        return;
    }
	$.ajax({
		cache : true,
		type : "POST",
		url : "/platform/farmInfomationUser/save",
		data : $('#signupForm').serialize(),// 你的formid
		async : false,
		error : function(request) {
			parent.layer.alert("Connection error");
		},
		success : function(data) {
			if (data.code == 0) {
				parent.layer.msg("操作成功");
				parent.reLoad();
				var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
				parent.layer.close(index);

			} else {
				parent.layer.alert(data.msg)
			}

		}
	});

}
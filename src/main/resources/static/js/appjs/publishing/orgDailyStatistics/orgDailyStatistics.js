$(function() {
    orgDaliyStatisticLoad();
});
// 机构日常统计数据
function orgDaliyStatisticLoad() {
	$('#orgDaliyStatisticTableFor7')
			.bootstrapTable(
					{
						method : 'get', // 服务器数据的请求方式 get or post
						url : "/publishing/orgDailyStatistics/list", // 服务器数据的加载地址
					//	showRefresh : true,
					//	showToggle : true,
					//	showColumns : true,
						iconSize : 'outline',
						toolbar : '#exampleToolbar',
						striped : true, // 设置为true会有隔行变色效果
						dataType : "json", // 服务器返回的数据类型
						pagination : true, // 设置为true会在底部显示分页条
						// queryParamsType : "limit",
						// //设置为limit则会发送符合RESTFull格式的参数
						singleSelect : false, // 设置为true将禁止多选
						// contentType : "application/x-www-form-urlencoded",
						// //发送到服务器的数据编码类型
						pageSize : 7, // 如果设置了分页，每页数据条数
						pageNumber : 1, // 如果设置了分布，首页页码
						//search : true, // 是否显示搜索框
						showColumns : false, // 是否显示内容下拉框（选择显示的列）
						sidePagination : "server", // 设置在哪里进行分页，可选值为"client" 或者 "server"
                        queryParams : getParams,
						// //请求服务器数据时，你可以通过重写参数的方式添加一些额外的参数，例如 toolbar 中的参数 如果
						// queryParamsType = 'limit' ,返回参数必须包含
						// limit, offset, search, sort, order 否则, 需要包含:
						// pageSize, pageNumber, searchText, sortName,
						// sortOrder.
						// 返回false将会终止请求
						columns : [
								{
									field : 'createdTime',
									title : '时间',
                                    align : 'center'
								},
								{
									field : 'newsCount', 
									title : '发布量',
                                    align : 'center'
								},
								{
									field : 'readCount',
									title : '阅读量',
                                    align : 'center'
								},
								{
									field : 'fansReadCount',
									title : '粉丝阅读量',
                                    align : 'center'
								},
								{
									field : 'commentCount', 
									title : '评论量',
                                    align : 'center'
								},
								{
									field : 'likeCount', 
									title : '点赞量',
                                    align : 'center'
								}]
					});
}

// 机构日常统计数据
function orgDaliyStatisticLoadFor30() {
    $('#orgDaliyStatisticTableFor30')
        .bootstrapTable(
            {
                method : 'get', // 服务器数据的请求方式 get or post
                url : "/publishing/orgDailyStatistics/list", // 服务器数据的加载地址
                //	showRefresh : true,
                //	showToggle : true,
                //	showColumns : true,
                iconSize : 'outline',
                toolbar : '#exampleToolbar',
                striped : true, // 设置为true会有隔行变色效果
                dataType : "json", // 服务器返回的数据类型
                pagination : true, // 设置为true会在底部显示分页条
                // queryParamsType : "limit",
                // //设置为limit则会发送符合RESTFull格式的参数
                singleSelect : false, // 设置为true将禁止多选
                // contentType : "application/x-www-form-urlencoded",
                // //发送到服务器的数据编码类型
                pageSize : 30, // 如果设置了分页，每页数据条数
                pageNumber : 1, // 如果设置了分布，首页页码
                //search : true, // 是否显示搜索框
                showColumns : false, // 是否显示内容下拉框（选择显示的列）
                sidePagination : "server", // 设置在哪里进行分页，可选值为"client" 或者 "server"
                queryParams : getParams,
                // //请求服务器数据时，你可以通过重写参数的方式添加一些额外的参数，例如 toolbar 中的参数 如果
                // queryParamsType = 'limit' ,返回参数必须包含
                // limit, offset, search, sort, order 否则, 需要包含:
                // pageSize, pageNumber, searchText, sortName,
                // sortOrder.
                // 返回false将会终止请求
                columns : [
                    {
                        field : 'createdTime',
                        title : '时间',
                        align : 'center'
                    },
                    {
                        field : 'newsCount',
                        title : '发布量',
                        align : 'center'
                    },
                    {
                        field : 'readCount',
                        title : '阅读量',
                        align : 'center'
                    },
                    {
                        field : 'fansReadCount',
                        title : '粉丝阅读量',
                        align : 'center'
                    },
                    {
                        field : 'commentCount',
                        title : '评论量',
                        align : 'center'
                    },
                    {
                        field : 'likeCount',
                        title : '点赞量',
                        align : 'center'
                    }]
            });
}



function getParams(params) {
    var temp = {
        //说明：传入后台的参数包括offset开始索引，limit步长，sort排序列，order：desc或者,以及所有列的键值对
        limit: params.limit,
        offset:params.offset,
        hasDelete:0
    }
    return temp;
}

// 文章日常统计数据
function newsDaliyStatisticLoad() {
    $('#newsDaliyStatisticTable')
        .bootstrapTable(
            {
                method : 'get', // 服务器数据的请求方式 get or post
                url : "/publishing/news/list", // 服务器数据的加载地址
                //	showRefresh : true,
                //	showToggle : true,
                //	showColumns : true,
                iconSize : 'outline',
                toolbar : '#exampleToolbar',
                striped : true, // 设置为true会有隔行变色效果
                dataType : "json", // 服务器返回的数据类型
                pagination : true, // 设置为true会在底部显示分页条
                // queryParamsType : "limit",
                // //设置为limit则会发送符合RESTFull格式的参数
                singleSelect : false, // 设置为true将禁止多选
                // contentType : "application/x-www-form-urlencoded",
                // //发送到服务器的数据编码类型
                pageSize : 10, // 如果设置了分页，每页数据条数
                pageNumber : 1, // 如果设置了分布，首页页码
                //search : true, // 是否显示搜索框
                showColumns : false, // 是否显示内容下拉框（选择显示的列）
                sidePagination : "server", // 设置在哪里进行分页，可选值为"client" 或者 "server"
                queryParams : function(params) {
                    return {
                        //说明：传入后台的参数包括offset开始索引，limit步长，sort排序列，order：desc或者,以及所有列的键值对
                        limit: params.limit,
                        offset:params.offset,
                        hasDelete:0,
                        status:3,
                        title : $('#searchName').val(),//搜索条件
                        // username:$('#searchName').val()
                    };
                },
                // //请求服务器数据时，你可以通过重写参数的方式添加一些额外的参数，例如 toolbar 中的参数 如果
                // queryParamsType = 'limit' ,返回参数必须包含
                // limit, offset, search, sort, order 否则, 需要包含:
                // pageSize, pageNumber, searchText, sortName,
                // sortOrder.
                // 返回false将会终止请求
                columns : [
                    {
                        field : 'title',
                        title : '标题',
                        align : 'center'
                    },
                    {
                        field : 'publishTime',
                        title : '发布日期',
                        align : 'center'
                    },
                    {
                        field : 'readCount',
                        title : '阅读量',
                        align : 'center',
                        formatter : function(value, row, index) {
                            return row.readCount+row.fansReadCount;
                        }
                    },
                    {
                        field : 'commentCount',
                        title : '评论量',
                        align : 'center',
                        formatter : function(value, row, index) {
                            return row.commentCount+row.fansCommentCount;
                        }
                    },
                    {
                        field : 'likeCount',
                        title : '点赞量',
                        align : 'center',
                        formatter : function(value, row, index) {
                            return row.likeCount+row.fansLikeCount;
                        }
                    },
                    {
                        field : 'collectCount',
                        title : '收藏量',
                        align : 'center',
                        formatter : function(value, row, index) {
                            return row.collectCount+row.fansCollectCount;
                        }
                    },
                    {
                        field : 'shareCount',
                        title : '分享次数',
                        align : 'center',
                        formatter : function(value, row, index) {
                            return row.shareCount+row.fansShareCount;
                        }
                    }]
            });
}

$('#myTab a').click(function (e) {
    $(this).tab('show');
    if($(this).attr('id') == 'list_1'){
        orgDaliyStatisticLoad();
    } else if ($(this).attr('id')== 'list_2') {
        newsDaliyStatisticLoad();
    }
})

$('#myTab2 a').click(function (e) {
    $(this).tab('show');
    if($(this).attr('id') == 'list_3'){
        orgDaliyStatisticLoad();
        $('#tabFor7').show();
        $('#tabFor30').hide();
        $('#orgDaliyStatisticTableFor7').bootstrapTable('refresh', {pageSize:7});
    } else if ($(this).attr('id')== 'list_4') {
        $('#tabFor30').show();
        $('#tabFor7').hide();
        orgDaliyStatisticLoadFor30();
        $('#orgDaliyStatisticTableFor30').bootstrapTable('refresh', {pageSize:30});
    }
})

function reLoad() {
    $('#newsDaliyStatisticTable').bootstrapTable('refresh');
}
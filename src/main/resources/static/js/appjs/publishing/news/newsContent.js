
var prefix = "/publishing/news"
$(function() {
	load();
});

// 加载内容列表数据
function load() {
	$('#exampleTable')
			.bootstrapTable(
					{
						method : 'get', // 服务器数据的请求方式 get or post
						url : prefix + "/list", // 服务器数据的加载地址
					//	showRefresh : true,
					//	showToggle : true,
					//	showColumns : true,
						iconSize : 'outline',
						toolbar : '#exampleToolbar',
						striped : true, // 设置为true会有隔行变色效果
						dataType : "json", // 服务器返回的数据类型
						pagination : true, // 设置为true会在底部显示分页条
						// queryParamsType : "limit",
						// //设置为limit则会发送符合RESTFull格式的参数
						singleSelect : false, // 设置为true将禁止多选
						// contentType : "application/x-www-form-urlencoded",
						// //发送到服务器的数据编码类型
						pageSize : 10, // 如果设置了分页，每页数据条数
						pageNumber : 1, // 如果设置了分布，首页页码
						//search : true, // 是否显示搜索框
						showColumns : false, // 是否显示内容下拉框（选择显示的列）
						sidePagination : "server", // 设置在哪里进行分页，可选值为"client" 或者 "server"
                        queryParams : getParams,
						// //请求服务器数据时，你可以通过重写参数的方式添加一些额外的参数，例如 toolbar 中的参数 如果
						// queryParamsType = 'limit' ,返回参数必须包含
						// limit, offset, search, sort, order 否则, 需要包含:
						// pageSize, pageNumber, searchText, sortName,
						// sortOrder.
						// 返回false将会终止请求
						columns : [
								{
									checkbox : true
								},
								{
									field : 'id', 
									title : '序号',
                                    align : 'center',
                                    formatter: function (value, row, index) {
                                        var options = $('#exampleTable').bootstrapTable('getOptions');
                                        return options.pageSize * (options.pageNumber - 1) + index + 1;
                                    }
								},
								{
									field : 'title', 
									title : '标题' ,
                                    align : 'center',
                                    formatter : function(value, row, index) {
                                        return  '<a href="javascript:void(0);" onclick="detailNews('+ row.id + ')" >'+value+'</a>';
                                    }
								},
                                {
                                    field : 'columnId',
                                    title : '栏目' ,
                                    align : 'center',
                                    formatter : function (value, row, index) {
                                        return columnDo[row.columnId]
                                    }
                                },
                            	{
									field : 'orgName',
									title : '发布机构',
                                    align : 'center'
								},
								{
									field : 'type', 
									title : '类型',
                                    align : 'center',
                                    formatter : function(value, row, index) {
                                        if (value == '0') {
                                            return '<span class="label label-danger">文章</span>';
                                        } else if (value == '1') {
                                            return '<span class="label label-default">视频</span>';
                                        } else if (value == '2') {
                                            return '<span class="label label-primary">音頻</span>';
                                        }
                                    }
								},
								{
									field : 'publishTime',
									title : '发布时间',
                                    align : 'center'
								},
								{
									field : 'status',
									title : '状态',
                                    align : 'center',
                                    formatter : function(value, row, index) {
                                        if (value == '0') {
                                            return '<span class="label label-danger">草稿</span>';
                                        } else if (value == '1') {
                                            return '<span class="label label-default">保存</span>';
                                        } else if (value == '2') {
                                            return '<span class="label label-primary">审核中</span>';
                                        } else if (value == '3') {
                                            return '<span class="label label-warning">审核通过</span>';
                                        } else if (value == '4') {
                                            return '<span class="label label-important">审核未通过</span>';
                                        }
                                    }
								},
								{
									title : '操作',
									field : 'id',
									align : 'center',
									formatter : function(value, row, index) {
                                        if (row.hasTop == '1') {
                                            var h = '<button class="btn btn-default btn-sm'+s_edit_h+'" onclick="offTop(\'' + row.id + '\')">取消置顶</button>';
                                        } else if (row.hasTop == '0') {
                                            var h = '<button class="btn btn-default btn-sm'+s_edit_h+'" onclick="onTop(\'' + row.id + '\')">置顶</button>';
                                        }
                                        var e = '<button class="btn btn-primary btn-sm'+s_edit_h+'" onclick="edit(\'' + row.id + '\')">编辑</button>';
                                        var d = '<button class="btn btn-warning btn-sm'+s_edit_h+'" onclick="remove(\'' + row.id + '\')">删除</button>';
										return h + e + d ;
									}
								} ]
					});
}

// 加载回收站数据
function load1() {
    $('#exampleTable1')
        .bootstrapTable(
            {
                method : 'get', // 服务器数据的请求方式 get or post
                url : prefix + "/list", // 服务器数据的加载地址
                //	showRefresh : true,
                //	showToggle : true,
                //	showColumns : true,
                iconSize : 'outline',
                toolbar : '#exampleToolbar',
                striped : true, // 设置为true会有隔行变色效果
                dataType : "json", // 服务器返回的数据类型
                pagination : true, // 设置为true会在底部显示分页条
                // queryParamsType : "limit",
                // //设置为limit则会发送符合RESTFull格式的参数
                singleSelect : false, // 设置为true将禁止多选
                // contentType : "application/x-www-form-urlencoded",
                // //发送到服务器的数据编码类型
                pageSize : 10, // 如果设置了分页，每页数据条数
                pageNumber : 1, // 如果设置了分布，首页页码
                //search : true, // 是否显示搜索框
                showColumns : false, // 是否显示内容下拉框（选择显示的列）
                sidePagination : "server", // 设置在哪里进行分页，可选值为"client" 或者 "server"
                queryParams : getParams2,
                // //请求服务器数据时，你可以通过重写参数的方式添加一些额外的参数，例如 toolbar 中的参数 如果
                // queryParamsType = 'limit' ,返回参数必须包含
                // limit, offset, search, sort, order 否则, 需要包含:
                // pageSize, pageNumber, searchText, sortName,
                // sortOrder.
                // 返回false将会终止请求
                columns : [
                    {
                        checkbox : true
                    },
                    {
                        field : 'id',
                        title : '序号',
                        align : 'center'
                    },
                    {
                        field : 'title',
                        title : '标题' ,
                        align : 'center'
                    },
                    {
                        field : 'columnId',
                        title : '栏目' ,
                        align : 'center',
                        formatter : function (value, row, index) {
                            return columnDo[row.columnId]
                        }
                    },
                    {
                        field : 'orgName',
                        title : '发布机构',
                        align : 'center'
                    },
                    {
                        field : 'type',
                        title : '类型',
                        align : 'center',
                        formatter : function(value, row, index) {
                            if (value == '0') {
                                return '<span class="label label-danger">文章</span>';
                            } else if (value == '1') {
                                return '<span class="label label-default">视频</span>';
                            } else if (value == '2') {
                                return '<span class="label label-primary">音頻</span>';
                            }
                        }
                    },
                    {
                        field : 'updateTime',
                        title : '删除时间',
                        align : 'center'
                    },
                    {
                        title : '操作',
                        field : 'id',
                        align : 'center',
                        formatter : function(value, row, index) {
                            var e = '<button class="btn btn-warning btn-sm'+s_remove_h+'" onclick="shiftRemove(\'' + row.id + '\')">彻底删除</button>'
                            var d = '<button class="btn btn-primary btn-sm'+s_edit_h+'" onclick="recovery(\'' + row.id + '\')">恢复原始状态</button>'
                            return e + d ;
                        }
                    } ]
            });
}

function getParams(params) {
    var temp = {
        //说明：传入后台的参数包括offset开始索引，limit步长，sort排序列，order：desc或者,以及所有列的键值对
        limit: params.limit,
        offset:params.offset,
        title : $('#searchName').val(),//搜索条件
        startTime : $('#startTime').val(),
        endTime:$('#endTime').val(),
        columnId:$('#columnId').val(),
        hasDelete:0,
        permission:s_admin_h
    }
    return temp;
}

function getParams2(params) {
    var temp = {
        //说明：传入后台的参数包括offset开始索引，limit步长，sort排序列，order：desc或者,以及所有列的键值对
        limit: params.limit,
        offset:params.offset,
        title : $('#searchName1').val(),//搜索条件
        delStartTime:$('#delStartTime').val(),
        delEndTime:$('#delEndTime').val(),
        columnId:$('#columnId1').val(),
        hasDelete:1,
        permission:s_admin_h
    }
    return temp;
}

function reLoad() {
	$('#exampleTable').bootstrapTable('refresh');
}

function reLoad1() {
    var opt = getParams2;
    $('#exampleTable1').bootstrapTable('refresh', opt);
}

function add() {
    var addPage = layer.open({
        type : 2,
        title : '增加',
        maxmin : true,
        shadeClose : false, // 点击遮罩关闭层
        area : [ '800px', '520px' ],
        content : prefix + '/add' // iframe的url
    });
    layer.full(addPage);
}

// 查看详情
function detailNews(id) {
    var addPage = layer.open({
        type : 2,
        title : '内容详情页',
        maxmin : true,
        shadeClose : false, // 点击遮罩关闭层
        area : [ '800px', '520px' ],
        content : "/api/news/newsContent?id="+ id // iframe的url
    });
    layer.full(addPage);
}

function edit(id) {
    var addPage = layer.open({
        type : 2,
        title : '编辑',
        maxmin : true,
        shadeClose : false, // 点击遮罩关闭层
        area : [ '800px', '520px' ],
        content : prefix + '/edit/' + id // iframe的url
    });
    layer.full(addPage);
}

// 逻辑删除
function remove(id) {
    layer.confirm('确定要删除选中的记录？', {
        btn : [ '确定', '取消' ]
    }, function() {
        $.ajax({
            url : prefix+"/update",
            type : "post",
            data : {
                'id' : id,
                'hasDelete':1
            },
            success : function(r) {
                if (r.code==0) {
                    layer.msg(r.msg);
                    reLoad();
                }else{
                    layer.msg(r.msg);
                }
            }
        });
    })
}

// 物理删除
function shiftRemove(id) {
    layer.confirm('确定要彻底删除选中的记录？', {
        btn : [ '确定', '取消' ]
    }, function() {
        $.ajax({
            url : prefix+"/remove",
            type : "post",
            data : {
                'id' : id
            },
            success : function(r) {
                if (r.code==0) {
                    layer.msg(r.msg);
                    reLoad1();
                }else{
                    layer.msg(r.msg);
                }
            }
        });
    })
}

// 恢复数据
function recovery(id) {
    layer.confirm('确定要恢复选中的记录？', {
        btn : [ '确定', '取消' ]
    }, function() {
        $.ajax({
            url : prefix+"/update",
            type : "post",
            data : {
                'id' : id,
                'hasDelete':0
            },
            success : function(r) {
                if (r.code==0) {
                    layer.msg(r.msg);
                    reLoad1();
                }else{
                    layer.msg(r.msg);
                }
            }
        });
    })
}

// 置顶
function onTop(id) {
	layer.confirm('确定要置顶选中的记录？', {
		btn : [ '确定', '取消' ]
	}, function() {
		$.ajax({
			url : prefix+"/update",
			type : "post",
			data : {
				'id' : id,
				'hasTop':1
			},
			success : function(r) {
				if (r.code==0) {
					layer.msg(r.msg);
					reLoad();
				}else{
					layer.msg(r.msg);
				}
			}
		});
	})
}

// 取消置顶
function offTop(id) {
    layer.confirm('确定要取消置顶选中的记录？', {
        btn : [ '确定', '取消' ]
    }, function() {
        $.ajax({
            url : prefix+"/update",
            type : "post",
            data : {
                'id' : id,
                'hasTop':0
            },
            success : function(r) {
                if (r.code==0) {
                    layer.msg(r.msg);
                    reLoad();
                }else{
                    layer.msg(r.msg);
                }
            }
        });
    })
}

//点击事件
$('.chosen-select').on('change', function(e, params) {
    var opt = {
        query : {
            status : $(".chosen-select").find("option:selected").val(),
        }
    }

    if(opt.query.status != ""){
        $('#exampleTable').bootstrapTable('refresh', opt);
    } else {
        $('#exampleTable').bootstrapTable('refresh');
    }
});

$(function () {
    $('#myTab a:first').tab('show');
})
$('#myTab a').click(function (e) {
    $(this).tab('show');
	if($(this).attr('id') == 'list_1'){
        load();
	} else if ($(this).attr('id')== 'list_2') {
        load1();
	}

})
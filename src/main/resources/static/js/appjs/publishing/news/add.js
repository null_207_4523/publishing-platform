var images = new Array();
var cover = new Array();

var websocket = null;
var serverName = $("#serverName").val();
var wsId = $("#wsId").val();
var wsPro = $("#wsPro").val();
// var scheme = $("#scheme").val();
if ('WebSocket' in window) {
    websocket = new ReconnectingWebSocket(wsPro+"://"+serverName+'/api/webSocket/'+wsId);
} else {
    alert('该浏览器不支持websocket,建议使用chrome或360极速模式!');
}

websocket.onopen = function (event) {
    console.log('建立连接');
}

websocket.onclose = function (e) {
    console.log('websocket 连接关闭: ' + e.code + ' ' + e.reason + ' ' + e.wasClean);
    console.log(e);
}

websocket.onmessage = function (event) {
    console.log(event.data);
    var contInfo = JSON.parse(event.data);
    var content = contInfo.content;
    if (content !== "" && content !== undefined) {
        $("#nocont").remove();
    }
    $("#cont").css("display", "block");
    // $("#cont").css("width", "100%");
    $("#cont").html(content)
    console.log('收到消息:' + event.data)
}

websocket.onerror = function (event) {
    console.error("WebSocket error observed:", event);
}

window.onbeforeunload = function () {
    debugger
    websocket.close();
}

$().ready(function() {
    $('.summernote').summernote({
        toolbar: [
            ['edit',['undo','redo']],//编辑
            ['headline', ['style']],//大字标题
            ['style', ['bold', 'italic', 'underline', 'superscript', 'subscript', 'strikethrough', 'clear']],//样式
            ['fontface', ['fontname','color','fontsize']],//字体
            ['alignment', ['ul', 'ol', 'paragraph', 'lineheight']],//对齐方式
            ['height', ['height']],//行高(自定义行高时一定不要忘记写这个)
            ['table', ['table']],//表单
            ['insert', ['link','picture','hr']],//插入链接，图片，下划线
            ['view', ['fullscreen', 'codeview']]//全屏，代码视图
        ],
        width: '750',
        height : '500',
        lang : 'zh-CN',
        callbacks: {
            onImageUpload: function(files, editor, $editable) {
                sendFile(files);
            }
        },
    });
    $('.summernote').summernote('fontSize',24);

    validateRule();
});

$.validator.setDefaults({
    submitHandler : function() {
        save();
    }
});

var imgList = []

function getContent(){
    // var contentSn = $("#content_sn").summernote('code');
    var content_sn = $("#cont").html()
    return contentSn
}

function save() {
    // var content_sn = $("#content_sn").summernote('code');
    var content_sn = $("#cont").html()
    var synopsis = getNoMarkupStr(content_sn);
    if ($.trim(synopsis).length==0) {
        parent.layer.alert("请输入正文");
        return
    }
    $("#content").val(content_sn);
    images = getImages(content_sn);
    var coverTtype = $("input[type='radio']:checked").val();
    if(coverTtype == 1){
        cover = imgList
        if(imgList.length != 1){
            parent.layer.alert("请选择一张封面图")
            return
        }
    }
    if(coverTtype == 2){
        cover = imgList
        if(imgList.length != 3){
            parent.layer.alert("请选择三张封面图")
            return
        }
    }
    if(coverTtype == 0){
        cover = []
    }
    $("#cover").val(cover);
    $("#coverTtype").val(coverTtype);
    $("#synopsis").val(synopsis);
    $("#status").val(condition);
    $.ajax({
        cache : true,
        type : "POST",
        url : "/publishing/news/save",
        data : $('#signupForm').serialize(),// 你的formid
        async : false,
        error : function(request) {
            parent.layer.alert("Connection error");
        },
        success : function(data) {
            if (data.code == 0) {
                parent.layer.msg("操作成功");
                $("#title").val('')
                $("#columnName").val('')
                // $(".summernote").summernote('code','')
                $("#cont").html('')
                imgList = []
                var itemImg1_img = document.getElementsByClassName("itemImg1_img")
                for(var i=0;i<itemImg1_img.length;i++){
                    itemImg1_img[i].setAttribute('src','/img/webuploader.png')
                }
                var itemImg1_img1 = document.getElementById("itemImg1_img1")
                itemImg1_img1.setAttribute('src','/img/webuploader.png')
                $("input[type='radio']")[2].checked = true;
                $(".imgRadio3").hide()
                $(".imgRadio0").show()
                $(".imgRadio1").hide()
            } else {
                parent.layer.alert(data.msg)
            }

        }
    });
}

function getImages(content) {
    content.replace(/<img [^>]*src=['"]([^'"]+)[^>]*>/gi, function (match, capture) {
        images.push(capture);
    });
    return images;
}

function getNoMarkupStr(content) {
    content=$("<div>").html(content).text();/* 得到可视文本(不含图片),将&nbsp;&lt;&gt;转为空字符串和<和>显示,同时去掉了换行,文本单行显示 */
    if(content.length>20){
        content = content.substring(0,20);
        content += '...';
    }
    return content;
}

function validateRule() {
    var icon = "<i class='fa fa-times-circle'></i> ";
    $("#signupForm").validate({
        rules : {
            title : {
                required : true,
                rangelength:[5,40]
            },
            columnName : {
                required : true
            }
        },
        messages : {
            title : {
                required : icon + "标题不能为空",
                rangelength: icon + "请输入文章标题（5-40个字）"
            },
            columnName : {
                required : icon + "栏目不能为空"
            }
        }
    })
}

var openColumn = function(){
    layer.open({
        type:2,
        title:"选择栏目",
        area : [ '300px', '450px' ],
        content:"/publishing/column/treeView"
    })
}


function loadColumn( columnId,columnName){
    $("#columnId").val(columnId);
    $("#columnName").val(columnName);
    $("#columnName-error").hide();
}

function uniqueArray(arr){
    if(null == arr || arr.length == 0 ){
        return arr;
    }else{
        var innerHashMap = {};
        for(var i = 0, j = arr.length; i<j; i++){
            innerHashMap[arr[i]] = null;
        }
        var rs = [];
        for (obj in innerHashMap){
            rs.push(obj);
        }
        return rs;
    }
}

$("input[name='coverType']").change(function(){
    var coverValue = $("input[name='coverType']:checked").val();
    if(coverValue == 1){
        $(".imgRadio3").hide()
        $(".imgRadio0").hide()
        $(".imgRadio1").show()
        imgList = []
        var imgDom = document.getElementById("itemImg1_img1")
        imgDom.setAttribute('src','/img/webuploader.png')
    }else if(coverValue == 2){
        $(".imgRadio3").show()
        $(".imgRadio0").hide()
        $(".imgRadio1").hide()
        imgList = []
        var imgDOms = document.getElementsByClassName("itemImg1_img")
        for (var i=0;i<imgDOms.length;i++){
            imgDOms[i].setAttribute('src','/img/webuploader.png')
        }
    }else if(coverValue == 0){
        $(".imgRadio3").hide()
        $(".imgRadio0").show()
        $(".imgRadio1").hide()
        imgList = []
    }
});
function show (num){
    layer.open({
        type:2,
        title:"选择图片",
        area : [ '800px', '600px' ],
        content:"/publishing/news/cover?num="+num
    })
}

function preview (){
    layer.open({
        type:2,
        title:"预览",
        area : [ '375px', '750px' ],
        content:"/publishing/news/preview"
    })
}

function xml(){
    images = []
    var content_sn = $("#cont").html();
    images = getImages(content_sn);
    return images;
}

function showEditor (id) {
    id = (id !== undefined ? id : "");
    var editor_domain = $("#editor_domain").val();
    console.log('editor_domain',editor_domain)
    var cont = $("#cont").html().trim();
    var json = {
        "wsId": wsId,
        "id": id,
        "callBackUrl": "//"+serverName+"/api/publishing/news/transContent",
        "getDetailUrl": "//"+serverName+"/api/publishing/news/getContent",
        "getTempContUrl": "//"+serverName+"/api/publishing/news/getTempContent"
        // "callBackUrl": scheme+"://"+serverName+"/api/publishing/news/transContent",
        // "getDetailUrl": scheme+"://"+serverName+"/api/publishing/news/getContent",
        // "getTempContUrl": scheme+"://"+serverName+"/api/publishing/news/getTempContent"
    };
    if (cont) {
        console.log('有内容')
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/api/publishing/news/saveTempContent',
            data: {
                "content": cont
            },
            success: function (data) {
                if (data.code == '200') {
                    json.tempid = data.data;
                    var url = editor_domain+"/edtoronly?param=" + JSON.stringify(json);
                    url = encodeURI(url);
                    window.open(url)
                } else {
                    layer.msg(data.msg);
                }
            },
            error: function (err) {
                console.log(err);
            }
        })
    } else {
        console.log('json',json)
        var url = editor_domain+"/edtoronly?param=" + JSON.stringify(json);
        url = encodeURI(url);
        window.open(url)
    }
};





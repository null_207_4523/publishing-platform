var prefix = "/publishing/news"
$(function() {
    load();
});

// 加载内容列表数据
function load() {
    $('#exampleTable')
        .bootstrapTable(
            {
                method : 'get', // 服务器数据的请求方式 get or post
                url : prefix + "/list", // 服务器数据的加载地址
                //	showRefresh : true,
                //	showToggle : true,
                //	showColumns : true,
                iconSize : 'outline',
                toolbar : '#exampleToolbar',
                striped : true, // 设置为true会有隔行变色效果
                dataType : "json", // 服务器返回的数据类型
                pagination : true, // 设置为true会在底部显示分页条
                // queryParamsType : "limit",
                // //设置为limit则会发送符合RESTFull格式的参数
                singleSelect : false, // 设置为true将禁止多选
                // contentType : "application/x-www-form-urlencoded",
                // //发送到服务器的数据编码类型
                pageSize : 10, // 如果设置了分页，每页数据条数
                pageNumber : 1, // 如果设置了分布，首页页码
                //search : true, // 是否显示搜索框
                showColumns : false, // 是否显示内容下拉框（选择显示的列）
                sidePagination : "server", // 设置在哪里进行分页，可选值为"client" 或者 "server"
                queryParams : getParams,
                // //请求服务器数据时，你可以通过重写参数的方式添加一些额外的参数，例如 toolbar 中的参数 如果
                // queryParamsType = 'limit' ,返回参数必须包含
                // limit, offset, search, sort, order 否则, 需要包含:
                // pageSize, pageNumber, searchText, sortName,
                // sortOrder.
                // 返回false将会终止请求
                columns : [
                    {
                        checkbox : true
                    },
                    {
                        field : 'id',
                        title : '序号',
                        align : 'center'
                    },
                    {
                        field : 'title',
                        title : '标题' ,
                        align : 'center',
                        formatter : function(value, row, index) {
                            return  '<a href="javascript:void(0);" onclick="detailNews('+ row.id + ')" >'+value+'</a>';
                        }
                    },
                    {
                        field : 'orgName',
                        title : '发布机构',
                        align : 'center'
                    },
                    {
                        field : 'publishTime',
                        title : '发布时间',
                        align : 'center'
                    },
                    {
                        title : '操作',
                        field : 'id',
                        align : 'center',
                        formatter : function(value, row, index) {
                            return '<button class="btn btn-primary btn-sm'+s_edit_h+'" onclick="viewComments(\'' + row.id + '\')">评论展示</button>';
                        }
                    } ]
            });
}

function getParams(params) {
    debugger
    var temp = {
        //说明：传入后台的参数包括offset开始索引，limit步长，sort排序列，order：desc或者,以及所有列的键值对
        limit: params.limit,
        offset:params.offset,
        title : $('#searchName').val(),//搜索条件
        hasDelete:0,
        status:3,
        permission:s_admin_h
    }
    return temp;
}

function reLoad() {
    $('#exampleTable').bootstrapTable('refresh');
}

// 查看文章详情
function detailNews(id) {
    var addPage = layer.open({
        type : 2,
        title : '内容详情页',
        maxmin : true,
        shadeClose : false, // 点击遮罩关闭层
        area : [ '800px', '520px' ],
        content : "/api/news/newsContent?id="+ id // iframe的url
    });
    layer.full(addPage);
}

// 查看评论详情
function viewComments(id) {
    var addPage = layer.open({
        type : 2,
        title : '评论列表',
        maxmin : true,
        shadeClose : false, // 点击遮罩关闭层
        area : [ '800px', '520px' ],
        content : '/publishing/comment/edit/' + id // iframe的url
    });
    layer.full(addPage);
}

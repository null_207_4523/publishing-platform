$().ready(function() {
	validateRule();
});

$.validator.setDefaults({
	submitHandler : function() {
		save();
	}
});
function save() {
	$.ajax({
		cache : true,
		type : "POST",
		url : "/publishing/version/save",
		data : $('#signupForm').serialize(),// 你的formid
		async : false,
		error : function(request) {
			parent.layer.alert("Connection error");
		},
		success : function(data) {
			if (data.code == 0) {
				parent.layer.msg("操作成功");
				parent.reLoad();
				var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
				parent.layer.close(index);

			} else {
				parent.layer.alert(data.msg)
			}

		}
	});

}
function validateRule() {
	var icon = "<i class='fa fa-times-circle'></i> ";
	$("#signupForm").validate({
		rules : {
            code : {
				required : true,
                digits:true
			},
            name : {
                required : true
            },
            message : {
                required : true
            },
            size : {
                required : true,
                number:true
            }
		},
		messages : {
            code : {
				required : icon + "请输入版本号",
                digits: icon + "只可输入数字"
			},
            name : {
                required : icon + "请输入版本名称"
            },
            message : {
                required : icon + "请输入版本信息"
            },
            size : {
                required : icon + "请输入文件大小",
                number: icon + "请输入合法数字"
            }
		}
	})
}